package com.iknowledge.hosted.indexer;

import com.google.common.collect.Sets;
import com.iknowledge.hosted.indexer.parsing.DocParser;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;
import com.iknowledge.hosted.indexer.parsing.Lang;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import org.iknowledge.nlp.tools.Sentence;
import org.iknowledge.nlp.tools.Token;
import org.iknowledge.nlp.tools.stemmers.OpenNlpSnowballStemmer;
import org.iknowledge.nlp.tools.stemmers.Stemmer;
import org.iknowledge.nlp.tools.tokenization.OpenNlpTokenizer;
import org.iknowledge.nlp.tools.tokenization.Tokenizer;
import org.iknowledge.nlp.tools.tokenization.filters.CompositeTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.EmptyTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.SpecialSymbolTokenFilter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@Test
public class DocParserTest {

    private DocParser docParser;

    @DataProvider(name = "texts")
    public Object[][] documentTextsToIndex() {
        return new Object[][] {
            {
                List.of(new DocumentSentence("The most complete, authoritative technical guide to "
                                + "the FreeBSD kernel’s internal structure has now been"
                                + " extensively updated to cover all major improvements"
                                + " between Versions 5 and 11. ", Lang.ENG),
                        new DocumentSentence("Approximately one-third of this edition’s content "
                                + "is completely new, and another one-third has "
                                + "been extensively rewritten.", Lang.ENG),
                        new DocumentSentence("Three long-time FreeBSD project leaders begin with a"
                                + " concise overview of the FreeBSD kernel’s current "
                                + "design and implementation.", Lang.ENG),
                        new DocumentSentence("Next, they cover the FreeBSD kernel "
                                + "from the system-call level down–from "
                                + "the interface to the kernel to the hardware. ", Lang.ENG),
                        new DocumentSentence("Explaining key design decisions, they detail the "
                                + "concepts, data structures, and algorithms used in "
                                + "implementing each significant system facility, "
                                + "including process management, security, virtual "
                                + "memory, the I/O system, filesystems, socket IPC, and"
                                + " networking.", Lang.ENG)),
                113, 80, 5
            }
        };
    }

    @BeforeClass
    public void setup() throws IOException {
        final HashMap<Lang, Stemmer> stemmers = new HashMap<>();
        stemmers.put(Lang.ENG, new OpenNlpSnowballStemmer(SnowballStemmer.ALGORITHM.ENGLISH));

        final HashMap<Lang, Tokenizer> tokenizers = new HashMap<>();
        final FileInputStream tokenizerModel
            = new FileInputStream(System.getProperty("tokenizer.model"));
        tokenizers.put(Lang.ENG, new OpenNlpTokenizer(tokenizerModel));

        final CompositeTokenFilter tokenFilter
            = new CompositeTokenFilter().addTokenFilter(new EmptyTokenFilter())
                                        .addTokenFilter(new SpecialSymbolTokenFilter());
        docParser = new DocParser(stemmers, tokenizers, tokenFilter);
    }

    @Test(dataProvider = "texts")
    public void testParsing(List<DocumentSentence> text,
                            int tokensInBlock,
                            int uniqueTokensInBlock,
                            int sentenceCountInBlock) {

        final List<Sentence> sentences = docParser.parse(text);

        assertThat(sentences)
            .isNotNull()
            .hasSize(sentenceCountInBlock);

        final List<Token> tokens = sentences.stream()
                                            .flatMap(sentence -> sentence.tokens().stream())
                                            .collect(toList());

        assertThat(tokens)
            .isNotNull()
            .hasSize(tokensInBlock);

        final HashSet<Token> uniqueTokensSet = Sets.newHashSet();
        for (Token token : tokens) {
            uniqueTokensSet.add(token);
        }
        assertThat(uniqueTokensSet)
            .hasSize(uniqueTokensInBlock);
    }
}
