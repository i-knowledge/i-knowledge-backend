package com.iknowledge.hosted.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import com.iknowledge.hosted.hbase.repositories.indexer.Change;
import com.iknowledge.hosted.hbase.repositories.indexer.ChangeType;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorChangesStore;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorReader;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorStore;
import com.iknowledge.hosted.hbase.repositories.indexer.TokenStore;
import com.iknowledge.hosted.indexer.parsing.DocParser;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;
import com.iknowledge.hosted.indexer.parsing.Lang;
import io.prometheus.client.CollectorRegistry;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.stemmers.OpenNlpSnowballStemmer;
import org.iknowledge.nlp.tools.stemmers.Stemmer;
import org.iknowledge.nlp.tools.tokenization.OpenNlpTokenizer;
import org.iknowledge.nlp.tools.tokenization.Tokenizer;
import org.iknowledge.nlp.tools.tokenization.filters.CompositeTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.EmptyTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.SpecialSymbolTokenFilter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("Duplicates")
@Test
public final class IndexerIt {

    private ExecutorService executorService
        = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 5);
    private Indexer indexer;
    private Connection connection;
    private StoreConfig tokenStoreConf;
    private StoreConfig docVecStoreConf;
    private StoreConfig docVecChangesStoreConf;
    private DocParser docParser;
    private DocVectorStore docVectorStore;
    private DocVectorChangesStore changesStore;

    @BeforeClass
    public void before() throws IOException, StoreException {
        final Map<Lang, Stemmer> stemmers = new HashMap<>();
        stemmers.put(Lang.ENG, new OpenNlpSnowballStemmer(SnowballStemmer.ALGORITHM.ENGLISH));

        final Map<Lang, Tokenizer> tokenizers = new HashMap<>();
        final FileInputStream tokenizerModel
            = new FileInputStream(System.getProperty("tokenizer.model"));
        tokenizers.put(Lang.ENG, new OpenNlpTokenizer(tokenizerModel));

        final CompositeTokenFilter tokenFilter
            = new CompositeTokenFilter().addTokenFilter(new EmptyTokenFilter())
                                        .addTokenFilter(new SpecialSymbolTokenFilter());
        docParser = new DocParser(stemmers, tokenizers, tokenFilter);

        final HConfig hconfig = HConfig.newBuilder().retryCount(5).retryBackoff(5000).build();
        connection = ConnectionFactory.createConnection(hconfig.asConfiguration());

        tokenStoreConf = StoreConfig.builder()
                                    .withConnection(connection)
                                    .withTableName("token_store_" + new Random().nextInt(10000))
                                    .withHconfig(hconfig)
                                    .build();
        docVecStoreConf = StoreConfig.builder()
                                     .withConnection(connection)
                                     .withTableName("docvec_store_" + new Random().nextInt(10000))
                                     .withHconfig(hconfig)
                                     .build();
        docVecChangesStoreConf = StoreConfig.builder()
                                            .withConnection(connection)
                                            .withTableName(("docvec_changes_store_"
                                                                + new Random().nextInt(10000)))
                                            .withHconfig(hconfig)
                                            .build();

        docVectorStore = new DocVectorStore(docVecStoreConf);
        changesStore = new DocVectorChangesStore(docVecChangesStoreConf);
        docVectorStore.createTable();
        changesStore.createTable();

        indexer = new Indexer(docParser,
                              new TokenStore(tokenStoreConf, true, true),
                              docVectorStore,
                              changesStore,
                              new CollectorRegistry());
    }

    @AfterMethod
    public void closeIndexer() throws Exception {
        connection.getAdmin().disableTable(tokenStoreConf.tableName());
        connection.getAdmin().truncateTable(tokenStoreConf.tableName(), false);
        connection.getAdmin().disableTable(docVecChangesStoreConf.tableName());
        connection.getAdmin().truncateTable(docVecChangesStoreConf.tableName(), false);
        connection.getAdmin().disableTable(docVecStoreConf.tableName());
        connection.getAdmin().truncateTable(docVecStoreConf.tableName(), false);
    }

    @AfterClass
    public void tearDown() throws Exception {
        indexer.close();
        executorService.shutdown();
        connection.close();
    }

    @DataProvider(name = "texts")
    public Object[][] documentTextsToIndex() {
        return new Object[][] {
            {
                List.of(new DocumentSentence("The most complete, authoritative technical guide to "
                        + "the FreeBSD kernel’s internal structure has now been"
                        + " extensively updated to cover all major improvements"
                        + " between Versions 5 and 11. ", Lang.ENG),
                        new DocumentSentence("Approximately one-third of this edition’s content "
                                + "is completely new, and another one-third has "
                                + "been extensively rewritten.", Lang.ENG),
                        new DocumentSentence("Three long-time FreeBSD project leaders begin with a"
                                + " concise overview of the FreeBSD kernel’s current "
                                + "design and implementation.", Lang.ENG),
                        new DocumentSentence("Next, they cover the FreeBSD kernel "
                                + "from the system-call level down–from "
                                + "the interface to the kernel to the hardware. ", Lang.ENG),
                        new DocumentSentence("Explaining key design decisions, they detail the "
                                + "concepts, data structures, and algorithms used in "
                                + "implementing each significant system facility, "
                                + "including process management, security, virtual "
                                + "memory, the I/O system, filesystems, socket IPC, and"
                                + " networking.", Lang.ENG)), 113
            }
        };
    }

    @Test(dataProvider = "texts")
    public void testNewDocIndexing(List<DocumentSentence> text, int tokensInBlock)
        throws Exception {

        final Id docId = Id.len8();
        final int srcId = 1;
        final TestDocument document = new TestDocument(docId, text);
        final Document resDoc = indexer.indexDoc(srcId, document)
                                       .get();

        assertThat(resDoc)
            .isSameAs(document);

        final DocVectorReader reader = new DocVectorStore(docVecStoreConf).reader(srcId, docId);
        final List<TokenProto.Token> nextBlock = reader.nextBlock();
        assertThat(nextBlock)
            .isNotNull()
            .hasSize(tokensInBlock);

        assertThat(reader.nextBlock())
            .isNull();
    }

    @Test(dataProvider = "texts")
    public void testRemoveDoc(List<DocumentSentence> text, int tokensInBlock)
        throws Exception {

        final Id docId = Id.len8();
        final int srcId = 1;
        final TestDocument document = new TestDocument(docId, text);
        indexer.indexDoc(srcId, document).get();
        final Id removedId = indexer.removeDoc(srcId, document.id()).get();

        assertThat(removedId)
            .isEqualTo(document.id());

        final DocVectorReader reader = docVectorStore.reader(srcId, docId);

        assertThat(reader.nextBlock())
            .isNull();

        reader.close();

        final List<Change> changes = changesStore.loadChanges(100);
        assertThat(changes)
            .isNotNull()
            .hasSize(1)
            .extracting(input -> input.type)
            .containsSequence(ChangeType.REMOVE_DOC);
    }

    @Test(dataProvider = "texts")
    public void testConcurrentIndexerCalls(List<DocumentSentence> text, int tokensInBlock)
        throws Exception {

        List<CompletableFuture<Document>> indexingTasks = new ArrayList<>();
        List<CompletableFuture<Void>> pauseResumeTasks = new ArrayList<>();
        int pauseCount = 0;

        for (int i = 0 ; i < 3000 ; i++) {
            indexingTasks.add(runNewDocIndexing(text));
            if (i % 55 == 0) {
                pauseResumeTasks.add(runIndexerPause());
                pauseCount++;
                if (pauseCount > 3) {
                    pauseResumeTasks.add(runIndexerResume());
                    pauseCount = 0;
                }
            }
        }

        assertThat(pauseResumeTasks)
            .isNotEmpty();
        CompletableFuture.allOf(pauseResumeTasks.toArray(new CompletableFuture[0]))
                         .get();
        indexer.resume();
        CompletableFuture.allOf(indexingTasks.toArray(new CompletableFuture[0]))
                         .get();
    }

    private CompletableFuture<Document> runNewDocIndexing(final List<DocumentSentence> text) {
        return CompletableFuture.supplyAsync(() -> {
            final Id docId = Id.len8();
            final int srcId = 1;
            final TestDocument document = new TestDocument(docId, text);
            try {
                return indexer.indexDoc(srcId, document);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }, executorService)
        .thenCompose(docIndexFuture -> docIndexFuture);
    }

    private CompletableFuture<Void> runIndexerPause() {
        return CompletableFuture.runAsync(() -> indexer.pause(), executorService);
    }

    private CompletableFuture<Void> runIndexerResume() {
        return CompletableFuture.runAsync(() -> indexer.resume(), executorService);
    }
}
