package com.iknowledge.hosted.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;

import java.util.Iterator;
import java.util.List;

public class TestDocument implements Document {

    private final Id id;
    private final Iterable<DocumentSentence> text;

    public TestDocument(Id id, Iterable<DocumentSentence> text) {
        this.id = id;
        this.text = text;
    }

    @Override
    public Id id() {
        return id;
    }

    @Override
    public Iterator<Iterable<DocumentSentence>> iterator() {
        return List.of(text).iterator();
    }
}
