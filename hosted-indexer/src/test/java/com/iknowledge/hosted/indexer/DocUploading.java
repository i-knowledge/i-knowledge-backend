package com.iknowledge.hosted.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorChangesStore;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorStore;
import com.iknowledge.hosted.hbase.repositories.indexer.TokenStore;
import com.iknowledge.hosted.indexer.parsing.CommonDocument;
import com.iknowledge.hosted.indexer.parsing.DocParser;
import com.iknowledge.hosted.indexer.parsing.Lang;
import com.iknowledge.hosted.indexer.parsing.LangDetector;
import io.prometheus.client.CollectorRegistry;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.stemmers.OpenNlpSnowballStemmer;
import org.iknowledge.nlp.tools.stemmers.Stemmer;
import org.iknowledge.nlp.tools.tokenization.OpenNlpSentenceTokenizer;
import org.iknowledge.nlp.tools.tokenization.OpenNlpTokenizer;
import org.iknowledge.nlp.tools.tokenization.SentenceTokenizer;
import org.iknowledge.nlp.tools.tokenization.Tokenizer;
import org.iknowledge.nlp.tools.tokenization.filters.CompositeTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.EmptyTokenFilter;
import org.iknowledge.nlp.tools.tokenization.filters.SpecialSymbolTokenFilter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
public final class DocUploading {
    private static final String DOCS_PATH = "/home/grbulat/Documents/test";
    private static final int SOURCE_ID = 1;

    private Indexer indexer;
    private Connection connection;
    private StoreConfig tokenStoreConf;
    private StoreConfig docVecStoreConf;
    private StoreConfig docVecChangesStoreConf;
    private DocParser docParser;
    private DocVectorStore docVectorStore;
    private DocVectorChangesStore changesStore;
    private ClassLoader classLoader;
    private LangDetector langDetector;
    private Map<Lang, SentenceTokenizer> sentenceTokenizers;

    @BeforeClass
    public void before() throws IOException, StoreException {
        classLoader = getClass().getClassLoader();

        URL langDetectModel = classLoader.getResource("models/langdetect-183.bin");
        langDetector = new LangDetector(langDetectModel.getPath());

        sentenceTokenizers = new HashMap<>();
        SentenceTokenizer enSentenceTokenizer = new OpenNlpSentenceTokenizer(
                classLoader.getResourceAsStream("models/en/sentence-detector-model.bin"));
        SentenceTokenizer ruSentenceTokenizer = new OpenNlpSentenceTokenizer(
                classLoader.getResourceAsStream("models/ru/sentence-detector-model.bin"));

        sentenceTokenizers.put(Lang.ENG, enSentenceTokenizer);
        sentenceTokenizers.put(Lang.RUS, ruSentenceTokenizer);

        final Map<Lang, Stemmer> stemmers = new HashMap<>();
        stemmers.put(Lang.ENG, new OpenNlpSnowballStemmer(SnowballStemmer.ALGORITHM.ENGLISH));
        stemmers.put(Lang.RUS, new OpenNlpSnowballStemmer(SnowballStemmer.ALGORITHM.RUSSIAN));

        final Map<Lang, Tokenizer> tokenizers = new HashMap<>();
        URL tokenizerModelUrl = classLoader.getResource("models/en/tokenization-model.bin");

        final FileInputStream tokenizerModel
                = new FileInputStream(tokenizerModelUrl.getFile());
        final OpenNlpTokenizer nlpTokenizer = new OpenNlpTokenizer(tokenizerModel);
        tokenizers.put(Lang.ENG, nlpTokenizer);
        tokenizers.put(Lang.RUS, nlpTokenizer);

        final CompositeTokenFilter tokenFilter
                = new CompositeTokenFilter().addTokenFilter(new EmptyTokenFilter())
                .addTokenFilter(new SpecialSymbolTokenFilter());
        docParser = new DocParser(stemmers, tokenizers, tokenFilter);

        final HConfig hconfig = HConfig.newBuilder()
//                .zkQuorum("192.168.1.10:2181")
                .retryCount(5)
                .retryBackoff(5000)
                .build();
        connection = ConnectionFactory.createConnection(hconfig.asConfiguration());

        tokenStoreConf = StoreConfig.builder()
                .withConnection(connection)
                .withTableName("token_store")
//                .withTableName("token_store_" + new Random().nextInt(10000))
                .withHconfig(hconfig)
                .build();
        docVecStoreConf = StoreConfig.builder()
                .withConnection(connection)
                .withTableName("docvec_store")
//                .withTableName("docvec_store_" + new Random().nextInt(10000))
                .withHconfig(hconfig)
                .build();
        docVecChangesStoreConf = StoreConfig.builder()
                .withConnection(connection)
                .withTableName(("docvec_changes_store"))
//                .withTableName(("docvec_changes_store_" + new Random().nextInt(10000)))
                .withHconfig(hconfig)
                .build();

        docVectorStore = new DocVectorStore(docVecStoreConf);
        changesStore = new DocVectorChangesStore(docVecChangesStoreConf);
        docVectorStore.createTable();
        changesStore.createTable();

        indexer = new Indexer(docParser,
                new TokenStore(tokenStoreConf, true, true),
                docVectorStore,
                changesStore,
                new CollectorRegistry());
    }

    @AfterClass
    public void tearDown() throws Exception {
        indexer.close();
        connection.close();
    }

    @Test(enabled = false)
    public void upload() throws Exception {
        final List<Path> files = getFilesInDir(Paths.get(DOCS_PATH));
        List<CompletableFuture<Document>> futures = new ArrayList<>(files.size());
        for (int i = 0; i < files.size(); i++) {
            Path file = files.get(i);
            InputStream inputStream = new FileInputStream(file.toFile());
            CommonDocument document = new CommonDocument(Id.from(i), inputStream,
                    5000,
                    langDetector,
                    sentenceTokenizers);
            final CompletableFuture<Document> future =
                    indexer.indexDoc(SOURCE_ID, document);
            futures.add(future);
        }
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).get();
    }

    private List<Path> getFilesInDir(Path path) throws IOException {
        final Stream<Path> pathStream = Files.list(path);
        final List<Path> paths = pathStream
                .collect(Collectors.toList());
        pathStream.close();
        return paths;
    }
}
