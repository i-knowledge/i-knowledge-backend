package com.iknowledge.hosted.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.indexer.parsing.CommonDocument;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;
import com.iknowledge.hosted.indexer.parsing.Lang;
import com.iknowledge.hosted.indexer.parsing.LangDetector;
import org.iknowledge.nlp.tools.tokenization.SentenceTokenizer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public final class DocContentReadingTests {

    private CommonDocument parser;
    private LangDetector detector;
    private Map<Lang, SentenceTokenizer> sentenceTokenizers;
    private ClassLoader classLoader;

    @BeforeClass
    public void setUp() throws Exception {
        classLoader = getClass().getClassLoader();

        sentenceTokenizers = new HashMap<>();

        initTokenizer(Lang.RUS,
                      "models/ru/sentence-detector-model.bin",
                      sentenceTokenizers);

        initTokenizer(Lang.ENG,
                      "models/en/sentence-detector-model.bin",
                      sentenceTokenizers);
        URL resource = classLoader.getResource("models/langdetect-183.bin");
        detector = new LangDetector(resource.getPath());
    }

    @Test
    public void testContentParsing() throws Exception {
        InputStream document = classLoader.getResourceAsStream("ru-test-sentences.docx");
        CommonDocument parser = new CommonDocument(Id.ZERO_ID,
                document,
                300,
                detector,
                sentenceTokenizers);

        Iterator<Iterable<DocumentSentence>> iterator =
            parser.iterator();
        List<DocumentSentence> list = new ArrayList<>();
        while (iterator.hasNext()) {
            Iterable<DocumentSentence> sentences = iterator.next();
            for (DocumentSentence sentence : sentences) {
                list.add(sentence);
            }
        }
        assertThat(list)
            .hasSize(8);
        DocumentSentence documentSentence1 = list.get(0);
        checkDocumentSentence(documentSentence1, Lang.RUS, "Тестовый.");

        DocumentSentence documentSentence2 = list.get(1);
        checkDocumentSentence(documentSentence2, Lang.RUS, "Раз два три тестовый.");

        DocumentSentence documentSentence3 = list.get(2);
        checkDocumentSentence(documentSentence3, Lang.RUS, "Новое предложение.");

        DocumentSentence documentSentence4 = list.get(3);
        checkDocumentSentence(documentSentence4, Lang.RUS, "Еще одно предложение.");

        DocumentSentence documentSentence5 = list.get(4);
        checkDocumentSentence(documentSentence5, Lang.RUS, "Чтобы протестить.");

        DocumentSentence documentSentence6 = list.get(5);
        checkDocumentSentence(documentSentence6, Lang.RUS,
                              "XML  построена по клиент-серверной технологии, позволяющей "
                              + "конечному пользователю связываться с информационными системами "
                              + "поставщиков товаров и услуг.");

        DocumentSentence documentSentence7 = list.get(6);
        checkDocumentSentence(documentSentence7, Lang.RUS,
                              "Формой коммуникации является модель «запрос-ответ».");

        DocumentSentence documentSentence8 = list.get(7);
        checkDocumentSentence(documentSentence8, Lang.RUS,
                              "Один или более запросов могут объединяться в один запрос, и "
                              + "соответственно в качестве отклика на запрос, формируется ответ "
                              + "содержащий ответы на каждый входящий запрос.");

    }

    private void checkDocumentSentence(DocumentSentence documentSentence,
                                       Lang lang,
                                       String sentence) {
        assertThat(documentSentence.getLang())
            .isEqualTo(lang);
        assertThat(documentSentence.getSentence())
            .isEqualTo(sentence);
    }

    private void initTokenizer(Lang lang,
                               String sentenceDetectModelPath,
                               Map<Lang, SentenceTokenizer> sentenceTokenizers) throws IOException {

        SentenceTokenizer tokenizer = new org.iknowledge.nlp.tools.tokenization
                                              .OpenNlpSentenceTokenizer(
            classLoader.getResourceAsStream(sentenceDetectModelPath));
        sentenceTokenizers.put(lang, tokenizer);
    }
}
