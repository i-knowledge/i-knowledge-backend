package com.iknowledge.hosted.indexer.queue;

import java.util.concurrent.CompletableFuture;

/**
 * Indexing related event.
 */
public interface Event<T> {

    /**
     * Event source ID(document source storage)
     */
    int source();

    /**
     * Human readable event name.
     */
    String name();

    /**
     * Future which notified when indexing event will be processed by core.
     */
    CompletableFuture<T> future();

}
