package com.iknowledge.hosted.indexer.queue;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Gauge;

import java.util.concurrent.ConcurrentLinkedQueue;

import static com.iknowledge.hosted.indexer.Metrics.INDEXER_CORE;
import static com.iknowledge.hosted.indexer.Metrics.INDEXER_NS;
import static com.iknowledge.hosted.indexer.Metrics.INDEXER_QUEUE_SIZE;
import static java.util.Objects.requireNonNull;

/**
 * In-memory queue contains indexing events: index new document, reindex existing, remove
 * document from index and so on.
 */
public final class InMemoryIndexQueue implements IndexingQueue {

    private final ConcurrentLinkedQueue<Event> queue = new ConcurrentLinkedQueue<>();
    private final Gauge queueSizeGauge;

    public InMemoryIndexQueue(CollectorRegistry metricRegistry) {
        queueSizeGauge = Gauge.build()
                              .name(INDEXER_QUEUE_SIZE)
                              .namespace(INDEXER_NS)
                              .subsystem(INDEXER_CORE)
                              .labelNames("source", "type")
                              .help("Indexer queue size")
                              .register(metricRegistry);
    }

    @Override
    public void append(Event event) {
        requireNonNull(event);

        queue.add(event);
        queueSizeGauge.labels(Integer.toString(event.source()), event.name())
                      .inc();
    }

    /**
     * Get and remove next event from queue.
     */
    @Override
    public Event poll() {
        Event event = queue.poll();
        if (event != null) {
            queueSizeGauge.labels(Integer.toString(event.source()), event.name())
                          .dec();
        }
        return event;
    }

    @Override
    public void clear() {
        queue.forEach(event -> {
            event.future()
                 .completeExceptionally(new AbortedException(event));
            queueSizeGauge.labels(Integer.toString(event.source()), event.name())
                          .dec();
        });
        queue.clear();
    }
}
