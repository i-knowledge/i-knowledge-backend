package com.iknowledge.hosted.indexer;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.common.util.concurrent.Uninterruptibles;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import com.iknowledge.hosted.hbase.repositories.indexer.ChangeType;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorChangesStore;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorStore;
import com.iknowledge.hosted.hbase.repositories.indexer.DocVectorWriter;
import com.iknowledge.hosted.hbase.repositories.indexer.TokenStore;
import com.iknowledge.hosted.indexer.parsing.DocParser;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;
import com.iknowledge.hosted.indexer.queue.Event;
import com.iknowledge.hosted.indexer.queue.InMemoryIndexQueue;
import com.iknowledge.hosted.indexer.queue.IndexEvent;
import com.iknowledge.hosted.indexer.queue.IndexRemoveEvent;
import com.iknowledge.hosted.indexer.queue.IndexingQueue;
import io.prometheus.client.CollectorRegistry;
import org.iknowledge.nlp.tools.Sentence;
import org.iknowledge.nlp.tools.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static com.google.common.base.Preconditions.checkState;

/**
 * Document indexer class.
 * This class represent indexing entry point which used by different document sources to process
 * it documents.
 */
public final class Indexer implements AutoCloseable {

    private final DocParser docParser;
    private final TokenStore tokenStore;
    private final DocVectorStore vectorStore;
    private final DocVectorChangesStore changesStore;
    private final IndexingQueue queue;
    private final ExecutorService executor;
    private final AtomicReference<State> state = new AtomicReference<>(State.RUN);
    private final Phaser pausePhaser = new Phaser(1);

    /**
     * Create new instance of {@link Indexer}.
     *
     * @param registry Metrics registry.
     */
    public Indexer(DocParser docParser,
                   TokenStore tokenStore,
                   DocVectorStore vectorStore,
                   DocVectorChangesStore changesStore,
                   CollectorRegistry registry) {

        this.docParser = docParser;
        this.tokenStore = tokenStore;
        this.vectorStore = vectorStore;
        this.changesStore = changesStore;
        this.queue = new InMemoryIndexQueue(registry);

        executor = Executors.newSingleThreadExecutor(new ThreadFactoryBuilder()
                                                          .setDaemon(true)
                                                          .setNameFormat("indexer-queue-poller-%d")
                                                          .build());

        executor.submit(this::poll);
    }

    /**
     * Add document to indexing queue.
     *
     * @param sourceId ID of document source.
     * @param document Document instance.
     *
     * @return Future which will notify about indexing result.
     */
    public CompletableFuture<Document> indexDoc(int sourceId, Document document) {
        checkState(state.get() != State.CLOSED, "Indexer closed");

        final IndexEvent event = new IndexEvent(sourceId, document);
        queue.append(event);
        if (state.get() == State.CLOSED) {
            queue.clear();
        }
        return event.future();
    }

    /**
     * Remove document from index.
     *
     * @param sourceId ID of document source.
     * @param docId Document ID.
     *
     * @return Future which will notify about removing result.
     */
    public CompletableFuture<Id> removeDoc(int sourceId, Id docId) {
        checkState(state.get() != State.CLOSED, "Indexer closed");

        final IndexRemoveEvent event = new IndexRemoveEvent(sourceId, docId);
        queue.append(event);
        if (state.get() == State.CLOSED) {
            queue.clear();
        }
        return event.future();
    }

    @Override
    public void close() throws Exception {
        while (!state.compareAndSet(State.RUN, State.CLOSE_REQUESTED)
                && !state.compareAndSet(State.PAUSED, State.CLOSE_REQUESTED)
                && !state.compareAndSet(State.CLOSED, State.CLOSED)) {}

        pausePhaser.arrive();
        while (state.get() != State.CLOSED) {}
        queue.clear();
        // no need to await termination, at this point all threads in executor stopped
        executor.shutdown();
    }

    /**
     * Pause indexing.
     */
    public void pause() {
        State lastState;
        while (true) {
            lastState = this.state.compareAndExchange(State.RUN, State.PAUSE_REQUESTED);
            if (lastState == State.RUN || lastState == State.CLOSED || lastState == State.PAUSED) {
                break;
            }
        }

        if (lastState == State.RUN) {
            while (state.get() != State.PAUSED && state.get() != State.CLOSED) {}
        }
    }

    /**
     * Resume indexing.
     */
    public void resume() {
        State lastState;
        while (true) {
            lastState = this.state.compareAndExchange(State.PAUSED, State.RUN);
            if (lastState == State.PAUSED || lastState == State.CLOSED || lastState == State.RUN) {
                break;
            }
        }

        if (lastState == State.PAUSED) {
            pausePhaser.arrive();
        }
    }

    private void poll() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                if (state.get() == State.PAUSE_REQUESTED) {
                    final int phase = pausePhaser.getPhase();
                    state.set(State.PAUSED);
                    pausePhaser.awaitAdvance(phase);
                }
                if (state.get() == State.CLOSE_REQUESTED) {
                    break;
                }

                final Event event = queue.poll();
                if (event == null) {
                    Uninterruptibles.sleepUninterruptibly(300, TimeUnit.MILLISECONDS);
                    continue;
                }
                else if (event instanceof IndexEvent) {
                    indexDocument((IndexEvent) event);
                }
                else if (event instanceof IndexRemoveEvent) {
                    removeDocument((IndexRemoveEvent) event);
                }
            }
        }
        finally {
            state.set(State.CLOSED);
        }
    }

    private void indexDocument(final IndexEvent event) {
        try (DocVectorWriter vectorWriter
                 = vectorStore.writer(event.source(), event.document().id())) {

            long tokenPos = 0;
            for (Iterable<DocumentSentence> textBlock : event.document()) {
                final List<Sentence> sentences = docParser.parse(textBlock);
                final List<TokenProto.Token> tokens = new ArrayList<>(tokenCount(sentences));
                for (Sentence sentence : sentences) {
                    for (Token token : sentence.tokens()) {
                        tokens.add(TokenProto.Token.newBuilder()
                                                      .setId(tokenStore.putToken(token.getTerm()))
                                                      .setPosition(tokenPos)
                                                      .build());
                        tokenPos++;
                    }
                }
                vectorWriter.writeBlock(tokens);
            }
            vectorWriter.commit();
            changesStore.addChange(event.source(), event.document().id(), ChangeType.INDEX_DOC);
            event.future().complete(event.document());
        }
        catch (Throwable e) {
            event.future().completeExceptionally(e);
        }
    }

    private void removeDocument(final IndexRemoveEvent event) {
        try {
            vectorStore.remove(event.source(), event.docId());
            changesStore.addChange(event.source(), event.docId(), ChangeType.REMOVE_DOC);
            event.future().complete(event.docId());
        }
        catch (StoreException e) {
            event.future().completeExceptionally(e);
        }
    }

    private int tokenCount(final Iterable<Sentence> sentences) {
        int tokenCount = 0;
        for (Sentence sentence : sentences) {
            tokenCount += sentence.tokens().size();
        }
        return tokenCount;
    }

    private enum State {
        RUN,
        PAUSE_REQUESTED,
        PAUSED,
        CLOSE_REQUESTED,
        CLOSED
    }
}
