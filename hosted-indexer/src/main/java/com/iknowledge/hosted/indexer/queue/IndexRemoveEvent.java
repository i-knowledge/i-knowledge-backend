package com.iknowledge.hosted.indexer.queue;

import com.iknowledge.common.Id;

import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;

public final class IndexRemoveEvent implements Event<Id> {

    private final int sourceId;
    private final Id docId;
    private CompletableFuture<Id> future;

    /**
     * Create new instance of {@link IndexRemoveEvent}
     *
     * @param sourceId Event source ID
     * @param docId Document ID which have to be removed from index
     */
    public IndexRemoveEvent(int sourceId, Id docId) {
        this.sourceId = sourceId;
        this.docId = docId;
        this.future = new CompletableFuture<>();
    }

    @Override
    public int source() {
        return sourceId;
    }

    @Override
    public String name() {
        return "remove_doc";
    }

    /**
     * Document ID which have to be removed from index
     */
    public Id docId() {
        return docId;
    }

    @Override
    public CompletableFuture<Id> future() {
        return future;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Event " + name(), "")
                   .add("sourceId = " + sourceId)
                   .add("documentId = " + docId)
                   .toString();
    }
}
