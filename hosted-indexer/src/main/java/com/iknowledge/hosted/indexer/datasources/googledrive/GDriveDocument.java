package com.iknowledge.hosted.indexer.datasources.googledrive;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.indexer.Document;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;

import java.util.Iterator;

public final class GDriveDocument implements Document {

    @Override
    public Id id() {
        return null;
    }

    @Override
    public Iterator<Iterable<DocumentSentence>> iterator() {
        return null;
    }
}
