package com.iknowledge.hosted.indexer.parsing;

public final class DocumentSentence {
    private final String sentence;
    private final Lang lang;

    public DocumentSentence(final String sentence,
                            final Lang lang) {
        this.sentence = sentence;
        this.lang = lang;
    }

    public String getSentence() {
        return sentence;
    }

    public Lang getLang() {
        return lang;
    }
}
