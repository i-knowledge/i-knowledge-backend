package com.iknowledge.hosted.indexer.parsing;

import org.iknowledge.nlp.tools.Sentence;
import org.iknowledge.nlp.tools.Token;
import org.iknowledge.nlp.tools.stemmers.Stemmer;
import org.iknowledge.nlp.tools.tokenization.Tokenizer;
import org.iknowledge.nlp.tools.tokenization.filters.TokenFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Document parser which detect language, find appropriate tokenizers, stemmers, etc
 * and parse document using it.
 */
public final class DocParser {

    private final Map<Lang, Stemmer> stemmers;
    private final Map<Lang, Tokenizer> tokenizers;
    private final TokenFilter tokenFilter;

    public DocParser(Map<Lang, Stemmer> stemmers,
                     Map<Lang, Tokenizer> tokenizers,
                     TokenFilter tokenFilter) {

        this.stemmers = stemmers;
        this.tokenizers = tokenizers;
        this.tokenFilter = tokenFilter;
    }

    /**
     * Parse document.
     *
     * @param textBlock Part of document(logically finished) to parse.
     *
     * @return parsed document instance.
     */
    public List<Sentence> parse(Iterable<DocumentSentence> textBlock) {
        List<Sentence> sentences = new ArrayList<>();

        for (DocumentSentence sentence : textBlock) {

            Stemmer stemmer = stemmers.get(sentence.getLang());
            if (stemmer == null) {
                continue;
            }

            Tokenizer tokenizer = tokenizers.get(sentence.getLang());
            if (tokenizer == null) {
                continue;
            }

            final List<Token> tokens
                = tokenFilter.filter(tokenizer.tokenize(sentence.getSentence()))
                           .stream()
                           .map(token -> Token.newBuilder(token)
                                              .withTokenTerm(stemmer.stem(token.getTerm()
                                                                               .toLowerCase()))
                                              .build())
                           .collect(toList());
            sentences.add(new Sentence(tokens));
        }

        return sentences;
    }
}
