package com.iknowledge.hosted.indexer.queue;

public final class AbortedException extends Exception {

    private final Event event;

    public AbortedException(Event event) {
        this.event = event;
    }

    /**
     * Aborted event instance.
     */
    public Event event() {
        return event;
    }

    @Override
    public String toString() {
        return "Event processing aborted: " + event.toString();
    }
}
