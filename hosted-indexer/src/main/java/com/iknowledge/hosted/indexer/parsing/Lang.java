package com.iknowledge.hosted.indexer.parsing;

public enum Lang {

    /**
     * Unknown language
     */
    UNKNOWN,

    /**
     * English
     */
    ENG,

    /**
     * Russian
     */
    RUS;

    static Lang fromString(String lang) {
        switch (lang.toLowerCase()) {
            case "rus":
                return RUS;
            case "eng":
                return ENG;
            default:
                return UNKNOWN;
        }
    }
}
