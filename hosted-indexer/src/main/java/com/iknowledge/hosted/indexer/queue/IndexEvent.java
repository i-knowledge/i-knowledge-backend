package com.iknowledge.hosted.indexer.queue;

import com.iknowledge.hosted.indexer.Document;

import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;

/**
 * Class represent document indexing event.
 */
public final class IndexEvent implements Event<Document> {

    private final int sourceId;
    private final Document document;
    private final CompletableFuture<Document> future;

    /**
     * Create new instance of {@link IndexEvent}
     *
     * @param sourceId Event source ID
     * @param document Document to index
     */
    public IndexEvent(int sourceId, Document document) {
        this.sourceId = sourceId;
        this.document = document;
        this.future = new CompletableFuture<>();
    }

    @Override
    public int source() {
        return sourceId;
    }

    @Override
    public String name() {
        return "new_doc";
    }

    /**
     * Document to index.
     */
    public Document document() {
        return document;
    }

    @Override
    public CompletableFuture<Document> future() {
        return future;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Event " + name(), "")
                   .add("sourceId = " + sourceId)
                   .add("documentId = " + document.id())
                   .toString();
    }
}
