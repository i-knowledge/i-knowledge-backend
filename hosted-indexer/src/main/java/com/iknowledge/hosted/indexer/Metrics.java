package com.iknowledge.hosted.indexer;

/**
 * Metrics related constants.
 */
public final class Metrics {

    /**
     * Indexer namespace.
     */
    public static final String INDEXER_NS = "hosted_indexer";

    /**
     * Indexer core subsystem.
     */
    public static final String INDEXER_CORE = "core";

    /**
     * Indexer queue size gauge name.
     */
    public static final String INDEXER_QUEUE_SIZE = "indexer_queue_size";

    private Metrics() {}
}
