package com.iknowledge.hosted.indexer.parsing;

import com.google.common.base.Strings;
import org.iknowledge.nlp.tools.RawSentence;
import org.iknowledge.nlp.tools.tokenization.SentenceTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class DocContentIterator implements Iterator<Iterable<DocumentSentence>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocContentIterator.class);

    private final Reader reader;
    private final int readBufferSize;
    private final LangDetector langDetector;
    private final Map<Lang, SentenceTokenizer> sentenceTokenizers;

    private String nextChunkOfData;
    private String unfinishedSentence;

    public DocContentIterator(final Reader reader,
                              final int readBufferSize,
                              final LangDetector langDetector,
                              final Map<Lang, SentenceTokenizer> sentenceTokenizers) {
        this.reader = reader;
        this.readBufferSize = readBufferSize;
        this.langDetector = langDetector;
        this.sentenceTokenizers = sentenceTokenizers;
        this.nextChunkOfData = read();
        if (Strings.isNullOrEmpty(this.nextChunkOfData)) {
            throw new IllegalArgumentException("Can not read any data with reader.");
        }
    }

    @Override
    public boolean hasNext() {
        return !Strings.isNullOrEmpty(nextChunkOfData)
                || !Strings.isNullOrEmpty(unfinishedSentence);
    }

    @Override
    public Iterable<DocumentSentence> next() {
        if (Strings.isNullOrEmpty(unfinishedSentence)
                && Strings.isNullOrEmpty(nextChunkOfData)) {
            return new ArrayList<>();
        }

        List<DocumentSentence> sentences = new ArrayList<>();
        if (!Strings.isNullOrEmpty(nextChunkOfData)) {
            sentences = processAlreadyReadData();
        }


        if (Strings.isNullOrEmpty(nextChunkOfData)
                && !Strings.isNullOrEmpty(unfinishedSentence)) {
            sentences.addAll(mergeUnfinishedSentence("", Lang.UNKNOWN));
        }

        return sentences;
    }

    private List<DocumentSentence> processAlreadyReadData() {
        final Lang lang = langDetector.detect(nextChunkOfData);
        if (lang == Lang.UNKNOWN) {
            unfinishedSentence = "";
            nextChunkOfData = read();
            return new ArrayList<>();
        }
        final SentenceTokenizer sentenceTokenizer = sentenceTokenizers.get(lang);
        final List<RawSentence> sentences = sentenceTokenizer.tokenize(nextChunkOfData);
        if (sentences.isEmpty()) {
            unfinishedSentence = "";
            nextChunkOfData = read();
            return new ArrayList<>();
        }
        final RawSentence firstSentence = sentences.remove(0);
        final List<DocumentSentence> documentSentences =
                mergeUnfinishedSentence(firstSentence.text(), lang);

        if (sentences.isEmpty()) {
            nextChunkOfData = read();
            return documentSentences;
        }

        // remove last possibly unfinished sentence
        final RawSentence lastSentence = sentences.remove(sentences.size() - 1);
        unfinishedSentence = lastSentence.text();

        sentences.stream()
                .map(sentence -> new DocumentSentence(sentence.text(), lang))
                .forEach(documentSentences::add);
        nextChunkOfData = read();
        return documentSentences;
    }

    private List<DocumentSentence> mergeUnfinishedSentence(String postSentence,
                                                           Lang postSentenceLang) {
        if (Strings.isNullOrEmpty(unfinishedSentence)) {
            final ArrayList<DocumentSentence> list = new ArrayList<>();
            list.add(new DocumentSentence(postSentence, postSentenceLang));
            return list;
        }
        final String mergedData = unfinishedSentence + postSentence;
        unfinishedSentence = ""; // because we've already used it
        final Lang lang = langDetector.detect(mergedData);
        if (lang == Lang.UNKNOWN) {
            return new ArrayList<>();
        }

        final SentenceTokenizer sentenceTokenizer = sentenceTokenizers.get(lang);
        final List<RawSentence> sentences = sentenceTokenizer.tokenize(mergedData);
        return sentences.stream()
                .map(s -> new DocumentSentence(s.text(), lang))
                .collect(Collectors.toList());
    }

    private String read() {
        char[] chars = new char[readBufferSize];
        try {
            // NOTE: reader.mark() and reader.reset() method are not supported
            final int readCharsCount = reader.read(chars, 0, readBufferSize);
            if (readCharsCount == -1) {
                return null;
            }
            return new String(chars);
        }
        catch (IOException e) {
            LOGGER.error("Error has occurred during getting next chunk of "
                    + "sentences from the document.", e);
            return null;
        }
    }
}
