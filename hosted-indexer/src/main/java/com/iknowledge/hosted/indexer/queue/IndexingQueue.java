package com.iknowledge.hosted.indexer.queue;

/**
 * Queue contains indexing events: index new document, reindex existing, remove
 * document from index and so on.
 */
public interface IndexingQueue {

    /**
     * Append indexing event to queue.
     *
     * @param event Indexing event
     */
    void append(Event event);

    /**
     * Poll event from queue.
     *
     * @return Event instance or null if queue empty.
     */
    Event poll();

    /**
     * Remove all remaining events from queue and mark it as failed.
     */
    void clear();
}
