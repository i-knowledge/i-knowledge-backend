package com.iknowledge.hosted.indexer.parsing;

import opennlp.tools.langdetect.LanguageDetector;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;

import java.io.File;
import java.io.IOException;

/**
 * Document language detector.
 */
public final class LangDetector {

    private final LanguageDetector detector;

    public LangDetector(String modelPath) throws IOException {
        LanguageDetectorModel model = new LanguageDetectorModel(new File(modelPath));
        detector = new LanguageDetectorME(model);
    }

    public LangDetector(LanguageDetectorModel model) {
        detector = new LanguageDetectorME(model);
    }

    /**
     * Detect language for passed text.
     */
    public Lang detect(CharSequence text) {
        return Lang.fromString(detector.predictLanguage(text).getLang());
    }
}
