package com.iknowledge.hosted.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.indexer.parsing.DocumentSentence;

/**
 * Interface represent document which can be read by logically
 * finished chunks(some logically finished text part such as paragraph, table, sentence, etc).
 */
public interface Document extends Iterable<Iterable<DocumentSentence>> {

    /**
     * Document ID.
     */
    Id id();
}
