package com.iknowledge.hosted.indexer.parsing;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.indexer.Document;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ParsingReader;
import org.iknowledge.nlp.tools.tokenization.SentenceTokenizer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;

public final class CommonDocument implements Document {

    private final Id documentId;
    private final InputStream document;
    private final int readBufferSize;
    private final LangDetector langDetector;
    private final Map<Lang, SentenceTokenizer> sentenceTokenizers;

    public CommonDocument(final Id documentId,
                          final InputStream document,
                          final int readBufferSize,
                          final LangDetector langDetector,
                          final Map<Lang, SentenceTokenizer> sentenceTokenizers) {
        this.documentId = documentId;
        this.document = document;
        this.readBufferSize = readBufferSize;
        this.langDetector = langDetector;
        this.sentenceTokenizers = sentenceTokenizers;
    }

    @Override
    public Id id() {
        return documentId;
    }

    @Override
    public Iterator<Iterable<DocumentSentence>> iterator() {
        final Reader reader;
        try {
            reader = getReaderForStream(document);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new DocContentIterator(reader, readBufferSize,
                langDetector, sentenceTokenizers);
    }

    private Reader getReaderForStream(InputStream stream)
            throws IOException {
        Parser parser = new AutoDetectParser();
        ParseContext context = new ParseContext();
        context.set(Parser.class, parser);
        return new ParsingReader(parser, stream, new Metadata(), context);
    }
}
