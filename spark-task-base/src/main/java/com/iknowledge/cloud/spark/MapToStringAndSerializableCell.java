package com.iknowledge.cloud.spark;

import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Class for read HBase table rows and parse key as UTF-8 string and map
 * HBase cell to serializable representation.
 */
public final class MapToStringAndSerializableCell extends MapToSerializableCell<String> {

    @Override
    public String mapKey( final ImmutableBytesWritable key ) {
        return Bytes.toString(key.get());
    }
}
