package com.iknowledge.cloud.spark;

import org.apache.spark.api.java.JavaPairRDD;

import java.util.Map;

import static java.util.stream.Collectors.toMap;

/**
 * Class contains Spark RDD utility methods which solve common routines.
 */
public final class RddUtils {

    private RddUtils() {}

    /**
     * Count the number of elements for each key, and return the result to the master as a Map.
     *
     * @param pairedRdd
     *         Input data RDD.
     * @param <T>
     * @param <V>
     *
     * @return
     */
    public static <T, V> Map<T, Long> countByKey( JavaPairRDD<T, V> pairedRdd ) {
        return pairedRdd.countByKey()
                        .entrySet().stream()
                        .collect(toMap(kv -> kv.getKey(), kv -> ( Long ) kv.getValue()));
    }
}
