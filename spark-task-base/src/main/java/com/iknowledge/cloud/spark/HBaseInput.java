package com.iknowledge.cloud.spark;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.protobuf.ProtobufUtil;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.iknowledge.hbase.client.HConfig;

import java.io.IOException;
import java.util.Base64;

import static java.util.Objects.requireNonNull;

/**
 * This class create Spark RDD which read rows from HBase table.
 */
public abstract class HBaseInput extends HContainer {

    protected final transient JavaSparkContext sparkContext;
    protected final String tableName;
    protected final  HConfig hconfig;
    private transient Configuration inputConfig;

    /**
     * For serialization.
     */
    protected HBaseInput() {
        sparkContext = null;
        inputConfig = null;
        tableName = null;
        hconfig = null;
    }

    /**
     * Create instance.
     *
     * @param sparkContext Spark context instance
     *
     * @param hbaseConf HBase connection configuration
     */
    protected HBaseInput( final JavaSparkContext sparkContext,
                          final HConfig hbaseConf,
                          final String tableName) {

        super(hbaseConf.asConfiguration());

        requireNonNull(sparkContext);
        requireNonNull(tableName);

        this.sparkContext = sparkContext;
        this.tableName = tableName;
        this.hconfig = hbaseConf;

        try {
            inputConfig = createInputConfig(hbaseConf, tableName);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Create raw HBase RDD.
     */
    public JavaPairRDD<ImmutableBytesWritable, Result> readRaw() {
        return sparkContext.newAPIHadoopRDD(inputConfig,
                                            TableInputFormat.class,
                                            ImmutableBytesWritable.class,
                                            Result.class);
    }

    /**
     * Method create HBase table scanner which used to load data into RDD.
     *
     * @return
     */
    protected abstract Scan createScanner();

    private Configuration createInputConfig(final HConfig hbaseConf,
                                            final String tableName)
            throws IOException {

        final Configuration config = new Configuration(hbaseConf.asConfiguration());
        final Scan seTableScan = createScanner();
        requireNonNull(seTableScan, "Scan object returned by \"createScanner\" "
                                        + "method cannot be null");
        final byte[] scanBytes = ProtobufUtil.toScan(seTableScan).toByteArray();
        config.set(TableInputFormat.SCAN, Base64.getEncoder().encodeToString(scanBytes));
        config.set(TableInputFormat.INPUT_TABLE, tableName);
        return config;
    }
}
