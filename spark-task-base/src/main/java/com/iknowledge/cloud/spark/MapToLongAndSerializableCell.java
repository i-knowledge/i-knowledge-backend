package com.iknowledge.cloud.spark;

import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Created by lagrang on 24.12.15.
 */
public final class MapToLongAndSerializableCell extends MapToSerializableCell<Long> {

    @Override
    public Long mapKey( final ImmutableBytesWritable key ) {
        return Bytes.toLong(key.get());
    }
}
