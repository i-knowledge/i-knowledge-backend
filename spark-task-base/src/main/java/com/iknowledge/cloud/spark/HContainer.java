package com.iknowledge.cloud.spark;

import com.google.common.base.Strings;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.hbase.HBaseConfiguration;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

/**
 * Base container class for Hadoop related non-serializable object instances,
 * such as, {@link Configuration}, HBase tables, etc.
 */
public class HContainer implements Serializable {

    protected final Map<String, String> hbaseConf;

    private transient Configuration hconfig;
    private transient FileSystem dfs;
    private transient LocalFileSystem localFs;

    /**
     * For serialization.
     */
    public HContainer() {
        this.hbaseConf = null;
        this.hconfig = null;
        this.dfs = null;
        this.localFs = null;
    }

    public HContainer(final Configuration hadoopConf) {
        requireNonNull(hadoopConf);

        this.hbaseConf = configToMap(hadoopConf);
    }

    /**
     * Method get copy of internally stored {@link Configuration} instance.
     *
     * @return
     */
    public final Configuration hconfig() {
        if (hconfig != null) {
            return hconfig;
        }
        hconfig = HBaseConfiguration.create();
        hbaseConf.forEach(hconfig::set);
        return hconfig;
    }

    /**
     * Get instance of Hadoop {@link FileSystem}.
     *
     * @throws IOException
     */
    public final FileSystem dfs() throws IOException {
        if ( dfs != null) {
            return dfs;
        }
        dfs = FileSystem.get(hconfig());
        return dfs;
    }

    /**
     * Get instance of local {@link FileSystem}.
     *
     * @throws IOException
     */
    public final FileSystem localFs() throws IOException {
        if ( localFs != null) {
            return localFs;
        }
        localFs = FileSystem.getLocal(hconfig());
        return localFs;
    }

    /**
     * Select valid {@link FileSystem} instance for file.
     *
     * @param fileUri File URI.
     *
     * @return {@link FileSystem} instance.
     *
     * @throws IOException
     */
    public final FileSystem selectFsByUri(String fileUri) throws IOException {
        FileSystem fs;
        try {
            final String scheme = new URI(fileUri).getScheme();
            if ( Strings.isNullOrEmpty(scheme) || scheme.equals("file")) {
                fs = localFs();
            }
            else {
                fs = dfs();
            }
        }
        catch ( URISyntaxException e ) {
            fs = localFs();
        }
        return fs;
    }

    /**
     * Method maps {@link Configuration} instance into {@link Map}.
     *
     * @param config Hadoop configuration instance.
     *
     * @return
     */
    private final Map<String, String> configToMap(final Configuration config) {
        return StreamSupport.stream(config.spliterator(), false)
                            .collect(toMap(Map.Entry::getKey,
                                           Map.Entry::getValue));
    }
}
