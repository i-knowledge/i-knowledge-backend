package com.iknowledge.cloud.spark;

import com.google.common.base.StandardSystemProperty;
import com.google.common.collect.Lists;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Base class for Spark tasks which works with Hadoop/HBase
 */
public abstract class HadoopSparkTaskBase extends SparkTaskBase {

    protected static final String SPARK_CONF_OPT = "sc";

    private HContainer hcontainer;

    /**
     * @param taskDesc
     *         Spark driver task description.
     * @param additionalCmdOptions
     */
    protected HadoopSparkTaskBase( final String taskDesc,
                                   final List<Option> additionalCmdOptions) {

        super(taskDesc, additionalCmdOptions);
    }

    /**
     * Create additional command line parameters for task.
     * @return
     */
    protected static List<Option> createCmdOptions() {
        final Option sparkTaskConf = new Option(SPARK_CONF_OPT,
                                                "spark-conf",
                                                true,
                                                "Spark driver configuration");
        sparkTaskConf.setArgName("conf-file");
        sparkTaskConf.setRequired(true);
        return Lists.newArrayList(sparkTaskConf);
    }

    @Override
    protected PropertiesConfiguration loadDriverConf(final CommandLine commandLine)
        throws Exception {

        if (!commandLine.hasOption(SPARK_CONF_OPT)) {
            printHelpMsg();
            System.exit(-2);
        }
        final String commonConfFile = commandLine.getOptionValue(SPARK_CONF_OPT);
        Path confFilePath = Paths.get(StandardSystemProperty.USER_DIR.value(),
                                      commonConfFile);
        logger.info("Load common conf from file '{}'", confFilePath.toString());
        final boolean confExists = Files.exists(confFilePath);
        if (!confExists) {
            confFilePath = Paths.get(commonConfFile);
        }
        try {
            PropertiesConfiguration driverConf
                = new PropertiesConfiguration(confFilePath.toString());
            driverConf.setThrowExceptionOnMissing(true);
            return driverConf;
        }
        catch (final ConfigurationException e) {
            throw new FileNotFoundException("Cannot load config from file='" + commonConfFile
                                            + ". Check value of '" + SPARK_CONF_OPT
                                            + "' parameter.");
        }
    }

    @Override
    protected SparkConf loadSparkConf(final PropertiesConfiguration driverProps) {
        final Configuration hadoopConf = loadHadoopConf(driverProps);
        requireNonNull(hadoopConf, "Hadoop config cannot be null");
        hcontainer = new HContainer(hadoopConf);

        return new SparkConf();
    }

    protected abstract Configuration loadHadoopConf(final PropertiesConfiguration driverProps);

    /**
     * Method get copy of internally stored {@link Configuration} instance.
     */
    protected final Configuration hconfig() {
        return hcontainer.hconfig();
    }

    /**
     * Get instance of Hadoop {@link FileSystem}.
     *
     * @throws IOException
     */
    protected final FileSystem dfs() throws IOException {
        return hcontainer.dfs();
    }

    /**
     * Get instance of local {@link FileSystem}.
     *
     * @throws IOException
     */
    protected final FileSystem localFs() throws IOException {
        return hcontainer.localFs();
    }
}
