package com.iknowledge.cloud.spark;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.primitives.Ints;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Spark task base class.
 */
public abstract class SparkTaskBase implements Serializable {

    protected static final String HELP_OPT = "h";
    protected static final String TEST_MODE_OPT = "t";
    protected static final String THREAD_COUNT = "tc";

    protected final transient Logger logger;
    private transient JavaSparkContext sparkCtx;
    private transient PropertiesConfiguration driverConf;
    private transient Options cmdOptions;
    private transient HelpFormatter helpFormatter = new HelpFormatter();
    private boolean isInitialized;
    private final String taskDesc;

    /**
     * @param taskDesc
     *         Spark driver task description.
     */
    protected SparkTaskBase( String taskDesc,
                             List<Option> additionalCmdOptions) {

        requireNonNull(taskDesc, "taskDesc");

        this.taskDesc = taskDesc;
        this.logger = LoggerFactory.getLogger(getClass().getName());
        this.cmdOptions = createDefaultCmdOptions(additionalCmdOptions);
    }

    /**
     * Spark StackExchange driver main entry.
     *
     * @param args
     *         Driver args
     */
    public static void main( String[] args ) throws Exception {
        //find all subtypes of this base class
        final Reflections reflections = new Reflections("com.iknowledge.cloud.spark");
        final Set<Class<? extends SparkTaskBase>> taskClasses
                = reflections.getSubTypesOf(SparkTaskBase.class);
        //choose first subtype
        final Optional<Class<? extends SparkTaskBase>> implementor
                = taskClasses.stream()
                             .filter(taskClass -> !Modifier.isAbstract(taskClass.getModifiers()))
                             .findFirst();
        if ( !implementor.isPresent() ) {
            throw new IllegalStateException("Cannot find any subtypes of "
                                            + SparkTaskBase.class.getName());
        }
        final Class<? extends SparkTaskBase> driverType = implementor.get();
        System.out.println("Loading driver class: " + driverType.getName());
        //create instance of first found subtype of this class
        final SparkTaskBase sparkDriver = driverType.newInstance();
        sparkDriver.initialize(args);
        sparkDriver.start();
    }

    /**
     * Start Spark task.
     */
    public void start() {
        checkState(isInitialized, "Object not initialized");

        executeProcessing(sparkCtx, driverConf);
        sparkCtx.close();
    }

    /**
     * Method with task data processing logic.
     * @param sparkCtx
     * @param driverConf
     */
    protected abstract void executeProcessing(
        final JavaSparkContext sparkCtx,
        final PropertiesConfiguration driverConf);

    /**
     * Initialization method for current Spark task driver.
     */
    protected abstract SparkConf loadSparkConf(final PropertiesConfiguration driverProps)
        throws Exception;

    /**
     * Load driver configuration.
     *
     * @throws Exception
     */
    protected abstract PropertiesConfiguration loadDriverConf(final CommandLine commandLine)
        throws Exception;

    private SparkConf setDefaults(SparkConf sparkConf, CommandLine cmd) {

        if (sparkConf.get("spark.app.name", null) == null) {
            sparkConf.setAppName(driverConf.getString("spark.app.name",
                                                      getClass().getSimpleName()));
        }

        if ( cmd.hasOption(TEST_MODE_OPT) ) {
            String tcString = cmd.getOptionValue(THREAD_COUNT);
            if (Strings.isNullOrEmpty(tcString)) {
                tcString = Integer.toString(Runtime.getRuntime().availableProcessors());
            }
            Integer threadCount = Ints.tryParse(tcString);
            if (threadCount == null ) {
                threadCount = Runtime.getRuntime().availableProcessors();
            }
            logger.warn("Run in test mode on " + threadCount + "thread(s)");
            sparkConf.setMaster("local[" + threadCount + "]");
        }
        return sparkConf;
    }

    protected boolean isTestMode() {
        Preconditions.checkNotNull(cmdOptions, "Command line options not available "
                                               + "in this point of execution");
        return cmdOptions.hasOption(TEST_MODE_OPT);
    }

    /**
     * Print help message in console.
     */
    protected void printHelpMsg() {
        helpFormatter.printHelp("./spark-submit driver.jar [options]",
                                taskDesc,
                                cmdOptions,
                                "");
    }

    /**
     * Initialization of current Spark task driver.
     *
     * @param args Driver command line parameters
     */
    private void initialize( String[] args ) throws Exception {
        requireNonNull(args, "args");

        final CommandLine commandLine = parseCmdLine(args);
        if ( commandLine.hasOption(HELP_OPT) ) {
            printHelpMsg();
            System.exit(0);
        }

        driverConf = loadDriverConf(commandLine);

        final SparkConf sparkConf = loadSparkConf(driverConf);
        setDefaults(sparkConf, commandLine);

        sparkCtx = new JavaSparkContext(sparkConf);
        isInitialized = true;
    }

    private CommandLine parseCmdLine( String[] args ) {
        final CommandLineParser parser = new GnuParser();
        CommandLine commandLine = null;
        try {
            commandLine = parser.parse(cmdOptions, args);
        }
        catch ( ParseException e ) {
            e.printStackTrace();
            System.out.println();
            printHelpMsg();
            System.exit(-1);
        }
        return commandLine;
    }

    private static Options createDefaultCmdOptions( final List<Option> additionalCmdOptions ) {
        final Option help = new Option(HELP_OPT, "help", false, "Print help message");
        help.setRequired(false);
        final Option testMode = new Option(TEST_MODE_OPT, "test-mode",
                                           false, "Run driver in test mode");
        testMode.setRequired(false);
        final Option sparkLocalThreads
            = new Option(THREAD_COUNT, "threads-count",
                         true,
                         "Set the thread count for Spark local mode(used with 'test' arg)");
        sparkLocalThreads.setRequired(false);
        final Options options = new Options();
        options.addOption(testMode)
               .addOption(help)
               .addOption(sparkLocalThreads);
        additionalCmdOptions.stream().forEach(options::addOption);
        return options;
    }
}
