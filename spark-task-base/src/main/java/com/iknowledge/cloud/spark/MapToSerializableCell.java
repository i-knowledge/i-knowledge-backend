package com.iknowledge.cloud.spark;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.iknowledge.hbase.client.SerializableCell;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 * Spark function to map HBase raw result to serializable raw result.
 */
public abstract class MapToSerializableCell<R extends Serializable>
        implements PairFlatMapFunction<Tuple2<ImmutableBytesWritable, Result>,
                                              R, SerializableCell>, Serializable {

    @Override
    public Iterator<Tuple2<R, SerializableCell>>
        call(final Tuple2<ImmutableBytesWritable, Result> kv) {

        final ImmutableBytesWritable key = kv._1();
        final Result result = kv._2();
        return result.listCells()
                     .stream()
                     .map(cell -> new Tuple2<>(mapKey(key), new SerializableCell(cell)))
                     .collect(Collectors.toList())
                     .iterator();
    }

    /**
     * Method to map key to some type.
     *
     * @param key
     *
     * @return
     */
    public abstract R mapKey( ImmutableBytesWritable key );
}
