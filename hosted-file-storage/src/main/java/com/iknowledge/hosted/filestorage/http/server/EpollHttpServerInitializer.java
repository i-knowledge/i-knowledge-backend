package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.epoll.EpollSocketChannel;

final class EpollHttpServerInitializer extends ChannelInitializer<EpollSocketChannel> {

    private final HttpServerChannelInitializer channelInitializer;

    public EpollHttpServerInitializer(final HttpServerConfig config,
                                      final FileSystem fs) {

        channelInitializer = new HttpServerChannelInitializer(config, fs);
    }

    @Override
    protected void initChannel(final EpollSocketChannel ch) throws Exception {
        channelInitializer.initChannel(ch);
    }
}
