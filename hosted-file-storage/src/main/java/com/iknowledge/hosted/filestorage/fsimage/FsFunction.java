package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathOperationException;

@FunctionalInterface
public interface FsFunction<T, R> {

    R call(T instance) throws PathOperationException, StoreException;
}
