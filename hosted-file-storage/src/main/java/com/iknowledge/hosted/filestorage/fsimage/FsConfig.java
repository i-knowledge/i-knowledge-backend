package com.iknowledge.hosted.filestorage.fsimage;

/**
 * Class contains configuration options for {@link InMemoryFsImage}
 */
public final class FsConfig {

    private final int readLockWaitTimeMs;
    private final int writeLockWaitTimeMs;
    private final int pathCacheMs;
    private final long pathCacheMaxSize;
    private final int backgroundIoPoolSize;
    private final FileRevisionConfig fileRevisionConfig;

    private FsConfig(final Builder builder) {
        readLockWaitTimeMs = builder.readLockWaitTimeMs;
        writeLockWaitTimeMs = builder.writeLockWaitTimeMs;
        pathCacheMs = builder.pathCacheMs;
        pathCacheMaxSize = builder.pathCacheMaxSize;
        backgroundIoPoolSize = builder.backgroundIoPoolSize;
        fileRevisionConfig = builder.fileRevisionConfig;
    }

    public static IReadLockWaitTimeMs builder() {
        return new Builder();
    }

    public int getReadLockWaitTimeMs() {
        return readLockWaitTimeMs;
    }

    public int getWriteLockWaitTimeMs() {
        return writeLockWaitTimeMs;
    }

    public int getPathCacheMs() {
        return pathCacheMs;
    }

    public long getPathCacheMaxSize() {
        return pathCacheMaxSize;
    }

    public FileRevisionConfig getFileRevisionConfig() {
        return fileRevisionConfig;
    }

    public int getBackgroundTaskPoolSize() {
        return backgroundIoPoolSize;
    }

    public interface IBuild {

        FsConfig build();
    }

    public interface IFileRevisionConfig {

        IBuild withFileRevisionConfig(FileRevisionConfig val);
    }

    public interface IBackgroundFsTaskPoolSize {

        IFileRevisionConfig withBackgroundIoPoolSize(int val);
    }

    public interface IPathCacheMaxSize {

        IBackgroundFsTaskPoolSize withPathCacheMaxSize(long val);
    }

    public interface IPathCacheMs {

        IPathCacheMaxSize withPathCacheMs(int val);
    }

    public interface IWriteLockWaitTimeMs {

        IPathCacheMs withWriteLockWaitTimeMs(int val);
    }

    public interface IReadLockWaitTimeMs {

        IWriteLockWaitTimeMs withReadLockWaitTimeMs(int val);
    }

    public static final class Builder
            implements IFileRevisionConfig, IBackgroundFsTaskPoolSize, IPathCacheMaxSize,
                       IPathCacheMs, IWriteLockWaitTimeMs, IReadLockWaitTimeMs, IBuild {

        private FileRevisionConfig fileRevisionConfig;
        private int backgroundIoPoolSize;
        private long pathCacheMaxSize;
        private int pathCacheMs;
        private int writeLockWaitTimeMs;
        private int readLockWaitTimeMs;

        private Builder() {}

        @Override
        public IBuild withFileRevisionConfig(final FileRevisionConfig val) {
            fileRevisionConfig = val;
            return this;
        }

        @Override
        public IFileRevisionConfig withBackgroundIoPoolSize(final int val) {
            backgroundIoPoolSize = val;
            return this;
        }

        @Override
        public IBackgroundFsTaskPoolSize withPathCacheMaxSize(final long val) {
            pathCacheMaxSize = val;
            return this;
        }

        @Override
        public IPathCacheMaxSize withPathCacheMs(final int val) {
            pathCacheMs = val;
            return this;
        }

        @Override
        public IPathCacheMs withWriteLockWaitTimeMs(final int val) {
            writeLockWaitTimeMs = val;
            return this;
        }

        @Override
        public IWriteLockWaitTimeMs withReadLockWaitTimeMs(final int val) {
            readLockWaitTimeMs = val;
            return this;
        }

        public FsConfig build() {
            return new FsConfig(this);
        }
    }
}
