package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hosted.filestorage.InvalidPathException;

import java.nio.file.Path;

public final class FsPath {

    /**
     * Check is this path points to FS root. Throw exception if true.
     *
     * @param path
     *
     * @throws InvalidPathException
     */
    public void checkNotRoot(Path path) throws InvalidPathException {
        if (path.getNameCount() == 0 && isRoot(path)) {
            throw new InvalidPathException("Cannot perform operation on FS root");
        }
    }

    private boolean isRoot(Path path) {
        for (Path root : path.getFileSystem().getRootDirectories()) {
            if (root.equals(path)) {
                return true;
            }
        }
        return false;
    }
}
