package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;

import java.util.function.Supplier;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asFileSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isFileSnapshot;

final class MoveOp extends BaseOp {

    private final HBaseFsImageStore backend;

    public MoveOp(HBaseFsImageStore backend,
                  int readLockTimeout,
                  int writeLockTimeout) {

        super(readLockTimeout, writeLockTimeout);
        this.backend = backend;
    }

    public void perform(INodeSnapshot<? extends UserLevelINode> srcNodeToMove,
                        DirINodeSnapshot srcParentDir,
                        DirINodeSnapshot destDir,
                        String newName) throws PathOperationException, StoreException {

        if (isDirSnapshot(srcNodeToMove)) {
            final DirectoryINode source = asDirSnapshot(srcNodeToMove).inode();
            moveSnap(srcNodeToMove,
                     srcParentDir,
                     destDir,
                     () -> new DirectoryINode(source, newName, destDir.inode()));
        }
        else if (isFileSnapshot(srcNodeToMove)) {
            final FileINode source = asFileSnapshot(srcNodeToMove).inode();
            moveSnap(srcNodeToMove,
                     srcParentDir,
                     destDir,
                     () -> source.copy(newName, destDir.inode()));
        }
        else {
            throw new IllegalArgumentException("Invalid inode type: " + srcNodeToMove);
        }
    }

    private void moveSnap(INodeSnapshot<? extends UserLevelINode> srcSnap,
                          DirINodeSnapshot srcParentSnap,
                          DirINodeSnapshot newParentSnap,
                          Supplier<INode> movedInodeGenFunc)
        throws PathOperationException, StoreException {

        // current lock order is important
        failFastWriteLock(srcSnap.inode(), (FsRunnable) () -> {
            failFastWriteLock(srcParentSnap.inode(), (FsRunnable) () -> {
                failFastWriteLock(newParentSnap.inode(), (FsRunnable) () -> {

                    throwIfModified(srcSnap);
                    throwIfModified(newParentSnap);
                    UserLevelINode source = srcSnap.inode();
                    DirectoryINode newParent = newParentSnap.inode();
                    throwIfNotAttached(source);
                    throwIfNotAttached(source.parent());
                    throwIfNotAttached(newParent);

                    DirectoryINode srcParent = srcParentSnap.inode();
                    moveInode(source, srcParent, newParent, movedInodeGenFunc);
                });
            });
        });
    }

    private void moveInode(final UserLevelINode source,
                           final DirectoryINode srcParent,
                           final DirectoryINode newParent,
                           final Supplier<INode> movedInodeGenFunc)
        throws PathExistsException, StoreException {

        // create inode copy with new name and parent(or same name
        // if simple move required)
        final INode destInode = movedInodeGenFunc.get();
        throwIfHasSameChild(newParent, destInode);

        if (source.name().equals(destInode.name())) { //simple move
            backend.move(source, newParent);
        }
        else { // move with rename or simple rename
            backend.rename(source, destInode);
        }

        // because current and new parent can be same node(in rename case)
        // we have to first remove from current parent and only after append to new
        srcParent.removeChild(source);
        source.rename(destInode.name());
        source.changeParent(newParent);
        newParent.addChild(source);
    }
}
