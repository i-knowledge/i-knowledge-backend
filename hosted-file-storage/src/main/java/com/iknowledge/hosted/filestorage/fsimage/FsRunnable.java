package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathOperationException;

@FunctionalInterface
public interface FsRunnable {

    void run() throws PathOperationException, StoreException;
}
