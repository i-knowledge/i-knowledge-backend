package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.AccessDeniedException;
import com.iknowledge.hosted.filestorage.InvalidPathException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathLockedException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.AccessRestrictedINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ProposedFileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import io.prometheus.client.CollectorRegistry;

import java.nio.file.Path;
import java.util.List;
import java.util.function.BiFunction;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asFileSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isFileSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;

public final class InMemoryFsImage {

    private static final FsPath fsPath = new FsPath();

    private final HBaseFsImageStore backend;
    private final FsMap fsMap;
    private final FsStats stats;
    private final CreateOp createOp;
    private final RemoveOp removeOp;
    private final RemoveRevisionOp removeRevOp;
    private final MoveOp moveOp;
    private final SearchOp searchOp;
    private final CheckRightsOp checkRightsOp;
    private final AttachRightsOp attachRightsOp;
    private final CreateRevisionOp createRevOp;

    /**
     * Create new instance of {@link InMemoryFsImage}
     *
     * @param fsImage HBase FS image instance. Used to load FS image
     *                     into memory and perform basic operations such as saving/removing inodes.
     *
     * @throws StoreException
     */
    public InMemoryFsImage(final HBaseFsImageStore fsImage,
                           final FsConfig config,
                           final CollectorRegistry metricsRegistry)
        throws StoreException {

        backend = fsImage;
        FsMap loadedImage = fsImage.load()
                                   .orElseThrow(() ->
                                              new StoreException("FS image cannot be loaded,"
                                                            + "check HBase FS image table "
                                                            + "state."));
        this.fsMap = loadedImage;
        final long initialCapacity = (long)((loadedImage.dirs() + loadedImage.files()) * 0.05);
        FsSearchCache cache = new FsSearchCache(initialCapacity, config, metricsRegistry);
        stats = new FsStats(fsMap, cache);

        removeOp = new RemoveOp(backend,
                                fsMap,
                                config.getReadLockWaitTimeMs(),
                                config.getWriteLockWaitTimeMs());
        removeRevOp = new RemoveRevisionOp(backend,
                                           fsMap,
                                           config.getReadLockWaitTimeMs(),
                                           config.getWriteLockWaitTimeMs());
        createOp = new CreateOp(backend,
                                fsMap,
                                config.getReadLockWaitTimeMs(),
                                config.getWriteLockWaitTimeMs());
        createRevOp = new CreateRevisionOp(backend,
                                        fsMap,
                                        config.getReadLockWaitTimeMs(),
                                        config.getWriteLockWaitTimeMs());
        moveOp = new MoveOp(backend,
                            config.getReadLockWaitTimeMs(),
                            config.getWriteLockWaitTimeMs());
        searchOp = new SearchOp(this.fsMap.root(),
                                cache,
                                config.getReadLockWaitTimeMs(),
                                config.getWriteLockWaitTimeMs());
        checkRightsOp = new CheckRightsOp(config.getReadLockWaitTimeMs(),
                                          config.getWriteLockWaitTimeMs());
        attachRightsOp = new AttachRightsOp(backend,
                                            config.getReadLockWaitTimeMs(),
                                            config.getWriteLockWaitTimeMs());
    }

    /**
     * Create new file in in-memory FS image
     *
     * @param filePath Absolute path for new file
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @return {@link INode} instance representing new file
     *
     * @throws PathOperationException
     * @throws StoreException
     */
    public INode createFile(Path filePath, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        return createInode(filePath, FileINode::new, currentUserInfo);
    }

    /**
     * Create new file revision in in-memory FS image
     *
     * @param revisionInode Proposed file revision inode
     *
     * @return {@link com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode}
     * instance representing new file revision
     *
     * @throws PathNotExistsException
     * @throws PathLockedException Path locked by other user
     * @throws StoreException
     */
    public FileRevisionINode createFileRevision(ProposedFileRevisionINode revisionInode)
            throws PathOperationException, StoreException {

        return createRevOp.perform(revisionInode);
    }

    /**
     * Create new directory in in-memory FS image
     *
     * @param dirPath Absolute path for new directory
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @return {@link INode} instance representing new file
     *
     * @throws PathNotExistsException
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException
     * @throws StoreException
     */
    public DirectoryINode createDirectory(Path dirPath, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        return createInode(dirPath, DirectoryINode::new, currentUserInfo);
    }

    /**
     * Remove existing path
     *
     * @param path Path to remove
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @return Removed inode
     *
     * @implNote This is operation not atomic
     *
     * @throws PathLockedException Removal path locked by other operation
     * @throws PathNotExistsException Removal path not exist
     * @throws InvalidPathException Removal path not exist
     * @throws AccessDeniedException
     * @throws StoreException
     */
    public INode remove(Path path, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        fsPath.checkNotRoot(path);

        final INodeSnapshot<? extends UserLevelINode> removalRoot = searchOp.find(path);
        checkWriteAccess(removalRoot.inode(), currentUserInfo);

        if (isFileSnapshot(removalRoot)) {
            removeOp.perform(asFileSnapshot(removalRoot));
        }
        else if (isDirSnapshot(removalRoot)) {
            removeOp.perform(asDirSnapshot(removalRoot));
        }
        else {
            throw new PathNotExistsException(path.toString());
        }
        return removalRoot.inode();
    }

    /**
     * Remove file revision
     *
     * @param path Path to file
     * @param revision Revision number to remove
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @return Removed file revision inode
     *
     * @throws PathLockedException Removal path locked by other operation
     * @throws PathNotExistsException Removal path not exist
     * @throws InvalidPathException Removal path not exist
     * @throws AccessDeniedException
     * @throws StoreException
     */
    public FileRevisionINode remove(Path path, long revision, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        final INodeSnapshot<? extends UserLevelINode> foundInode = searchOp.find(path);
        if (!isFileSnapshot(foundInode)) {
            throw new InvalidPathException(path + " is not a file. "
                                           + "Revision attached only to files.");
        }
        final FileINodeSnapshot file = asFileSnapshot(foundInode);
        checkRightsOp.throwIfCantWriteToINode(file.inode(), currentUserInfo);
        return removeRevOp.perform(file.inode(), revision);
    }

    /**
     * Method move inode from source into destination place.
     * It acts like transaction by proposing new parent for source path, detaching from current
     * parent.
     *
     * @implNote This is operation not atomic
     *
     * @param srcPath Source path.
     * @param destPath Destination path.
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @return INode instance which has been moved
     *         or empty if after move operation new inode not found(concurrent move or remove).
     *
     * @throws PathNotExistsException Source path not exists
     * @throws PathExistsException Destination path already exists
     * @throws PathLockedException Destination path locked by other operation
     * @throws InvalidPathException
     * @throws StoreException
     */
    public void move(Path srcPath, Path destPath, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        fsPath.checkNotRoot(srcPath);
        fsPath.checkNotRoot(destPath);

        Path srcParentPath = srcPath.getParent();
        Path destParentPath = destPath.getParent();
        final INodeSnapshot<? extends UserLevelINode> parentInode
            = searchOp.find(srcParentPath);
        final INodeSnapshot<? extends UserLevelINode> foundInode
            = searchOp.find(srcPath);
        final INodeSnapshot<? extends UserLevelINode> destParent
            = searchOp.find(destParentPath);

        if (!isDirSnapshot(destParent)) {
            throw new PathNotExistsException(destParentPath.toString());
        }
        if (!isDirSnapshot(parentInode)) {
            throw new PathNotExistsException(srcParentPath.toString());
        }

        final UserLevelINode srcInode = foundInode.inode();
        checkWriteAccess(srcInode, currentUserInfo);
        // if this is not dir rename => check that destination dir is not
        // a child of source dir. We cannot move parent directory into child directory
        if (isUsualDir(srcInode)) {
            INode parent = destParent.inode();
            do {
                if (parent.equals(srcInode)) {
                    throw new InvalidPathException("Cannot move inode: move destination inode is "
                                                   + "child inode of inode which should be moved"
                                                   + "(parent cannot be moved into it's child)");
                }
                parent = parent.parent();
            }
            while (parent != null);
        }

        checkRightsOp.throwIfCantWriteToINode(destParent.inode(), currentUserInfo);

        final String destInodeName = destPath.getFileName().toString();
        moveOp.perform(foundInode,
                       asDirSnapshot(parentInode),
                       asDirSnapshot(destParent),
                       destInodeName);
    }

    /**
     * Find inode which associated with FS path bypassing user checks.
     *
     * @param path FS path to find
     *
     * @return
     *
     * @throws PathNotExistsException Path not exists
     * @throws PathLockedException Path locked by other user
     */
    public INode find(final Path path) throws PathOperationException {
        return searchOp.find(path).inode();
    }

    /**
     * Find inode which associated with FS path.
     *
     * @param path FS path to find
     *
     * @param currentUserInfo User which request inode instance
     *
     * @return
     *
     * @throws PathNotExistsException Path not exists
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException
     */
    public INode find(final Path path, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        final INode inode = find(path);
        checkReadAccess(inode, currentUserInfo);
        return inode;
    }

    /**
     * Find file revision inode which associated with FS file path.
     *
     * @param path FS file path to find
     *
     * @param currentUserInfo User which request inode instance
     *
     * @return
     *
     * @throws PathNotExistsException Path not exists
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException
     */
    public FileRevisionINode findRev(final Path path,
                                     long revNumber,
                                     final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        final INodeSnapshot<? extends UserLevelINode> inode = searchOp.find(path);
        if (!isFileSnapshot(inode)) {
            throw new InvalidPathException("Path must point to file");
        }

        final FileINode file = asFileSnapshot(inode).inode();
        checkReadAccess(file, currentUserInfo);
        return searchOp.findRev(file, revNumber);
    }

    /**
     * Attach user rights to the inode
     * @param path FS path
     * @param currentUserInfo Current user that is performing the operation.
     * @param rights Rights that are need to be added to the inode by the specified path
     * @throws StoreException
     */
    public void attachAccessRights(final Path path,
                                   final UserInfoLite currentUserInfo,
                                   final List<HBaseINodeRights> rights)
            throws PathOperationException, StoreException {

        INode inode = find(path);
        checkWriteAccess(inode, currentUserInfo);
        attachRightsOp.attachAccessRights((AccessRestrictedINode)inode, rights);
    }

    /**
     * Return file system stats
     *
     * @return
     */
    public FsStats stats() {
        return stats;
    }

    /**
     * Return loaded fsMap instance
     * @return
     */
    public FsMap fsMap() {
        return fsMap;
    }

    private void checkReadAccess(final INode inode, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        if (inode instanceof AccessRestrictedINode) {
            checkRightsOp.throwIfCantReadINode((AccessRestrictedINode) inode, currentUserInfo);
        }
    }

    private void checkWriteAccess(final INode inode, final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        if (inode instanceof AccessRestrictedINode) {
            checkRightsOp.throwIfCantWriteToINode((AccessRestrictedINode) inode,
                                                  currentUserInfo);
        }
    }

    private <T extends UserLevelINode> T createInode(final Path filePath,
                                            BiFunction<String, DirectoryINode, T> inodeGenerator,
                                            final UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        fsPath.checkNotRoot(filePath);

        DirectoryINode parentDir = searchOp.findParentDir(filePath)
                                  .getOrElseThrow(ioEx -> ioEx)
                                  .orElseThrow(PathNotExistsException::new);

        checkRightsOp.throwIfCantWriteToINode(parentDir, currentUserInfo);

        final String inodeName = filePath.getFileName().toString();
        final T newInode = createOp.perform(parentDir, inodeName, inodeGenerator);
        return newInode;
    }
}
