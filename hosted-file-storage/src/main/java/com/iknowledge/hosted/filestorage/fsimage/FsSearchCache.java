package com.iknowledge.hosted.filestorage.fsimage;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.State;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.cache.caffeine.CacheMetricsCollector;

import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static com.iknowledge.hosted.filestorage.fsimage.INodeLocks.tryReadLock;

final class FsSearchCache {

    private final Cache<Path, UserLevelINode> pathCache;
    private final FsConfig config;

    /**
     * Create new instance of {@link FsSearchCache}
     *
     * @param initialCapacity Initial capacity of cache
     * @param config FS config instance
     */
    public FsSearchCache(final long initialCapacity,
                         final FsConfig config,
                         final CollectorRegistry metricsRegistry) {

        int truncatedInitCapacity = initialCapacity > Integer.MAX_VALUE
                                        ? Integer.MAX_VALUE
                                        : (int)initialCapacity;

        this.config = config;

        pathCache = Caffeine.newBuilder()
                            .executor(Executors.newSingleThreadExecutor())
                            .expireAfterAccess(config.getPathCacheMs(), TimeUnit.MILLISECONDS)
                            .initialCapacity(truncatedInitCapacity)
                            .maximumSize(config.getPathCacheMaxSize())
                            .recordStats()
                            .build();
        CacheMetricsCollector cacheMetrics = new CacheMetricsCollector()
                                                     .register(metricsRegistry);
        cacheMetrics.addCache("fs_search_cache", pathCache);
    }

    public void put(UserLevelINode inode) {
        pathCache.put(inode.path(), inode);
    }

    public void put(Path path, UserLevelINode inode) {
        pathCache.put(path, inode);
    }

    public void invalidate(Path path) {
        pathCache.invalidate(path);
    }

    /**
     * Return inode associated by path. If node in cache but already detached it will be evicted
     * from cache.
     *
     * @param path
     * @return
     */
    public Optional<UserLevelINode> getByPath(Path path) {
        final UserLevelINode cachedInode = pathCache.getIfPresent(path);
        if (cachedInode == null) {
            return Optional.empty();
        }

        UserLevelINode ensuredInode
            = tryReadLock(cachedInode, config.getReadLockWaitTimeMs(), TimeUnit.MILLISECONDS,
                          (Supplier<UserLevelINode>)() -> {
                              if (cachedInode.state() == State.DETACHED) {
                                  pathCache.invalidate(path);
                                  return null;
                              }
                              // to invalidate moved inodes
                              if (!cachedInode.path().equals(path)) {
                                  pathCache.invalidate(path);
                                  return null;
                              }
                              return cachedInode;
                          })
                      .getOrElse((UserLevelINode)null);

        return ensuredInode != null
               ? Optional.of(ensuredInode)
               : Optional.empty();
    }

    public CacheStats stats() {
        return pathCache.stats();
    }
}
