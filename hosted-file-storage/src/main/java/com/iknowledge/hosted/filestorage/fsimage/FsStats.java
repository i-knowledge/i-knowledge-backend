package com.iknowledge.hosted.filestorage.fsimage;

import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;

public final class FsStats {

    private final FsMap fsMap;
    private final FsSearchCache fsCache;

    FsStats(final FsMap fsMap, FsSearchCache fsCache) {
        this.fsMap = fsMap;
        this.fsCache = fsCache;
    }

    public long files() {
        return fsMap.files();
    }

    public long dirs() {
        return fsMap.dirs();
    }

    public long revs() {
        return fsMap.revs();
    }

    public CacheStats cacheStats() {
        return fsCache.stats();
    }
}
