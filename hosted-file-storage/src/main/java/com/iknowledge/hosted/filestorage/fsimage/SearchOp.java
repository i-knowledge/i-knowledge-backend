package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ContainerINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;
import io.vavr.control.Either;

import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Supplier;

final class SearchOp extends BaseOp {

    private final DirectoryINode root;
    private final FsSearchCache cache;

    SearchOp(final DirectoryINode root,
             final FsSearchCache cache,
             final int readLockWaitTimeMs,
             final int writeLockWaitTimeMs) {

        super(readLockWaitTimeMs, writeLockWaitTimeMs);
        this.root = root;
        this.cache = cache;
    }

    public FileRevisionINode findRev(FileINode file, long revNumber)
            throws PathOperationException {

        final Optional<FileRevisionINode> revOptional
            = tryReadLock(file, (PathSupplier<Optional<FileRevisionINode>>) () -> {
                throwIfDetached(file);
                return file.children()
                           .stream()
                           .filter(rev -> rev.revision() == revNumber)
                           .findFirst();
            });

        if (!revOptional.isPresent()) {
            throw new PathNotExistsException(file,  revNumber);
        }
        return revOptional.get();
    }

    public INodeSnapshot<? extends UserLevelINode> find(final Path inodePath)
        throws PathOperationException {

        final Optional<UserLevelINode> cachedEntry = cache.getByPath(inodePath);
        if (cachedEntry.isPresent()) {
            return asSnapshot(cachedEntry.get(), inodePath);
        }

        UserLevelINode foundInode
            = findParentDir(inodePath)
                   .getOrElseThrow(ioEx -> ioEx)
                   .map(parent -> findChildByName(parent, inodePath.getFileName().toString()))
                   .orElseThrow(() -> new PathNotExistsException(
                       "Parent not found: " + inodePath.getParent().toString()))
                   .getOrElseThrow(ioExc -> ioExc)
                   .filter(INodeTypes::isUserLevelInode)
                   .map(child -> (UserLevelINode)child)
                   .orElseThrow(() -> new PathNotExistsException(inodePath.toString()));

        return asSnapshot(foundInode, inodePath);
    }

    /**
     * Find inode for parent directory for passed file path.
     *
     * @param inodePath
     * @throws PathNotExistsException
     */
    public Either<PathOperationException, Optional<DirectoryINode>>
            findParentDir(final Path inodePath) {

        Path parent = inodePath.getParent();
        if (parent == null) { // if this is root dir => use it as parent
            parent = inodePath;
        }

        final Optional<DirectoryINode> cachedEntry
                = cache.getByPath(parent)
                       .filter(INodeTypes::isUsualDir)
                       .map(dirInode -> (DirectoryINode)dirInode);
        if (cachedEntry.isPresent()) {
            return Either.right(cachedEntry);
        }

        DirectoryINode parentNode = root;
        // start iterating path components beginning from root
        // but skip last path component(this is file name which doesn't exists)
        for ( int i = 0 ; i < parent.getNameCount(); i++ ) {
            final String pathName = parent.getName(i).toString();
            Either<PathOperationException, Optional<DirectoryINode>> child
                    = getDirINode(parentNode, pathName);
            if (child.isLeft()) {
                return child;
            }
            if (!child.get().isPresent()) {
                return Either.right(Optional.empty());
            }
            parentNode = child.get().get();
        }

        cache.put(parentNode);
        return Either.right(Optional.of(parentNode));
    }

    /**
     * Find directory inode in children of passed inode.
     *
     * @param searchRoot
     * @param pathName
     * @return
     */
    private Either<PathOperationException, Optional<DirectoryINode>>
        getDirINode(final ContainerINode searchRoot, final String pathName) {

        return findChildByName(searchRoot, pathName)
                   .map(inode -> inode.filter(INodeTypes::isUsualDir)
                                      .map(dirInode -> (DirectoryINode)dirInode));
    }

    public Either<PathOperationException, Optional<? extends INode>>
            findChildByName(final ContainerINode searchRoot, final String pathName) {

        final Path childFullPath;
        try {
            childFullPath = tryReadLock(searchRoot, (PathSupplier<Path>)() ->
                    searchRoot.path().resolve(pathName));
        }
        catch (PathOperationException e) {
            return Either.left(e);
        }

        final Optional<UserLevelINode> cachedEntry = cache.getByPath(childFullPath);
        if (cachedEntry.isPresent()) {
            return Either.right(cachedEntry);
        }

        return findChild(searchRoot, pathName, childFullPath);
    }

    private Either<PathOperationException, Optional<? extends INode>>
            findChild(final ContainerINode<? extends INode> searchRoot,
                      final String pathName,
                      final Path childFullPath) {

        return tryReadLock(searchRoot,
                (Supplier<Optional<? extends INode>>) () -> {
                    final Optional<? extends INode> foundInode
                            = searchRoot.findChildByName(pathName);
                    foundInode.filter(INodeTypes::isUserLevelInode)
                              .ifPresent(inode -> cache.put(childFullPath, (UserLevelINode) inode));
                    return foundInode;
                });
    }
}
