package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.filestorage.fsimage.FileRevisionManager;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public final class ReadableFileRevHandle {

    private final FileRevisionINode revInode;
    private final FileChannel channel;
    private final FileRevisionManager revManager;
    private final Executor ioExecutor;

    public ReadableFileRevHandle(final FileRevisionINode revInode,
                                 final FileChannel channel,
                                 final FileRevisionManager revManager,
                                 final Executor ioExecutor) {

        this.revInode = revInode;
        this.channel = channel;
        this.revManager = revManager;
        this.ioExecutor = ioExecutor;
    }

    /**
     * Read channel for file revision
     * @return
     */
    public FileChannel channel() {
        return channel;
    }

    /**
     * Revision inode
     * @return
     */
    public FileRevisionINode revInode() {
        return revInode;
    }

    /**
     * Asynchronously close handle: close file channel and release
     * file revision(decrement usage counter)
     * @throws Exception
     */
    public CompletableFuture<Void> closeAsync() {
        return CompletableFuture.runAsync(() -> {
            try {
                channel.close();
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, ioExecutor);
    }
}
