package com.iknowledge.hosted.filestorage.filesystem;

import com.google.common.collect.Lists;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.AccessDeniedException;
import com.iknowledge.hosted.filestorage.InvalidPathException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathLockedException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.filestorage.RevisionTranIdAlreadyExists;
import com.iknowledge.hosted.filestorage.RevisionTranIdNotFound;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionManager;
import com.iknowledge.hosted.filestorage.fsimage.FsConfig;
import com.iknowledge.hosted.filestorage.fsimage.FsStats;
import com.iknowledge.hosted.filestorage.fsimage.InMemoryFsImage;
import com.iknowledge.hosted.filestorage.fsimage.RevisionTransaction;
import com.iknowledge.hosted.filestorage.rawfs.UnixFs;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import io.prometheus.client.CollectorRegistry;
import io.vavr.Tuple2;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;
import static java.util.Objects.requireNonNull;

/**
 * Class contains method to access FS image and make basic operations on it.
 */
public final class FileSystem implements AutoCloseable {

    /**
     * Metric namespace
     */
    public static final String METRICS_NAMESPACE = "file_system";

    private final InMemoryFsImage fsImage;
    private final FileRevisionManager revManager;
    private final Executor ioExecutor;

    /**
     * Load file system image without initial format.
     * This constructor used when FS already initialized before
     * and can be used without additional actions.
     *
     * @param hbaseFsImage HBase FS image instance. Used to load FS image
     *                     into memory and perform basic operations such as saving/removing inodes.
     */
    public FileSystem(final HBaseFsImageStore hbaseFsImage,
                      final UnixFs unixFs,
                      final FsConfig config,
                      final CollectorRegistry metricsRegistry)
        throws StoreException, IOException {

        this(hbaseFsImage, unixFs, config, metricsRegistry, false);
    }

    /**
     * Load file system image with format operation support.
     *
     * @param hbaseFsImage HBase FS image instance. Used to load FS image
     *                     into memory and perform basic operations such as saving/removing inodes.
     * @param format Is FS should be formatted(create initial metadata, create FS root, etc)
     */
    public FileSystem( final HBaseFsImageStore hbaseFsImage,
                       final UnixFs unixFs,
                       final FsConfig config,
                       final CollectorRegistry metricsRegistry,
                       final boolean format ) throws StoreException, IOException {

        requireNonNull(hbaseFsImage);
        requireNonNull(unixFs);
        requireNonNull(config);

        if (format) {
            hbaseFsImage.format();
            unixFs.format();
        }
        fsImage = new InMemoryFsImage(hbaseFsImage, config, metricsRegistry);
        revManager = new FileRevisionManager(fsImage, config, unixFs, metricsRegistry);
        ioExecutor = Executors.newWorkStealingPool(config.getBackgroundTaskPoolSize());
    }

    /**
     * Load file system image with format operation support.
     * @param hbaseFsImage
     * @param unixFs
     * @param config
     * @param fsImage
     * @param revManager
     * @throws StoreException
     * @throws IOException
     */
    public FileSystem(final HBaseFsImageStore hbaseFsImage,
                      final UnixFs unixFs,
                      final FsConfig config,
                      final InMemoryFsImage fsImage,
                      final FileRevisionManager revManager) {

        requireNonNull(hbaseFsImage);
        requireNonNull(unixFs);
        requireNonNull(config);
        this.fsImage = fsImage;
        this.revManager = revManager;
        ioExecutor = Executors.newWorkStealingPool(config.getBackgroundTaskPoolSize());
    }

    /**
     * Method create new file on given path.
     *
     * @param filePath File path with name.
     *
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @throws PathNotExistsException Path not exists
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     */
    public void newFile(Path filePath, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(filePath);
        fsImage.createFile(normalizedPath, currentUserInfo);
    }

    /**
     * Method create new directory on given path.
     *
     * @param filePath New directory path.
     *
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @throws PathNotExistsException
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     */
    public void newDirectory(Path filePath, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(filePath);
        fsImage.createDirectory(normalizedPath, currentUserInfo);
    }

    /**
     * Remove path from FS.
     *
     * @param path
     *
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @throws PathLockedException Removal path locked by other operation
     * @throws PathNotExistsException Removal path not exist
     * @throws InvalidPathException Removal path not exist
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     */
    public void remove(Path path, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(path);
        fsImage.remove(normalizedPath, currentUserInfo);
    }

    /**
     * Remove file revision from FS.
     *
     * @param path File path
     * @param revNumber Revision number
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @throws PathLockedException Removal path locked by other operation
     * @throws PathNotExistsException Removal path not exist
     * @throws InvalidPathException Removal path not exist
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     */
    public void remove(Path path, long revNumber, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(path);
        final FileRevisionINode revInode = fsImage.remove(normalizedPath,
                                                          revNumber,
                                                          currentUserInfo);
        revManager.collect(revInode.sysPath());
    }

    /**
     * Method move file/directory to given destination.
     *
     * @param srcPath Path to file or directory which should be moved.
     * @param destPath Destination path. Last component of this path denotes new name
     *                 of source file or directory in destination path and must not exist.
     * @param currentUserInfo Current user that is performing the operation.
     *
     * @throws PathNotExistsException Source path not exists
     * @throws PathExistsException Destination path already exists
     * @throws PathLockedException Destination path locked by other operation
     * @throws InvalidPathException
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     * @throws IOException
     */
    public void move(Path srcPath, Path destPath, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path srcNormalizedPath = checkAndNormalizePath(srcPath);
        Path destNormalizedPath = checkAndNormalizePath(destPath);
        fsImage.move(srcNormalizedPath,
                     destNormalizedPath,
                     currentUserInfo);
    }

    /**
     * Find FS object by path
     *
     * @param path FS path
     *
     * @param currentUserInfo User which request search operation
     *
     * @return
     *
     * @throws PathNotExistsException Path not exists
     * @throws PathLockedException Path locked by other user
     * @throws AccessDeniedException Current user can not perform this operation
     * @throws StoreException
     */
    public FsHandle find(Path path, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        final INode inode = findInode(path, currentUserInfo);
        return FsHandle.from(inode);
    }

    /**
     * Create new revision for file.
     *
     * @param path File path
     * @param currentUserInfo User which request revision operation
     *
     * @return Revision token. Used to access revision handle later.
     *
     * @throws PathOperationException
     * @throws RevisionTranIdAlreadyExists Internal transitive error, simply retry in this case.
     */
    public String newRevision(Path path, UserInfoLite currentUserInfo)
            throws PathOperationException, RevisionTranIdAlreadyExists, StoreException {

        final INode file = findInode(path, currentUserInfo);
        if (!isUsualFile(file)) {
            throw new InvalidPathException("Path " + path.toString() + " not a file inode");
        }
        return revManager.openNewTran(asUsualFile(file));
    }

    /**
     * Open file revision handle by transaction ID for append write operation
     * and consequently committing current revision as new valid revision.
     *
     * @param tranId Revision transaction ID
     *
     * @return Writable revision handle
     *
     * @throws IOException
     * @throws RevisionTranIdNotFound
     */
    public NewFileRevHandle openRevision(String tranId) throws IOException, RevisionTranIdNotFound {
        final Tuple2<RevisionTransaction, FileChannel> revData = revManager.getNewTran(tranId);
        return new NewFileRevHandle(revData._1,
                                    revData._2,
                                    this.ioExecutor,
                                    revManager);
    }

    /**
     * Open read revision for file.
     *
     * @param path File path
     * @param currentUserInfo User which request revision operation
     *
     * @return Revision token. Used to access revision handle later.
     *
     * @throws PathOperationException
     * @throws RevisionTranIdAlreadyExists Internal transitive error, simply retry in this case.
     */
    public String requestRevisionRead(Path path, long revNumber, UserInfoLite currentUserInfo)
            throws PathOperationException, RevisionTranIdAlreadyExists, StoreException {

        final FileRevisionINode rev = fsImage.findRev(path, revNumber, currentUserInfo);
        return revManager.openReadTran(rev);
    }

    /**
     * Open file revision handle by transaction ID for read operation.
     *
     * @param tranId Read transaction ID
     *
     * @return Readable revision handle
     *
     * @throws IOException
     * @throws RevisionTranIdNotFound
     */
    public ReadableFileRevHandle openReadRevision(String tranId)
            throws IOException, RevisionTranIdNotFound {

        final Tuple2<FileRevisionINode, FileChannel> revData = revManager.getReadTran(tranId);
        return new ReadableFileRevHandle(revData._1, revData._2, revManager, ioExecutor);
    }

    /**
     * Attach user rights to the inode
     * @param path FS path
     * @param currentUserInfo Current user that is performing the operation.
     */
    public void attachAccessRights(final Path path, final UserInfoLite currentUserInfo,
                                   final HBaseINodeRights rights)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(path);
        fsImage.attachAccessRights(normalizedPath, currentUserInfo, Lists.newArrayList(rights));
    }

    /**
     * Get file system's statistics information
     * @return
     */
    public FsStats stats() {
        return fsImage.stats();
    }

    /**
     * Close all closable internal resources
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        if (revManager != null) {
            revManager.close();
        }
    }

    private INode findInode(Path path, UserInfoLite currentUserInfo)
            throws PathOperationException, StoreException {

        Path normalizedPath = checkAndNormalizePath(path);
        return fsImage.find(normalizedPath, currentUserInfo);
    }

    private Path checkAndNormalizePath(final Path path) throws PathOperationException {
        if (path == null) {
            throw new InvalidPathException("Path must be present.");
        }
        if (!path.isAbsolute()) {
            throw new InvalidPathException("Path must be absolute: " + path);
        }
        return path.normalize();
    }
}
