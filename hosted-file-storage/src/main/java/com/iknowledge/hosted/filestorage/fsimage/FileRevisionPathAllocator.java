package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hosted.filestorage.PathAllocationException;
import com.iknowledge.hosted.filestorage.rawfs.UnixFs;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.text.MessageFormat.format;

/**
 * Path allocator for the new revision files
 */
public final class FileRevisionPathAllocator {
    private final ReadWriteLock lock;
    private final long maxFileCount;
    private final String fileRevBaseDir;
    private final UnixFs unixFs;
    private final FileRevisionGc fileRevisionGc;

    private long filesCountInDir;
    private long currentDirNumber;
    private Path currentDirPath;

    /**
     *
     * @param maxFileCount
     * @param fileRevBaseDir
     * @param unixFs
     * @throws IOException
     */
    public FileRevisionPathAllocator(final long maxFileCount,
                                     final String fileRevBaseDir,
                                     final UnixFs unixFs,
                                     final FileRevisionGc fileRevisionGc)
            throws PathAllocationException {
        this.fileRevBaseDir = fileRevBaseDir;
        this.unixFs = unixFs;
        this.fileRevisionGc = fileRevisionGc;
        this.lock = new ReentrantReadWriteLock();
        this.maxFileCount = maxFileCount;
        this.currentDirNumber = 0;

         try {
             final Path fileRefBaseDirPath = Paths.get(fileRevBaseDir);
             if (!unixFs.exists(fileRefBaseDirPath)) {
                 unixFs.createDirectory(fileRefBaseDirPath);
             }
             List<Path> filesInDir = unixFs.getFilesInDir(fileRefBaseDirPath);
             if (filesInDir.isEmpty()) {
                 currentDirPath = createNewDirectory();
             } else {
                 currentDirPath = filesInDir.stream()
                         // sort in reversed order
                         .sorted(Comparator.comparing(Path::getFileName).reversed())
                         .findFirst()
                         .get();
                 currentDirNumber = getDirNumberFromPath(currentDirPath);
             }

             this.filesCountInDir = unixFs.filesInDir(currentDirPath);
         }
         catch (IOException e) {
             throw new PathAllocationException("Error has occurred during "
                     + "FileRevisionPathAllocator initialization. "
                     + "FileRevBaseDir: " + fileRevBaseDir.toString(), e);
         }
     }

    /**
     * Allocate the new path for file revision
     * @throws IOException
     */
    public Path allocate(byte[] newInodeId) throws PathAllocationException {
        if (newInodeId.length == 0) {
            throw new PathAllocationException("INode id can not be empty");
        }
        String inodeId = getFileNameByINodeId(newInodeId);
        try {
            lock.writeLock().lock();
            final Path releasedPath = getReleasedPath();
            // return released path by GC if exists
            if (releasedPath != null) {
                return releasedPath.resolve(inodeId);
            }

            // if there is still enough place in the current directory
            if (filesCountInDir < maxFileCount) {
                filesCountInDir++;
                try {
                    byte[] tmp = getINodeIdFromPath(currentDirPath.resolve(inodeId));
                    boolean equal = MessageDigest.isEqual(newInodeId, tmp);
                    int num = 0;
                }
                catch (DecoderException e) {
                    e.printStackTrace();
                }

                return currentDirPath.resolve(inodeId);
            }

            try {
                currentDirPath = createNewDirectory();
            }
            catch (IOException e) {
                throw new PathAllocationException("Error has occurred during creating "
                        + "directory for file revisions. "
                        + "CurrentDirPath: " + currentDirPath.toString(), e);
            }
            filesCountInDir = 1;
            return currentDirPath.resolve(inodeId);

        }
        finally {
            lock.writeLock().unlock();
        }
    }

    public static String getFileNameByINodeId(byte[] inodeId) {
        return Hex.encodeHexString(inodeId) + "_" + generateFsEntityName();
    }

    public static byte[] getINodeIdFromPath(Path path) throws DecoderException {
        String fileName = path.getFileName().toString();
        String[] nameParts = fileName.split("_");
        return Hex.decodeHex(nameParts[0].toCharArray());
    }

    private static String generateFsEntityName() {
        return UUID.randomUUID().toString();
    }

    private Path createNewDirectory() throws IOException {
        currentDirNumber++;
        return createNewDirectory(currentDirNumber);
    }

    private Path createNewDirectory(final long directoryNumber) throws IOException {
        String directoryName = format("{0}_{1}", directoryNumber, generateFsEntityName());
        Path path = Paths.get(fileRevBaseDir, directoryName);
        try {
            return unixFs.createDirectory(path);
        }
        catch (IOException e) {
            throw new PathAllocationException("Error has occurred during creating "
                    + "directory for file revisions. "
                    + "Path: " + path.toString(), e);
        }
    }

    private long getDirNumberFromPath(Path path) {
        String dirName = path.getFileName().toString();
        String[] nameParts = dirName.split("_");
        return Long.parseLong(nameParts[0]);
    }

    private Path getReleasedPath() {
        final Optional<Path> firstReleasedPath = fileRevisionGc.getFirstReleasedPath();
        if (firstReleasedPath.isPresent()) {
            return firstReleasedPath.get().getParent();
        }
        return null;
    }
}
