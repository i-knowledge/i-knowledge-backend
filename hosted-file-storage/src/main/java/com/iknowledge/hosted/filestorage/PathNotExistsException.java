package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

/**
 * Requested path not exists
 */
public class PathNotExistsException extends PathOperationException {

    public PathNotExistsException() {
        super();
    }

    public PathNotExistsException(final String message) {
        super(message);
    }

    public PathNotExistsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PathNotExistsException(final Throwable cause) {
        super(cause);
    }

    public PathNotExistsException(final INode inode) {
        super(inode.toString());
    }

    public PathNotExistsException(final FileINode fileInode, long revision) {
        super("Revision '" + revision + "' not found for file: " + fileInode.toString());
    }
}
