package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.NewFileRevHandle;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class HttpFileUploadHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpFileUploadHandler.class);

    private final HttpVersion httpVersion;
    private final NewFileRevHandle revHandle;

    public HttpFileUploadHandler(final NewFileRevHandle revHandle, final HttpVersion httpVersion) {
        this.revHandle = revHandle;
        this.httpVersion = httpVersion;
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {
        final HttpContent httpContent = (HttpContent) msg;
        try {
            writeFileChunk(httpContent.content());

            if (httpContent instanceof LastHttpContent) {
                revHandle.commit()
                         .whenComplete((tranId, throwable) -> {
                             if (throwable != null) {
                                 handleException(ctx, throwable);
                             }
                             else {
                                 writeFullResponse(ctx, HttpResponseStatus.OK);
                             }
                         });
            }
        }
        finally {
            httpContent.release();
        }

    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx) {
        closeResources(ctx);
    }

    @Override
    public boolean isSharable() {
        return false;
    }

    private void writeFileChunk(ByteBuf byteBuf) throws IOException {
        if (byteBuf.nioBufferCount() == -1) {
            return;
        }

        //TODO: metrics
        final WritableByteChannel fileChannel = revHandle.channel();
        // suppose that versioned file doesn't require write lock
        // because it not visible to other users
        if (byteBuf.nioBufferCount() == 1) {
            fileChannel.write(byteBuf.nioBuffer());
        }
        else {
            for (ByteBuffer buffer : byteBuf.nioBuffers()) {
                fileChannel.write(buffer);
            }
        }
    }

    private void handleException(final ChannelHandlerContext ctx, final Throwable cause) {
        writeFullResponse(ctx, HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }

    private void writeFullResponse(final ChannelHandlerContext ctx,
                                   final HttpResponseStatus status) {

        final DefaultFullHttpResponse httpResponse
            = new DefaultFullHttpResponse(httpVersion, status);
        httpResponse.headers().add(HttpHeaderNames.CONTENT_LENGTH, 0);

        ctx.writeAndFlush(httpResponse)
           .addListener(future -> {
               closeResources(ctx);
               if (future.isDone() && !future.isSuccess()) {
                   //TODO: metrics
                   LOGGER.trace("Fail to write HTTP response status {} to channel{}{}",
                                status, System.lineSeparator(), future.cause());
               }
           });
    }

    private void closeResources(ChannelHandlerContext ctx) {
        revHandle.closeAsync()
                 .whenComplete((tranId, ex) -> {
                     if (ex != null) {
                         //TODO: metrics
                         LOGGER.warn("Error during closing of file revision "
                                     + "transaction: {}{}",
                                     System.lineSeparator(), ex);
                     }
                 });
    }
}
