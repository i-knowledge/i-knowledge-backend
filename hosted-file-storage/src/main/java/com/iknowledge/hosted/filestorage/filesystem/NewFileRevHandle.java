package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionManager;
import com.iknowledge.hosted.filestorage.fsimage.RevisionTransaction;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ProposedFileRevisionINode;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * New file revision handle.
 * Revision created(more precisely, proposed) in in-memory FS image and waiting when file content
 * will be written. After that new revision may be committed as new valid file revision.
 */
public final class NewFileRevHandle {

    private final RevisionTransaction transaction;
    private final FileChannel channel;
    private final Executor ioExecutor;
    private final FileRevisionManager revManager;
    private volatile boolean committed;

    NewFileRevHandle(final RevisionTransaction transaction,
                     final FileChannel channel,
                     final Executor ioExecutor,
                     final FileRevisionManager revManager) {

        this.transaction = transaction;
        this.channel = channel;
        this.ioExecutor = ioExecutor;
        this.revManager = revManager;
    }

    public WritableByteChannel channel() {
        return channel;
    }

    public ProposedFileRevisionINode revisionInode() {
        return transaction.revisionInode();
    }

    /**
     * Fsync file content and commit new revision as new valid version of file.
     *
     * @throws IOException
     * @throws StoreException
     */
    public CompletableFuture<String> commit() {
        return CompletableFuture.supplyAsync(this::commitInternal, ioExecutor);
    }

    /**
     * Asynchronously close file revision handle. Rollback proposed revision if not committed.
     *
     * @return Completable future with transaction ID as result
     *
     * @throws Exception
     */
    public CompletableFuture<String> closeAsync() {
        return committed
                ? CompletableFuture.completedFuture(transaction.id())
                : CompletableFuture.supplyAsync(this::rollbackTran, ioExecutor);
    }

    private String commitInternal() {
        try {
            channel.force(true);
            channel.close();
            transaction.commit();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        committed = true;
        return transaction.id();
    }

    private String rollbackTran() {
        try {
            channel.close();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            revManager.collect(transaction.revisionInode().sysPath());
        }
        return transaction.id();
    }
}
