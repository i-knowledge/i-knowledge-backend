package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;

import java.nio.file.Path;

public final class FileHandle implements FsHandle {

    private final String name;
    private final Path path;
    private final long[] revisions;
    private final UserRights userRights;

    FileHandle(FileINode fileInode) {
        fileInode.lock().readLock().lock();
        try {
            name = fileInode.name();
            path = fileInode.path();
            userRights = fileInode.userRights()
                    .copy();
            revisions = fileInode.children()
                                 .stream()
                                 .mapToLong(FileRevisionINode::revision)
                                 .sorted()
                                 .toArray();
        }
        finally {
            fileInode.lock().readLock().unlock();
        }
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Path path() {
        return path;
    }

    @Override
    public UserRights userRights() {
        return userRights;
    }

    public long[] revisions() {
        return revisions;
    }
}
