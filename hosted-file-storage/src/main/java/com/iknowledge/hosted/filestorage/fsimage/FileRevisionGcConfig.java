package com.iknowledge.hosted.filestorage.fsimage;

import java.util.concurrent.TimeUnit;

public final class FileRevisionGcConfig {
    private final String rootPath;
    private final int startAtHour;
    private final int runEveryTimeUnit;
    private final TimeUnit timeUnit;
    private final int reservedTenthOfFreeMemory;

    private FileRevisionGcConfig(Builder builder) {
        rootPath = builder.rootPath;
        startAtHour = builder.startAtHour;
        runEveryTimeUnit = builder.runEveryTimeUnit;
        timeUnit = builder.timeUnit;
        reservedTenthOfFreeMemory = builder.reservedTenthOfFreeMemory;
    }

    public static Builder newBuilder(FileRevisionGcConfig copy) {
        Builder builder = new Builder();
        builder.reservedTenthOfFreeMemory = copy.reservedTenthOfFreeMemory;
        builder.timeUnit = copy.timeUnit;
        builder.runEveryTimeUnit = copy.runEveryTimeUnit;
        builder.startAtHour = copy.startAtHour;
        builder.rootPath = copy.rootPath;
        return builder;
    }

    public String getRootPath() {
        return rootPath;
    }

    public int getStartAtHour() {
        return startAtHour;
    }

    public int getRunEveryTimeUnit() {
        return runEveryTimeUnit;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public int getReservedTenthOfFreeMemory() {
        return reservedTenthOfFreeMemory;
    }

    public static IRootPath builder() {
        return new Builder();
    }

    public interface IBuild {
        FileRevisionGcConfig build();
    }

    public interface IReservedTenthOfFreeMemory {
        IBuild withReservedTenthOfFreeMemory(int val);
    }

    public interface ITimeUnit {
        IReservedTenthOfFreeMemory withTimeUnit(TimeUnit val);
    }

    public interface IRunEveryTimeUnit {
        ITimeUnit withRunEveryTimeUnit(int val);
    }

    public interface IStartAtHour {
        IRunEveryTimeUnit withStartAtHour(int val);
    }

    public interface IRootPath {
        IStartAtHour withRootPath(String val);
    }

    /**
     * {@code FileRevisionGcConfig} builder static inner class.
     */
    public static final class Builder implements IReservedTenthOfFreeMemory, ITimeUnit,
            IRunEveryTimeUnit, IStartAtHour, IRootPath, IBuild {
        private int reservedTenthOfFreeMemory;
        private TimeUnit timeUnit;
        private int runEveryTimeUnit;
        private int startAtHour;
        private String rootPath;

        private Builder() {
        }

        /**
         * Sets the {@code reservedTenthOfFreeMemory} and returns a reference to {@code IBuild}
         *
         * @param val the {@code reservedTenthOfFreeMemory} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IBuild withReservedTenthOfFreeMemory(int val) {
            reservedTenthOfFreeMemory = val;
            return this;
        }

        /**
         * Sets the {@code timeUnit} and returns a reference to {@code IReservedTenthOfFreeMemory}
         *
         * @param val the {@code timeUnit} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IReservedTenthOfFreeMemory withTimeUnit(TimeUnit val) {
            timeUnit = val;
            return this;
        }

        /**
         * Sets the {@code runEveryTimeUnit} and returns a reference to {@code ITimeUnit}
         *
         * @param val the {@code runEveryTimeUnit} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public ITimeUnit withRunEveryTimeUnit(int val) {
            runEveryTimeUnit = val;
            return this;
        }

        /**
         * Sets the {@code startAtHour} and returns a reference to {@code IRunEveryTimeUnit}
         *
         * @param val the {@code startAtHour} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IRunEveryTimeUnit withStartAtHour(int val) {
            startAtHour = val;
            return this;
        }

        /**
         * Sets the {@code rootPath} and returns a reference to {@code IStartAtHour}
         *
         * @param val the {@code rootPath} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IStartAtHour withRootPath(String val) {
            rootPath = val;
            return this;
        }

        /**
         * Returns a {@code FileRevisionGcConfig} built from the parameters previously set.
         *
         * @return a {@code FileRevisionGcConfig} built with parameters
         * of this {@code FileRevisionGcConfig.Builder}
         */
        public FileRevisionGcConfig build() {
            return new FileRevisionGcConfig(this);
        }
    }
}
