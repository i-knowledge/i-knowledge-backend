package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.AccessDeniedException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.AccessRestrictedINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;

import static java.text.MessageFormat.format;

/**
 * Object for checking users' rights in specified inode
 */
public final class CheckRightsOp extends BaseOp {

    public CheckRightsOp(int readLockTimeoutMs,
                         int writeLockWaitTimeMs) {

        super(readLockTimeoutMs, writeLockWaitTimeMs);
    }

    /**
     * Can user create inode in the specified parent inode
     * @param inode
     * @param userInfo
     * @throws AccessDeniedException
     */
    public void throwIfCantWriteToINode(final AccessRestrictedINode inode,
                                        final UserInfoLite userInfo)
            throws PathOperationException, StoreException {

        tryReadLock(inode, (PathRunnable) () -> {
            final boolean canWrite = canWrite(inode, userInfo);

            if (!canWrite) {
                throw new AccessDeniedException(
                        format("Specified user with id [{0}] "
                                        + "have no rights for creating in inode [{1}]",
                                userInfo.getUserId(), inode.path().toString()));
            }
        });
    }

    /**
     * Can user read content of the specified inode
     * @param inode
     * @param userInfo
     * @throws AccessDeniedException
     */
    public void throwIfCantReadINode(final AccessRestrictedINode inode, final UserInfoLite userInfo)
            throws PathOperationException, StoreException {

        tryReadLock(inode, (PathRunnable) () -> {
            final boolean canRead = canRead(inode, userInfo);

            if (!canRead) {
                throw new AccessDeniedException(
                        format("Specified user with id [{0}] "
                                        + "have no rights for reading inode content [{1}]",
                                userInfo.getUserId(), inode.path().toString()));
            }
        });
    }

    private boolean canUploadIn(final AccessRestrictedINode inode, final UserInfoLite userInfo)
            throws PathOperationException {
        return canCallOpOnINode(inode, userInfo,
                false, false, true);
    }

    private boolean canWrite(final AccessRestrictedINode inode, final UserInfoLite userInfo)
            throws PathOperationException {
        return canCallOpOnINode(inode, userInfo,
                false, true, false);
    }

    private boolean canRead(final AccessRestrictedINode inode, final UserInfoLite userInfo)
            throws PathOperationException {
        return canCallOpOnINode(inode, userInfo,
                true, false, false);
    }

    private boolean canCallOpOnINode(final AccessRestrictedINode inode,
                                     final UserInfoLite userInfo,
                                     final boolean checkCanRead,
                                     final boolean checkCanWrite,
                                     final boolean checkCanUpload)
            throws PathOperationException {

        CanCallOpResult canCallOpResult = tryReadLock(inode, (PathSupplier<CanCallOpResult>) () -> {
            CanCallOpResult canCallOp = canCallOp(inode, userInfo, checkCanRead,
                    checkCanWrite, checkCanUpload);
            canCallOp.parent = inode.parent();
            return canCallOp;
        });
        while (!canCallOpResult.rightIsFound) {
            if (canCallOpResult.parent == null
                    || !(canCallOpResult.parent instanceof AccessRestrictedINode)) {
                break;
            }
            final INode parent = canCallOpResult.parent;
            canCallOpResult = tryReadLock(inode, (PathSupplier<CanCallOpResult>) () -> {
                CanCallOpResult canCallOp = canCallOp((AccessRestrictedINode)parent,
                        userInfo,
                        checkCanRead,
                        checkCanWrite,
                        checkCanUpload);
                canCallOp.parent = parent.parent();
                return canCallOp;
            });
        }

        return canCallOpResult.canCallOp;
    }

    private CanCallOpResult canCallOp(final AccessRestrictedINode inode,
                                      final UserInfoLite userInfo,
                                      final boolean checkCanRead,
                                      final boolean checkCanWrite,
                                      final boolean checkCanUpload) {

        boolean canCallOp = false;
        CanCallOpResult canCallOpResult = new CanCallOpResult();
        canCallOpResult.rightIsFound = false;
        canCallOpResult.canCallOp = false;
        UserRights rights = inode.userRights();
        if (rights.containsUser(userInfo.getUserId())) {
            final HBaseINodeRights userRights = rights.getUserRightsByUserId(userInfo.getUserId());
            canCallOp = canCallOp(userRights, checkCanRead, checkCanWrite, checkCanUpload);
            if (canCallOp) {
                canCallOpResult.rightIsFound = true;
                canCallOpResult.canCallOp = true;
                return canCallOpResult;
            }
        }

        for (Id groupId : userInfo.getGroupsId()) {
            if (rights.containsUserGroup(groupId)) {
                final HBaseINodeRights userGroupRights =
                        rights.getUserGroupRightsByUserGroupId(groupId);
                canCallOp = canCallOp(userGroupRights, checkCanRead, checkCanWrite, checkCanUpload);
            }

            if (canCallOp) {
                canCallOpResult.rightIsFound = true;
                canCallOpResult.canCallOp = true;
                return canCallOpResult;
            }
        }
        return canCallOpResult;
    }

    private boolean canCallOp(final HBaseINodeRights rights, final boolean checkCanRead,
                              final boolean checkCanWrite, final boolean checkCanUpload) {
        if (checkCanRead) {
            return rights.getEntityRights().isCanRead();
        }

        if (checkCanWrite) {
            return rights.getEntityRights().isCanWrite();
        }

        if (checkCanUpload) {
            return rights.getEntityRights().isCanUpload();
        }

        return false;
    }

    class CanCallOpResult {
        public boolean rightIsFound;
        public boolean canCallOp;
        public INode parent;
    }
}
