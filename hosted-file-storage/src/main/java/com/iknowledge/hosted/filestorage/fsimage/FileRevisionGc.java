package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.filestorage.PathAllocationException;
import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import com.iknowledge.hosted.filestorage.rawfs.UnixFs;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import io.prometheus.client.Gauge;
import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.iknowledge.hosted.filestorage.fsimage.FileRevisionPathAllocator.getINodeIdFromPath;
import static java.text.MessageFormat.format;

/**
 * Garbage collector for file revision paths
 */
public final class FileRevisionGc implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileRevisionGc.class);

    private final UnixFs unixFs;
    private final FsMap fsMap;
    private final Path rootPath;
    private final ConcurrentMap<Path, AtomicInteger> inUseRevisions;
    private final ConcurrentMap<Path, Boolean> releasedPaths;
    private final ConcurrentMap<Path, Boolean> pathsToRelease;
    private final ExecutorService executorService;
    private final ScheduledExecutorService scheduledExecutorService;
    private final AtomicBoolean collectInProgress;
    private final Gauge inUseRevsGauge;
    private final int reservedTenthOfFreeMemory;

    private int dirToSkip;

    /**
     *
     * @param unixFs
     * @param gcConfig
     */
    public FileRevisionGc(final UnixFs unixFs,
                          final FsMap fsMap,
                          final FileRevisionGcConfig gcConfig)
            throws PathAllocationException {
        final Path fileRefBaseDirPath = Paths.get(gcConfig.getRootPath());
        if (!unixFs.exists(fileRefBaseDirPath)) {
            try {
                unixFs.createDirectory(fileRefBaseDirPath);
            }
            catch (IOException e) {
                throw new PathAllocationException("Error has occurred during "
                        + "FileRevisionGc initialization. "
                        + "RootPath: " + gcConfig.getRootPath(), e);
            }
        }
        this.unixFs = unixFs;
        this.fsMap = fsMap;
        this.rootPath = fileRefBaseDirPath;
        this.releasedPaths = new ConcurrentHashMap<>();
        this.pathsToRelease = new ConcurrentHashMap<>();
        this.dirToSkip = 0;
        this.reservedTenthOfFreeMemory = gcConfig.getReservedTenthOfFreeMemory();
        this.inUseRevsGauge = Gauge.build()
                .namespace(FileSystem.METRICS_NAMESPACE)
                .subsystem("rev_manager_gc")
                .name("in_use_revs_count")
                .help("Count of revisions which currently uploading")
                .create();
        this.inUseRevisions = new ConcurrentHashMap<>();
        this.collectInProgress = new AtomicBoolean(false);

        this.executorService = Executors.newFixedThreadPool(2);
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        final LocalTime localTime = LocalTime.of(gcConfig.getStartAtHour(), 0);
        long initialDelay = 0;
        // if specified starting hour is more than the current then setup the initial delay
        // or else run gc immediately
        if ( localTime.getHour() > LocalTime.now().getHour() ) {
            final LocalTime startAfterHour = localTime.minusHours(LocalTime.now().getHour());
            initialDelay = TimeUnit.SECONDS.convert(startAfterHour.getHour(), TimeUnit.HOURS);
        }
        final long periodSeconds = gcConfig.getTimeUnit().toSeconds(gcConfig.getRunEveryTimeUnit());
        this.scheduledExecutorService.scheduleAtFixedRate(
                () -> collect(),
                initialDelay,
                periodSeconds,
                TimeUnit.SECONDS);
    }

    @Override
    public void close() throws Exception {
        if (executorService != null) {
            executorService.shutdown();
        }
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdown();
        }
    }

    /**
     * Open file channel
     * @param path
     * @param options
     * @throws IOException
     */
    public FileChannel openFile(Path path, OpenOption... options) throws IOException {
        final FileChannel fileChannel = unixFs.openFile(path, options);
        return new FileRevisionChannel(this, fileChannel, path);
    }

    /**
     * Manually acquire specific path.
     * For the automatic acquiring and releasing use {@link #openFile(Path, OpenOption...)}
     * @param path
     */
    public void acquire(Path path) {
        AtomicInteger atomicInteger = new AtomicInteger();
        final AtomicInteger atomicValue = inUseRevisions.putIfAbsent(path, atomicInteger);
        if (atomicValue != null) {
            atomicInteger = atomicValue;
        }
        atomicInteger.incrementAndGet();
        inUseRevsGauge.inc();
    }

    /**
     * Manually release specific path.
     * For the automatic acquiring and releasing use {@link #openFile(Path, OpenOption...)}
     * @param path
     */
    public void release(Path path) {
        AtomicInteger atomicInteger = inUseRevisions.get(path);
        if (atomicInteger == null) {
            return;
        }
        int acquireCount = atomicInteger.decrementAndGet();
        if (acquireCount <= 0) {
            inUseRevisions.remove(path);
        }
        inUseRevsGauge.dec();
    }

    /**
     * Get paths that had been released
     *
     * @return
     */
    public Optional<Path> getFirstReleasedPath() {
        final Optional<Path> path = releasedPaths.entrySet()
                .stream()
                .findFirst()
                .map(s -> s.getKey());
        if (path.isPresent()) {
            releasedPaths.remove(path.get());
        }
        return path;
    }

    /**
     * Get currently in use paths
     * @return
     */
    public List<Path> getInUsePaths() {
        return inUseRevisions.keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Get currently released paths
     * @return
     */
    public List<Path> getReleasedPaths() {
        return releasedPaths.keySet()
                .stream()
                .collect(Collectors.toList());
    }

    /**
     * Release paths throughout the all file system
     */
    public void collectAll() {
        executorService.execute(() -> collect());
    }

    /**
     * Collect the specified path of revision
     *
     * @param path
     */
    public void collect(Path path) {
        pathsToRelease.putIfAbsent(path, false);

        // if there is enough paths to be released then start releasing process
        if (pathsToRelease.size() >= 20) {
            executorService.execute(() -> releasePaths());
        }
    }

    private void collect() {
        LOGGER.debug("Starting file revisions collection. DirToSkip: [{}]", dirToSkip);
        try {
            if (!collectInProgress.compareAndSet(false, true)) {
                return;
            }
            final Optional<Path> pathOptional = unixFs.getFirstPathInDirAsc(rootPath, dirToSkip);
            if (!pathOptional.isPresent()) {
                dirToSkip = 0;
                return;
            }

            findAndReleasePathsIfCan(pathOptional.get());
            dirToSkip++;

            releasePaths();
        }
        catch (Exception e) {
            LOGGER.error(format("Error has occurred during getting dir path. "
                            + "Path: [{0}], dirToSkip: [{1}]",
                    rootPath, dirToSkip), e);
        }
        finally {
            collectInProgress.set(false);
        }

        LOGGER.debug("Finishing file revisions collection. DirToSkip: [{}]", dirToSkip);
    }

    private void findAndReleasePathsIfCan(Path path) {
        final long totalFilesCountInDir;
        try {
            totalFilesCountInDir = unixFs.filesInDir(path);
            // if dir is empty stop processing
            if (totalFilesCountInDir == 0) {
                return;
            }
        }
        catch (IOException e) {
            LOGGER.error(format("Error has occurred during "
                    + "getting files count in dir. "
                    + "Path: [{0}]", path), e);
            return;
        }
        int filesPartToLoad = 1;
        long filesCountInDir = totalFilesCountInDir;

        // if can not load the entire dir into the memory then try to divide it into the parts
        while (!canLoadPaths(filesCountInDir)) {
            filesPartToLoad++;

            if (filesPartToLoad > 100) {

                LOGGER.warn(format("Can not load so many files into the memory. "
                        + "Path: [{0}], filesPartToLoad: [{1}], totalFilesCountInDir: [{2}]",
                        path, filesPartToLoad, totalFilesCountInDir));
                return;
            }

            filesCountInDir = totalFilesCountInDir / filesPartToLoad;
        }

        long filesToSkip = 0;
        try {
            // if to load partially or else load full directory
            if (filesPartToLoad > 1) {
                List<Path> filesInDir = unixFs.getFilesInDir(path, 0, filesCountInDir);
                int currentLoopIndex = 0;
                while ( !filesInDir.isEmpty() ) {
                    removeRevisionsIfCan(filesInDir);
                    currentLoopIndex++;
                    filesToSkip = filesCountInDir * currentLoopIndex;
                    filesInDir = unixFs.getFilesInDir(path, filesToSkip, filesCountInDir);
                }
            } else {
                List<Path> filesInDir = unixFs.getFilesInDir(path);
                removeRevisionsIfCan(filesInDir);
            }
        }
        catch (IOException e) {
            LOGGER.error(format("Error has occurred during getting files in dir. "
                            + "Path: [{0}]", path), e);
        }
    }

    private boolean canLoadPaths(long filesInDir) {
        // measured by org.openjdk.jol.info.ClassLayout
        final long onePathSize = 16;
        final long totalPathsSize = filesInDir * onePathSize + 24; // 24 for Set<Path>
        Runtime runtime = Runtime.getRuntime();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long freeMemory = runtime.freeMemory();
        final long freeMemoryPart = freeMemory / 10;
        final long freeMemAfterAllocation = freeMemory - totalPathsSize;
        LOGGER.info("Runtime information. "
                        + "MaxMemory: [{}], AllocatedMemory: [{}], "
                        + "FreeMemory: [{}], NeedToAllocate: [{}],"
                        + "FreeMemoryPart: [{}], FreeMemAfterAllocation: [{}]",
                maxMemory, totalMemory, freeMemory, totalPathsSize,
                freeMemoryPart, freeMemAfterAllocation);

        if (totalPathsSize > freeMemory) {
            return false;
        }

        if (freeMemAfterAllocation < freeMemoryPart * reservedTenthOfFreeMemory) {
            return false;
        }
        return true;
    }

    private void releasePaths() {
        LOGGER.debug("Starting collection paths to release.");
        final List<Path> paths = getPathsToRelease();
        removeRevisionsIfCan(paths);
        for (Path path : paths) {
            pathsToRelease.remove(path);
        }
        LOGGER.debug("Finishing collection paths to release.");
    }

    private List<Path> getPathsToRelease() {
        return pathsToRelease.entrySet()
                .stream()
                .map(s -> s.getKey())
                .collect(Collectors.toList());
    }

    private void removeRevisionsIfCan(List<Path> paths) {
        for (Path file : paths) {
            if (inUseRevisions.containsKey(file)) {
                continue;
            }
            Id inodeId;
            try {
                inodeId = Id.of(getINodeIdFromPath(file));
            }
            catch (DecoderException e) {
                LOGGER.error(format("Error has occurred during getting inode id from path. "
                                + "Path: [{0}]",
                        file), e);
                continue;
            }
            if (!fsMap.get(inodeId).isPresent()) {
                removeRevision(file);
            }
        }
    }

    private void removeRevision(Path path) {
        try {
            LOGGER.debug("Removing file revision. Path: [{}].", path);
            final boolean isRemoved = unixFs.remove(path);
            if ( !isRemoved ) {
                LOGGER.error("Error has occurred during file revision deletion. Path: [{}]", path);
                return;
            }
            releasedPaths.putIfAbsent(path, false);
        }
        catch (IOException e) {
            LOGGER.error(format("Error has occurred during file revision deletion. "
                            + "Path: [{0}]",
                                path), e);
        }
    }
}
