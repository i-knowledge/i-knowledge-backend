package com.iknowledge.hosted.filestorage.rawfs;

import com.google.common.base.StandardSystemProperty;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.attribute.PosixFilePermission.GROUP_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.GROUP_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.util.Objects.requireNonNull;

public final class UnixFs {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnixFs.class);

    private final Set<PosixFilePermission> defaultDirPermissions;
    private final Set<PosixFilePermission> defaultFilePermissions;
    private final FileAttribute<Set<PosixFilePermission>> defaultFileAttrs;
    private final FileAttribute<Set<PosixFilePermission>> defaultDirAttrs;
    private final Path baseDir;
    private final Path fsRootDir;

    /**
     * Create new instance {@link UnixFs}
     *
     * @param rootDirName Name of directory which will represent root directory for new file system.
     */
    public UnixFs(Path baseDir, String rootDirName) throws IOException {
        requireNonNull(baseDir);
        checkBaseDirOwner(baseDir);

        this.baseDir = baseDir;
        this.fsRootDir = baseDir.resolve(rootDirName);
        defaultDirPermissions = Sets.newHashSet(OWNER_READ,
                                                OWNER_WRITE,
                                                OWNER_EXECUTE,
                                                GROUP_READ,
                                                GROUP_EXECUTE);
        defaultDirAttrs = PosixFilePermissions.asFileAttribute(defaultDirPermissions);
        defaultFilePermissions = Sets.newHashSet(OWNER_READ,
                                                 OWNER_WRITE,
                                                 GROUP_READ);
        defaultFileAttrs = PosixFilePermissions.asFileAttribute(defaultFilePermissions);

        LOGGER.info("Unix FS store initialized with base folder= '{}'."
                    + "Base folder permission: user=",
                    baseDir);
    }

    /**
     * Prepare base directory to be used by file storage
     *
     */
    public void format() throws IOException {
        if (Files.list(baseDir).findAny().isPresent()) {
            throw new IOException("Base directory '" + baseDir.toString() + "' is not empty "
                    + "directory. Remove all data from it or set other base "
                    + "directory path.");
        }
        Files.createFile(baseDir.resolve(".filestoreImage"), defaultFileAttrs);
        Files.createDirectory(fsRootDir, defaultDirAttrs);
    }

    /**
     * Create new directory
     *
     * @implNote This is atomic operation
     *
     * @param dirPath
     *
     * @throws IOException
     */
    public Path createDirectory(Path dirPath) throws IOException {
        final Path absPath = getAbsPath(dirPath);
        final Path dir = Files.createDirectory(absPath, defaultDirAttrs);
        LOGGER.debug("Directory '{}' created", dir);
        return dir;
    }

    /**
     * Create new file
     *
     * @implNote This is atomic operation
     *
     * @param filePath
     *
     * @throws FileAlreadyExistsException
     * @throws IOException
     */
    public Path createFile(Path filePath) throws IOException {
        final Path absPath = getAbsPath(filePath);
        final Path file = Files.createFile(absPath, defaultFileAttrs);
        LOGGER.debug("File '{}' created", file);
        return file;
    }

    /**
     * Check for path existence
     * @param path
     * @return
     */
    public boolean exists(Path path) {
        final Path absPath = getAbsPath(path);
        return Files.exists(absPath);
    }

    /**
     * Get count of how many files in the specified path.
     * Method is NOT recursive.
     * @param path
     * @throws IOException
     */
    public long filesInDir(Path path) throws IOException {
        final Path absPath = getAbsPath(path);
        final Stream<Path> pathStream = Files.list(absPath);
        final long count = pathStream.count();
        // stream must be closed or else "filesystemexception too many open files"
        pathStream.close();
        return count;
    }

    /**
     * Get files paths in the specified directory path.
     * Method is NOT recursive.
     * @param path
     * @throws IOException
     */
    public List<Path> getFilesInDir(Path path) throws IOException {
        final Path absPath = getAbsPath(path);
        final Stream<Path> pathStream = Files.list(absPath);
        final List<Path> paths = pathStream
                .collect(Collectors.toList());
        // stream must be closed or else "filesystemexception too many open files"
        pathStream.close();
        return paths;
    }

    /**
     * Get files paths in the specified directory path. Method is NOT recursive.
     *
     * @param path
     *
     * @throws IOException
     */
    public List<Path> getFilesInDir(final Path path,
                                    final long skip,
                                    final long take)
            throws IOException {
        final Path absPath = getAbsPath(path);
        final Stream<Path> pathStream = Files.list(absPath);
        final List<Path> paths = pathStream
                .skip(skip)
                .limit(take)
                .collect(Collectors.toList());
        // stream must be closed or else "filesystemexception too many open files"
        pathStream.close();
        return paths;
    }

    /**
     * Get first path (with skip value) in the specified directory path.
     * @throws IOException
     */
    public Optional<Path> getFirstPathInDirAsc(final Path path,
                                               final long skip)
            throws IOException {
        final Path absPath = getAbsPath(path);
        final Stream<Path> pathStream = Files.list(absPath);
        final Optional<Path> pathOptional = pathStream
                .sorted(Comparator.comparing(Path::getFileName))
                .skip(skip)
                .findFirst();
        // stream must be closed or else "filesystemexception too many open files"
        pathStream.close();
        return pathOptional;
    }

    /**
     * Remove file or empty directory.
     *
     * @implNote This is atomic operation
     *
     * @param path
     *
     * @throws IOException
     */
    public boolean remove(Path path) throws IOException {
        final Path absPath = getAbsPath(path);
        return Files.deleteIfExists(absPath);
    }

    /**
     * Move file into new destination.
     *
     * @implNote This is atomic operation
     *
     * @param srcPath File path to move
     * @param destPath Destination path to move file(must end with file name)
     *
     * @throws IOException
     * @throws FileAlreadyExistsException
     */
    public void move(Path srcPath, Path destPath) throws IOException {
        final Path srcAbsPath = getAbsPath(srcPath);
        final Path destAbsPath = getAbsPath(destPath);
        Files.move(srcAbsPath,
                   destAbsPath,
                   StandardCopyOption.ATOMIC_MOVE);
        LOGGER.debug("'{}' moved to '{}'", srcAbsPath, destAbsPath);
    }

    /**
     * Open file channel
     *
     * @param path File path
     * @param options File open options
     *
     * @return
     *
     * @throws IOException
     */
    public FileChannel openFile(Path path, OpenOption... options) throws IOException {
        return FileChannel.open(path, options);
    }

    private Path getAbsPath( final Path filePath ) {
        return fsRootDir.resolve(filePath);
    }

    private void checkBaseDirOwner( final Path baseDir ) throws IOException {
        final String baseDirOwner;
        try {
            final PosixFileAttributeView attrs
                    = Files.getFileAttributeView(baseDir, PosixFileAttributeView.class);
            baseDirOwner = attrs.getOwner().getName();
            LOGGER.info("Base folder permissions: {}",
                        PosixFilePermissions.toString(attrs.readAttributes().permissions()));
        }
        catch ( IOException e ) {
            LOGGER.error("Can't receive UNIX file permissions for folder={}, aborting...",
                         baseDir);
            throw e;
        }

        final String jvmUser = StandardSystemProperty.USER_NAME.value();
        if (!baseDirOwner.equalsIgnoreCase(jvmUser)) {
            LOGGER.error("Folder {} has owner '{}' but process run as '{}' user."
                         + "Change owner of base folder to '{}' user.",
                         baseDir,
                         baseDirOwner,
                         jvmUser,
                         jvmUser);
            throw new IOException();
        }
    }
}
