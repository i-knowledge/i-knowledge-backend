package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hosted.filestorage.PathOperationException;

@FunctionalInterface
public interface PathRunnable {

    void run() throws PathOperationException;
}
