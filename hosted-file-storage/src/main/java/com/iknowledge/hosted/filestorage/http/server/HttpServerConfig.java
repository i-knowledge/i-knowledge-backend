package com.iknowledge.hosted.filestorage.http.server;

import java.net.SocketAddress;

public final class HttpServerConfig {

    private final int acceptThreads;
    private final int workerThreads;
    private final boolean useEpoll;
    private final EpollConfig epollConfig;
    private final SocketAddress bindAddress;
    private final String fileUploadPath;
    private final String fileDownloadPath;
    private final long idleConnectionTimeoutMs;

    private HttpServerConfig(final Builder builder) {
        acceptThreads = builder.acceptThreads;
        workerThreads = builder.workerThreads;
        useEpoll = builder.useEpoll;
        epollConfig = builder.epollConfig;
        bindAddress = builder.bindAddress;
        fileUploadPath = builder.fileUploadPath;
        fileDownloadPath = builder.fileDownloadPath;
        idleConnectionTimeoutMs = builder.idleConnectionTimeoutMs;
    }

    public static IAcceptThreads builder() {
        return new Builder();
    }

    public int getAcceptThreads() {
        return acceptThreads;
    }

    public int getWorkerThreads() {
        return workerThreads;
    }

    public SocketAddress getBindAddress() {
        return bindAddress;
    }

    public String getFileUploadPath() {
        return fileUploadPath;
    }

    public long getIdleConnectionTimeoutMs() {
        return idleConnectionTimeoutMs;
    }

    public String getFileDownloadPath() {
        return fileDownloadPath;
    }

    public boolean isUseEpoll() {
        return useEpoll;
    }

    public EpollConfig getEpollConfig() {
        return epollConfig;
    }

    public interface IBuild {

        HttpServerConfig build();
    }

    public interface IIdleConnectionTimeoutMs {

        IBuild withIdleConnectionTimeoutMs(long val);
    }

    public interface IFileDownloadPath {

        IIdleConnectionTimeoutMs withFileDownloadPath(String val);
    }

    public interface IFileUploadPath {

        IFileDownloadPath withFileUploadPath(String val);
    }

    public interface IBindAddress {

        IFileUploadPath withBindAddress(SocketAddress val);
    }

    public interface IUseEpoll {

        IBindAddress withEpollConfig(EpollConfig val);

        IBindAddress withUseEpoll(boolean val);
    }

    public interface IWorkerThreads {

        IUseEpoll withWorkerThreads(int val);
    }

    public interface IAcceptThreads {

        IWorkerThreads withAcceptThreads(int val);
    }

    public static final class Builder
        implements IIdleConnectionTimeoutMs, IFileDownloadPath, IFileUploadPath, IBindAddress,
                   IUseEpoll, IWorkerThreads, IAcceptThreads, IBuild {

        private long idleConnectionTimeoutMs;
        private String fileDownloadPath;
        private String fileUploadPath;
        private SocketAddress bindAddress;
        private EpollConfig epollConfig = EpollConfig.newBuilder().build();
        private boolean useEpoll;
        private int workerThreads;
        private int acceptThreads;

        private Builder() {}

        @Override
        public IBuild withIdleConnectionTimeoutMs(final long val) {
            idleConnectionTimeoutMs = val;
            return this;
        }

        @Override
        public IIdleConnectionTimeoutMs withFileDownloadPath(final String val) {
            fileDownloadPath = val;
            return this;
        }

        @Override
        public IFileDownloadPath withFileUploadPath(final String val) {
            fileUploadPath = val;
            return this;
        }

        @Override
        public IFileUploadPath withBindAddress(final SocketAddress val) {
            bindAddress = val;
            return this;
        }

        @Override
        public IBindAddress withEpollConfig(final EpollConfig val) {
            useEpoll = true;
            epollConfig = val;
            return this;
        }

        @Override
        public IBindAddress withUseEpoll(final boolean val) {
            useEpoll = val;
            return this;
        }

        @Override
        public IUseEpoll withWorkerThreads(final int val) {
            workerThreads = val;
            return this;
        }

        @Override
        public IWorkerThreads withAcceptThreads(final int val) {
            acceptThreads = val;
            return this;
        }

        public HttpServerConfig build() {
            return new HttpServerConfig(this);
        }
    }
}
