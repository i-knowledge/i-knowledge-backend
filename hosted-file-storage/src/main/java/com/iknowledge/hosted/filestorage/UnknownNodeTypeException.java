package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

public class UnknownNodeTypeException extends PathOperationException {

    public UnknownNodeTypeException() {
    }

    public UnknownNodeTypeException(final String message) {
        super(message);
    }

    public UnknownNodeTypeException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnknownNodeTypeException(final Throwable cause) {
        super(cause);
    }

    public UnknownNodeTypeException(final INode inode) {
        super(inode.toString());
    }
}
