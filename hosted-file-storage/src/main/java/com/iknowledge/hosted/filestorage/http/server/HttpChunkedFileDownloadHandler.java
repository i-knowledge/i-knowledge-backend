package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.ReadableFileRevHandle;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.stream.ChunkedNioFile;
import org.slf4j.LoggerFactory;

public class HttpChunkedFileDownloadHandler extends HttpFileDownloadHandlerBase {

    public HttpChunkedFileDownloadHandler(ReadableFileRevHandle revHandle,
                                          HttpHeaders httpHeaders,
                                          HttpVersion httpVersion) {
        super(revHandle,
              httpHeaders,
              httpVersion,
              LoggerFactory.getLogger(HttpChunkedFileDownloadHandler.class));
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {

        final HttpHeaders headers = new DefaultHttpHeaders()
                                        .add(HttpHeaderNames.TRANSFER_ENCODING,
                                             HttpHeaderValues.CHUNKED)
                                        .add(HttpHeaderNames.CONTENT_LENGTH,
                                             revHandle.channel().size());

        ctx.write(new DefaultHttpResponse(httpVersion,
                                          HttpResponseStatus.OK,
                                          headers));
        ctx.writeAndFlush(new HttpChunkedInput(new ChunkedNioFile(revHandle.channel())))
           .addListener(future -> {
               closeResources();
               logResult(future);
           });
    }

}
