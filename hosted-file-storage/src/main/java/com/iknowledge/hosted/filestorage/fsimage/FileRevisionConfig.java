package com.iknowledge.hosted.filestorage.fsimage;

import java.util.concurrent.TimeUnit;

public final class FileRevisionConfig {

    private final String fsRevRoot;
    private final int maxFileCount;
    private final long revisionCacheTtlMs;
    private final int revisionCacheInitCapacity;
    private final long revisionCacheMaxSize;
    private final int reservedTenthOfFreeMemory;
    private final int startAtHour;
    private final int runEveryTimeUnit;
    private final TimeUnit timeUnit;

    private FileRevisionConfig(Builder builder) {
        fsRevRoot = builder.fsRevRoot;
        maxFileCount = builder.maxFileCount;
        revisionCacheTtlMs = builder.revisionCacheTtlMs;
        revisionCacheInitCapacity = builder.revisionCacheInitCapacity;
        revisionCacheMaxSize = builder.revisionCacheMaxSize;
        reservedTenthOfFreeMemory = builder.reservedTenthOfFreeMemory;
        startAtHour = builder.startAtHour;
        runEveryTimeUnit = builder.runEveryTimeUnit;
        timeUnit = builder.timeUnit;
    }

    public static Builder newBuilder(FileRevisionConfig copy) {
        Builder builder = new Builder();
        builder.timeUnit = copy.timeUnit;
        builder.runEveryTimeUnit = copy.runEveryTimeUnit;
        builder.startAtHour = copy.startAtHour;
        builder.reservedTenthOfFreeMemory = copy.reservedTenthOfFreeMemory;
        builder.revisionCacheMaxSize = copy.revisionCacheMaxSize;
        builder.revisionCacheInitCapacity = copy.revisionCacheInitCapacity;
        builder.revisionCacheTtlMs = copy.revisionCacheTtlMs;
        builder.maxFileCount = copy.maxFileCount;
        builder.fsRevRoot = copy.fsRevRoot;
        return builder;
    }

    public String getFsRevRoot() {
        return fsRevRoot;
    }

    public int getMaxFileCount() {
        return maxFileCount;
    }

    public long getRevisionCacheTtlMs() {
        return revisionCacheTtlMs;
    }

    public int getRevisionCacheInitCapacity() {
        return revisionCacheInitCapacity;
    }

    public long getRevisionCacheMaxSize() {
        return revisionCacheMaxSize;
    }

    public int getReservedTenthOfFreeMemory() {
        return reservedTenthOfFreeMemory;
    }

    public int getStartAtHour() {
        return startAtHour;
    }

    public int getRunEveryTimeUnit() {
        return runEveryTimeUnit;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public static IFsRevRoot builder() {
        return new Builder();
    }

    public interface IBuild {
        FileRevisionConfig build();
    }

    public interface ITimeUnit {
        IBuild withTimeUnit(TimeUnit val);
    }

    public interface IRunEveryTimeUnit {
        ITimeUnit withRunEveryTimeUnit(int val);
    }

    public interface IStartAtHour {
        IRunEveryTimeUnit withStartAtHour(int val);
    }

    public interface IReservedTenthOfFreeMemory {
        IStartAtHour withReservedTenthOfFreeMemory(int val);
    }

    public interface IRevisionCacheMaxSize {
        IReservedTenthOfFreeMemory withRevisionCacheMaxSize(long val);
    }

    public interface IRevisionCacheInitCapacity {
        IRevisionCacheMaxSize withRevisionCacheInitCapacity(int val);
    }

    public interface IRevisionCacheTtlMs {
        IRevisionCacheInitCapacity withRevisionCacheTtlMs(long val);
    }

    public interface IMaxFileCount {
        IRevisionCacheTtlMs withMaxFileCount(int val);
    }

    public interface IFsRevRoot {
        IMaxFileCount withFsRevRoot(String val);
    }

    /**
     * {@code FileRevisionConfig} builder static inner class.
     */
    public static final class Builder implements ITimeUnit, IRunEveryTimeUnit, IStartAtHour,
            IReservedTenthOfFreeMemory, IRevisionCacheMaxSize, IRevisionCacheInitCapacity,
            IRevisionCacheTtlMs, IMaxFileCount, IFsRevRoot, IBuild {
        private TimeUnit timeUnit;
        private int runEveryTimeUnit;
        private int startAtHour;
        private int reservedTenthOfFreeMemory;
        private long revisionCacheMaxSize;
        private int revisionCacheInitCapacity;
        private long revisionCacheTtlMs;
        private int maxFileCount;
        private String fsRevRoot;

        private Builder() {
        }

        /**
         * Sets the {@code timeUnit} and returns a reference to {@code IBuild}
         *
         * @param val the {@code timeUnit} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IBuild withTimeUnit(TimeUnit val) {
            timeUnit = val;
            return this;
        }

        /**
         * Sets the {@code runEveryTimeUnit} and returns a reference to {@code ITimeUnit}
         *
         * @param val the {@code runEveryTimeUnit} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public ITimeUnit withRunEveryTimeUnit(int val) {
            runEveryTimeUnit = val;
            return this;
        }

        /**
         * Sets the {@code startAtHour} and returns a reference to {@code IRunEveryTimeUnit}
         *
         * @param val the {@code startAtHour} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IRunEveryTimeUnit withStartAtHour(int val) {
            startAtHour = val;
            return this;
        }

        /**
         * Sets the {@code reservedTenthOfFreeMemory} and returns a reference to {@code
         * IStartAtHour}
         *
         * @param val the {@code reservedTenthOfFreeMemory} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IStartAtHour withReservedTenthOfFreeMemory(int val) {
            reservedTenthOfFreeMemory = val;
            return this;
        }

        /**
         * Sets the {@code revisionCacheMaxSize} and returns a reference to {@code
         * IReservedTenthOfFreeMemory}
         *
         * @param val the {@code revisionCacheMaxSize} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IReservedTenthOfFreeMemory withRevisionCacheMaxSize(long val) {
            revisionCacheMaxSize = val;
            return this;
        }

        /**
         * Sets the {@code revisionCacheInitCapacity} and returns a reference to {@code
         * IRevisionCacheMaxSize}
         *
         * @param val the {@code revisionCacheInitCapacity} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IRevisionCacheMaxSize withRevisionCacheInitCapacity(int val) {
            revisionCacheInitCapacity = val;
            return this;
        }

        /**
         * Sets the {@code revisionCacheTtlMs} and returns a reference to {@code
         * IRevisionCacheInitCapacity}
         *
         * @param val the {@code revisionCacheTtlMs} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IRevisionCacheInitCapacity withRevisionCacheTtlMs(long val) {
            revisionCacheTtlMs = val;
            return this;
        }

        /**
         * Sets the {@code maxFileCount} and returns a reference to {@code IRevisionCacheTtlMs}
         *
         * @param val the {@code maxFileCount} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IRevisionCacheTtlMs withMaxFileCount(int val) {
            maxFileCount = val;
            return this;
        }

        /**
         * Sets the {@code fsRevRoot} and returns a reference to {@code IMaxFileCount}
         *
         * @param val the {@code fsRevRoot} to set
         *
         * @return a reference to this Builder
         */
        @Override
        public IMaxFileCount withFsRevRoot(String val) {
            fsRevRoot = val;
            return this;
        }

        /**
         * Returns a {@code FileRevisionConfig} built from the parameters previously set.
         *
         * @return a {@code FileRevisionConfig} built with parameters
         * of this {@code FileRevisionConfig.Builder}
         */
        public FileRevisionConfig build() {
            return new FileRevisionConfig(this);
        }
    }
}
