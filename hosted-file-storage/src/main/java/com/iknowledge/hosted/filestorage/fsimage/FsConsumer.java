package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathOperationException;

@FunctionalInterface
public interface FsConsumer<T> {

    void consume(T instance) throws PathOperationException, StoreException;
}
