package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.RevisionTranIdNotFound;
import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import com.iknowledge.hosted.filestorage.filesystem.NewFileRevHandle;
import com.iknowledge.hosted.filestorage.filesystem.ReadableFileRevHandle;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public final class RouterHandler extends ChannelDuplexHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouterHandler.class);
    private static final String FILE_TOKEN_HEADER = "file_token";
    private static final long COMPRESSION_ENABLED_THRESHOLD = 5L * 1024L * 1024L * 1024L;

    private final FileSystem fs;
    private final HttpServerConfig config;
    private RouteType routeType = RouteType.NONE;
    private HttpVersion httpVersion = HttpVersion.HTTP_1_1;

    public RouterHandler(final FileSystem fs, final HttpServerConfig config) {
        this.fs = fs;
        this.config = config;
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx) {
        routeType = RouteType.NONE;
        ctx.fireChannelInactive();
        LOGGER.trace("Close channel {}", ctx.channel());
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
        routeType = RouteType.NONE;
        writeFullResponse(ctx, httpVersion, HttpResponseStatus.INTERNAL_SERVER_ERROR);
        //TODO: metrics
        LOGGER.error("Channel {} raise exception: {}", ctx.channel(), cause);
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {

        if (msg instanceof HttpRequest) {
            final HttpRequest request = (HttpRequest) msg;
            httpVersion = request.protocolVersion();
            routeType = routeRequest(ctx, request);

            switch (routeType) {
                case DOWNLOAD:
                    ctx.fireChannelRead(msg);
                    break;
                default:
                    break;
            }
        }
        if (msg instanceof HttpContent) {
            switch (routeType) {
                case UPLOAD:
                    ctx.fireChannelRead(msg);
                    break;
                default:
                    ((HttpContent)msg).release();
                    break;
            }
        }
    }

    private RouteType routeRequest(final ChannelHandlerContext ctx, final HttpRequest request)
            throws URISyntaxException {

        final URI uri = new URI(request.uri());
        RouteType type;
        if (uri.getPath().equalsIgnoreCase(config.getFileUploadPath())) {
            type = routeFileUpload(ctx, request);
        }
        else if (uri.getPath().equalsIgnoreCase(config.getFileDownloadPath())) {
            type = routeFileDownload(ctx, request);
        }
        else {
            type = RouteType.NONE;
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.NOT_FOUND);
        }

        return type;
    }

    private RouteType routeFileUpload(final ChannelHandlerContext ctx, final HttpRequest request) {
        if (request.method() != HttpMethod.POST) {
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.METHOD_NOT_ALLOWED);
            return RouteType.NONE;
        }

        final String fileToken = request.headers().get(FILE_TOKEN_HEADER);
        if (fileToken == null) {
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.BAD_REQUEST);
            return RouteType.NONE;
        }

        try {
            cleanupPipeline(ctx);
            NewFileRevHandle revHandle = fs.openRevision(fileToken);
            ctx.pipeline().addLast(HttpFileUploadHandler.class.getSimpleName(),
                                   new HttpFileUploadHandler(revHandle, httpVersion));
            return RouteType.UPLOAD;
        }
        catch (IOException e) {
            LOGGER.warn("Can't open file revision: {}", e);
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            return RouteType.NONE;
        }
        catch (RevisionTranIdNotFound e) {
            LOGGER.trace("Can't find file revision '{}': {}", fileToken, e);
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.BAD_REQUEST);
            return RouteType.NONE;
        }
    }

    private RouteType routeFileDownload(final ChannelHandlerContext ctx,
                                        final HttpRequest request) {

        if (request.method() != HttpMethod.GET) {
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.METHOD_NOT_ALLOWED);
            return RouteType.NONE;
        }

        final String fileToken = request.headers().get(FILE_TOKEN_HEADER);
        if (fileToken == null) {
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.BAD_REQUEST);
            return RouteType.NONE;
        }

        try {
            cleanupPipeline(ctx);
            ReadableFileRevHandle revHandle = fs.openReadRevision(fileToken);
            if (revHandle.channel().size() > COMPRESSION_ENABLED_THRESHOLD) {
                ctx.pipeline().addLast(HttpContentCompressor.class.getSimpleName(),
                                       new HttpContentCompressor());
                ctx.pipeline().addLast(ChunkedWriteHandler.class.getSimpleName(),
                                       new ChunkedWriteHandler());
                ctx.pipeline().addLast(HttpChunkedFileDownloadHandler.class.getSimpleName(),
                                       new HttpChunkedFileDownloadHandler(revHandle,
                                                                          request.headers(),
                                                                          httpVersion));
            }
            else {
                ctx.pipeline().addLast(HttpZeroCopyFileDownloadHandler.class.getSimpleName(),
                                       new HttpZeroCopyFileDownloadHandler(revHandle,
                                                                           request.headers(),
                                                                           httpVersion));
            }
        }
        catch (IOException e) {
            LOGGER.warn("Can't read file revision: {}", e);
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            return RouteType.NONE;
        }
        catch (RevisionTranIdNotFound e) {
            LOGGER.trace("Can't find file revision '{}': {}", fileToken, e);
            writeFullResponse(ctx, httpVersion, HttpResponseStatus.BAD_REQUEST);
            return RouteType.NONE;
        }
        return RouteType.DOWNLOAD;
    }

    private void cleanupPipeline(final ChannelHandlerContext ctx) {
        while (ctx.pipeline().last() != this) {
            ctx.pipeline().removeLast();
        }
    }

    private void writeFullResponse(final ChannelHandlerContext ctx,
                                   final HttpVersion httpVersion,
                                   final HttpResponseStatus status) {

        final DefaultFullHttpResponse httpResponse
            = new DefaultFullHttpResponse(httpVersion, status);
        // we can keep TCP connection with client opened => we must ensure that we send
        // response content length to client side(ptherwise, client will wait connection close)
        httpResponse.headers().add(HttpHeaderNames.CONTENT_LENGTH, 0);

        ctx.writeAndFlush(httpResponse);
    }

    enum RouteType {
        NONE,
        UPLOAD,
        DOWNLOAD
    }
}
