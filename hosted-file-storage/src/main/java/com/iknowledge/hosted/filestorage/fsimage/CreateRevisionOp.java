package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ProposedFileRevisionINode;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;

final class CreateRevisionOp extends BaseOp {

    private final HBaseFsImageStore backend;
    private final FsMap fsMap;

    public CreateRevisionOp(HBaseFsImageStore backend,
                            FsMap fsMap,
                            int readLockTimeoutMs,
                            int writeLockTimeoutMs) {

        super(readLockTimeoutMs, writeLockTimeoutMs);
        this.backend = backend;
        this.fsMap = fsMap;
    }

    /**
     * Create file revision based on proposal
     *
     * @param proposedRevInode Revision proposal
     *
     * @return
     */
    public FileRevisionINode perform(ProposedFileRevisionINode proposedRevInode)
            throws PathOperationException, StoreException {

        final FileINode fileInode = asUsualFile(proposedRevInode.parent());
        return tryWriteLock(fileInode, () -> {
            throwIfNotAttached(fileInode); // to protect from file removal

            final FileRevisionINode newRevision = addFileRevision(proposedRevInode, fileInode);
            commitNewRev(fileInode, newRevision);
            return newRevision;
        });
    }

    private void commitNewRev(final FileINode fileInode, final FileRevisionINode newRevision)
        throws PathExistsException, StoreException {
        if (!fsMap.put(newRevision)) {
            fileInode.removeChild(newRevision);
            throw new PathExistsException("inode with ID="
                                          + newRevision.id()
                                          + " already exists");
        }

        try {
            backend.addInode(newRevision);
        }
        catch (StoreException e) {
            fileInode.removeChild(newRevision);
            fsMap.remove(newRevision);
            throw e;
        }
    }

    private FileRevisionINode addFileRevision(final ProposedFileRevisionINode proposedRevInode,
                                              final FileINode fileInode) {
        while (true) {
            final FileRevisionINode newRevision
                = proposedRevInode.toRevision(System.currentTimeMillis());
            try {
                fileInode.addChild(newRevision);
                return newRevision;
            }
            catch (IllegalArgumentException e) {
                // repeat until we stop conflict to add new revision with same timestamp
                Thread.yield();
                continue;
            }
        }
    }
}
