package com.iknowledge.hosted.filestorage.filesystem;

import java.nio.file.Path;
import java.util.Collection;

/**
 * FS container object with can have children(for instance, directory object)
 */
public interface FsContainerHandle extends FsHandle {

    /**
     * Return children of current file system object
     */
    Collection<Path> children();
}
