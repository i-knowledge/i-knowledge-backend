package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ProposedFileRevisionINode;

import java.util.StringJoiner;

public final class RevisionTransaction {

    private final String tranId;
    private final ProposedFileRevisionINode proposedRevInode;
    private final InMemoryFsImage fsImage;

    private RevisionTransaction(final Builder builder) {
        tranId = builder.tranId;
        proposedRevInode = builder.proposedRevInode;
        fsImage = builder.fsImage;
    }

    public static ITranId builder() {
        return new Builder();
    }

    public String id() {
        return tranId;
    }

    public ProposedFileRevisionINode revisionInode() {
        return proposedRevInode;
    }

    /**
     * Commit new file revision
     *
     * @throws StoreException
     * @throws PathOperationException
     */
    public void commit() throws StoreException, PathOperationException {
        fsImage.createFileRevision(proposedRevInode);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                       .add("tranId = " + tranId)
                       .add("proposedRevInode = " + proposedRevInode)
                       .toString();
    }

    public interface IBuild {

        RevisionTransaction build();
    }

    public interface IFsImage {

        IBuild withFsImage(InMemoryFsImage val);
    }

    public interface IProposedRevInode {

        IFsImage withProposedRevInode(ProposedFileRevisionINode val);
    }

    public interface ITranId {

        IProposedRevInode withTranId(String val);
    }

    public static final class Builder
            implements IFsImage, IProposedRevInode, ITranId, IBuild {

        private InMemoryFsImage fsImage;
        private ProposedFileRevisionINode proposedRevInode;
        private String tranId;

        private Builder() {}

        @Override
        public IBuild withFsImage(final InMemoryFsImage val) {
            fsImage = val;
            return this;
        }

        @Override
        public IFsImage withProposedRevInode(final ProposedFileRevisionINode val) {
            proposedRevInode = val;
            return this;
        }

        @Override
        public IProposedRevInode withTranId(final String val) {
            tranId = val;
            return this;
        }

        public RevisionTransaction build() {
            return new RevisionTransaction(this);
        }
    }
}
