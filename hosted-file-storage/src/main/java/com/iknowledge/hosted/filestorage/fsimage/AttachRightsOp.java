package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.AccessRestrictedINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;

import java.io.IOException;
import java.util.List;

public class AttachRightsOp extends BaseOp {

    private final HBaseFsImageStore backend;

    /**
     *
     * @param backend
     * @param readLockTimeoutMs
     * @param writeLockTimeoutMs
     */
    public AttachRightsOp(final HBaseFsImageStore backend,
                          final int readLockTimeoutMs,
                          final int writeLockTimeoutMs) {

        super(readLockTimeoutMs, writeLockTimeoutMs);
        this.backend = backend;
    }

    /**
     * Attach user rights to the inode
     * @param inode
     * @param rights
     * @throws IOException
     * @throws StoreException
     */
    public void attachAccessRights(final AccessRestrictedINode inode,
                                   final List<HBaseINodeRights> rights)
            throws PathOperationException, StoreException {

        if (rights.isEmpty()) {
            return;
        }

        tryWriteLock(inode, (FsRunnable)() -> {
            backend.attachUserRights(inode.id().bytes(), rights);
            inode.userRights().attachUserRights(rights);
        });
    }
}
