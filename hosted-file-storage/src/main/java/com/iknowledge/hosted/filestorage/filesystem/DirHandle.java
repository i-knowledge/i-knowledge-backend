package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public final class DirHandle implements FsContainerHandle {

    private final String name;
    private final Path path;
    private final List<Path> children;
    private final UserRights userRights;

    DirHandle(DirectoryINode dirInode) {
        dirInode.lock().readLock().lock();
        try {
            name = dirInode.name();
            path = dirInode.path();
            children = dirInode.children()
                               .stream()
                               .filter(INodeTypes::isUserLevelInode)
                               .map(INode::path)
                               .collect(toList());
            userRights = dirInode.userRights()
                    .copy();
        }
        finally {
            dirInode.lock().readLock().unlock();
        }
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Path path() {
        return path;
    }

    @Override
    public UserRights userRights() {
        return userRights;
    }

    @Override
    public Collection<Path> children() {
        return children;
    }
}
