package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.filestorage.UnknownNodeTypeException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;

import java.nio.file.Path;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;

public interface FsHandle {

    /**
     * Name of filesystem object(for instance, file name or dir name)
     */
    String name();

    /**
     * Full file system path of this object.
     */
    Path path();

    /**
     * Get the all list of user rights that are connected with the inode
     * @return
     */
    UserRights userRights();

    static FsHandle from(INode inode) throws UnknownNodeTypeException {
        if (isUsualDir(inode)) {
            return new DirHandle(asUsualDir(inode));
        }
        else if (isUsualFile(inode)) {
            return new FileHandle(asUsualFile(inode));
        }
        throw new UnknownNodeTypeException();
    }
}
