package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hosted.filestorage.PathOperationException;

@FunctionalInterface
public interface PathSupplier<T> {

    T get() throws PathOperationException;
}
