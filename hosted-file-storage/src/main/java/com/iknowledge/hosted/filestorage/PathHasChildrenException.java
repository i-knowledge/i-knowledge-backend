package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

/**
 * Path has children and requested operation cannot be performed on it in this state.
 */
public class PathHasChildrenException extends PathOperationException {

    public PathHasChildrenException() {
        super();
    }

    public PathHasChildrenException(final String message) {
        super(message);
    }

    public PathHasChildrenException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PathHasChildrenException(final Throwable cause) {
        super(cause);
    }

    public PathHasChildrenException(final INode inode) {
        super(inode.toString());
    }
}
