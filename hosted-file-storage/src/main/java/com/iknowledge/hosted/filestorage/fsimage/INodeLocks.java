package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathLockedException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import io.vavr.control.Either;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public final class INodeLocks {

    private INodeLocks() {}

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryWriteLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    Runnable lockedAction) throws PathLockedException {

        tryWriteLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryWriteLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    Runnable lockedAction,
                                    Runnable lockAcquireFailedAction) throws PathLockedException {

        tryAcquireWriteLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            lockedAction.run();
        }
        finally {
            inode.lock().writeLock().unlock();
        }
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryWriteLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    FsRunnable lockedAction)
            throws PathOperationException, StoreException {

        tryWriteLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryWriteLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    FsRunnable lockedAction,
                                    FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        tryAcquireWriteLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            lockedAction.run();
        }
        finally {
            inode.lock().writeLock().unlock();
        }
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryWriteLock(INode inode,
                                     int time,
                                     TimeUnit timeUnit,
                                     FsSupplier<T> lockedAction)
            throws PathOperationException, StoreException {

        return tryWriteLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryWriteLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    FsSupplier<T> lockedAction,
                                    FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        tryAcquireWriteLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().writeLock().unlock();
        }
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   Runnable lockedAction) throws PathLockedException {

        tryReadLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   FsRunnable lockedAction)
            throws PathOperationException, StoreException {

        tryReadLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   PathRunnable lockedAction)
            throws PathOperationException, StoreException {

        tryReadLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   PathSupplier<T> lockedAction)
            throws PathOperationException {

        return tryReadLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   FsRunnable lockedAction,
                                   FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        tryAcquireReadLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            lockedAction.run();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   PathRunnable lockedAction,
                                   PathRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        tryAcquireReadLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            lockedAction.run();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   PathSupplier<T> lockedAction,
                                   PathRunnable lockAcquireFailedAction)
            throws PathOperationException {

        tryAcquireReadLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static void tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   Runnable lockedAction,
                                   Runnable lockAcquireFailedAction)
            throws PathLockedException {

        tryAcquireReadLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            lockedAction.run();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryReadLock(INode inode,
                                   int time,
                                   TimeUnit timeUnit,
                                   FsSupplier<T> lockedAction)
            throws PathOperationException, StoreException {

        return tryReadLock(inode, time, timeUnit, lockedAction, null);
    }

    /**
     * Try acquire read lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> Either<PathOperationException, T> tryReadLock(INode inode,
                                                                    int time,
                                                                    TimeUnit timeUnit,
                                                                    Supplier<T> lockedAction) {

        try {
            tryAcquireReadLock(inode, time, timeUnit, (Runnable) null);
        }
        catch (PathOperationException e) {
            return Either.left(e);
        }

        try {
            return Either.right(lockedAction.get());
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    /**
     * Try acquire write lock during specified time
     *
     * @param inode
     * @param time
     * @param timeUnit
     *
     * @throws IOException
     * @throws PathLockedException if can't acquire lock during specified time
     */
    public static <T> T tryReadLock(INode inode,
                                    int time,
                                    TimeUnit timeUnit,
                                    FsSupplier<T> lockedAction,
                                    FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        tryAcquireReadLock(inode, time, timeUnit, lockAcquireFailedAction);

        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    public static void writeLock(INode inode, FsRunnable lockedAction)
            throws StoreException, IOException {

        inode.lock().writeLock().lock();
        try {
            lockedAction.run();
        }
        finally {
            inode.lock().writeLock().unlock();
        }
    }

    public static <T> T writeLock(INode inode, FsSupplier<T> lockedAction)
            throws StoreException, IOException {

        inode.lock().writeLock().lock();
        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().writeLock().unlock();
        }
    }

    public static void readLock(INode inode, FsRunnable lockedAction)
            throws StoreException, IOException {

        inode.lock().readLock().lock();
        try {
            lockedAction.run();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    public static void readLock(INode inode, Runnable lockedAction) {

        inode.lock().readLock().lock();
        try {
            lockedAction.run();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    public static <T> T readLock(INode inode, FsSupplier<T> lockedAction)
            throws StoreException, IOException {

        inode.lock().readLock().lock();
        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    public static <T> T checkedReadLock(INode inode, Supplier<T> lockedAction) {

        inode.lock().readLock().lock();
        try {
            return lockedAction.get();
        }
        finally {
            inode.lock().readLock().unlock();
        }
    }

    private static void tryAcquireWriteLock(final INode inode, final int time,
                                            final TimeUnit timeUnit,
                                            final FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        try {
            if (!inode.lock().writeLock().tryLock(time, timeUnit)) {
                if (lockAcquireFailedAction != null) {
                    lockAcquireFailedAction.run();
                }
                throw new PathLockedException();
            }
        }
        catch (InterruptedException e) {
            if (lockAcquireFailedAction != null) {
                lockAcquireFailedAction.run();
            }
            throw new PathLockedException(e);
        }
    }

    private static void tryAcquireWriteLock(final INode inode, final int time,
                                            final TimeUnit timeUnit,
                                            final Runnable lockAcquireFailedAction)
            throws PathLockedException {

        try {
            if (!inode.lock().writeLock().tryLock(time, timeUnit)) {
                if (lockAcquireFailedAction != null) {
                    lockAcquireFailedAction.run();
                }
                throw new PathLockedException();
            }
        }
        catch (InterruptedException e) {
            if (lockAcquireFailedAction != null) {
                lockAcquireFailedAction.run();
            }
            throw new PathLockedException(e);
        }
    }

    private static void tryAcquireReadLock(final INode inode, final int time,
                                           final TimeUnit timeUnit,
                                           final FsRunnable lockAcquireFailedAction)
            throws PathOperationException, StoreException {

        try {
            if (!inode.lock().readLock().tryLock(time, timeUnit)) {
                if (lockAcquireFailedAction != null) {
                    lockAcquireFailedAction.run();
                }
                throw new PathLockedException();
            }
        }
        catch (InterruptedException e) {
            if (lockAcquireFailedAction != null) {
                lockAcquireFailedAction.run();
            }
            throw new PathLockedException(e);
        }
    }

    private static void tryAcquireReadLock(final INode inode, final int time,
                                           final TimeUnit timeUnit,
                                           final PathRunnable lockAcquireFailedAction)
            throws PathOperationException {

        try {
            if (!inode.lock().readLock().tryLock(time, timeUnit)) {
                if (lockAcquireFailedAction != null) {
                    lockAcquireFailedAction.run();
                }
                throw new PathLockedException();
            }
        }
        catch (InterruptedException e) {
            if (lockAcquireFailedAction != null) {
                lockAcquireFailedAction.run();
            }
            throw new PathLockedException(e);
        }
    }

    private static void tryAcquireReadLock(final INode inode, final int time,
                                           final TimeUnit timeUnit,
                                           final Runnable lockAcquireFailedAction)
            throws PathLockedException {

        try {
            if (!inode.lock().readLock().tryLock(time, timeUnit)) {
                if (lockAcquireFailedAction != null) {
                    lockAcquireFailedAction.run();
                }
                throw new PathLockedException();
            }
        }
        catch (InterruptedException e) {
            if (lockAcquireFailedAction != null) {
                lockAcquireFailedAction.run();
            }
            throw new PathLockedException(e);
        }
    }
}
