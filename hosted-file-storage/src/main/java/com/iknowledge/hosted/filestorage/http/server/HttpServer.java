package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollChannelOption;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.internal.logging.InternalLoggerFactory;
import io.netty.util.internal.logging.Slf4JLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AccessController;
import java.security.PrivilegedAction;

public final class HttpServer implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServer.class);
    private static final Path TCP_FAST_OPEN_FILE_PATH
        = Paths.get("/proc/sys/net/ipv4/tcp_fastopen");

    private boolean closed;
    private final Channel serverChannel;
    private final EventLoopGroup acceptGroup;
    private final EventLoopGroup workerGroup;

    /**
     * Create instance of {@link HttpServer}
     *
     * @param config HTTP server config
     * @param fs File system instance
     *
     * @throws IOException
     */
    public HttpServer(HttpServerConfig config, FileSystem fs) throws IOException {
        InternalLoggerFactory.setDefaultFactory(Slf4JLoggerFactory.INSTANCE);

        final ServerBootstrap bootstrap = new ServerBootstrap();

        if (config.isUseEpoll()) {
            acceptGroup = new EpollEventLoopGroup(config.getAcceptThreads());
            workerGroup = new EpollEventLoopGroup(config.getWorkerThreads());
            bootstrap.channel(EpollServerSocketChannel.class)
                     .childHandler(new EpollHttpServerInitializer(config, fs))
                     .option(EpollChannelOption.TCP_DEFER_ACCEPT,
                                  config.getEpollConfig().tcpDeferAcceptSec());
            if (isServerFastOpen()) {
                bootstrap.option(EpollChannelOption.TCP_FASTOPEN,
                                      config.getEpollConfig().tcpFastOpenQueue());
            }
        }
        else {
            acceptGroup = new NioEventLoopGroup(config.getAcceptThreads());
            workerGroup = new NioEventLoopGroup(config.getWorkerThreads());
            bootstrap.channel(NioServerSocketChannel.class)
                     .childHandler(new NioHttpServerInitializer(config, fs));
        }

        try {
            serverChannel
                = bootstrap
                    .childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true)
                    .group(acceptGroup, workerGroup)
                    .bind(config.getBindAddress())
                    .sync()
                    .channel();
        }
        catch (InterruptedException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws Exception {
        if (closed) {
            return;
        }
        serverChannel.close().sync();
        acceptGroup.shutdownGracefully().get();
        workerGroup.shutdownGracefully().get();
        closed = true;
    }

    public boolean isServerFastOpen() {
        return AccessController.doPrivileged((PrivilegedAction<Integer>) () -> {
            int fastOpenVal = 0;
            if (Files.exists(TCP_FAST_OPEN_FILE_PATH)) {
                try {
                    fastOpenVal = Integer.parseInt(Files.readAllLines(TCP_FAST_OPEN_FILE_PATH)
                                                        .get(0));
                    LOGGER.debug("{}: {}", TCP_FAST_OPEN_FILE_PATH, fastOpenVal);
                }
                catch (Exception e) {
                    LOGGER.debug("Failed to get TCP_FASTOPEN from: {}",
                                 TCP_FAST_OPEN_FILE_PATH, e);
                }
            } else {
                LOGGER.debug("{} file not exist, disable TCP fast open",
                             TCP_FAST_OPEN_FILE_PATH, fastOpenVal);
            }
            return fastOpenVal;
        }) == 3;
    }
}
