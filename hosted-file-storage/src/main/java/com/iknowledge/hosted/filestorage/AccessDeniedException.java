package com.iknowledge.hosted.filestorage;

public final class AccessDeniedException extends PathOperationException {
    public AccessDeniedException() {
        super();
    }

    public AccessDeniedException(final String message) {
        super(message);
    }

    public AccessDeniedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AccessDeniedException(final Throwable cause) {
        super(cause);
    }
}
