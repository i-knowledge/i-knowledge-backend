package com.iknowledge.hosted.filestorage.fsimage;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.iknowledge.common.Id;
import com.iknowledge.hosted.filestorage.PathAllocationException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.filestorage.RevisionTranIdAlreadyExists;
import com.iknowledge.hosted.filestorage.RevisionTranIdNotFound;
import com.iknowledge.hosted.filestorage.rawfs.UnixFs;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ProposedFileRevisionINode;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.cache.caffeine.CacheMetricsCollector;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.requireNonNull;

public final class FileRevisionManager implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileRevisionManager.class);

    private final InMemoryFsImage fsImage;
    private final UnixFs unixFs;
    private final FileRevisionPathAllocator fileRevisionPathAllocator;
    private final FileRevisionConfig revConf;
    private final ConcurrentMap<String, RevisionTransaction> proposedRevs;
    private final ConcurrentMap<String, FileRevisionINode> readRevs;
    private final FileRevisionGc gc;

    public FileRevisionManager(InMemoryFsImage fsImage,
                               FsConfig config,
                               UnixFs unixFs,
                               CollectorRegistry metricsRegistry) throws PathAllocationException {

        this.fsImage = fsImage;
        this.unixFs = unixFs;
        revConf = config.getFileRevisionConfig();

        Cache<String, RevisionTransaction> revisionsCache
                = Caffeine.newBuilder()
                          .expireAfterAccess(revConf.getRevisionCacheTtlMs(),
                                             TimeUnit.MILLISECONDS)
                          .initialCapacity(revConf.getRevisionCacheInitCapacity())
                          .maximumSize(revConf.getRevisionCacheMaxSize())
                          .executor(Executors.newSingleThreadExecutor())
                          .removalListener(revisionsRemovalListener())
                          .build();

        Cache<String, FileRevisionINode> readRevCache
                = Caffeine.newBuilder()
                          .expireAfterAccess(revConf.getRevisionCacheTtlMs(),
                                             TimeUnit.MILLISECONDS)
                          .initialCapacity(revConf.getRevisionCacheInitCapacity())
                          .maximumSize(revConf.getRevisionCacheMaxSize())
                          .executor(Executors.newSingleThreadExecutor())
                          .removalListener(readRevisionsRemovalListener())
                          .build();

        this.proposedRevs = revisionsCache.asMap();
        this.readRevs = readRevCache.asMap();
        CacheMetricsCollector cacheMetrics = new CacheMetricsCollector()
                                                     .register(metricsRegistry);
        cacheMetrics.addCache("new_rev_cache", revisionsCache);
        cacheMetrics.addCache("read_rev_cache", readRevCache);

        FileRevisionGcConfig gcConfig = FileRevisionGcConfig.builder()
                .withRootPath(revConf.getFsRevRoot())
                .withStartAtHour(revConf.getStartAtHour())
                .withRunEveryTimeUnit(revConf.getRunEveryTimeUnit())
                .withTimeUnit(revConf.getTimeUnit())
                .withReservedTenthOfFreeMemory(revConf.getReservedTenthOfFreeMemory())
                .build();
        gc = new FileRevisionGc(unixFs, fsImage.fsMap(), gcConfig);
        this.fileRevisionPathAllocator = new FileRevisionPathAllocator(revConf.getMaxFileCount(),
                                                                       revConf.getFsRevRoot(),
                                                                       unixFs,
                                                                       gc);
    }

    public FileRevisionManager(InMemoryFsImage fsImage,
                               FsConfig config,
                               UnixFs unixFs,
                               CollectorRegistry metricsRegistry,
                               FileRevisionGc gc)
            throws PathAllocationException {

        this.fsImage = fsImage;
        this.unixFs = unixFs;
        revConf = config.getFileRevisionConfig();

        Cache<String, RevisionTransaction> revisionsCache
                = Caffeine.newBuilder()
                .expireAfterAccess(revConf.getRevisionCacheTtlMs(),
                        TimeUnit.MILLISECONDS)
                .initialCapacity(revConf.getRevisionCacheInitCapacity())
                .maximumSize(revConf.getRevisionCacheMaxSize())
                .executor(Executors.newSingleThreadExecutor())
                .removalListener(revisionsRemovalListener())
                .build();

        Cache<String, FileRevisionINode> readRevCache
                = Caffeine.newBuilder()
                .expireAfterAccess(revConf.getRevisionCacheTtlMs(),
                        TimeUnit.MILLISECONDS)
                .initialCapacity(revConf.getRevisionCacheInitCapacity())
                .maximumSize(revConf.getRevisionCacheMaxSize())
                .executor(Executors.newSingleThreadExecutor())
                .removalListener(readRevisionsRemovalListener())
                .build();

        this.proposedRevs = revisionsCache.asMap();
        this.readRevs = readRevCache.asMap();
        CacheMetricsCollector cacheMetrics = new CacheMetricsCollector()
                .register(metricsRegistry);
        cacheMetrics.addCache("new_rev_cache", revisionsCache);
        cacheMetrics.addCache("read_rev_cache", readRevCache);

        this.gc = gc;
        this.fileRevisionPathAllocator = new FileRevisionPathAllocator(revConf.getMaxFileCount(),
                revConf.getFsRevRoot(),
                unixFs,
                gc);
    }

    @Override
    public void close() throws Exception {
        readRevs.clear();
        proposedRevs.clear();
        gc.close();
    }

    /**
     * Create new proposed revision transaction.
     *
     * @param file File inode new revision will be created for
     *
     * @return Revision transaction ID
     *
     * @throws RevisionTranIdAlreadyExists
     * @throws PathOperationException
     */
    public String openNewTran(FileINode file)
            throws RevisionTranIdAlreadyExists, PathOperationException {

        Id fileRevId = Id.len16();
        final Path sysPath = fileRevisionPathAllocator.allocate(fileRevId.bytes());
        final ProposedFileRevisionINode revisionInode
                = new ProposedFileRevisionINode(fileRevId, sysPath, file);
        RevisionTransaction revTran = RevisionTransaction.builder()
                                                         .withTranId(generateTranId())
                                                         .withProposedRevInode(revisionInode)
                                                         .withFsImage(fsImage)
                                                         .build();
        if (proposedRevs.putIfAbsent(revTran.id(), revTran) != null) {
            throw new RevisionTranIdAlreadyExists();
        }
        return revTran.id();
    }

    /**
     * Create revision read transaction
     *
     * @param revInode
     *
     * @return Transaction ID
     *
     * @throws PathOperationException
     */
    public String openReadTran(FileRevisionINode revInode)
        throws RevisionTranIdAlreadyExists {

        requireNonNull(revInode);

        final String tranId = generateTranId();
        if (readRevs.putIfAbsent(tranId, revInode) != null) {
            throw new RevisionTranIdAlreadyExists();
        }
        return tranId;
    }

    /**
     * Find revision transaction by it ID and move this transaction into "in use" state until it
     * committed/rollback.
     *
     * @param tranId Tran ID
     *
     * @return
     */
    public Tuple2<RevisionTransaction, FileChannel> getNewTran(String tranId)
            throws IOException, RevisionTranIdNotFound {

        final RevisionTransaction tran = proposedRevs.remove(tranId);
        if (tran == null) {
            throw new RevisionTranIdNotFound();
        }
        final FileChannel fileChannel = gc.openFile(tran.revisionInode().sysPath(),
                                                        StandardOpenOption.CREATE,
                                                        StandardOpenOption.WRITE,
                                                        StandardOpenOption.TRUNCATE_EXISTING);
        return Tuple.of(tran, fileChannel);
    }

    /**
     * Get revision read transaction
     *
     * @param tranId Transaction ID
     *
     * @return Readable file revision
     *
     * @throws RevisionTranIdNotFound
     */
    public Tuple2<FileRevisionINode, FileChannel> getReadTran(String tranId)
            throws RevisionTranIdNotFound, IOException {

        final FileRevisionINode revInode = readRevs.remove(tranId);
        if (revInode == null) {
            throw new RevisionTranIdNotFound();
        }
        final FileChannel fileChannel = gc.openFile(revInode.sysPath(),
                                                        StandardOpenOption.READ);
        return Tuple.of(revInode, fileChannel);
    }

    /**
     * Remove revision file in underlying FS
     * @param revSysPath
     */
    public void collect(Path revSysPath) {
        gc.collect(revSysPath);
    }

    private String generateTranId() {
        return UUID.randomUUID().toString();
    }

    private RemovalListener<String, RevisionTransaction> revisionsRemovalListener() {
        return (tranId, tran, cause) -> {
            if (cause == RemovalCause.EXPLICIT) {
                return;
            }
            LOGGER.info("Proposed file revision was evicted. Cause: {}. "
                        + "This indicate that user try create new file revision but "
                        + "will not upload any data to it(or upload failed by some reason), "
                        + "hence revision will be removed",
                        cause);
            if (cause == RemovalCause.SIZE) {
                LOGGER.warn("Proposed file revision was evicted because of cache size. Try to "
                            + "increase max file revision cache size to prevent this kind of "
                            + "errors.");
            }
        };
    }

    private RemovalListener<String, FileRevisionINode> readRevisionsRemovalListener() {
        return (tranId, inode, cause) -> {
            if (cause == RemovalCause.EXPLICIT) {
                return;
            }
            LOGGER.info("Revision read handle was evicted. Cause: {}. "
                        + "This indicate that user try read file revision but "
                        + "will not issue read request during some configured time({}ms)",
                        cause, revConf.getRevisionCacheTtlMs());
            if (cause == RemovalCause.SIZE) {
                LOGGER.warn("Read revision handle was evicted because of cache size. Try to "
                            + "increase max file revision cache size to prevent this kind of "
                            + "errors.");
            }
        };
    }
}
