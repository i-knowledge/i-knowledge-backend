package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;

import java.util.Optional;

final class RemoveRevisionOp extends BaseOp {

    private final HBaseFsImageStore backend;
    private final FsMap fsMap;

    public RemoveRevisionOp(HBaseFsImageStore backend,
                            FsMap fsMap,
                            int readLockTimeoutMs,
                            int writeLockTimeoutMs) {

        super(readLockTimeoutMs, writeLockTimeoutMs);
        this.backend = backend;
        this.fsMap = fsMap;
    }

    /**
     * Perform file revision removal.
     *
     * @param file
     *
     * @throws PathOperationException
     * @throws StoreException
     */
    public FileRevisionINode perform(FileINode file, long revision)
            throws PathOperationException, StoreException {

       return tryWriteLock(file, () -> {
           throwIfDetached(file);
           final Optional<FileRevisionINode> rev = file.findRev(revision);
           if (!rev.isPresent()) {
               throw new PathNotExistsException(file, revision);
           }
           final FileRevisionINode revInode = rev.get();
           backend.removeInode(revInode);
           updateInMemoryImage(revInode);
           return revInode;
       });
    }

    private void updateInMemoryImage(final FileRevisionINode curNode) {
        curNode.parent().removeChild(curNode);
        fsMap.remove(curNode);
    }
}
