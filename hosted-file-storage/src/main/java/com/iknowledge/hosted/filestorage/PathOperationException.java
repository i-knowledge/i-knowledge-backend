package com.iknowledge.hosted.filestorage;

import java.io.IOException;

public abstract class PathOperationException extends IOException {

    public PathOperationException() {
        super();
    }

    public PathOperationException(final String message) {
        super(message);
    }

    public PathOperationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PathOperationException(final Throwable cause) {
        super(cause);
    }
}
