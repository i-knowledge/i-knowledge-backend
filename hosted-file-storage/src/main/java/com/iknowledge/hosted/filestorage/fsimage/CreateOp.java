package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;

import java.util.function.BiFunction;

final class CreateOp extends BaseOp {

    private final HBaseFsImageStore backend;
    private final FsMap fsMap;

    public CreateOp(HBaseFsImageStore backend,
                    FsMap fsMap,
                    int readLockTimeoutMs,
                    int writeLockTimeoutMs) {

        super(readLockTimeoutMs, writeLockTimeoutMs);
        this.backend = backend;
        this.fsMap = fsMap;
    }

    public <T extends UserLevelINode> T
            perform(DirectoryINode parentDir,
                    String inodeName,
                    BiFunction<String, DirectoryINode, T> inodeGenerator)
            throws PathOperationException, StoreException {

        final T createdInode
            = tryWriteLock(parentDir,
                   () -> {
                        throwIfNotAttached(parentDir);
                        final T newInode = inodeGenerator.apply(inodeName, parentDir);
                        throwIfHasSameChild(parentDir, newInode);

                        if (!fsMap.put(newInode)) {
                            throw new PathExistsException("inode with ID="
                                                          + newInode.id()
                                                          + " already exists");
                        }

                        try {
                            backend.addInode(newInode);
                        }
                        catch (Throwable e) {
                            fsMap.remove(newInode);
                            throw e;
                        }
                       parentDir.addChild(newInode);
                       return newInode;
                   });
        return createdInode;
    }
}
