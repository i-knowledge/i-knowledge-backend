package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;

final class NioHttpServerInitializer extends ChannelInitializer<NioSocketChannel> {

    private final HttpServerChannelInitializer channelInitializer;

    public NioHttpServerInitializer(final HttpServerConfig config,
                                    final FileSystem fs) {

        channelInitializer = new HttpServerChannelInitializer(config, fs);
    }

    @Override
    protected void initChannel(final NioSocketChannel ch) throws Exception {
        channelInitializer.initChannel(ch);
    }
}
