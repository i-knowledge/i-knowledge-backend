package com.iknowledge.hosted.filestorage;

/**
 * Path locked by other user/operation
 */
public class PathLockedException extends PathOperationException {

    public PathLockedException() {
        super();
    }

    public PathLockedException(final String message) {
        super(message);
    }

    public PathLockedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PathLockedException(final Throwable cause) {
        super(cause);
    }
}
