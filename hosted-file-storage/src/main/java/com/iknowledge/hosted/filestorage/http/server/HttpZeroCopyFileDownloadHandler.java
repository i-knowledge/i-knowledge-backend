package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.ReadableFileRevHandle;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionChannel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.DefaultFileRegion;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.DefaultLastHttpContent;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import org.slf4j.LoggerFactory;

import java.nio.channels.FileChannel;

public class HttpZeroCopyFileDownloadHandler extends HttpFileDownloadHandlerBase {

    public HttpZeroCopyFileDownloadHandler(ReadableFileRevHandle revHandle,
                                           HttpHeaders httpHeaders,
                                           HttpVersion httpVersion) {
        super(revHandle,
              httpHeaders,
              httpVersion,
              LoggerFactory.getLogger(HttpZeroCopyFileDownloadHandler.class));
    }

    @Override
    public void channelRead(final ChannelHandlerContext ctx, final Object msg) throws Exception {

        final long fileSize = revHandle.channel().size();
        final HttpHeaders headers = new DefaultHttpHeaders()
                                        .add(HttpHeaderNames.CONTENT_ENCODING,
                                             HttpHeaderValues.APPLICATION_OCTET_STREAM)
                                        .add(HttpHeaderNames.CONTENT_LENGTH,
                                             revHandle.channel().size());
        final FileChannel fileChannel = getFileChannel();

        ctx.write(new DefaultHttpResponse(httpVersion,
                                          HttpResponseStatus.OK,
                                          headers));
        ctx.write(new DefaultFileRegion(fileChannel, 0, fileSize));
        ctx.writeAndFlush(new DefaultLastHttpContent())
           .addListener(future -> {
                            closeResources();
                            logResult(future);
                       });
    }

    private FileChannel getFileChannel() {
        // WARNING: Netty epoll zero-copy only support default JDK FileChannel impl
        // otherwise, sendfile syscall return errno EBADF and Netty close connection
        final FileChannel fileChannel;
        if (revHandle.channel() instanceof FileRevisionChannel) {
            fileChannel = ( (FileRevisionChannel) revHandle.channel() ).fileChannel();
        }
        else {
            fileChannel = revHandle.channel();
        }
        return fileChannel;
    }

}
