package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.ReadableFileRevHandle;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;

import static com.google.common.base.Throwables.getStackTraceAsString;

abstract class HttpFileDownloadHandlerBase extends ChannelInboundHandlerAdapter {

    protected final ReadableFileRevHandle revHandle;
    protected final HttpHeaders httpHeaders;
    protected final HttpVersion httpVersion;
    protected final Logger logger;

    HttpFileDownloadHandlerBase(ReadableFileRevHandle revHandle,
                                HttpHeaders httpHeaders,
                                HttpVersion httpVersion,
                                Logger logger) {

        this.logger = logger;
        this.revHandle = revHandle;
        this.httpHeaders = httpHeaders;
        this.httpVersion = httpVersion;
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx) {
        closeResources();
    }

    @Override
    public boolean isSharable() {
        return false;
    }

    protected void closeResources() {
        revHandle.closeAsync()
                 .whenComplete((ignore, ex) -> {
                     if (ex != null) {
                         //TODO: metrics
                         logger.warn("Error during closing of file revision "
                                     + "read transaction: {}{}",
                                     System.lineSeparator(),
                                     getStackTraceAsString(ex));
                     }
                 });
    }

    protected void logResult(Future<? super Void> contentFuture) {
        if (contentFuture.isDone() && !contentFuture.isSuccess()) {
            //TODO: metrics
            if (logger.isTraceEnabled()) {
                logger.trace("Fail to write file revision response "
                             + "to channel{}{}",
                             System.lineSeparator(),
                             getStackTraceAsString(contentFuture.cause()));
            }
        }
    }
}
