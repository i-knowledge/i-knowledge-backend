package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

/**
 * Exception raised when inode in unknown state and requested operation cannot be performed in
 * this state.
 */
public class PathHasUnknownStateException extends PathOperationException {

    public PathHasUnknownStateException(INode inode) {
        super(inode.toString());
    }
}
