package com.iknowledge.hosted.filestorage;

/**
 * Invalid path value: path which cannot be used for particular operation or has invalid format
 */
public class InvalidPathException extends PathOperationException {

    public InvalidPathException() {
        super();
    }

    public InvalidPathException(final String message) {
        super(message);
    }

    public InvalidPathException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidPathException(final Throwable cause) {
        super(cause);
    }
}
