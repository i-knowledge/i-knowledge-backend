package com.iknowledge.hosted.filestorage.http.server;

public final class EpollConfig {

    private final int tcpDeferAcceptSec;
    private final int tcpFastOpenQueue;

    private EpollConfig(final Builder builder) {
        tcpDeferAcceptSec = builder.tcpDeferAcceptSec;
        tcpFastOpenQueue = builder.tcpFastOpenQueue;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public int tcpFastOpenQueue() {
        return tcpFastOpenQueue;
    }

    public int tcpDeferAcceptSec() {
        return tcpDeferAcceptSec;
    }

    public static final class Builder {

        private int tcpDeferAcceptSec = 10;
        private int tcpFastOpenQueue = 5;

        private Builder() {}

        public Builder withTcpDeferAcceptSec(final int tcpDeferAcceptSec) {
            this.tcpDeferAcceptSec = tcpDeferAcceptSec;
            return this;
        }

        public Builder withTcpFastOpenQueue(final int tcpFastOpenQueue) {
            this.tcpFastOpenQueue = tcpFastOpenQueue;
            return this;
        }

        public EpollConfig build() {
            return new EpollConfig(this);
        }
    }
}
