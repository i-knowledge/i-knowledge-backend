package com.iknowledge.hosted.filestorage;

/**
 * Path can not be allocated in the file system
 */
public final class PathAllocationException extends PathOperationException {
    public PathAllocationException() {
        super();
    }

    public PathAllocationException(String message) {
        super(message);
    }

    public PathAllocationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PathAllocationException(Throwable cause) {
        super(cause);
    }
}
