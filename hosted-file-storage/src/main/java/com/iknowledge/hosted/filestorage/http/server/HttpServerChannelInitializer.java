package com.iknowledge.hosted.filestorage.http.server;

import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentDecompressor;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.HttpServerKeepAliveHandler;

final class HttpServerChannelInitializer {

    private final HttpServerConfig config;
    private final FileSystem fs;

    public HttpServerChannelInitializer(final HttpServerConfig config, final FileSystem fs) {
        this.config = config;
        this.fs = fs;
    }

    public void initChannel(final SocketChannel ch) {
        //TODO: idle state can happen when file downloading using zero-copy,
        // this doesn't break downloading, but close connection after finish
//        ch.pipeline().addLast(new IdleStateHandler(0,
//                                                   0,
//                                                   config.getIdleConnectionTimeoutMs(),
//                                                   TimeUnit.MILLISECONDS));
//        ch.pipeline().addLast(new ChannelIdleHandler(config.getIdleConnectionTimeoutMs()));

        ch.pipeline().addLast(new HttpServerCodec());
        ch.pipeline().addLast(new HttpServerKeepAliveHandler());
        ch.pipeline().addLast(new HttpContentDecompressor());

        ch.pipeline().addLast(RouterHandler.class.getSimpleName(),
                              new RouterHandler(fs, config));
    }
}
