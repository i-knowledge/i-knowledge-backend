package com.iknowledge.hosted.filestorage.http.server;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.FileRegion;
import io.netty.handler.stream.ChunkedNioFile;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ChannelIdleHandler extends ChannelDuplexHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelIdleHandler.class);

    private final long idleConnectionTimeoutMs;
    private long fileWriteTs = System.currentTimeMillis();

    public ChannelIdleHandler(final long idleConnectionTimeoutMs) {
        this.idleConnectionTimeoutMs = idleConnectionTimeoutMs;
    }

    @Override
    public void userEventTriggered(final ChannelHandlerContext ctx, final Object evt)
        throws Exception {

        if (((IdleStateEvent)evt).state() == IdleState.ALL_IDLE && !hasRecentFileWrite()) {
            LOGGER.trace("Connection {} closed, no activity during specified timeout",
                         ctx.channel());
            ctx.close();
        }
    }

    @Override
    public void write(final ChannelHandlerContext ctx, final Object msg,
                      final ChannelPromise promise)
        throws Exception {

        // zero-copy file download doesn't raise periodic writes to channel
        // and hence we have to catch this special use cases
        if (msg instanceof FileRegion || msg instanceof ChunkedNioFile) {
            fileWriteTs = System.currentTimeMillis();
            promise.addListener(future -> {
                fileWriteTs = System.currentTimeMillis();
            });
        }
        super.write(ctx, msg, promise);
    }

    private boolean hasRecentFileWrite() {
        return System.currentTimeMillis() - fileWriteTs <= idleConnectionTimeoutMs;
    }
}
