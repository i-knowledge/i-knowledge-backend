package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;

import java.io.IOException;

@FunctionalInterface
public interface FsBiConsumer<T, T1> {

    void consume(T instance1, T1 instance2) throws IOException, StoreException;
}
