package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathLockedException;
import com.iknowledge.hosted.filestorage.PathModifiedException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ContainerINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.MutableINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.State;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;
import io.vavr.control.Either;

import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;

public abstract class BaseOp {

    protected final int readLockTimeout;
    protected final int writeLockTimeout;

    public BaseOp(final int readLockTimeout,
                  final int writeLockTimeout) {

        this.readLockTimeout = readLockTimeout;
        this.writeLockTimeout = writeLockTimeout;
    }

    protected INodeSnapshot<? extends UserLevelINode> asSnapshot(final UserLevelINode foundInode,
                                                                 final Path searchPath)
        throws PathOperationException {

        return tryReadLock(foundInode,
                           (PathSupplier<INodeSnapshot<? extends UserLevelINode>>)() -> {

                               if (!foundInode.path().equals(searchPath)) {
                                   throw new PathNotExistsException(searchPath.toString());
                               }

                               if (isUsualDir(foundInode)) {
                                   return new DirINodeSnapshot(asUsualDir(foundInode));
                               }
                               else if (isUsualFile(foundInode)) {
                                   return new FileINodeSnapshot(asUsualFile(foundInode));
                               }
                               else {
                                   throw new PathNotExistsException(searchPath.toString());
                               }
                           });
    }

    protected void throwIfHasSameChild(final DirectoryINode newParent, final INode inodeName)
            throws PathExistsException {

        final Optional<? extends INode> existingInode = newParent.findChildByName(inodeName.name());
        if (existingInode.isPresent()) {
            throw new PathExistsException(existingInode.get());
        }
    }

    protected void throwIfNotAttached(final INode inode) throws PathNotExistsException {
        if (inode instanceof MutableINode && ((MutableINode)inode).state() != State.ATTACHED) {
            throw new PathNotExistsException(inode);
        }
    }

    protected void throwIfModified(final INodeSnapshot inodeSnap) throws PathModifiedException {
        if (inodeSnap.isModified()) {
            throw new PathModifiedException(inodeSnap);
        }
    }

    protected boolean isNotAttached(final INode inode) {
        return inode instanceof MutableINode
               && ((MutableINode)inode).state() != State.ATTACHED;
    }

    protected void throwIfDetached(final INode inode) throws PathNotExistsException {
        if (inode instanceof MutableINode && ((MutableINode)inode).state() == State.DETACHED) {
            throw new PathNotExistsException(inode);
        }
    }

    protected boolean isDetached(final INode inode) {
        return inode instanceof MutableINode
                && ((MutableINode)inode).state() == State.DETACHED;
    }

    protected boolean isDetached(final MutableINode inode) {
        return inode.state() == State.DETACHED;
    }

    protected <T extends MutableINode> boolean hasValidChildren(ContainerINode<T> inode) {
        for (MutableINode child : inode.children()) {
            if (child.state() != State.DETACHED) {
                return true;
            }
        }
        return false;
    }

    protected <T extends INode> boolean allChildrenIsUserLevel(ContainerINode<T> inode) {
        for (INode child : inode.children()) {
            if (!(child instanceof UserLevelINode)) {
                return false;
            }
        }
        return true;
    }

    protected <T> Either<PathOperationException, T> tryReadLock(INode inodeToLock,
                                                                Supplier<T> lockedAction) {

        return INodeLocks.tryReadLock(inodeToLock,
                                      readLockTimeout,
                                      TimeUnit.MILLISECONDS,
                                      lockedAction);
    }

    protected <T> T tryReadLock(INode inodeToLock, FsSupplier<T> lockedAction)
            throws PathOperationException, StoreException {

        return INodeLocks.tryReadLock(inodeToLock,
                                      readLockTimeout,
                                      TimeUnit.MILLISECONDS,
                                      lockedAction);
    }

    protected void tryReadLock(INode inodeToLock, FsRunnable lockedAction)
            throws PathOperationException, StoreException {

        INodeLocks.tryReadLock(inodeToLock,
                               readLockTimeout,
                               TimeUnit.MILLISECONDS,
                               lockedAction);
    }

    protected void tryReadLock(INode inodeToLock, PathRunnable lockedAction)
            throws PathOperationException, StoreException {

        INodeLocks.tryReadLock(inodeToLock,
                               readLockTimeout,
                               TimeUnit.MILLISECONDS,
                               lockedAction);
    }

    protected <T> T tryReadLock(INode inodeToLock, PathSupplier<T> lockedAction)
            throws PathOperationException {

        return INodeLocks.tryReadLock(inodeToLock,
                               readLockTimeout,
                               TimeUnit.MILLISECONDS,
                               lockedAction);
    }

    protected void tryReadLock(INode inodeToLock, Runnable lockedAction)
            throws PathLockedException {

        INodeLocks.tryReadLock(inodeToLock,
                               readLockTimeout,
                               TimeUnit.MILLISECONDS,
                               lockedAction);
    }

    protected void tryWriteLock(INode inodeToLock, FsRunnable lockedAction)
            throws PathOperationException, StoreException {

        INodeLocks.tryWriteLock(inodeToLock,
                                writeLockTimeout,
                                TimeUnit.MILLISECONDS,
                                lockedAction);
    }

    protected void tryWriteLock(INode inodeToLock, Runnable lockedAction)
            throws PathLockedException {

        INodeLocks.tryWriteLock(inodeToLock,
                                writeLockTimeout,
                                TimeUnit.MILLISECONDS,
                                lockedAction);
    }

    protected <T> T tryWriteLock(INode inodeToLock, FsSupplier<T> lockedAction)
            throws PathOperationException, StoreException {

        return INodeLocks.tryWriteLock(inodeToLock,
                                writeLockTimeout,
                                TimeUnit.MILLISECONDS,
                                lockedAction);
    }

    protected void failFastWriteLock(INode inodeToLock, FsRunnable lockedAction)
        throws PathOperationException, StoreException {

        if (!inodeToLock.lock().writeLock().tryLock()) {
            throw new PathLockedException();
        }

        try {
            lockedAction.run();
        }
        finally {
            inodeToLock.lock().writeLock().unlock();
        }
    }

    protected <T> T failFastReadLock(INode inodeToLock, FsSupplier<T> lockedAction)
        throws PathOperationException, StoreException {

        if (!inodeToLock.lock().readLock().tryLock()) {
            throw new PathLockedException();
        }

        try {
            return lockedAction.get();
        }
        finally {
            inodeToLock.lock().readLock().unlock();
        }
    }
}
