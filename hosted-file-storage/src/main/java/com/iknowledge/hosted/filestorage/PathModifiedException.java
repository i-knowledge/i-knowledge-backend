package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;

public class PathModifiedException extends PathOperationException {

    public PathModifiedException(final INodeSnapshot inodeSnap) {
        super(inodeSnap.toString());
    }
}
