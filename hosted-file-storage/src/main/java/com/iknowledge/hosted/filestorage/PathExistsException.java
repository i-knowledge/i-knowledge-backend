package com.iknowledge.hosted.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

/**
 * Path already exists in FS image
 */
public class PathExistsException extends PathOperationException {

    public PathExistsException() {
        super();
    }

    public PathExistsException(final INode existingInode) {
        super(existingInode.toString());
    }

    public PathExistsException(final String message) {
        super(message);
    }

    public PathExistsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PathExistsException(final Throwable cause) {
        super(cause);
    }
}
