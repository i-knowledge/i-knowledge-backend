package com.iknowledge.hosted.filestorage.fsimage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathHasChildrenException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.filestorage.UnknownNodeTypeException;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsMap;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ContainerINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeSnapshot;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.UserLevelINode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asFileSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isDirSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isFileSnapshot;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;

final class RemoveOp extends BaseOp {

    private final HBaseFsImageStore backend;
    private final FsMap fsMap;

    public RemoveOp(HBaseFsImageStore backend,
                    FsMap fsMap,
                    int readLockTimeoutMs,
                    int writeLockTimeoutMs) {

        super(readLockTimeoutMs, writeLockTimeoutMs);
        this.backend = backend;
        this.fsMap = fsMap;
    }

    /**
     * Perform file removal.
     *
     * @param file File inode to remove
     *
     * @throws PathOperationException
     * @throws StoreException
     */
    public void perform(FileINodeSnapshot file) throws PathOperationException, StoreException {
        removeSnap(file, file.inode());
    }

    /**
     * Perform recursive removal of inode.
     *
     * @throws PathOperationException
     * @throws StoreException
     */
    public void perform(DirINodeSnapshot dirSnap)
        throws PathOperationException, StoreException {

        Deque<INodeSnapshot<? extends UserLevelINode>> pendingNodes = new ArrayDeque<>();
        pendingNodes.push(dirSnap);

        while (!pendingNodes.isEmpty()) {
            INodeSnapshot<? extends UserLevelINode> curNode = pendingNodes.pop();

            if (isFileSnapshot(curNode)) {
                FileINodeSnapshot fileSnapshot = asFileSnapshot(curNode);
                removeSnap(fileSnapshot, fileSnapshot.inode());
            }
            else if (isDirSnapshot(curNode)) {
                removeDir(asDirSnapshot(curNode), pendingNodes);
            }
            else {
                throw new UnknownNodeTypeException(curNode.getClass().getSimpleName());
            }
        }
    }

    private void removeDir(final DirINodeSnapshot curDirNode,
                           final Deque<INodeSnapshot<? extends UserLevelINode>> pendingNodes)
            throws PathOperationException, StoreException {

        final Boolean canRemove = failFastReadLock(curDirNode.inode(), (FsSupplier<Boolean>) () -> {
            throwIfModified(curDirNode);

            if (isDetached(curDirNode.inode())) {
                return true;
            }
            else if (!hasValidChildren(curDirNode.inode())) {
                return true;
            }
            else {
                pendingNodes.push(curDirNode);
                for (UserLevelINode inode : curDirNode.inode().children()) {
                    if (isUsualFile(inode)) {
                        pendingNodes.push(new FileINodeSnapshot(asUsualFile(inode)));
                    }
                    else if (isUsualDir(inode)) {
                        pendingNodes.push(new DirINodeSnapshot(asUsualDir(inode)));
                    }
                    else {
                        throw new UnknownNodeTypeException("Can't remove inode of type: "
                                                           + inode.getClass().getSimpleName());
                    }
                }
                return false;
            }
        });

        if (canRemove) {
            removeSnap(curDirNode, curDirNode.inode());
        }
    }

    private <T extends ContainerINode & UserLevelINode>
        void removeSnap(final INodeSnapshot<? extends UserLevelINode> curNode, T inode)
        throws StoreException, PathOperationException {

        // lock order is important
        failFastWriteLock(curNode.inode(), (FsRunnable) () -> {
            failFastWriteLock(curNode.inode().parent(), (FsRunnable) () -> {
                throwIfModified(curNode);
                removeInode(inode);
            });
        });
    }

    private <T extends ContainerINode & UserLevelINode> void removeInode(final T curNode)
        throws PathHasChildrenException, StoreException {

        if (isDetached(curNode) || isDetached(curNode.parent())) {
            return;
        }

        if (isUsualDir(curNode) && hasValidChildren(asUsualDir(curNode))) {
            throw new PathHasChildrenException(curNode);
        }

        // remove file with all revisions, for dirs simply remove dir inode
        final boolean usualFile = isUsualFile(curNode);
        if (usualFile) {
            backend.removeContainerInode(asUsualFile(curNode));
        }
        else { //remove empty dir
            backend.removeInode(curNode);
        }
        curNode.detach();
        removeInMemImg(curNode);
        // file revision removal from in-memory image not require any lock(revisions is
        // readonly). File inode write lock is sufficient to access it.
        if (usualFile) {
            final Iterable<FileRevisionINode> revisions
                = new ArrayList<>(asUsualFile(curNode).children());
            revisions.forEach(this::removeInMemImg);
        }
    }

    private void removeInMemImg(final INode curNode) {
        curNode.parent().removeChild(curNode);
        fsMap.remove(curNode);
    }
}
