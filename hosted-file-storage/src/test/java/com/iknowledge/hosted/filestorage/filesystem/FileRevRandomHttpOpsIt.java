package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.filestorage.fsimage.FileRevisionConfig;
import org.assertj.core.api.exception.RuntimeIOException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class FileRevRandomHttpOpsIt extends FileRevHttpOpsItBase {

    private AtomicInteger uploadedFileCount;
    private ConcurrentHashMap<Integer, SavedFileRevData> uploadedFiles;
    private ConcurrentHashMap<Integer, FileContentSize> contentSizeMap;

    @BeforeClass
    @Override
    public void setUp() throws Exception {
        super.setUp();
        uploadedFileCount = new AtomicInteger();
        uploadedFiles = new ConcurrentHashMap<>();
        contentSizeMap = new ConcurrentHashMap<>();
        contentSizeMap.put(1, FileContentSize.SMALL);
        contentSizeMap.put(2, FileContentSize.MEDIUM);
        contentSizeMap.put(3, FileContentSize.HUGE);
    }

    @AfterMethod
    public void tearDownTest() {
        uploadedFileCount.set(0);
        uploadedFiles.clear();
    }

    public FileRevisionConfig initFileRevisionConfig() {
        return FileRevisionConfig.builder()
                                 .withFsRevRoot("filerev_v1")
                                 .withMaxFileCount(10000)
                                 .withRevisionCacheTtlMs(10000000)
                                 .withRevisionCacheInitCapacity(1000)
                                 .withRevisionCacheMaxSize(1000)
                                 .withReservedTenthOfFreeMemory(6)
                                 .withStartAtHour(LocalTime.now().getHour()) // to run immediately
                                 .withRunEveryTimeUnit(1)
                                 .withTimeUnit(TimeUnit.HOURS)
                                 .build();
    }

    @Test(dataProvider = "withEpoll")
    public void testRandomHttpOps(final boolean withEpoll) throws Exception {
        final int iterCount = 3000;
        performRandomOps(withEpoll, iterCount);
    }

    @Test(dataProvider = "withEpoll")
    public void testRandomHttpOpsForLargeFiles(final boolean withEpoll) throws Exception {
        final int iterCount = 50;
        contentSizeMap.put(4, FileContentSize.VERY_HUGE);
        try {
            performRandomOps(withEpoll, iterCount);
        }
        finally {
            contentSizeMap.remove(4);
        }
    }

    private void performRandomOps(final boolean withEpoll, final int iterCount)
        throws InterruptedException, java.util.concurrent.ExecutionException {
        Runnable uploadRunnable = () -> {
            try {
                uploadFile(withEpoll);
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during upload op.", e);
            }
        };
        Runnable downloadRunnable = () -> {
            try {
                downloadFile(withEpoll);
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during download op.", e);
            }
        };

        List<CompletableFuture> futures = new ArrayList<>(iterCount);

        for (int i = 0; i < iterCount; i++) {
            final boolean randomOp = getRandomBool();
            if (randomOp) {
                futures.add(submit(uploadRunnable));
            } else {
                futures.add(submit(downloadRunnable));
            }
        }
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                .get();

        awaitFutures(futures);
    }

    private void downloadFile(final boolean withEpoll) throws Exception {
        final SavedFileRevData uploadedFile = getRandomUploadedFile();
        if (uploadedFile == null) {
            return;
        }
        // TODO: after fixing gzip downloading set the last parameter to the random one
        download(uploadedFile.filePath, uploadedFile.sysPath, withEpoll, false);
    }

    private SavedFileRevData getRandomUploadedFile() {
        if (uploadedFiles.isEmpty()) {
            return null;
        }
        final int size = uploadedFiles.size();
        final int randomInt;
        if (size == 1) {
            randomInt = 1;
        } else {
            randomInt = getRandomInt(1, size);
        }
        return uploadedFiles.get(randomInt);
    }

    private void uploadFile(final boolean withEpoll) throws Exception {
        boolean withGzip = getRandomBool();
        final FileContentSize randomContentSize = getRandomContentSize();
        final SavedFileRevData fileRevData = upload(withEpoll, withGzip, randomContentSize);
        final int uploadedFileNumber = uploadedFileCount.incrementAndGet();
        uploadedFiles.put(uploadedFileNumber, fileRevData);
    }

    private FileContentSize getRandomContentSize() {
        final int sizesCount = contentSizeMap.size();
        final int sizeNumber = getRandomInt(1, sizesCount);
        return contentSizeMap.get(sizeNumber);
    }
}
