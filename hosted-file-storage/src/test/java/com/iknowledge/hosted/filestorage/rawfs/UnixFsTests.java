package com.iknowledge.hosted.filestorage.rawfs;

import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public final class UnixFsTests extends UnixFsTestsBase {

    @Test
    public void testCreateDirectory() throws Exception {
        final Path directoryPath = getNewDirPath();

        // double check for existence - it is for checking unixFs.exists method
        assertThat(Files.exists(directoryPath))
                .isFalse();
        assertThat(unixFs.exists(directoryPath))
                .isFalse();

        unixFs.createDirectory(directoryPath);

        // double check for existence - it is for checking unixFs.exists method
        assertThat(Files.exists(directoryPath))
                .isTrue();
        assertThat(unixFs.exists(directoryPath))
                .isTrue();
    }

    @Test
    public void testCreateFile() throws Exception {
        final Path filePath = getNewFilePath();

        // double check for existence - it is for checking unixFs.exists method
        assertThat(Files.exists(filePath))
                .isFalse();
        assertThat(unixFs.exists(filePath))
                .isFalse();

        unixFs.createFile(filePath);

        // double check for existence - it is for checking unixFs.exists method
        assertThat(Files.exists(filePath))
                .isTrue();
        assertThat(unixFs.exists(filePath))
                .isTrue();
    }

    @Test(expectedExceptions = NoSuchFileException.class)
    public void testCreateDirectoryWithWrongPath() throws Exception {
        final Path directoryPath = Paths.get(getRandomDirectoryName(), getRandomDirectoryName());
        unixFs.createDirectory(directoryPath);
    }

    @Test(expectedExceptions = NoSuchFileException.class)
    public void testCreateFileWithWrongPath() throws Exception {
        final Path filePath = Paths.get(getRandomDirectoryName(), getRandomFileName());
        unixFs.createFile(filePath);
    }

    @Test
    public void testRemoveDirectory() throws Exception {
        final Path directoryPath = getNewDirPath();

        boolean isRemoved = unixFs.remove(directoryPath);
        assertThat(isRemoved)
                .isFalse();

        unixFs.createDirectory(directoryPath);
        assertThat(unixFs.exists(directoryPath))
                .isTrue();

        isRemoved = unixFs.remove(directoryPath);
        assertThat(isRemoved)
                .isTrue();

        assertThat(unixFs.exists(directoryPath))
                .isFalse();
    }

    @Test
    public void testRemoveFile() throws Exception {
        final Path filePath = getNewFilePath();

        boolean isRemoved = unixFs.remove(filePath);
        assertThat(isRemoved)
                .isFalse();

        unixFs.createFile(filePath);
        assertThat(unixFs.exists(filePath))
                .isTrue();
        isRemoved = unixFs.remove(filePath);
        assertThat(isRemoved)
                .isTrue();

        assertThat(unixFs.exists(filePath))
                .isFalse();
    }

    @Test
    public void testMoveDirectory() throws Exception {
        final Path srcDirectoryPath = getNewDirPath();
        final Path destDirectoryPath = getNewDirPath();

        unixFs.createDirectory(srcDirectoryPath);
        unixFs.createDirectory(destDirectoryPath);
        final Path movingDirPath = destDirectoryPath.resolve(srcDirectoryPath.getFileName());

        unixFs.move(srcDirectoryPath, movingDirPath);

        assertThat(unixFs.exists(srcDirectoryPath))
                .isFalse();

        assertThat(unixFs.exists(movingDirPath))
                .isTrue();
    }

    @Test
    public void testMoveFile() throws Exception {
        final Path destDirectoryPath = getNewDirPath();
        final Path filePath = getNewFilePath();

        unixFs.createFile(filePath);
        unixFs.createDirectory(destDirectoryPath);
        final Path movedFilePath = destDirectoryPath.resolve(filePath.getFileName());

        unixFs.move(filePath, movedFilePath);

        assertThat(unixFs.exists(filePath))
                .isFalse();
        assertThat(unixFs.exists(movedFilePath))
                .isTrue();
    }

    @Test
    public void testRenameDirectory() throws Exception {
        final Path srcDirectoryPath = getNewDirPath();
        final Path destDirectoryPath = getNewDirPath();

        unixFs.createDirectory(srcDirectoryPath);
        final Path srcInnerDirectoryPath = srcDirectoryPath.resolve(getRandomDirectoryName());
        unixFs.createDirectory(srcInnerDirectoryPath);
        unixFs.createDirectory(destDirectoryPath);

        unixFs.move(srcDirectoryPath, destDirectoryPath);

        assertThat(unixFs.exists(srcDirectoryPath))
                .isFalse();
        assertThat(unixFs.exists(srcInnerDirectoryPath))
                .isFalse();

        assertThat(unixFs.exists(destDirectoryPath))
                .isTrue();
        assertThat(unixFs.exists(destDirectoryPath.resolve(srcDirectoryPath.getFileName())))
                .isFalse();
        assertThat(unixFs.exists(destDirectoryPath.resolve(srcInnerDirectoryPath.getFileName())))
                .isTrue();
    }

    @Test(expectedExceptions = FileSystemException.class,
            expectedExceptionsMessageRegExp = ".*Directory not empty")
    public void testRenameDirectoryToNotEmptyOne() throws Exception {
        final Path srcDirectoryPath = getNewDirPath();
        final Path destDirectoryPath = getNewDirPath();

        unixFs.createDirectory(srcDirectoryPath);
        unixFs.createDirectory(srcDirectoryPath.resolve(getRandomDirectoryName()));
        unixFs.createDirectory(destDirectoryPath);
        unixFs.createFile(destDirectoryPath.resolve(getRandomFileName()));
        unixFs.move(srcDirectoryPath, destDirectoryPath);
    }

    @Test(expectedExceptions = FileSystemException.class)
    public void testMoveFileWithWrongPath() throws Exception {
        final Path destDirectoryPath = getNewDirPath();
        final Path filePath = getNewFilePath();
        unixFs.createFile(filePath);
        unixFs.createDirectory(destDirectoryPath);

        unixFs.move(filePath, destDirectoryPath);
    }

    @Test(expectedExceptions = IOException.class,
            expectedExceptionsMessageRegExp =
                    ".*Remove all data from it or set other base directory path.")
    public void testFormat() throws Exception {
        unixFs.format();
    }
}
