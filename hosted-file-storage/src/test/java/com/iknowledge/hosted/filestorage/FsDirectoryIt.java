package com.iknowledge.hosted.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.State;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Test
public final class FsDirectoryIt extends FileSystemItBase {
    private static final String TEST_ROOT_DIRECTORY = "dirTest";
    private Path testRootDirPath;


    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @Test(priority = 1)
    public void testFindRootDir() throws IOException, StoreException {
        final DirHandle dirHandle = (DirHandle) fileSystem.find(rootPath, adminUser);
        assertThat(dirHandle)
                .isNotNull();
        List<Path> rootChildren = new ArrayList<>();
        rootChildren.add(testRootDirPath);
        INodeInfo nodeInfo = new INodeInfo();
        nodeInfo.inodeClass = DirHandle.class;
        nodeInfo.name = rootPathString;
        nodeInfo.path = rootPath;
        nodeInfo.children = rootChildren;
        nodeInfo.state = State.ATTACHED;
        nodeInfo.userRights = new HashMap<>();
        nodeInfo.userGroupRights = getUserGroupRightsWithAdmin();
        validateINode(dirHandle, nodeInfo, true);
    }

    @Test(priority = 1)
    public void createDirectory() throws IOException, StoreException {
        final DirHandle dirHandle = (DirHandle) fileSystem.find(testRootDirPath, adminUser);
        assertThat(dirHandle)
                .isNotNull();
        INodeInfo nodeInfo = new INodeInfo();
        nodeInfo.inodeClass = DirHandle.class;
        nodeInfo.name = TEST_ROOT_DIRECTORY;
        nodeInfo.path = testRootDirPath;
        nodeInfo.children = new ArrayList<>();
        nodeInfo.state = State.ATTACHED;
        nodeInfo.userRights = new HashMap<>();
        nodeInfo.userGroupRights = new HashMap<>();

        validateINode(dirHandle, nodeInfo, true);

        List<Path> parentChildren = new ArrayList<>();
        parentChildren.add(dirHandle.path());
        final DirHandle parentDirHandle = (DirHandle) fileSystem.find(dirHandle.path().getParent(),
                adminUser);
        INodeInfo parentNodeInfo = new INodeInfo();
        parentNodeInfo.inodeClass = DirHandle.class;
        parentNodeInfo.name = rootPathString;
        parentNodeInfo.path = Paths.get(rootPathString);
        parentNodeInfo.children = parentChildren;
        parentNodeInfo.state = State.ATTACHED;
        parentNodeInfo.userRights = new HashMap<>();
        parentNodeInfo.userGroupRights = getUserGroupRightsWithAdmin();

        validateINode(parentDirHandle, parentNodeInfo, true);
    }

    @Test(priority = 2)
    public void removeDirectory() throws IOException, StoreException {
        String directoryName = "/t2";
        final Path path = createDirectory(directoryName, testRootDirPath.toString(), adminUser);
        final DirHandle dirHandle = (DirHandle) fileSystem.find(path, adminUser);
        assertThat(dirHandle)
                .isNotNull();

        fileSystem.remove(path, adminUser);

        assertThatThrownBy(() -> {
            fileSystem.find(path, adminUser);
        }).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(directoryName);

        final DirHandle parentDirHandle = (DirHandle) fileSystem.find(testRootDirPath, adminUser);
        INodeInfo parentNodeInfo = new INodeInfo();
        parentNodeInfo.inodeClass = DirHandle.class;
        parentNodeInfo.name = TEST_ROOT_DIRECTORY;
        parentNodeInfo.path = testRootDirPath;
        parentNodeInfo.children = new ArrayList<>();
        parentNodeInfo.state = State.ATTACHED;
        parentNodeInfo.userRights = new HashMap<>();
        parentNodeInfo.userGroupRights = new HashMap<>();

        validateINode(parentDirHandle, parentNodeInfo, false);

        assertThat(parentDirHandle.children())
                .doesNotContain(dirHandle.path());
    }

    @Test(priority = 3,
        dataProvider = "getDataForMoveOp")
    public void moveDirectory(boolean needToRenameDir)
            throws IOException, StoreException {
        String directoryName = "/myFolder";
        final Path path = createDirectory(directoryName, testRootDirPath.toString(), adminUser);
        String innerDirectoryName = "innerFolder";
        String innerDirectoryPath = "/" + innerDirectoryName;

        final Path innerPath = createDirectory(innerDirectoryPath,
                path.toString(),
                adminUser);

        final DirHandle inode = (DirHandle) fileSystem.find(innerPath, adminUser);
        assertThat(inode)
                .isNotNull();

        DirHandle tempParentNode = (DirHandle) fileSystem.find(path, adminUser);
        assertThat(tempParentNode)
                .isNotNull();
        assertThat(tempParentNode.children())
                .containsExactly(inode.path());

        if (needToRenameDir) {
            innerDirectoryName = getRandomDirectoryName();
            innerDirectoryPath = "/" + innerDirectoryName;
        }

        final Path movedPath = Paths.get(testRootDirPath.toString(), innerDirectoryName);
        fileSystem.move(innerPath,
                movedPath,
                adminUser);

        tempParentNode = (DirHandle) fileSystem.find(path, adminUser);
        assertThat(tempParentNode)
                .isNotNull();
        assertThat(tempParentNode.children())
                .doesNotContain(inode.path());

        DirHandle movedNode = (DirHandle) fileSystem.find(movedPath, adminUser);

        assertThat(movedNode)
                .isNotNull();

        INodeInfo movedNodeInfo = new INodeInfo();
        movedNodeInfo.inodeClass = DirHandle.class;
        movedNodeInfo.name = innerDirectoryName;
        movedNodeInfo.path = Paths.get(testRootDirPath.toString(), innerDirectoryPath);
        movedNodeInfo.children = new ArrayList<>();
        movedNodeInfo.state = State.ATTACHED;
        movedNodeInfo.userRights = new HashMap<>();
        movedNodeInfo.userGroupRights = new HashMap<>();

        validateINode(movedNode, movedNodeInfo, true);

        fileSystem.remove(path, adminUser);
    }

    @Test(priority = 5,
            expectedExceptions = PathExistsException.class
    )
    public void moveFailDirectoryAlreadyExists()
            throws IOException, StoreException {
        final Path srcPath = getNewDirPath(testRootDirPath);
        createDirectory(srcPath, adminUser);

        fileSystem.move(srcPath, srcPath, adminUser);
    }

    @Test(priority = 6,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = ".*parent cannot be moved into it's child.*"
    )
    public void moveDirectoryToChildDirectoryFail()
            throws IOException, StoreException {
        final Path srcPath = getNewDirPath(testRootDirPath);
        createDirectory(srcPath, adminUser);
        final Path destParentPath = getNewDirPath(srcPath);
        createDirectory(destParentPath, adminUser);

        final Path destPath = destParentPath.resolve(srcPath.getFileName());
        fileSystem.move(srcPath, destPath, adminUser);
    }

    @Test(priority = 7,
            expectedExceptions = PathNotExistsException.class
    )
    public void moveDirectoryFailDestNotFound()
            throws IOException, StoreException {
        final Path srcPath = getNewDirPath(testRootDirPath);
        createDirectory(srcPath, adminUser);
        final Path destParentPath = getNewDirPath(srcPath);
        // here we don't create dest directory
        final Path destPath = destParentPath.resolve(srcPath.getFileName());
        fileSystem.move(srcPath, destPath, adminUser);
    }

    @Test(priority = 8,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be present\\."
    )
    public void createDirectoryFailNullPath()
            throws IOException, StoreException {
        createDirectory(null, adminUser);
    }

    @Test(priority = 9,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be absolute.*"
    )
    public void createDirectoryFailPathIsNotAbsolute()
            throws IOException, StoreException {
        createDirectory(Paths.get(getRandomDirectoryName()), adminUser);
    }

    @Test(priority = 10,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be present\\."
    )
    public void removeDirectoryFailNullPath()
            throws IOException, StoreException {
        fileSystem.remove(null, adminUser);
    }

    @Test(priority = 11,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be absolute.*"
    )
    public void removeDirectoryFailPathIsNotAbsolute()
            throws IOException, StoreException {
        fileSystem.remove(Paths.get(getRandomDirectoryName()), adminUser);
    }

    private Path getNewDirPath(Path parent) {
        return parent.resolve(getRandomDirectoryName());
    }

    /**
     * Data for move operation
     * @return
     */
    @DataProvider
    public static Object[][] getDataForMoveOp() {
        return new Object[][]{
            new Object[]{true},
            new Object[]{false}
        };
    }
}
