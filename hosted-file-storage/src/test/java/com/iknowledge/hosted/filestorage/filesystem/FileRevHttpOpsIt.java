package com.iknowledge.hosted.filestorage.filesystem;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.testng.annotations.Test;

import java.net.URI;
import java.nio.file.Path;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class FileRevHttpOpsIt extends FileRevHttpOpsItBase {

    @Test(dataProvider = "withEpoll")
    public void testUpload(boolean withEpoll) throws Exception {
        upload(withEpoll, false, FileContentSize.SMALL);
    }

    @Test(dataProvider = "withEpoll")
    public void testUploadWithGzip(boolean withEpoll) throws Exception {
        upload(withEpoll, true, FileContentSize.HUGE);
    }

    @Test(dataProvider = "withEpoll")
    public void testUploadFailFileTokenAbsent(boolean withEpoll) throws Exception {
        byte[] data = getRandomSizedData(FileContentSize.SMALL);
        final URI uri = getUploadLink(withEpoll);
        final HttpResponse httpResponse = Request.Post(uri)
                .bodyByteArray(data)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(400);
    }

    @Test(dataProvider = "withEpoll")
    public void testUploadFailBadFileToken(boolean withEpoll) throws Exception {
        byte[] data = getRandomSizedData(FileContentSize.SMALL);
        final URI uri = getUploadLink(withEpoll);
        final HttpResponse httpResponse = Request.Post(uri)
                .addHeader("file_token", "some_unexpected_string")
                .bodyByteArray(data)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(400);
    }

    @Test(dataProvider = "withEpoll")
    public void testUploadFailBadUrl(boolean withEpoll) throws Exception {
        final SavedFileRevData fileRev = prepareFileRevToUpload(FileContentSize.SMALL);
        final URI uri = getUploadLink(withEpoll).resolve("someNonexistentPath");
        final HttpResponse httpResponse = Request.Post(uri)
                .addHeader("file_token", fileRev.revisionTranId)
                .bodyByteArray(fileRev.saveData)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(404);
    }

    @Test(dataProvider = "withEpoll")
    public void testUploadFailUnexpectedHttpMethod(boolean withEpoll) throws Exception {
        final SavedFileRevData fileRev = prepareFileRevToUpload(FileContentSize.SMALL);
        final URI uri = getUploadLink(withEpoll);
        final HttpResponse httpResponse = Request.Get(uri)
                .addHeader("file_token", fileRev.revisionTranId)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(405);
    }

    @Test(dataProvider = "withEpoll")
    public void testDownload(boolean withEpoll) throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        SavedFileRevData fileRev = createFileRev(filePath, true,
                FileContentSize.HUGE, adminUser);
        download(filePath, fileRev.sysPath, withEpoll, false);
        download(filePath, fileRev.sysPath, withEpoll, true);
    }

    @Test(dataProvider = "withEpoll")
    public void testDownloadFailFileTokenAbsent(boolean withEpoll) throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        createFileRev(filePath, true,
                FileContentSize.SMALL, adminUser);
        final URI uri = getDownloadLink(withEpoll);
        final HttpResponse httpResponse = Request.Get(uri)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(400);
    }

    @Test(dataProvider = "withEpoll")
    public void testDownloadFailBadFileToken(boolean withEpoll) throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        createFileRev(filePath, true,
                FileContentSize.SMALL, adminUser);
        final URI uri = getDownloadLink(withEpoll);
        final HttpResponse httpResponse = Request.Get(uri)
                .addHeader("file_token", "some_unexpected_string")
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(400);
    }

    @Test(dataProvider = "withEpoll")
    public void testDownloadFailBadUrl(boolean withEpoll) throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        SavedFileRevData fileRev = createFileRev(filePath, true,
                FileContentSize.SMALL, adminUser);
        String fileToken
            = fileSystem.requestRevisionRead(fileRev.filePath, fileRev.fileRevNumber, adminUser);
        final URI uri = getDownloadLink(withEpoll).resolve("someNonexistentPath");
        final HttpResponse httpResponse = Request.Get(uri)
                .addHeader("file_token", fileToken)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(404);
    }

    @Test(dataProvider = "withEpoll")
    public void testDownloadFailBadUnexpectedHttpMethod(boolean withEpoll)
            throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        SavedFileRevData fileRev = createFileRev(filePath, true,
                FileContentSize.SMALL, adminUser);
        String fileToken
            = fileSystem.requestRevisionRead(fileRev.filePath, fileRev.fileRevNumber, adminUser);
        final URI uri = getDownloadLink(withEpoll);
        final HttpResponse httpResponse = Request.Post(uri)
                .addHeader("file_token", fileToken)
                .execute()
                .returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(405);
    }
}
