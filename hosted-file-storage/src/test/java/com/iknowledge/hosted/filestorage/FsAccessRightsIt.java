package com.iknowledge.hosted.filestorage;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import org.assertj.core.data.MapEntry;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class FsAccessRightsIt extends FileSystemItBase {
    private static final String TEST_ROOT_DIRECTORY = "arTests";
    private Path testRootDirPath;

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @Test(priority = 300,
            expectedExceptions = AccessDeniedException.class)
    public void createDirectoryAccessDenied()
        throws StoreException, IOException {
        String directoryName = getRandomDirectoryName();
        final Id anotherUserId = getRandomId();
        createDirectory(directoryName, testRootDirPath.toString(),
                new UserInfoLite(anotherUserId, new ArrayList<>()));
    }

    @Test(priority = 301)
    public void directoryCreateDeleteWithUserRights()
            throws StoreException, IOException {
        String directoryName = getRandomDirectoryName();

        final Id anotherUserId = getRandomId();
        final UserInfoLite userInfo = new UserInfoLite(anotherUserId, new ArrayList<>());

        // try to create dir - fail. Because user has no write rights
        assertThatThrownBy(() ->
                createDirectory(directoryName, testRootDirPath.toString(), userInfo))
                .isInstanceOf(AccessDeniedException.class);

        // attach write rights
        fileSystem.attachAccessRights(testRootDirPath, adminUser,
                                      new INodeUserRights(anotherUserId,
                        new INodeRights(false, true, false)));

        final Path createdDir = createDirectory(directoryName,
                testRootDirPath.toString(),
                userInfo);

        // because user has no read rights. Only write right at this moment
        assertThatThrownBy(() ->
                fileSystem.find(createdDir, userInfo))
                .isInstanceOf(AccessDeniedException.class);

        // attach read rights
        final INodeRights readWriteAccess =
                new INodeRights(true, true, false);
        INodeUserRights userRights = new INodeUserRights(anotherUserId, readWriteAccess);
        fileSystem.attachAccessRights(testRootDirPath, adminUser, userRights);

        final DirHandle inode = (DirHandle) fileSystem.find(createdDir.getParent(), userInfo);
        MapEntry<Id, INodeUserRights> entry =
                MapEntry.entry(anotherUserId, userRights);
        assertThat(inode.userRights().userRights())
                .containsOnly(entry);

        // remove write rights
        fileSystem.attachAccessRights(testRootDirPath, userInfo,
                                      new INodeUserRights(anotherUserId,
                        new INodeRights(true, false, false)));

        // try to remove dir - fail. Because user has no write rights
        assertThatThrownBy(() ->
                fileSystem.remove(createdDir, userInfo))
                .isInstanceOf(AccessDeniedException.class);

        // attach write rights
        fileSystem.attachAccessRights(testRootDirPath, adminUser,
                                      new INodeUserRights(anotherUserId,
                        new INodeRights(true, true, false)));

        fileSystem.remove(createdDir, userInfo);
        assertThatThrownBy(() ->
                fileSystem.find(createdDir, adminUser)).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(directoryName);
    }
}
