package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.fsimage.FsStats;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * All file system operations with full system reload after operation execution
 */
@Test(priority = 10000)
public final class OpsWithSystemReloadIt extends FsComplexItBase {
    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testCreateDir(FsCounts counts)
            throws Exception {
        createNode(false);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testCreateFile(FsCounts counts)
            throws Exception {
        createNode(true);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testRemoveDir(FsCounts counts)
            throws Exception {
        removeNode(false);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testRemoveFile(FsCounts counts)
            throws Exception {
        removeNode(true);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testMoveDir(FsCounts counts)
            throws Exception {
        moveNode(false, false);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testMoveFile(FsCounts counts)
            throws Exception {
        moveNode(true, false);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testMoveDirWithRename(FsCounts counts)
            throws Exception {
        moveNode(false, true);
    }

    @Test(priority = 10000, dataProvider = "getFilesAndDirsData")
    public void testMoveFileWithRename(FsCounts counts)
            throws Exception {
        moveNode(true, true);
    }

    private void moveNode(boolean isFile, boolean needToRename)
            throws Exception {
        final FsStats stats = fileSystem.stats();
        final long dirsCount = stats.dirs();
        final long filesCount = stats.files();
        final long fileRevsCount = stats.revs();
        final MoveOpResult moveOpResult = moveFileOrDirectory(isFile, needToRename);
        // that is mean that path is not found (maybe there is no directories or files)
        if (moveOpResult == null) {
            return;
        }
        checkMovedNode(moveOpResult, dirsCount, filesCount, fileRevsCount);
        reloadFs();
        testFs.recheckFileRevs();
        checkMovedNode(moveOpResult, dirsCount, filesCount, fileRevsCount);
    }

    private void checkMovedNode(MoveOpResult moveOpResult, long dirsCount, long filesCount,
                                long fileRevsCount)
            throws IOException, StoreException {
        final FsStats stats = fileSystem.stats();
        assertThat(stats.dirs())
                .isEqualTo(dirsCount);
        assertThat(stats.files())
                .isEqualTo(filesCount);
        assertThat(stats.revs())
                .isEqualTo(fileRevsCount);

        final DirHandle srcParentNodeAfterOp = (DirHandle)
                fileSystem.find(moveOpResult.srcParentBeforeOp.path(), adminUser);

        final DirHandle destParentNodeAfterOp = (DirHandle)
                fileSystem.find(moveOpResult.destParentBeforeOp.path(), adminUser);

        // if it is move
        if (moveOpResult.srcName.equals(moveOpResult.destName)) {
            assertThat(srcParentNodeAfterOp.children())
                    .hasSize(moveOpResult.srcParentBeforeOp.children().size() - 1);

            final List<String> srcParentNodeAfterOpChildren = srcParentNodeAfterOp.children()
                    .stream()
                    .map(s -> s.getFileName().toString())
                    .collect(Collectors.toList());
            assertThat(srcParentNodeAfterOpChildren)
                    .doesNotContain(moveOpResult.srcName)
                    .doesNotContain(moveOpResult.destName);

            assertThat(destParentNodeAfterOp.children())
                    .hasSize(moveOpResult.destParentBeforeOp.children().size() + 1);

        } else { // if it is rename

            // if it is rename at the same dir
            if (moveOpResult.srcParentBeforeOp.path().equals(
                    moveOpResult.destParentBeforeOp.path())) {
                assertThat(srcParentNodeAfterOp.children())
                        .hasSameSizeAs(moveOpResult.srcParentBeforeOp.children());
                final List<String> srcParentNodeAfterOpChildren = srcParentNodeAfterOp.children()
                        .stream()
                        .map(s -> s.getFileName().toString())
                        .collect(Collectors.toList());
                assertThat(srcParentNodeAfterOpChildren)
                        .doesNotContain(moveOpResult.srcName)
                        .contains(moveOpResult.destName);

                final Path destPath = srcParentNodeAfterOp.children().stream()
                        .filter(s -> s.getFileName().toString().equals(moveOpResult.destName))
                        .findFirst().get();
//                final INode desNode = fsImage.find(destPath, adminUser);
//                assertThat(desNode.state())
//                        .isEqualTo(State.ATTACHED);

                assertThat(destParentNodeAfterOp.children())
                        .hasSize(moveOpResult.destParentBeforeOp.children().size());

            } else { // if it is rename with move to the another than source parent path dir
                assertThat(srcParentNodeAfterOp.children())
                        .hasSize(moveOpResult.srcParentBeforeOp.children().size() - 1);
                final List<String> srcParentNodeAfterOpChildren = srcParentNodeAfterOp.children()
                        .stream()
                        .map(s -> s.getFileName().toString())
                        .collect(Collectors.toList());
                assertThat(srcParentNodeAfterOpChildren)
                        .doesNotContain(moveOpResult.srcName)
                        .doesNotContain(moveOpResult.destName);
                assertThat(destParentNodeAfterOp.children())
                        .hasSize(moveOpResult.destParentBeforeOp.children().size() + 1);
            }
        }
        final List<String> destParentNodeAfterOpChildren = destParentNodeAfterOp.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(destParentNodeAfterOpChildren)
                .contains(moveOpResult.destName);
    }

    private void removeNode(boolean isFile)
            throws Exception {
        final FsStats stats = fileSystem.stats();
        final long dirsCount = stats.dirs();
        final long filesCount = stats.files();
        final long fileRevsCount = stats.revs();
        final RemoveOpResult removeOpResult = removeFileOrDirectory(isFile, true);
        // that is mean that path is not found (maybe there is no directories or files)
        if (removeOpResult == null) {
            return;
        }
        checkDeletedNode(removeOpResult, dirsCount, filesCount, fileRevsCount, isFile);
        reloadFs();
        testFs.recheckFileRevs();
        checkDeletedNode(removeOpResult, dirsCount, filesCount, fileRevsCount, isFile);
    }

    private void checkDeletedNode(RemoveOpResult removeOpResult, long dirsCount,
                                  long filesCount, long fileRevsCount, boolean isFile)
            throws IOException, StoreException {
        final FsStats stats = fileSystem.stats();
        if (isFile) {
            assertThat(stats.dirs())
                    .isEqualTo(dirsCount);
            assertThat(stats.files())
                    .isEqualTo(filesCount - 1);
        } else {
            assertThat(stats.dirs())
                    .isEqualTo(dirsCount - 1);
            assertThat(stats.files())
                    .isEqualTo(filesCount);
        }

        assertThat(stats.revs())
                .isEqualTo(fileRevsCount);

        final DirHandle parentNodeAfterOp = (DirHandle)
                fileSystem.find(removeOpResult.parentNodeBeforeOp.path(), adminUser);

        assertThat(parentNodeAfterOp.children())
                .hasSize(removeOpResult.parentNodeBeforeOp.children().size() - 1);

        final List<String> parentNodeAfterOpChildren = parentNodeAfterOp.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(parentNodeAfterOpChildren)
                .doesNotContain(removeOpResult.removedPath.getFileName().toString());
    }

    private void createNode(boolean isFile)
            throws Exception {
        final FsStats stats = fileSystem.stats();
        final long dirsCount = stats.dirs();
        final long filesCount = stats.files();
        final long fileRevsCount = stats.revs();
        final CreateOpResult createOpResult = createFileOrDirectory(isFile);
        checkCreatedNode(createOpResult, dirsCount, filesCount, fileRevsCount, isFile);
        reloadFs();
        testFs.recheckFileRevs();
        checkCreatedNode(createOpResult, dirsCount, filesCount, fileRevsCount, isFile);
    }

    private void checkCreatedNode(CreateOpResult createOpResult, long dirsCount,
                                  long filesCount, long fileRevsCount, boolean isFile)
            throws IOException, StoreException {
        final FsStats stats = fileSystem.stats();
        if (isFile) {
            assertThat(stats.dirs())
                    .isEqualTo(dirsCount);
            assertThat(stats.files())
                    .isEqualTo(filesCount + 1);
        } else {
            assertThat(stats.dirs())
                    .isEqualTo(dirsCount + 1);
            assertThat(stats.files())
                    .isEqualTo(filesCount);
        }

        assertThat(stats.revs())
                .isEqualTo(fileRevsCount);

        final DirHandle parentNodeAfterOp = (DirHandle)
                fileSystem.find(createOpResult.parentNodeBeforeOp.path(), adminUser);

        assertThat(parentNodeAfterOp.children())
                .hasSize(createOpResult.parentNodeBeforeOp.children().size() + 1);

        final List<String> parentNodeAfterOpChildren = parentNodeAfterOp.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(parentNodeAfterOpChildren)
                .contains(createOpResult.createdPath.getFileName().toString());
    }
}
