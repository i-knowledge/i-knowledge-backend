package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.FileSystemItBase;
import com.iknowledge.hosted.filestorage.InvalidPathException;
import com.iknowledge.hosted.filestorage.PathExistsException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.filestorage.TestFs;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.fsimage.FsStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.util.Strings;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("Duplicates")
public class FsComplexItBase extends FileSystemItBase {
    private static final String TEST_ROOT_DIRECTORY = "complexTests";
    private static final Object[][] BEFORE_METHOD_DATA = {
            new Object[]{new FsCounts(3, 1, 1)},
            new Object[]{new FsCounts(5, 2, 5)},
            new Object[]{new FsCounts(10, 0, 13)},
            new Object[]{new FsCounts(15, 12, 0)},

            new Object[]{new FsCounts(3, 1, 1)},
            new Object[]{new FsCounts(5, 2, 5)},
            new Object[]{new FsCounts(10, 0, 13)},
            new Object[]{new FsCounts(15, 12, 0)},
    };

    private static final Object[][] ONLY_FILES_DATA = {
            new Object[]{new FsCounts(6, 0, 1)},
            new Object[]{new FsCounts(10, 0, 5)},
    };

    private static final Object[][] DATA_WITH_FILE_REVS = {
            new Object[]{new FsCounts(3, 1, 1, 1)},
            new Object[]{new FsCounts(5, 2, 5, 2)},
            new Object[]{new FsCounts(10, 0, 13, 4)},
            new Object[]{new FsCounts(15, 12, 2, 10)},
    };

    protected TestFs testFs;

    private Path testRootDirPath;
    private Path fsHierarchyPath;
    private final Logger logger = LoggerFactory.getLogger(FsComplexItBase.class);

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @DataProvider
    public static Object[][] getFilesAndDirsData() {
        return BEFORE_METHOD_DATA;
    }

    @DataProvider
    public static Object[][] getOnlyFilesData() {
        return ONLY_FILES_DATA;
    }

    @DataProvider
    public static Object[][] getDataWithFileRevs() {
        return DATA_WITH_FILE_REVS;
    }

    /**
     * Create complex fs hierarchy
     * @throws IOException
     * @throws StoreException
     */
    @BeforeMethod
    public void beforeMethod(Object[] data) throws Exception {
        final FsCounts counts = (FsCounts) data[0];
        testFs = new TestFs(counts.directoryCountInDepth,
                counts.directoryCountInWidth,
                counts.fileCountInDir,
                counts.fileRevs,
                this);
        fsHierarchyPath = Paths.get(testRootDirPath.toString(), "FsHierarchy");
        try {
            fileSystem.remove(fsHierarchyPath, adminUser);
        }
        catch (PathNotExistsException e) {
            //ignore
        }
        FsStats stats = fileSystem.stats();
        long dirsCount = stats.dirs();
        long filesCount = stats.files();
        long fileRevsCount = stats.revs();

        assertThat(dirsCount)
                .isEqualTo(2); // plus 2 for TEST_ROOT_DIRECTORY and root dir

        assertThat(filesCount)
                .isEqualTo(0);

        assertThat(fileRevsCount)
                .isEqualTo(0);

        testFs.createFsHierarchy(fsHierarchyPath, adminUser);
        stats = fileSystem.stats();
        dirsCount = stats.dirs();
        filesCount = stats.files();
        fileRevsCount = stats.revs();

        int directoryCount = getCurrentDirectoryCount();
        assertThat(dirsCount)
                .isEqualTo(directoryCount);

        assertThat(filesCount)
                .isEqualTo(testFs.getInitialTotalFileCount());

        assertThat(fileRevsCount)
                .isEqualTo(testFs.getInitialFileRevCount());
    }

    /**
     * Remove base folder for complex fs hierarchy
     * @throws IOException
     * @throws StoreException
     */
    @AfterClass
    public void afterMethod() throws IOException, StoreException {
        fileSystem.remove(fsHierarchyPath, adminUser);
    }

    /**
     * Get path to directory that contains another inodes
     * @param directoryDepth Directory number from one of the directories (in depth) that
     *                        contains another inodes
     * @throws UnsupportedOperationException
     */
    protected Path getDirectoryPathFromFsHierarchy(int directoryDepth)
            throws UnsupportedOperationException {
        return testFs.getRootDirectoryPathFromFsHierarchy(directoryDepth);
    }

    protected int getCurrentDirectoryCount() {
        // plus 2 for TEST_ROOT_DIRECTORY and root dir
        return testFs.getInitialTotalDirectoryCount() + 2;
    }

    protected int getLeftoverDirs(int depth) {
        int totalDirectoryCount = getCurrentDirectoryCount();
        int removedDirCount = testFs.getDirectoryCountInDir(depth);
        int leftoverDirs = totalDirectoryCount - removedDirCount;
        return leftoverDirs;
    }

    protected int getLeftoverFiles(int depth) {
        final int totalFileCount = testFs.getInitialTotalFileCount();
        final int removedFileCount = testFs.getFileCountInDir(depth);
        int leftoverFiles = totalFileCount - removedFileCount;
        return leftoverFiles;
    }

    protected int getLeftoverFileRevs(int depth) {
        return getLeftoverFiles(depth) * testFs.getFileRevCount();
    }

    protected CreateOpResult createFileOrDirectory(boolean isFile)
            throws StoreException, IOException {
        final Path randomDirectory = testFs.getRandomDirectory(false);
        final DirHandle parentNode;
        boolean exists = testFs.exists(randomDirectory);
        try {
            parentNode = (DirHandle) fileSystem.find(randomDirectory, adminUser);
        }
        catch (PathNotExistsException e) {
            if (testFs.exists(randomDirectory)) {
//                try {
//                    final INode inode = fsImage.find(randomDirectory, adminUser);
//                    if (inode.state() == State.DETACHED) {
//                        testFs.removeFileOrDirectory(randomDirectory);
//                        return null;
//                    }
//                }
//                catch (PathOperationException e1) {
//                    throw e1;
//                }
                // this should not happen, because if directory exists
                // then it should be in testFs
                throw e;
            } else {
                return null;
            }
        }
        CreateOpResult createOpResult = new CreateOpResult();
        createOpResult.parentNodeBeforeOp = parentNode;
        if (isFile) {
            final String randomFileName = getRandomFileName();
            try {
                final Path path = createFile(randomFileName,
                        randomDirectory.toString(),
                        adminUser);
                testFs.addFileOrDirectory(path);
                createOpResult.createdPath = path;
            }
            catch (PathNotExistsException e) {
                if (testFs.exists(randomDirectory)) {
                    // this should not happen, because if directory exists
                    // then it should be in testFs
                    throw e;
                } else {
                    return null;
                }
            }
        } else {
            final String randomDirectoryName = getRandomDirectoryName();
            try {
                final Path path = createDirectory(randomDirectoryName,
                        randomDirectory.toString(),
                        adminUser);
                testFs.addFileOrDirectory(path);
                createOpResult.createdPath = path;
            }
            catch (PathNotExistsException e) {
                if (testFs.exists(randomDirectory)) {
                    // this should not happen, because if directory exists
                    // then it should be in testFs
                    throw e;
                } else {
                    return null;
                }
            }
        }
        return createOpResult;
    }

    protected MoveOpResult moveFileOrDirectory(boolean isFile, boolean needToChangeName)
            throws IOException,
            StoreException {

        final Path randomSrc = getRandomPath(isFile, false);
        if (randomSrc == null) {
            return null;
        }

        String destFileName;
        if (needToChangeName) {
            if (isFile) {
                destFileName = getRandomFileName();
            } else {
                destFileName = getRandomDirectoryName();
            }
        } else {
            destFileName = randomSrc.getFileName().toString();
        }
        final Path destParentDirectory = testFs.getRandomDirectory(false);
        if (destParentDirectory == null) {
            return null;
        }
        final Path randomDest = Paths.get(destParentDirectory.toString(), destFileName);

        MoveOpResult moveOpResult = new MoveOpResult();
        try {
            moveOpResult.srcParentBeforeOp =
                    (DirHandle) fileSystem.find(randomSrc.getParent(), adminUser);
            moveOpResult.destParentBeforeOp =
                    (DirHandle) fileSystem.find(destParentDirectory, adminUser);
        }
        catch (PathOperationException e) {
            return null;
        }
        moveOpResult.destFullPath = randomDest;
        moveOpResult.srcName = randomSrc.getFileName().toString();
        moveOpResult.destName = destFileName;
        try {
            fileSystem.move(randomSrc, randomDest, adminUser);
            testFs.removeFileOrDirectory(randomSrc);
            testFs.addFileOrDirectory(randomDest);
            logger.warn("Moved src:[{}] to dest: [{}]", randomSrc, randomDest);
        }
        catch (PathNotExistsException e) {
            String message = e.getMessage();
            if (Strings.isNullOrEmpty(message)) {
                return null;
            }
            if (message.contains(randomSrc.toString())) {
                if (testFs.exists(randomSrc)) {
                    // this should not happen, because if directory exists
                    // then it should be in testFs
                    throw e;
                }
            } else if (e.getMessage().contains(destParentDirectory.toString())) {
                if (testFs.exists(destParentDirectory)) {
                    // this should not happen, because if directory exists
                    // then it should be in testFs
                    throw e;
                }
            } else {
                return null;
            }
        }
        catch (InvalidPathException e) {
            if (!e.getMessage().contains("parent cannot be moved into it's child")) {
                throw e;
            } else {
                return null;
            }
        }
        catch (PathExistsException e) {
            if (!randomSrc.getParent().equals(randomDest.getParent())) {
                throw e;
            } else {
                return null;
            }
        }

        return moveOpResult;
    }

    protected RemoveOpResult removeFileOrDirectory(boolean isFile, boolean onlyNonRootDirectories)
            throws IOException,
            StoreException {
        final Path randomPath = getRandomPath(isFile, onlyNonRootDirectories);
        if (randomPath == null) {
            return null;
        }
        RemoveOpResult removeOpResult = new RemoveOpResult();
        removeOpResult.removedPath = randomPath;
        try {
            removeOpResult.parentNodeBeforeOp =
                    (DirHandle) fileSystem.find(randomPath.getParent(), adminUser);
            fileSystem.remove(randomPath, adminUser);
            testFs.removeFileOrDirectory(randomPath);
            logger.warn("Removed path:[{}]", randomPath);
        }
        catch (PathNotExistsException e) {
            if (testFs.exists(randomPath)
                    || testFs.exists(randomPath.getParent())) {
                // this should not happen, because if file exists
                // then it should be in testFs
                throw e;
            } else {
                return null;
            }
        }

        return removeOpResult;
    }

    private Path getRandomPath(boolean isFile, boolean onlyNonRootDirectories)
            throws IOException, StoreException {
        Path path = null;
        try {
            int maxNumberToTry = 15;
            path = getRandomPathAndIgnoreDetachedNodes(isFile, onlyNonRootDirectories);
            while (path == null
                    && maxNumberToTry > 0) {
                path = getRandomPathAndIgnoreDetachedNodes(isFile, onlyNonRootDirectories);
                maxNumberToTry--;
            }
        }
        catch (UnsupportedOperationException e) {
            return null;
        }
        return path;
    }

    private Path getRandomPathAndIgnoreDetachedNodes(boolean isFile,
                                                     boolean onlyNonRootDirectories)
            throws IOException, StoreException {
        if (isFile) {
            if (testFs.getTotalFileCount() < 1) {
                throw new UnsupportedOperationException("Path is not found");
            }
        } else {
            if (testFs.getTotalDirectoryCount() < 1) {
                throw new UnsupportedOperationException("Path is not found");
            }
        }
        Path randomPath;

        if (isFile) {
            randomPath = testFs.getRandomFile();
            if (randomPath == null) {
                throw new UnsupportedOperationException("Path is not found");
            }

        } else {
            if (onlyNonRootDirectories) {
                randomPath = testFs.getRandomNonRootDirectory();
            } else {
                randomPath = testFs.getRandomDirectory(true);
            }
            if (randomPath == null) {
                throw new UnsupportedOperationException("Path is not found");
            }
        }
        final DirHandle node;
        try {
            node = (DirHandle) fileSystem.find(randomPath, adminUser);
        }
        catch (PathNotExistsException e) {
            if (testFs.exists(randomPath)) {
                throw e;
            } else {
                return null;
            }
        }
        return randomPath;
    }

    public static class FsCounts {

        /**
         * How many directory levels to create
         */
        public int directoryCountInDepth;

        /**
         * How many directories another directory can contain
         */
        public int directoryCountInWidth;

        /**
         * How many files directory can contain
         */
        public int fileCountInDir;

        /**
         * How many file revisions contains in one file
         */
        public final int fileRevs;

        public FsCounts(int directoryCountInDepth,
                        int directoryCountInWidth,
                        int fileCountInDir) {
            this(directoryCountInDepth, directoryCountInWidth,
                    fileCountInDir, 0);
        }

        public FsCounts(int directoryCountInDepth,
                        int directoryCountInWidth,
                        int fileCountInDir,
                        int fileRevs) {
            this.directoryCountInDepth = directoryCountInDepth;
            this.directoryCountInWidth = directoryCountInWidth;
            this.fileCountInDir = fileCountInDir;
            this.fileRevs = fileRevs;
        }
    }
}
