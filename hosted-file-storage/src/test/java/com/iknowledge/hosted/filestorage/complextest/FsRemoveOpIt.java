package com.iknowledge.hosted.filestorage.complextest;

import com.google.common.io.Files;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionConfig;
import com.iknowledge.hosted.filestorage.fsimage.FsStats;
import org.awaitility.Awaitility;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class FsRemoveOpIt extends FsComplexItBase {

    public FileRevisionConfig initFileRevisionConfig() {
        return FileRevisionConfig.builder()
                .withFsRevRoot(getRandomDirectoryName())
                .withMaxFileCount(10)
                .withRevisionCacheTtlMs(10000000)
                .withRevisionCacheInitCapacity(1000)
                .withRevisionCacheMaxSize(1000)
                .withReservedTenthOfFreeMemory(6)
                .withStartAtHour(LocalTime.now().getHour()) // to run immediately
                .withRunEveryTimeUnit(1)
                .withTimeUnit(TimeUnit.SECONDS)
                .build();
    }

    @Test(priority = 1000, dataProvider = "getFilesAndDirsData")
    public void removeFirstDirectoryWithFilesAndDirs(FsCounts fsCounts)
            throws IOException, StoreException {
        int directoryDepth = 1;
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getFilesAndDirsData")
    public void removeLastDirectoryWithFilesAndDirs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getFilesAndDirsData")
    public void removeMiddleDirectoryWithFilesAndDirs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getOnlyFilesData")
    public void removeFirstDirectoryWithFiles(FsCounts fsCounts)
            throws IOException, StoreException {
        int directoryDepth = 2;
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getOnlyFilesData")
    public void removeMiddleDirectoryWithFiles(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getOnlyFilesData")
    public void removeLastDirectoryWithFiles(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        removeDirectory(directoryDepth);
    }

    protected void removeDirectory(int directoryDepth)
            throws IOException, StoreException {
        final Path path =
                getDirectoryPathFromFsHierarchy(directoryDepth);
        final List<Path> childrenBeforeRemove = getRandomChildrenToCheckAfterRemove(path);
        fileSystem.remove(path, adminUser);
        final Path parentDirectoryPath = path.getParent();
        DirHandle parentINode = (DirHandle) fileSystem.find(parentDirectoryPath, adminUser);

        checksAfterDirectoryRemoving(directoryDepth, parentINode, childrenBeforeRemove);
    }

    private List<Path> getRandomChildrenToCheckAfterRemove(Path path)
            throws PathOperationException, StoreException {
        List<Path> list = new ArrayList<>();
        DirHandle dirHandle = (DirHandle) fileSystem.find(path, adminUser);
        int count = 0;
        try {
            while (!dirHandle.children().isEmpty()
                    && count < 3) {

                final Iterator<Path> pathIterator = dirHandle.children().iterator();
                while (pathIterator.hasNext()) {
                    final Path next = pathIterator.next();
                    list.add(next);
                    final String fileExtension = Files.getFileExtension(
                            next.getFileName().toString());
                    if (!fileExtension.equals("txt")) {
                        dirHandle = (DirHandle) fileSystem.find(next, adminUser);
                        break;
                    }
                }
                count++;
            }
        }
        catch (PathOperationException e) {
            return list;
        }

        return list;
    }

    private void checksAfterDirectoryRemoving(int directoryDepth,
                                              DirHandle parentINode,
                                              List<Path> childrenBeforeRemove) {

        // minus 1 because we delete root folder
        int oneLayerINodeCount = testFs.getInitialOneLayerINodesCount() - 1;
        assertThat(parentINode.children())
                .hasSize(oneLayerINodeCount);

        FsStats stats = fileSystem.stats();
        long dirsCount = stats.dirs();

        int leftoverDirs = getLeftoverDirs(directoryDepth);
        assertThat(dirsCount)
                .isEqualTo(leftoverDirs);

        int leftoverFiles = getLeftoverFiles(directoryDepth);
        long filesCount = stats.files();

        assertThat(filesCount)
                .isEqualTo(leftoverFiles);

        int leftoverFileRevs = getLeftoverFileRevs(directoryDepth);
        long fileRevsCount = stats.revs();

        assertThat(fileRevsCount)
                .isEqualTo(leftoverFileRevs);

        if (!childrenBeforeRemove.isEmpty()) {
            for (Path path : childrenBeforeRemove) {
                assertThatThrownBy(() -> {
                    fileSystem.find(path, adminUser);
                }).isInstanceOf(PathNotExistsException.class)
                        .hasMessageContaining("Parent not found");
            }
        }

        final int fileRevCountToRemove = testFs.getInitialFileRevCount() - leftoverFileRevs;
        awaitGcCollect(fileRevCountToRemove);
    }

    private void awaitGcCollect(final int fileRevCountToRemove) {
        Awaitility.waitAtMost(60, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    List<Path> releasedPaths = gc.getReleasedPaths();
                    assertThat(releasedPaths.size())
                            .isEqualTo(fileRevCountToRemove);
                });
    }
}
