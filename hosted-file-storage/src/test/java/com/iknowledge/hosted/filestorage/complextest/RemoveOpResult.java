package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hosted.filestorage.filesystem.DirHandle;

import java.nio.file.Path;

final class RemoveOpResult {
    public DirHandle parentNodeBeforeOp;
    public Path removedPath;
}
