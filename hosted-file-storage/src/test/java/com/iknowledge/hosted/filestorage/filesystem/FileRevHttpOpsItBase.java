package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.filestorage.FileSystemItBase;
import com.iknowledge.hosted.filestorage.http.server.HttpServer;
import com.iknowledge.hosted.filestorage.http.server.HttpServerConfig;
import io.netty.handler.codec.http.HttpHeaderValues;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.GzipCompressingEntity;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class FileRevHttpOpsItBase extends FileSystemItBase {
    protected static final String TEST_ROOT_DIRECTORY = "fileRevHttpOpsTests";
    protected static final String FILE_UPLOAD_URL_PATH = "/upload";
    protected static final String FILE_DOWNLOAD_URL_PATH = "/download";

    protected int httpServerIdleConnectionTimeoutMs = 10000;
    protected Path testRootDirPath;
    protected HttpServer httpServer;
    protected HttpServer httpServerWithoutEpoll;
    protected Integer serverPort;
    protected Integer serverPortWithoutEpoll;
    private final int socketTimeout = 15000;

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();

        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);

        final HttpServerConfig httpServerConfig = initHttpServerConfigWithEpoll();
        httpServer = new HttpServer(httpServerConfig, fileSystem);

        final HttpServerConfig httpServerWithoutEpollConfig = initHttpServerConfigWithoutEpoll();
        httpServerWithoutEpoll = new HttpServer(httpServerWithoutEpollConfig, fileSystem);
    }

    @AfterClass
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        if (httpServer != null) {
            httpServer.close();
        }
        if (httpServerWithoutEpoll != null) {
            httpServerWithoutEpoll.close();
        }
    }

    protected SavedFileRevData upload(final boolean withEpoll,
                                      final boolean withGzip,
                                      final FileContentSize contentSize)
            throws Exception {
        final SavedFileRevData fileRev = prepareFileRevToUpload(contentSize);
        final URI uri = getUploadLink(withEpoll);
        final Request request = Request.Post(uri)
                .addHeader("file_token", fileRev.revisionTranId);
        if (withGzip) {
            request.removeHeaders(HttpHeaders.CONTENT_ENCODING)
                    .addHeader(HttpHeaders.CONTENT_ENCODING, "gzip")
                    .body(new GzipCompressingEntity(new ByteArrayEntity(fileRev.saveData)));
        } else {
            request.bodyByteArray(fileRev.saveData);
        }
        final HttpResponse httpResponse = request.socketTimeout(socketTimeout)
                                                 .execute()
                                                 .returnResponse();

        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(200);
        fileRev.fileRevNumber = getLastRevNumber(fileRev.filePath);
        checkSavedFileRev(fileRev);
        EntityUtils.consume(httpResponse.getEntity());
        return fileRev;
    }

    protected void download(final Path filePath,
                            final Path sysPath,
                            final boolean withEpoll,
                            final boolean withGzip) throws Exception {
        final FileHandle fileHandle = (FileHandle) fileSystem.find(filePath, adminUser);
        assertThat(fileHandle.revisions())
            .hasSize(1);
        long revNumber = fileHandle.revisions()[0];
        String fileToken = fileSystem.requestRevisionRead(filePath, revNumber, adminUser);
        final URI uri = getDownloadLink(withEpoll);
        //TODO: enforce the http server to return content in gzip
        final Request request = Request.Get(uri)
                .addHeader("file_token", fileToken);
        if (withGzip) {
            request.addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip");
        } else {
            request.addHeader(HttpHeaders.ACCEPT_ENCODING, "identity;q=0");
        }
        final Response response = request.socketTimeout(socketTimeout)
                                        .execute();
        final HttpResponse httpResponse = response.returnResponse();
        assertThat(httpResponse.getStatusLine().getStatusCode())
                .isEqualTo(200);
        final HttpEntity httpEntity = httpResponse.getEntity();
        final byte[] bytes;
        if (httpEntity.getContentEncoding() != null
            && !httpEntity.getContentEncoding().getValue()
                         .equalsIgnoreCase(HttpHeaderValues.APPLICATION_OCTET_STREAM.toString())) {
            GzipDecompressingEntity gzipDecompressingEntity =
                    new GzipDecompressingEntity(httpEntity);
            bytes = IOUtils.toByteArray(gzipDecompressingEntity.getContent());
        } else {
            bytes = IOUtils.toByteArray(httpEntity.getContent());
        }
        if (sysPath == null) {
            checkSaveFileRevInFs(filePath, revNumber, bytes);
        } else {

            final byte[] dataToCheck = Files.readAllBytes(sysPath);
            assertThat(bytes)
                    .hasSameSizeAs(dataToCheck);
            assertThat(bytes)
                    .containsExactly(dataToCheck);
        }
        EntityUtils.consume(httpEntity);
    }

    protected URI getUploadLink(boolean withEpoll) throws URISyntaxException {
        Integer port = serverPortWithoutEpoll;
        if (withEpoll) {
            port = serverPort;
        }
        return new URI(MessageFormat.format("http://localhost:{0}{1}",
                port.toString(), FILE_UPLOAD_URL_PATH));
    }

    protected URI getDownloadLink(boolean withEpoll) throws URISyntaxException {
        Integer port = serverPortWithoutEpoll;
        if (withEpoll) {
            port = serverPort;
        }
        return new URI(MessageFormat.format("http://localhost:{0}{1}",
                port.toString(), FILE_DOWNLOAD_URL_PATH));
    }

    protected SavedFileRevData prepareFileRevToUpload(FileContentSize contentSize)
            throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        SavedFileRevData fileRev = createFileRev(filePath, true, adminUser);
        byte[] data = getRandomSizedData(contentSize);
        fileRev.saveData = data;
        return fileRev;
    }

    protected File createTmpFile(FileContentSize contentSize) throws IOException {
        final Path tempFile =
                Files.createTempFile(getRandomDirectoryName(), ".txt");
        final File file = new File(tempFile.toUri());
        final BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter(file));
        writeRandomSizedData(bufferedWriter, contentSize);
        return file;
    }

    protected HttpServerConfig initHttpServerConfigWithEpoll() throws IOException {
        final ServerSocket serverSocket = new ServerSocket(0);
        serverPort = serverSocket.getLocalPort();
        serverSocket.close();
        final InetSocketAddress inetSocketAddress
                = new InetSocketAddress(Inet4Address.getLoopbackAddress(),
                serverPort);

        return HttpServerConfig.builder()
                .withAcceptThreads(2)
                .withWorkerThreads(2)
                .withUseEpoll(true)
                .withBindAddress(inetSocketAddress)
                .withFileUploadPath(FILE_UPLOAD_URL_PATH)
                .withFileDownloadPath(FILE_DOWNLOAD_URL_PATH)
                .withIdleConnectionTimeoutMs(httpServerIdleConnectionTimeoutMs)
                .build();
    }

    protected HttpServerConfig initHttpServerConfigWithoutEpoll() throws IOException {
        final ServerSocket serverSocket = new ServerSocket(0);
        serverPortWithoutEpoll = serverSocket.getLocalPort();
        serverSocket.close();
        final InetSocketAddress inetSocketAddress2
                = new InetSocketAddress(Inet4Address.getLoopbackAddress(),
                serverPortWithoutEpoll);
        return HttpServerConfig.builder()
                .withAcceptThreads(2)
                .withWorkerThreads(2)
                .withUseEpoll(false)
                .withBindAddress(inetSocketAddress2)
                .withFileUploadPath(FILE_UPLOAD_URL_PATH)
                .withFileDownloadPath(FILE_DOWNLOAD_URL_PATH)
                .withIdleConnectionTimeoutMs(httpServerIdleConnectionTimeoutMs)
                .build();
    }

    @DataProvider(name = "withEpoll")
    protected Object[][] withEpoll() {
        return new Object[][]{
                {true},
                {false},
        };
    }
}
