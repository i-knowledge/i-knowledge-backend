package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.AccessDeniedException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.fsimage.FsStats;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserGroupRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public final class FsMoveOpIt extends FsComplexItBase {

    @Test(priority = 1100,
            dataProvider = "getFilesAndDirsData")
    public void moveFirstDirectoryWithFiles(FsCounts counts) throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1101,
            dataProvider = "getFilesAndDirsData")
    public void moveMiddleDirectoryWithFiles(FsCounts counts) throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1102,
            dataProvider = "getFilesAndDirsData")
    public void moveLastDirectoryWithFiles(FsCounts counts) throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1103,
            dataProvider = "getOnlyFilesData")
    public void moveLastDirectoryWithFilesOnly(FsCounts counts) throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1104,
            dataProvider = "getFilesAndDirsData")
    public void moveFirstDirectoryWithFilesAndUserRights(FsCounts counts) throws IOException,
                                                                                 StoreException,
            AccessDeniedException {
        int directoryDepth = 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, true, false);
    }

    @Test(priority = 1105,
            dataProvider = "getFilesAndDirsData")
    public void moveMiddleDirectoryWithFilesAndUserRights(FsCounts counts)
            throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, true, false);
    }

    @Test(priority = 1106,
            dataProvider = "getFilesAndDirsData")
    public void moveLastDirectoryWithFilesAndUserRights(FsCounts counts)
            throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, true, false);
    }

    @Test(priority = 1107,
            dataProvider = "getOnlyFilesData")
    public void moveLastDirectoryWithFilesOnlyAndUserRights(FsCounts counts)
            throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, true, false);
    }

    @Test(priority = 1108,
            dataProvider = "getFilesAndDirsData")
    public void moveFirstDirectoryWithFilesAndUserGroupRights(FsCounts counts) throws IOException,
            StoreException,
            AccessDeniedException {
        int directoryDepth = 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, false, true);
    }

    @Test(priority = 1109,
            dataProvider = "getFilesAndDirsData")
    public void moveMiddleDirectoryWithFilesAndUserGroupRights(FsCounts counts)
            throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, false, true);
    }

    @Test(priority = 1110,
            dataProvider = "getFilesAndDirsData")
    public void moveLastDirectoryWithFilesAndUserGroupRights(FsCounts counts)
            throws IOException, StoreException,
            AccessDeniedException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                true, false, true);
    }

    @Test(priority = 1111,
            dataProvider = "getDataWithFileRevs")
    public void moveFirstDirectoryWithFilesAndFileRevs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1112,
            dataProvider = "getDataWithFileRevs")
    public void moveMiddleDirectoryWithFilesAndFileRevs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    @Test(priority = 1113,
            dataProvider = "getDataWithFileRevs")
    public void moveLastDirectoryWithFilesAndFileRevs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        moveDirectory(directoryDepth, counts.directoryCountInWidth,
                false, false, false);
    }

    protected void moveDirectory(int directoryDepth, int nonRootDirCount,
                                 boolean changeName, boolean checkUserRights,
                                 boolean checkUserGroupRights)
            throws IOException, StoreException {
        // stop the test if we are trying to move root directory into itself
        if (directoryDepth == 1
                && nonRootDirCount == 0) {
            return;
        }

        final Path src =
                getDirectoryPathFromFsHierarchy(directoryDepth);
        Path nonRootDir = testFs.getRandomNonRootDirectory(src);
        if (nonRootDir == null) {
            nonRootDir = testFs.getRandomRootDirectory(src);
        }
        DirHandle destParentINode = (DirHandle) fileSystem.find(nonRootDir, adminUser);
        Path destDir;
        if (changeName) {
            destDir = nonRootDir.resolve(getRandomDirectoryName());
        } else {
            Path directoryName = src.getFileName();
            destDir = nonRootDir.resolve(directoryName);
        }

        FsStats statsBeforeMove = fileSystem.stats();
        DirHandle srcParentINode = (DirHandle) fileSystem.find(src.getParent(), adminUser);
        final DirHandle srcNode = (DirHandle) fileSystem.find(src, adminUser);

        if (checkUserRights) {
            moveDirectoryWithRandomUserAndCheckRights(src, destDir);
        } else if (checkUserGroupRights) {
            moveDirectoryWithRandomUserAndCheckGroupRights(src, destDir);
        } else  {
            fileSystem.move(src, destDir, adminUser);
        }

        checkAfterDirectoryMoving(srcParentINode, destParentINode, statsBeforeMove,
                src.getFileName(), destDir.getFileName(), srcNode);
    }

    protected void checkAfterDirectoryMoving(DirHandle srcParentINode, DirHandle destParentINode,
                                           FsStats statsBeforeMove, Path srcFileName,
                                           Path destFileName, DirHandle srcNode)
            throws IOException, StoreException {
        FsStats stats = fileSystem.stats();

        // stats should be the same after move operation
        assertThat(stats.files())
                .isEqualTo(statsBeforeMove.files());

        assertThat(stats.dirs())
                .isEqualTo(statsBeforeMove.dirs());

        assertThat(stats.revs())
                .isEqualTo(statsBeforeMove.revs());

        DirHandle newSrcParentINode = (DirHandle) fileSystem.find(srcParentINode.path(), adminUser);
        assertThat(newSrcParentINode.children())
                .hasSize(srcParentINode.children().size() - 1); // moved folder

        final Path oldPath = srcParentINode.path().resolve(srcFileName);
        assertThat(newSrcParentINode.children())
                .doesNotContain(oldPath);

        assertThatThrownBy(() -> {
            fileSystem.find(oldPath, adminUser);
        }).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(oldPath.getFileName().toString());

        final List<String> newSrcParentINodeChildren = newSrcParentINode.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(newSrcParentINodeChildren)
                .doesNotContain(srcFileName.toString());

        DirHandle newDestParentINode =
                (DirHandle) fileSystem.find(destParentINode.path(), adminUser);
        assertThat(newDestParentINode.children())
                .hasSize(destParentINode.children().size() + 1);
        final List<String> newDestParentINodeChildren = newDestParentINode.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(newDestParentINodeChildren)
                .contains(destFileName.toString());

        final Path destPath = destParentINode.path().resolve(destFileName);
        final DirHandle destNode = (DirHandle) fileSystem.find(destPath, adminUser);
        final String[] destChildrenNames = destNode.children().stream()
                .map(s -> s.getFileName().toString())
                .toArray(String[]::new);

        final List<String> srcNodeChildren = srcNode.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());

        assertThat(srcNodeChildren)
                .hasSize(destChildrenNames.length)
                .containsExactlyInAnyOrder(destChildrenNames);


    }

    private void moveDirectoryWithRandomUserAndCheckRights(
            Path src, Path dest) throws IOException, StoreException {
        final Id anotherUserId = getRandomId();
        final UserInfoLite user = new UserInfoLite(anotherUserId, new ArrayList<>());

        // try to move dir - fail. Because user has no write rights to src
        assertThatThrownBy(() ->
                fileSystem.move(src, dest, user))
                .isInstanceOf(AccessDeniedException.class);

        // attach read rights for src
        fileSystem.attachAccessRights(src, adminUser,
                                      new INodeUserRights(anotherUserId,
                        new INodeRights(false, true, false)));

        // try to move dir - fail. Because user has no write rights to dest
        assertThatThrownBy(() ->
                fileSystem.move(src, dest, user))
                .isInstanceOf(AccessDeniedException.class);

        // attach write rights for dests' parent
        fileSystem.attachAccessRights(dest.getParent(), adminUser,
                                      new INodeUserRights(anotherUserId,
                        new INodeRights(false, true, false)));

        fileSystem.move(src, dest, user);
    }

    private void moveDirectoryWithRandomUserAndCheckGroupRights(
            Path src, Path dest) throws IOException, StoreException {
        final Id anotherUserId = getRandomId();
        final Id randomGroupId = getRandomId();
        List<Id> groupIds = new ArrayList<>();
        groupIds.add(randomGroupId);
        final UserInfoLite user = new UserInfoLite(anotherUserId, groupIds);

        // try to move dir - fail. Because user has no write rights to src
        assertThatThrownBy(() ->
                fileSystem.move(src, dest, user))
                .isInstanceOf(AccessDeniedException.class);

        // attach read rights for src
        fileSystem.attachAccessRights(src, adminUser,
                new INodeUserGroupRights(randomGroupId,
                        new INodeRights(false, true, false)));

        // try to move dir - fail. Because user has no write rights to dest
        assertThatThrownBy(() ->
                fileSystem.move(src, dest, user))
                .isInstanceOf(AccessDeniedException.class);

        // attach write rights for dests' parent
        fileSystem.attachAccessRights(dest.getParent(), adminUser,
                new INodeUserGroupRights(randomGroupId,
                        new INodeRights(false, true, false)));

        fileSystem.move(src, dest, user);
    }
}
