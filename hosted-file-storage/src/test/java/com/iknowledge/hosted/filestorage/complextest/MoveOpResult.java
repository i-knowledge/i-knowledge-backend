package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hosted.filestorage.filesystem.DirHandle;

import java.nio.file.Path;

final class MoveOpResult {
    public DirHandle srcParentBeforeOp;
    public DirHandle destParentBeforeOp;
    public Path destFullPath;
    public String srcName;
    public String destName;
}
