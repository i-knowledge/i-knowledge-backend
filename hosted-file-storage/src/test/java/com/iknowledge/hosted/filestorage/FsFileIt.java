package com.iknowledge.hosted.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.filesystem.FileHandle;
import com.iknowledge.hosted.filestorage.filesystem.FsHandle;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.State;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@Test
public final class FsFileIt extends FileSystemItBase {
    private static final String TEST_ROOT_DIRECTORY = "fileTest";
    private Path testRootDirPath;

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @BeforeMethod
    public void beforeMethod() throws IOException, StoreException {
        try {
            final FsHandle fsHandle = fileSystem.find(testRootDirPath, adminUser);
            fileSystem.remove(testRootDirPath, adminUser);
        }
        catch (PathNotExistsException e) {
            // nothing to do
        }
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @Test(priority = 100)
    public void createFile() throws IOException, StoreException {

        String fileName = "file1.txt";
        final Path path = createFile(fileName, testRootDirPath.toString(), adminUser);
        final FileHandle inode = (FileHandle) fileSystem.find(path, adminUser);

        assertThat(inode)
                .isNotNull();

        INodeInfo nodeInfo = new INodeInfo();
        nodeInfo.inodeClass = FileHandle.class;
        nodeInfo.name = fileName;
        nodeInfo.path = path;
        nodeInfo.children = new ArrayList<>();
        nodeInfo.state = State.ATTACHED;
        nodeInfo.userRights = new HashMap<>();
        nodeInfo.userGroupRights = new HashMap<>();

        validateINode(inode, nodeInfo, false);

        //TODO: check inode children

        List<Path> parentChildren = new ArrayList<>();
        parentChildren.add(inode.path());
        final DirHandle parent = (DirHandle) fileSystem.find(inode.path().getParent(), adminUser);

        INodeInfo parentNodeInfo = new INodeInfo();
        parentNodeInfo.inodeClass = DirHandle.class;
        parentNodeInfo.name = TEST_ROOT_DIRECTORY;
        parentNodeInfo.path = testRootDirPath;
        parentNodeInfo.children = parentChildren;
        parentNodeInfo.state = State.ATTACHED;
        parentNodeInfo.userRights = new HashMap<>();
        parentNodeInfo.userGroupRights = new HashMap<>();

        validateINode(parent, parentNodeInfo, true);

        // clear root folder
        fileSystem.remove(path, adminUser);
    }

    @Test(priority = 101)
    public void removeFile() throws IOException, StoreException {
        String fileName = "file2.txt";
        final Path path = createFile(fileName, testRootDirPath.toString(), adminUser);
        final FileHandle inode = (FileHandle) fileSystem.find(path, adminUser);
        assertThat(inode)
                .isNotNull();
        fileSystem.remove(path, adminUser);

        assertThatThrownBy(() -> {
            fileSystem.find(path, adminUser);
        }).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(fileName);
        final DirHandle parent = (DirHandle) fileSystem.find(testRootDirPath, adminUser);
        INodeInfo parentNodeInfo = new INodeInfo();
        parentNodeInfo.inodeClass = DirHandle.class;
        parentNodeInfo.name = TEST_ROOT_DIRECTORY;
        parentNodeInfo.path = testRootDirPath;
        parentNodeInfo.children = new ArrayList<>();
        parentNodeInfo.state = State.ATTACHED;
        parentNodeInfo.userRights = new HashMap<>();
        parentNodeInfo.userGroupRights = new HashMap<>();

        validateINode(parent, parentNodeInfo, false);
        assertThat(parent.children())
                .doesNotContain(inode.path());
    }

    @Test(priority = 102)
    public void renameFile() throws IOException, StoreException {
        String fileName = "qwer.txt";
        String newFileName = "ttt.pdf";
        final Path srcPath = createFile(fileName, testRootDirPath.toString(), adminUser);
        final FileHandle srcNode = (FileHandle) fileSystem.find(srcPath, adminUser);

        final Path destPath = Paths.get(testRootDirPath.toString(), newFileName);
        fileSystem.move(srcPath, destPath, adminUser);
        final FileHandle destNode = (FileHandle) fileSystem.find(destPath, adminUser);
        final DirHandle rootNode = (DirHandle) fileSystem.find(testRootDirPath, adminUser);

        assertThat(srcNode.path().getParent())
                .isEqualTo(destNode.path().getParent());
        assertThat(rootNode)
                .isNotNull();
        assertThat(rootNode.children())
                .containsExactly(destNode.path());

        assertThatThrownBy(() -> {
            fileSystem.find(srcPath, adminUser);
        }).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(fileName);
    }

    @Test(priority = 103,
            expectedExceptions = PathExistsException.class
    )
    public void moveFailFileAlreadyExists()
            throws IOException, StoreException, AccessDeniedException {
        final Path srcPath = getNewFilePath(testRootDirPath);
        createFile(srcPath, adminUser);

        fileSystem.move(srcPath, srcPath, adminUser);
    }

    @Test(priority = 104)
    public void moveFileToChildDirectory()
            throws IOException, StoreException {
        final Path srcPath = getNewFilePath(testRootDirPath);
        createFile(srcPath, adminUser);

        final Path destParentPath = getNewDirPath(testRootDirPath);
        createDirectory(destParentPath, adminUser);
        final Path destPath = destParentPath.resolve(srcPath.getFileName());

        fileSystem.move(srcPath, destPath, adminUser);

        final DirHandle destParentNode = (DirHandle) fileSystem.find(destParentPath, adminUser);

        final List<String> list = destParentNode.children()
                .stream()
                .map(s -> s.getFileName().toString())
                .collect(Collectors.toList());
        assertThat(list)
                .containsExactly(srcPath.getFileName().toString());
        assertThatThrownBy(() -> {
            fileSystem.find(srcPath, adminUser);
        }).isInstanceOf(PathNotExistsException.class)
                .hasMessageContaining(srcPath.getFileName().toString());
    }

    @Test(priority = 105,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be present\\."
    )
    public void createFileFailNullPath()
            throws IOException, StoreException {
        createFile(null, adminUser);
    }

    @Test(priority = 106,
            expectedExceptions = InvalidPathException.class,
            expectedExceptionsMessageRegExp = "Path must be absolute.*"
    )
    public void createFileFailPathIsNotAbsolute()
            throws IOException, StoreException {
        createFile(Paths.get(getRandomFileName()), adminUser);
    }

    @Test(priority = 107,
            expectedExceptions = PathNotExistsException.class)
    public void moveFileIntoFile()
            throws IOException, StoreException {
        final Path srcPath = getNewFilePath(testRootDirPath);
        createFile(srcPath, adminUser);

        final Path destParentPath = getNewFilePath(testRootDirPath);
        createFile(destParentPath, adminUser);


        final Path destPath = destParentPath.resolve(srcPath.getFileName());

        fileSystem.move(srcPath, destPath, adminUser);
    }

    private Path getNewDirPath(Path parent) {
        return parent.resolve(getRandomDirectoryName());
    }

    private Path getNewFilePath(Path parent) {
        return parent.resolve(getRandomFileName());
    }
}
