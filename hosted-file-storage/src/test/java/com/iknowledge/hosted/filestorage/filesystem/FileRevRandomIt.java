package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.FileSystemItBase;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.PathOperationException;
import org.assertj.core.api.exception.RuntimeIOException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public final class FileRevRandomIt extends FileSystemItBase {
    private static final Object[][] ITER_COUNT = {
            new Object[]{10},
            new Object[]{100},
            new Object[]{1000},
//            new Object[]{10000}, // veeeery long
    };

    private static final String TEST_ROOT_DIRECTORY = "fileRevRandomTests";
    private Path testRootDirPath;
    private ConcurrentMap<Path, AtomicInteger> revs;

    @DataProvider
    public static Object[][] getIterCount() {
        return ITER_COUNT;
    }

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
        revs = new ConcurrentHashMap<>();

        for (int i = 0; i < 1000; i++) {
            createFileRev();
        }
    }

    @Test(dataProvider = "getIterCount")
    public void testRandomOps(int iterCount) throws Exception {

        Runnable createRunnable = () -> {
            try {
                createFileRev();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during create op.", e);
            }
        };

        Runnable removeRunnable = () -> {
            try {
                removeFileRev();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during create op.", e);
            }
        };
        List<CompletableFuture> futures = new ArrayList<>();

        for (int i = 0; i < iterCount; i++) {
            futures.add(submit(createRunnable));
            futures.add(submit(removeRunnable));
        }
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                .get();

        awaitFutures(futures);
    }

    private void createFileRev() throws Exception {
        boolean needToCreateFile = getRandomBool();
        Path filePath;
        if (needToCreateFile) {
            filePath = testRootDirPath.resolve(getRandomFileName());
        } else {
            final RandomFileRev randomRevPath = getRandomRevPath();
            if (randomRevPath == null) {
                return;
            }
            filePath = randomRevPath.path;
        }

        createFileRev(filePath, needToCreateFile, FileContentSize.SMALL, adminUser);

        // it was the existed file rev
        if (!needToCreateFile) {
            final AtomicInteger atomicInteger = revs.get(filePath);
            if (atomicInteger == null) {
                return;
            }
            atomicInteger.incrementAndGet();
        }
    }

    private void removeFileRev() throws PathOperationException, StoreException {
        final RandomFileRev randomRevPath = getRandomRevPath();
        if (randomRevPath == null) {
            return;
        }
        try {
            removeFileRev(randomRevPath.path, randomRevPath.fileRevNumber, adminUser);
        }
        catch (PathNotExistsException e) {
            // nothing to do. this is normal
        }
    }

    private RandomFileRev getRandomRevPath() {
        final List<Map.Entry<Path, AtomicInteger>> fileRevs = revs.entrySet()
                .stream()
                .collect(Collectors.toList());
        if (fileRevs.isEmpty()) {
            return null;
        }

        int fileRevIndex = ThreadLocalRandom.current()
                .ints(0, fileRevs.size())
                .findFirst()
                .getAsInt();
        final Map.Entry<Path, AtomicInteger> entry = fileRevs.get(fileRevIndex);
        RandomFileRev fileRev = new RandomFileRev();
        fileRev.path = entry.getKey();
        final int revNumber = entry.getValue().get();
        fileRev.fileRevNumber = revNumber;
        if (revNumber > 0) {
            fileRev.fileRevNumber = ThreadLocalRandom.current()
                    .ints(0, revNumber)
                    .findFirst()
                    .getAsInt();
        }
        return fileRev;
    }

    class RandomFileRev {
        public Path path;
        public long fileRevNumber;
    }
}
