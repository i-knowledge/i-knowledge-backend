package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.PathNotExistsException;
import com.iknowledge.hosted.filestorage.filesystem.FsHandle;
import org.assertj.core.api.exception.RuntimeIOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SuppressWarnings("Duplicates")
/**
 * Perform concurrent random operations.
 * NOTE: This must be the last integration tests in the execution order.
 */
@Test()
public final class RandomOpIt extends FsComplexItBase {

    private final Logger logger = LoggerFactory.getLogger(RandomOpIt.class);

    @Test(priority = 100000000, dataProvider = "getFilesAndDirsData", enabled = false)
    public void testRandomOpsExecution(FsCounts counts) throws Exception {

        final int iterCount = counts.directoryCountInWidth * counts.directoryCountInDepth;
        Runnable createRunnable = () -> {
            try {
                createFileOrDirectory();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during create op.", e);
            }
        };
        Runnable moveRunnable = () -> {
            try {
                moveFileOrDirectory();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during move op.", e);
            }
        };
        Runnable removeRunnable = () -> {
            try {
                removeFileOrDirectory();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during remove op.", e);
            }
        };
        List<CompletableFuture> futures = new ArrayList<>();

        for (int i = 0; i < iterCount; i++) {
            futures.add(submit(createRunnable));
            futures.add(submit(moveRunnable));
            futures.add(submit(removeRunnable));
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                .get();

        awaitFutures(futures);
    }

    @Test(priority = 100000000, dataProvider = "getFilesAndDirsData")
    public void testRandomOpsWithCheckAllFs(FsCounts counts) throws Exception {

        final int iterCount = 150;
        Runnable createRunnable = () -> {
            try {
                createFileOrDirectoryNotThrow();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during create op.", e);
            }
        };
        Runnable moveRunnable = () -> {
            try {
                moveFileOrDirectoryNotThrow();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during move op.", e);
            }
        };
        Runnable removeRunnable = () -> {
            try {
                removeFileOrDirectoryNotThrow();
            }
            catch (Exception e) {
                throw new RuntimeIOException("Error has occurred during remove op.", e);
            }
        };
        List<CompletableFuture> futures = new ArrayList<>();

        for (int i = 0; i < iterCount; i++) {
            futures.add(submit(createRunnable));
            futures.add(submit(moveRunnable));
            futures.add(submit(removeRunnable));
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                .get();

        awaitFutures(futures);
        // test some random dirs and files for existence
        for (int i = 0; i < iterCount; i++) {
//            boolean isFile = getRandomBool();
//            Path path;
//            if (isFile) {
//                path = testFs.getRandomFile();
//            } else {
//                path = testFs.getRandomDirectory(false);
//            }
//            boolean existsInFs = true;
//            try {
//                final FsHandle fsHandle = fileSystem.find(path, adminUser);
//            }
//            catch (PathOperationException e) {
//                existsInFs = false;
//            }
//            assertThat(existsInFs)
//                    .isEqualTo(!testFs.isRemoved(path));

            boolean existsInFs = true;
            Path deletedPath = testFs.getRandomRemovedPath();
            try {
                final FsHandle fsHandle = fileSystem.find(deletedPath, adminUser);
            }
            catch (PathNotExistsException e) {
                existsInFs = false;
            }
            assertThat(existsInFs)
                    .isFalse();
        }
    }

    private void createFileOrDirectoryNotThrow() {
        try {
            createFileOrDirectory();
        }
        catch (Exception e) {
            logger.warn("Error has occurred in createFileOrDirectoryNotThrow", e);
        }
    }

    private void moveFileOrDirectoryNotThrow() {
        try {
            moveFileOrDirectory();
        }
        catch (Exception e) {
            logger.warn("Error has occurred in moveFileOrDirectoryNotThrow", e);
        }
    }

    private void removeFileOrDirectoryNotThrow() {
        try {
            removeFileOrDirectory();
        }
        catch (Exception e) {
            logger.warn("Error has occurred in removeFileOrDirectoryNotThrow", e);
        }
    }

    private void createFileOrDirectory() throws StoreException, IOException {
        final boolean isFile = getRandomBool();
        createFileOrDirectory(isFile);
    }

    private void moveFileOrDirectory() throws IOException, StoreException {
        boolean isFile = getRandomBool();
        boolean needToChangeName = getRandomBool();
        moveFileOrDirectory(isFile, needToChangeName);
    }

    private void removeFileOrDirectory() throws IOException, StoreException {
        boolean isFile = getRandomBool();
        removeFileOrDirectory(isFile, false);
    }

}
