package com.iknowledge.hosted.filestorage.filesystem;

import com.iknowledge.hosted.filestorage.AccessDeniedException;
import com.iknowledge.hosted.filestorage.FileSystemItBase;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionConfig;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import org.awaitility.Awaitility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SuppressWarnings("checkstyle:variabledeclarationusagedistance")
public final class FileRevSimpleIt extends FileSystemItBase {
    private static final String TEST_ROOT_DIRECTORY = "fileRevTests";
    private Path testRootDirPath;

    public FileRevisionConfig initFileRevisionConfig() {
        return FileRevisionConfig.builder()
                .withFsRevRoot(getRandomDirectoryName())
                .withMaxFileCount(10)
                .withRevisionCacheTtlMs(10000000)
                .withRevisionCacheInitCapacity(1000)
                .withRevisionCacheMaxSize(1000)
                .withReservedTenthOfFreeMemory(6)
                .withStartAtHour(LocalTime.now().getHour()) // to run immediately
                .withRunEveryTimeUnit(1)
                .withTimeUnit(TimeUnit.SECONDS)
                .build();
    }

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        testRootDirPath = createDirectory(TEST_ROOT_DIRECTORY, rootPathString, adminUser);
    }

    @Test
    public void testCreate() throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        final long revsCountBeforeOp = fsImage.fsMap().revs();
        SavedFileRevData fileRev =
                createFileRev(filePath, true, FileContentSize.MEDIUM, adminUser);
        checkSaveFileRevInFs(fileRev);
        checkSavedFileRev(fileRev);
        assertThat(fsImage.fsMap().revs())
                .isEqualTo(revsCountBeforeOp + 1);
    }

    @Test
    public void testCreateWithAccessRights() throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());

        final UserInfoLite userInfo = getRandomUser();
        // try to create dir - fail. Because user has no write rights
        assertThatThrownBy(() ->
                createFileRev(filePath, true, FileContentSize.SMALL, userInfo))
                .isInstanceOf(AccessDeniedException.class);

        // attach read rights
        attachUserRights(testRootDirPath, userInfo,
                true, false, false);
        SavedFileRevData fileRev =
                createFileRev(filePath, false, FileContentSize.SMALL, userInfo);
        checkSaveFileRevInFs(fileRev);
        checkSavedFileRev(fileRev);
    }

    @Test
    public void testRemove() throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());

        SavedFileRevData fileRev =
                createFileRev(filePath, true, FileContentSize.SMALL, adminUser);
        SavedFileRevData secondFileRev =
                createFileRev(filePath, false,
                        FileContentSize.SMALL, adminUser);
        SavedFileRevData thirdFileRev =
                createFileRev(filePath, false,
                        FileContentSize.SMALL, adminUser);
        final long revsCountBeforeOp = fsImage.fsMap().revs();
        removeFileRev(filePath, secondFileRev.fileRevNumber, adminUser);
        awaitGcCollect(secondFileRev.sysPath);
        checkSaveFileRevInFs(fileRev);
        checkSaveFileRevInFs(thirdFileRev);
        checkSavedFileRev(fileRev);
        checkSavedFileRev(thirdFileRev);
        assertThat(fsImage.fsMap().revs())
                .isEqualTo(revsCountBeforeOp - 1);
    }

    @Test
    public void testRemoveWithAccessRights() throws Exception {
        final Path filePath = testRootDirPath.resolve(getRandomFileName());
        SavedFileRevData fileRev =
                createFileRev(filePath, true, FileContentSize.SMALL, adminUser);
        final UserInfoLite userInfo = getRandomUser();
        // try to create dir - fail. Because user has no write rights
        assertThatThrownBy(() ->
                removeFileRev(filePath, fileRev.fileRevNumber, userInfo))
                .isInstanceOf(AccessDeniedException.class);

        attachUserRights(testRootDirPath, userInfo,
                true, true, false);

        removeFileRev(filePath, fileRev.fileRevNumber, userInfo);
        Thread.sleep(2000);
        assertThat(Files.exists(fileRev.sysPath))
                .isFalse();
    }

    private void awaitGcCollect(final Path fileRevSysPath) {
        Awaitility.waitAtMost(5, TimeUnit.SECONDS)
                .untilAsserted(() -> assertThat(Files.exists(fileRevSysPath)).isFalse());
    }
}
