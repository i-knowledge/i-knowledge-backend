package com.iknowledge.hosted.filestorage;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.filestorage.filesystem.DirHandle;
import com.iknowledge.hosted.filestorage.filesystem.FileHandle;
import com.iknowledge.hosted.filestorage.filesystem.FileSystem;
import com.iknowledge.hosted.filestorage.filesystem.FsHandle;
import com.iknowledge.hosted.filestorage.filesystem.NewFileRevHandle;
import com.iknowledge.hosted.filestorage.filesystem.ReadableFileRevHandle;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionConfig;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionGc;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionGcConfig;
import com.iknowledge.hosted.filestorage.fsimage.FileRevisionManager;
import com.iknowledge.hosted.filestorage.fsimage.FsConfig;
import com.iknowledge.hosted.filestorage.fsimage.InMemoryFsImage;
import com.iknowledge.hosted.filestorage.rawfs.UnixFs;
import com.iknowledge.hosted.hbase.repositories.filestorage.FsLogConfig;
import com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.State;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserGroupRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserRights;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;
import io.prometheus.client.CollectorRegistry;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.assertj.core.api.AssertionsForClassTypes;
import org.iknowledge.hbase.client.HConfig;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("checkstyle:javadocmethod")
public class FileSystemItBase {
    protected static final Id ADMIN_USER_GROUP_ID = Id.ZERO_ID;
    protected static final Id ADMIN_USER_ID = Id.ZERO_ID;
//    private static final byte[] ZERO_ARRAY = new byte[32];

    protected UnixFs unixFs;
    protected FsConfig fsConfig;
    protected HBaseFsImageStore fsImageStore;
    protected FileSystem fileSystem;
    protected InMemoryFsImage fsImage;
    protected FileRevisionManager fileRevisionManager;
    protected Path baseTempDir;
    protected UserInfoLite adminUser;
    protected String rootPathString;
    protected Path rootPath;
    protected String fsImageStoreTableName;
    protected String tempDir;
    protected StoreConfig storeConfig;
    protected FileRevisionGc gc;
    protected ExecutorService executorService;

    protected FileSystemItBase() {}

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        Thread.UncaughtExceptionHandler handler =
                (thread, exception) -> Assert.fail("Error in test executing", exception);
        Thread.setDefaultUncaughtExceptionHandler(handler);
        tempDir = System.getProperty("java.io.tmpdir");
        if (baseTempDir != null) {
            if (Files.exists(baseTempDir)) {
                format();
            }
        }
        baseTempDir = Paths.get(tempDir, getRandomDirectoryName());
        Files.createDirectory(baseTempDir);
        final ArrayList<Id> userGroups = new ArrayList<>();
        userGroups.add(ADMIN_USER_GROUP_ID);
        adminUser = new UserInfoLite(ADMIN_USER_ID, userGroups);
        rootPathString = "/";
        rootPath = Paths.get(rootPathString);
        fsImageStoreTableName = UUID.randomUUID().toString();
        int poolSize = Runtime.getRuntime().availableProcessors() * 64;
        executorService = Executors.newFixedThreadPool(poolSize);

        initFs(true);
    }

    protected void reloadFs() throws Exception {
        closeFs();
        initFs(false);
    }

    protected Map<Id, HBaseINodeRights> getUserGroupRightsWithAdmin() {
        Map<Id, HBaseINodeRights> userGroupRights = new HashMap<>();
        userGroupRights.put(ADMIN_USER_GROUP_ID,
                new INodeUserGroupRights(ADMIN_USER_GROUP_ID,
                    new INodeRights(true, true, true)));
        return userGroupRights;
    }

    protected Map<Id, HBaseINodeRights> getUserRightsWithAdmin() {
        Map<Id, HBaseINodeRights> userRights = new HashMap<>();
        userRights.put(ADMIN_USER_ID,
                new INodeUserGroupRights(ADMIN_USER_ID,
                        new INodeRights(true, true, true)));
        return userRights;
    }

    protected void validateINode(FsHandle fsHandle,
                                 INodeInfo nodeInfo,
                                 boolean checkChildren) {

        assertThat(fsHandle.getClass())
                .isEqualTo(nodeInfo.inodeClass);

        assertThat(fsHandle.name())
                .isEqualTo(nodeInfo.name);

        if (checkChildren) {
            final DirHandle dirHandle = (DirHandle) fsHandle;
            assertThat(dirHandle.children())
                    .containsExactlyElementsOf(nodeInfo.children);
        }

        assertThat(fsHandle.path())
                .isEqualTo(nodeInfo.path);

//        assertThat(dirHandle.state())
//                .isEqualTo(nodeInfo.state);

        Map.Entry<Id, HBaseINodeRights>[] userEntries =
                (Map.Entry<Id, HBaseINodeRights>[])
                (nodeInfo.userRights.entrySet().toArray(new Map.Entry[
                        nodeInfo.userRights.size()]));
        assertThat(fsHandle.userRights().userRights())
                .hasSameSizeAs(nodeInfo.userRights)
                .containsExactly(userEntries);

        Map.Entry<Id, HBaseINodeRights>[] userGroupEntries =
                (Map.Entry<Id, HBaseINodeRights>[])
                        (nodeInfo.userGroupRights.entrySet().toArray(
                                new Map.Entry[nodeInfo.userGroupRights.size()]));
        assertThat(fsHandle.userRights().userGroupRights())
                .hasSameSizeAs(nodeInfo.userGroupRights)
                .containsExactly(userGroupEntries);
    }

    protected Path createDirectory(String name, String parent, UserInfoLite user)
            throws IOException, StoreException {
        final Path path = Paths.get(parent, name);
        return createDirectory(path, user);
    }

    protected Path createDirectory(Path path, UserInfoLite user)
            throws IOException, StoreException {
        fileSystem.newDirectory(path, user);
        return path;
    }

    protected Path createFile(String name, String parent, UserInfoLite user)
            throws IOException, StoreException {
        final Path path = Paths.get(parent, name);
        return createFile(path, user);
    }

    protected Path createFile(Path path, UserInfoLite user)
            throws IOException, StoreException {
        fileSystem.newFile(path, user);
        return path;
    }

    public static String getRandomDirectoryName() {
        return UUID.randomUUID().toString();
    }

    public static String getRandomFileName() {
        return UUID.randomUUID().toString() + ".txt";
    }

    public static boolean isFile(Path path) {
        return path.getFileName().toString().contains(".txt");
    }

    public UserInfoLite getRandomUser() {
        final Id anotherUserId = getRandomId();
        return new UserInfoLite(anotherUserId, new ArrayList<>());
    }

    public void attachUserRights(final Path path,
                                 final UserInfoLite userInfo,
                                 final boolean canRead,
                                 final boolean canWrite,
                                 final boolean canUpload)
            throws PathOperationException, StoreException {
        fileSystem.attachAccessRights(path, adminUser,
                new INodeUserRights(userInfo.getUserId(),
                        new INodeRights(canRead, canWrite, canUpload)));
    }

    public void attachUserGroupRights(final Path path,
                                 final Id groupId,
                                 final boolean canRead,
                                 final boolean canWrite,
                                 final boolean canUpload)
            throws PathOperationException, StoreException {
        fileSystem.attachAccessRights(path, adminUser,
                new INodeUserGroupRights(groupId,
                        new INodeRights(canRead, canWrite, canUpload)));
    }

    protected boolean getRandomBool() {
        return io.netty.util.internal.ThreadLocalRandom.current().nextBoolean();
    }

    protected int getRandomInt(int randomNumberOrigin, int randomNumberBound) {
        return java.util.concurrent.ThreadLocalRandom.current()
                .ints(randomNumberOrigin, randomNumberBound)
                .findFirst()
                .getAsInt();
    }

    public static Id getRandomId() {
        return Id.of(getRandomByteArray());
    }

    public static byte[] getRandomByteArray() {
        final byte[] bytes = new byte[8];
        ThreadLocalRandom.current().nextBytes(bytes);
        return bytes;
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        closeFs();
        format();
    }

    private void closeFs() throws Exception {
        if (fileSystem != null) {
            fileSystem.close();
        }
        if (fsImageStore != null) {
            fsImageStore.close();
        }
        if (storeConfig != null) {
            storeConfig.connection().close();
        }
        if (gc != null) {
            gc.close();
        }
    }

    private void format() throws IOException {
        FileUtils.forceDelete(new File(baseTempDir.toUri()));
    }

    protected void initFs(boolean format) throws Exception {
        closeFs();
        final String rootDirName = "fs_root";
        unixFs = new UnixFs(baseTempDir, rootDirName);

        final FileRevisionConfig fileRevisionConfig = initFileRevisionConfig();
        fsConfig = initFsConfig(fileRevisionConfig);

        final HConfig hconfig = initHConfig();
        Connection connection
            = ConnectionFactory.createConnection(hconfig.asConfiguration());
        storeConfig = StoreConfig.builder()
                                 .withConnection(connection)
                                 .withTableName(fsImageStoreTableName)
                                 .withHconfig(hconfig)
                                 .build();
        if (fsImageStore != null) {
            fsImageStore.close();
        }
        FsLogConfig fsLogConfig = initFsLogConfig();
        fsImageStore = new HBaseFsImageStore(storeConfig,
                                             fsLogConfig,
                                             new CollectorRegistry());
        final CollectorRegistry collectorRegistry = new CollectorRegistry();
        if (format) {
            fsImageStore.format();
            unixFs.format();
        }
        fsImage = new InMemoryFsImage(fsImageStore, fsConfig, collectorRegistry);

        final FileRevisionGcConfig gcConfig = initFileRevisionGcConfig(fileRevisionConfig);

        gc = new FileRevisionGc(unixFs, fsImage.fsMap(), gcConfig);
        fileRevisionManager = new FileRevisionManager(fsImage, fsConfig, unixFs,
                collectorRegistry, gc);

        fileSystem = new FileSystem(fsImageStore, unixFs, fsConfig,
                fsImage, fileRevisionManager);
    }

    public FsConfig initFsConfig(FileRevisionConfig fileRevisionConfig) {
        return FsConfig.builder()
                .withReadLockWaitTimeMs(10000)
                .withWriteLockWaitTimeMs(10000)
                .withPathCacheMs(1000000)
                .withPathCacheMaxSize(1000)
                .withBackgroundIoPoolSize(Runtime.getRuntime().availableProcessors())
                .withFileRevisionConfig(fileRevisionConfig)
                .build();
    }

    public FileRevisionConfig initFileRevisionConfig() {
        return FileRevisionConfig.builder()
                .withFsRevRoot(getRandomDirectoryName())
                .withMaxFileCount(10)
                .withRevisionCacheTtlMs(10000000)
                .withRevisionCacheInitCapacity(1000)
                .withRevisionCacheMaxSize(1000)
                .withReservedTenthOfFreeMemory(6)
                .withStartAtHour(LocalTime.now().getHour()) // to run immediately
                .withRunEveryTimeUnit(1)
                .withTimeUnit(TimeUnit.HOURS)
                .build();
    }

    public HConfig initHConfig() {
        return HConfig.newBuilder()
                .retryCount(10)
                .retryBackoff(1000)
                .connectionThreads(Runtime.getRuntime()
                        .availableProcessors())
                .metaLookupThreads(Runtime.getRuntime()
                        .availableProcessors() / 2)
                .build();
    }

    public FsLogConfig initFsLogConfig() {
        return FsLogConfig.newBuilder()
                .maxLogSize(10000)
                .compactLogEverySec(60)
                .shutdownListener(e -> { })
                .build();
    }

    private FileRevisionGcConfig initFileRevisionGcConfig(FileRevisionConfig fileRevisionConfig) {
        return FileRevisionGcConfig.builder()
                .withRootPath(fileRevisionConfig.getFsRevRoot())
                .withStartAtHour(fileRevisionConfig.getStartAtHour())
                .withRunEveryTimeUnit(fileRevisionConfig.getRunEveryTimeUnit())
                .withTimeUnit(fileRevisionConfig.getTimeUnit())
                .withReservedTenthOfFreeMemory(fileRevisionConfig.getReservedTenthOfFreeMemory())
                .build();
    }

    public SavedFileRevData createFileRev(final Path filePath,
                                          final boolean needToCreateFile,
                                          final UserInfoLite userInfo)
            throws Exception {
        if (needToCreateFile) {
            createFile(filePath, adminUser);
        }
        final String tranId = fileSystem.newRevision(filePath, userInfo);
        SavedFileRevData data = new SavedFileRevData();
        data.filePath = filePath;
        data.revisionTranId = tranId;
        return data;
    }

    public SavedFileRevData createFileRev(final Path filePath,
                                          final boolean needToCreateFile,
                                          final FileContentSize fileContentSize,
                                          final UserInfoLite userInfo)
            throws Exception {
        if (needToCreateFile) {
            createFile(filePath, adminUser);
        }

        final String tranId = fileSystem.newRevision(filePath, userInfo);
        final NewFileRevHandle fileRevHandle = fileSystem.openRevision(tranId);
        final Path sysPath = fileRevHandle.revisionInode().sysPath();
        final byte[] randomBytes = getRandomSizedData(fileContentSize);
        final ByteBuffer byteBuffer = ByteBuffer.wrap(randomBytes);
        fileRevHandle.channel().write(byteBuffer);
        final CompletableFuture<String> commitFuture = fileRevHandle.commit();
        final String committedTranId = commitFuture.get();
        final CompletableFuture<String> closeFuture = fileRevHandle.closeAsync();
        final String closedTranId = closeFuture.get();

        AssertionsForClassTypes.assertThat(tranId)
                .isEqualTo(committedTranId)
                .isEqualTo(closedTranId);
        SavedFileRevData data = new SavedFileRevData();
        data.filePath = filePath;
        data.saveData = randomBytes;
        data.sysPath = sysPath;
        data.revisionTranId = tranId;
        data.fileRevNumber = getLastRevNumber(filePath);
        return data;
    }

    public void checkSaveFileRevInFs(Path filePath, long fileRevNumber,
                                     byte[] dataToCheck) throws IOException, StoreException {
        Path sysPath = getSysPathByPath(filePath, fileRevNumber);
        final byte[] fileContent = Files.readAllBytes(sysPath);
        assertThat(Arrays.equals(fileContent, dataToCheck))
            .withFailMessage("File revision content mismatch")
            .isTrue();
    }

    public void checkSaveFileRevInFs(SavedFileRevData savedFileRevData)
            throws IOException, StoreException {
        savedFileRevData.sysPath =
                getSysPathByPath(savedFileRevData.filePath, savedFileRevData.fileRevNumber);
        final byte[] fileContent = Files.readAllBytes(savedFileRevData.sysPath);

        assertThat(Arrays.equals(fileContent, savedFileRevData.saveData))
            .withFailMessage("File revision content mismatch")
            .isTrue();
    }

    public void checkSavedFileRev(SavedFileRevData savedFileRevData)
            throws IOException, RevisionTranIdAlreadyExists,
            RevisionTranIdNotFound, StoreException {
        final String revisionReadTranId = fileSystem.requestRevisionRead(
                savedFileRevData.filePath, savedFileRevData.fileRevNumber, adminUser);
        final ReadableFileRevHandle revHandle = fileSystem.openReadRevision(revisionReadTranId);
        ByteBuffer readBuffer = ByteBuffer.allocate(savedFileRevData.saveData.length);
        revHandle.channel().read(readBuffer);
        assertThat(Arrays.equals(readBuffer.array(), savedFileRevData.saveData))
            .withFailMessage("File revision content mismatch")
            .isTrue();
    }

    public void removeFileRev(final Path filePath,
                              final long revNumber,
                              final UserInfoLite userInfoLite)
            throws PathOperationException, StoreException {
        fileSystem.remove(filePath, revNumber, userInfoLite);
    }

    protected Path getSysPathByPath(Path path, long fileRevNumber)
            throws PathOperationException, StoreException {
        return getFileRevINode(path, fileRevNumber).sysPath();
    }

    protected FileRevisionINode getFileRevINode(Path path, long fileRevNumber)
            throws PathOperationException, StoreException {
        return fsImage.findRev(path, fileRevNumber, adminUser);
    }

    protected void writeRandomSizedData(BufferedWriter writer, FileContentSize contentSize)
            throws IOException {
        switch (contentSize) {
            case SMALL:
                writeRandomSizedData(writer, 1);
                break;

            case MEDIUM:
                writeRandomSizedData(writer, 1000);
                break;

            case HUGE:
                writeRandomSizedData(writer, 1000000);
                break;

            case VERY_HUGE:
                writeRandomSizedData(writer, 100000000);
                break;

            default:
                writeRandomSizedData(writer, 1);
                break;
        }
    }

    protected void writeRandomSizedData(BufferedWriter writer, int iterCount) throws IOException {
        for (int i = 0; i < iterCount; i++) {
            writer.append(getRandomDirectoryName());
            writer.append(System.lineSeparator());
        }
    }

    protected byte[] getRandomSizedData(FileContentSize contentSize) {
        switch (contentSize) {
            case SMALL:
                return getRandomSmallSizedData();

            case MEDIUM:
                return getRandomMediumSizedData();

            case HUGE:
                return getRandomHugeSizedData();

            case VERY_HUGE:
                return getRandomVeryHugeSizedData();

            default:
                return getRandomSmallSizedData();
        }
    }

    protected CompletableFuture<Void> submit(Runnable runnable) {
        return CompletableFuture.runAsync(() -> {
            try {
                runnable.run();
            }
            catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }, executorService);
    }

    protected void awaitFutures(final List<CompletableFuture> futures) {
        assertThat(futures)
                .allMatch(f -> f.isDone()
                        && !f.isCompletedExceptionally()
                        && !f.isCancelled());
    }

    protected long getLastRevNumber(Path filePath) throws PathOperationException, StoreException {
        final FileHandle fileHandle = (FileHandle) fileSystem.find(filePath, adminUser);
        // get last (newly created file rev)
        assertThat(fileHandle.revisions())
            .isNotEmpty();
        return fileHandle.revisions()[fileHandle.revisions().length - 1];
    }

    private byte[] getRandomSizedDataByIterationCount(int iterCount) {
        return new byte[iterCount * 32];
    }

    private byte[] getRandomSmallSizedData() {
        return getRandomSizedDataByIterationCount(1);
    }

    private byte[] getRandomMediumSizedData() {
        return getRandomSizedDataByIterationCount(1000);
    }

    private byte[] getRandomHugeSizedData() {
        return getRandomSizedDataByIterationCount(1000000);
    }

    private byte[] getRandomVeryHugeSizedData() {
        return getRandomSizedDataByIterationCount(10000000);
    }

    protected enum FileContentSize {

        /**
         * Near 36B
         */
        SMALL,

        /**
         * Near 37Kb
         */
        MEDIUM,

        /**
         * Near 37Mb
         */
        HUGE,

        /**
         * Near 3,7Gb
         */
        VERY_HUGE,
    }

    protected class SavedFileRevData {
        public Path filePath;

        public Path sysPath;

        public byte[] saveData;

        public String revisionTranId;

        public long fileRevNumber;
    }

    public class INodeInfo {
        /**
         * inodeClass
         */
        public Class inodeClass;

        /**
         * name
         */
        public String name;

        /**
         * path
         */
        public Path path;

        /**
         * children
         */
        public Collection<Path> children;

        /**
         * state
         */
        public State state;

        /**
         * userRights
         */
        public Map<Id, HBaseINodeRights> userRights;

        /**
         * userGroupRights
         */
        public Map<Id, HBaseINodeRights> userGroupRights;
    }
}
