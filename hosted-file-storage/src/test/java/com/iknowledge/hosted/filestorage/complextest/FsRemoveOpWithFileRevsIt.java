package com.iknowledge.hosted.filestorage.complextest;

import com.iknowledge.hbase.repositories.StoreException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public final class FsRemoveOpWithFileRevsIt extends FsRemoveOpIt {

    @Override
    @BeforeMethod()
    public void beforeMethod(Object[] data) throws Exception {
        super.setUp(); // need to reinit gc
        super.beforeMethod(data);
    }

    @Test(priority = 1000,
            dataProvider = "getDataWithFileRevs")
    public void removeFirstDirectoryWithFilesAndFileRevs(FsCounts fsCounts)
            throws IOException, StoreException {
        int directoryDepth = 1;
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getDataWithFileRevs")
    public void removeLastDirectoryWithFilesAndFileRevs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = counts.directoryCountInDepth - 1;
        removeDirectory(directoryDepth);
    }

    @Test(priority = 1000, dataProvider = "getDataWithFileRevs")
    public void removeMiddleDirectoryWithFilesAndFileRevs(FsCounts counts)
            throws IOException, StoreException {
        int directoryDepth = Math.floorDiv(counts.directoryCountInDepth, 2);
        removeDirectory(directoryDepth);
    }
}
