package com.iknowledge.hosted.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.users.UserInfoLite;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static com.iknowledge.hosted.filestorage.FileSystemItBase.getRandomDirectoryName;
import static com.iknowledge.hosted.filestorage.FileSystemItBase.getRandomFileName;
import static com.iknowledge.hosted.filestorage.FileSystemItBase.isFile;

/**
 * Helper class for using test file system
 */
@SuppressWarnings({"Duplicates", "checkstyle:javadocmethod"})
public final class TestFs {

    private final int directoryCountInDepth;
    private final int directoryCountInWidth;
    private final int fileCountInDir;
    private final int fileRevCount;
    private final FileSystemItBase testClass;

    private final ConcurrentMap<Path, Path> allINodes;
    private final ConcurrentMap<Path, Path> rootDirectories;
    private final ConcurrentMap<Path, Path> leftoverDirectories;
    private final ConcurrentMap<Path, Path> files;
    private final ConcurrentMap<Path, Path> removedPaths;
    private final ReadWriteLock lock;
    private final Map<Path, FileSystemItBase.SavedFileRevData> fileRevs;

    private Path fsRootDir;

    public TestFs(final int directoryCountInDepth,
                  final int directoryCountInWidth,
                  final int fileCountInDir,
                  final int fileRevCount,
                  final FileSystemItBase testClass) {
        this.directoryCountInDepth = directoryCountInDepth;
        this.directoryCountInWidth = directoryCountInWidth;
        this.fileCountInDir = fileCountInDir;
        this.fileRevCount = fileRevCount;
        this.testClass = testClass;
        this.allINodes = new ConcurrentHashMap<>();
        this.rootDirectories = new ConcurrentHashMap<>();
        this.leftoverDirectories = new ConcurrentHashMap<>();
        this.removedPaths = new ConcurrentHashMap<>();
        this.files = new ConcurrentHashMap<>();
        this.lock = new ReentrantReadWriteLock();
        this.fileRevs = new HashMap<>();
    }

    public void createFsHierarchy(final Path baseTempDir,
                                        final UserInfoLite user)
            throws Exception {
        Path path = testClass.createDirectory(baseTempDir, user);
        fsRootDir = path;
        allINodes.put(path, path);
        rootDirectories.put(path, path);
        createFilesAndDirs(path, user, directoryCountInWidth, fileCountInDir, allINodes);

        // minus 1 because we already create one layer of files
        for (int i = 0; i < directoryCountInDepth - 1; i++) {
            path = createDirectory(getRandomDirectoryName(), path.toString(), user);
            allINodes.put(path, path);
            rootDirectories.put(path, path);
            createFilesAndDirs(path, user, directoryCountInWidth, fileCountInDir, allINodes);
        }
    }

    public Path getRootDirectoryPathFromFsHierarchy(final int directoryDepthIndex)
            throws UnsupportedOperationException {
        if (directoryDepthIndex < 0) {
            throw new UnsupportedOperationException(
                    "directoryDepthIndex must be a positive number");
        }
        final List<Path> paths = rootDirectories.values()
                .stream()
                .sorted(Comparator.comparingInt(Path::getNameCount))
                .collect(Collectors.toList());

        return paths.get(directoryDepthIndex);
    }

    public int getFileCountInDir(int depth) {
        if (depth > directoryCountInDepth) {
            throw new UnsupportedOperationException("depth must be less "
                    + "or equal to directoryCountInDepth");
        }
        final int totalCount = getInitialTotalFileCount();
        final int oneLayerCount = fileCountInDir;
        return totalCount - (depth * oneLayerCount);
    }

    public int getDirectoryCountInDir(int depth) {
        if (depth > directoryCountInDepth) {
            throw new UnsupportedOperationException("depth must be less "
                    + "or equal to directoryCountInDepth");
        }
        final int totalCount = getInitialTotalDirectoryCount();
        final int oneLayerCount = directoryCountInWidth + 1;
        return totalCount - (depth * oneLayerCount);
    }

    public Path getRandomNonRootDirectory() {
        List<Path> possibleDirectories = leftoverDirectories.values()
                .stream()
                .collect(Collectors.toList());
        if (possibleDirectories.isEmpty()) {
            return null;
        }
        int dirIndex = ThreadLocalRandom.current().nextInt(0, possibleDirectories.size());
        return possibleDirectories.get(dirIndex);
    }

    public Path getRandomNonRootDirectory(Path rootNameToExclude) {
        // get only directories that doesn't contains rootNameToExclude in its path
        List<Path> possibleDirectories = leftoverDirectories.values().stream()
                .filter(s -> !s.toString().contains(rootNameToExclude.getFileName().toString()))
                .collect(Collectors.toList());
        if (possibleDirectories.isEmpty()) {
            return null;
        }
        int dirIndex = ThreadLocalRandom.current().nextInt(0, possibleDirectories.size());
        return possibleDirectories.get(dirIndex);
    }

    public Path getRandomRootDirectory(Path rootNameToExclude) {
        // get only directories that doesn't contains rootNameToExclude in its path
        List<Path> possibleDirectories = rootDirectories.values().stream()
                .filter(s -> !s.toString().contains(rootNameToExclude.toString())
                        && !s.toString()
                        .contains(rootNameToExclude.getParent()
                                .toString()))
                .collect(Collectors.toList());
        int dirIndex = ThreadLocalRandom.current().nextInt(0, possibleDirectories.size());
        return possibleDirectories.get(dirIndex);
    }

    public Path getRandomDirectory(final boolean excludeFsRoot) {
        List<Path> possibleDirectories = allINodes.values().stream()
                .filter(s -> {
                    if (excludeFsRoot
                            && s == fsRootDir) {
                        return false;
                    }
                    if (!isFile(s)) {
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toList());
        if (possibleDirectories.isEmpty()) {
            return null;
        }
        int dirIndex = ThreadLocalRandom.current().nextInt(0, possibleDirectories.size());
        return possibleDirectories.get(dirIndex);
    }

    public Path getRandomFile() {
        List<Path> possibleFiles = files.values().stream()
                .filter(s -> !isFile(s))
                .collect(Collectors.toList());
        if (possibleFiles.isEmpty()) {
            return null;
        }
        int dirIndex = ThreadLocalRandom.current().nextInt(0, possibleFiles.size());
        return possibleFiles.get(dirIndex);
    }

    public Path getRandomRemovedPath() {
        List<Path> possiblePaths = removedPaths.values().stream()
                .collect(Collectors.toList());
        if (possiblePaths.isEmpty()) {
            return null;
        }
        int pathIndex = ThreadLocalRandom.current().nextInt(0, possiblePaths.size());
        return possiblePaths.get(pathIndex);
    }

    public boolean exists(Path path) {
        try {
            lock.readLock().lock();
            return allINodes.containsKey(path);
        }
        finally {
            lock.readLock().unlock();
        }
    }

    public void addFileOrDirectory(Path path) {
        try {
            lock.writeLock().lock();
            allINodes.put(path, path);
            if (isFile(path)) {
                files.put(path, path);
            } else {
                leftoverDirectories.put(path, path);
            }
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    public void removeFileOrDirectory(Path path) {
        try {
            lock.writeLock().lock();
            allINodes.remove(path);
            removedPaths.putIfAbsent(path, path);
            if (isFile(path)) {
                files.remove(path);
            } else {
                leftoverDirectories.remove(path);
                rootDirectories.remove(path);

                // remove all path's children nodes
                final List<Path> paths = allINodes.values().stream()
                        .collect(Collectors.toList());
                for (Path pathToCheck : paths) {
//                    if (pathToCheck.toString().contains(path.toString())) {
                    if (pathToCheck.startsWith(path)) {
                        removedPaths.putIfAbsent(pathToCheck, pathToCheck);
                        allINodes.remove(pathToCheck);
                        leftoverDirectories.remove(pathToCheck);
                        rootDirectories.remove(pathToCheck);
                        files.remove(pathToCheck);
                    }
                }
            }
        }
        finally {
            lock.writeLock().unlock();
        }
    }

    public int getTotalDirectoryCount() {
        final List<Path> list = allINodes.values().stream()
                .filter(s -> !isFile(s))
                .collect(Collectors.toList());
        return list.size();
    }

    public int getTotalFileCount() {
        return files.size();
    }

    public List<Path> getOnlyFilesInRoot(Path rootPath) {
        return files.values().stream()
                .filter(s -> s.getParent().getFileName().equals(rootPath.getFileName()))
                .collect(Collectors.toList());
    }

    public int getInitialOneLayerINodesCount() {
        // plus one for directory that contains another directory
        return directoryCountInWidth + fileCountInDir + 1;
    }

    public int getInitialTotalDirectoryCount() {
        return directoryCountInDepth * directoryCountInWidth + directoryCountInDepth;
    }

    public int getInitialTotalFileCount() {
        return directoryCountInDepth * fileCountInDir;
    }

    public int getInitialFileRevCount() {
        return directoryCountInDepth * fileCountInDir * fileRevCount;
    }

    public int getFileRevCount() {
        return fileRevCount;
    }

    public void recheckFileRevs()
        throws StoreException, RevisionTranIdNotFound,
               RevisionTranIdAlreadyExists, IOException {
        for (FileSystemItBase.SavedFileRevData fileRevData : fileRevs.values()) {
            testClass.checkSavedFileRev(fileRevData);
        }
    }

    private void createFilesAndDirs(final Path parentFolder,
                                           final UserInfoLite user,
                                           final int directoryCountInWidth,
                                           final int fileCountInDir,
                                           final ConcurrentMap<Path, Path> paths)
            throws Exception {
        for (int i = 0; i < directoryCountInWidth; i++) {
            final Path path = createDirectory(
                    getRandomDirectoryName(), parentFolder.toString(), user);
            paths.put(path, path);
            leftoverDirectories.put(path, path);
        }

        for (int i = 0; i < fileCountInDir; i++) {
            final Path path = createFile(getRandomFileName(), parentFolder.toString(), user);
            paths.put(path, path);
            files.put(path, path);

            for (int j = 1; j <= fileRevCount; j++) {
                final FileSystemItBase.SavedFileRevData fileRev = createFileRev(path, user);
                fileRevs.put(path, fileRev);
            }
        }
    }

    private FileSystemItBase.SavedFileRevData createFileRev(final Path filePath,
                                 final UserInfoLite user) throws
            Exception {
        return testClass.createFileRev(filePath, false,
                FileSystemItBase.FileContentSize.SMALL, user);
    }

    private Path createDirectory(String name, String parent, UserInfoLite user)
            throws IOException, StoreException {
        final Path path = Paths.get(parent, name);
        return testClass.createDirectory(path, user);
    }

    private Path createFile(String name, String parent, UserInfoLite user)
            throws IOException, StoreException {
        final Path path = Paths.get(parent, name);
        return testClass.createFile(path, user);
    }
}
