package com.iknowledge.hosted.filestorage.rawfs;

import com.iknowledge.hbase.repositories.StoreException;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@SuppressWarnings("checkstyle:javadocmethod")
public class UnixFsTestsBase {
    protected static UnixFs unixFs;
    protected static Path baseTempDir;
    protected static Path fsRootDir;

    @BeforeClass
    public static void setUp() throws IOException {
        String tempDir = System.getProperty("java.io.tmpdir") + "/unixFsTests";
        baseTempDir = Paths.get(tempDir);
        if (Files.exists(baseTempDir)) {
            format();
        }
        Files.createDirectory(baseTempDir);
        final String fsRoot = "fs_root";
        fsRootDir = baseTempDir.resolve(fsRoot);
        unixFs = new UnixFs(baseTempDir, fsRoot);
        unixFs.format();
    }

    @AfterClass
    public static void tearDown() throws IOException, StoreException {
        format();
    }

    public static String getRandomDirectoryName() {
        return UUID.randomUUID().toString();
    }

    public static String getRandomFileName() {
        return UUID.randomUUID().toString() + ".txt";
    }

    protected Path getNewDirPath() {
        return fsRootDir.resolve(getRandomDirectoryName());
    }

    protected Path getNewFilePath() {
        return fsRootDir.resolve(getRandomFileName());
    }

    private static void format() throws IOException {
        FileUtils.forceDelete(new File(baseTempDir.toUri()));
    }
}
