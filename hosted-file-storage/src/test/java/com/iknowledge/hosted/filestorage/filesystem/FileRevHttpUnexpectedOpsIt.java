package com.iknowledge.hosted.filestorage.filesystem;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.fluent.Async;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.awaitility.Awaitility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public final class FileRevHttpUnexpectedOpsIt extends FileRevHttpOpsItBase {

    /**
     * Initialization method.
     */
    @BeforeClass
    public void setUp() throws Exception {
        httpServerIdleConnectionTimeoutMs = 1000;
        super.setUp();
    }

    @Test(dataProvider = "withEpoll", enabled = false)
    public void testUploadWithTranRollback(boolean withEpoll) throws Exception {
        final File file = createTmpFile(FileContentSize.VERY_HUGE);
        try {
            final Path filePath = testRootDirPath.resolve(getRandomFileName());
            SavedFileRevData fileRev = createFileRev(filePath, true, adminUser);
            final URI uri = getUploadLink(withEpoll);
            final Request request = Request.Post(uri)
                    .addHeader("file_token", fileRev.revisionTranId)
                    .bodyFile(file, ContentType.DEFAULT_TEXT);
            final Future<Content> requestFuture = Async.newInstance().execute(request);
            Thread.sleep(1000);
            // get initial gc state
            List<Path> inUsePaths = gc.getInUsePaths();
            assertThat(inUsePaths)
                    .isNotNull();
            assertThat(inUsePaths.size())
                    .isEqualTo(1);
            List<Path> releasedPaths = gc.getReleasedPaths();
            assertThat(releasedPaths)
                    .isNotNull();
            assertThat(releasedPaths.size())
                    .isEqualTo(0);
            final Path sysPath = inUsePaths.get(0);
            // TODO: this cancel doesn't really cancel the connection
            requestFuture.cancel(true);
            awaitGcCollect(sysPath);
        }
        finally {
            FileUtils.forceDelete(file);
        }
    }

    private void awaitGcCollect(final Path sysPath) {
        //TODO: decrease amount of time when cancel will really work
        Awaitility.waitAtMost(60, TimeUnit.SECONDS)
                .untilAsserted(() -> {
                    gc.collectAll();

                    // check gc state after the rollback op
                    List<Path> inUsePaths = gc.getInUsePaths();
                    List<Path> releasedPaths = gc.getReleasedPaths();

                    assertThat(inUsePaths)
                            .isNotNull();
                    assertThat(inUsePaths.size())
                            .isEqualTo(0);
                    assertThat(releasedPaths)
                            .isNotNull();
                    assertThat(releasedPaths.size())
                            .isEqualTo(1);
                    final Path releasedPath = releasedPaths.get(0);
                    assertThat(releasedPath)
                            .isEqualTo(sysPath);
                    assertThat(Files.exists(sysPath))
                            .isFalse();
                });
    }
}
