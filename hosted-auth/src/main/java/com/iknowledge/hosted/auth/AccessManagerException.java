package com.iknowledge.hosted.auth;

/**
 * Exception that must be thrown after handling the source exception in AccessManager
 */
public final class AccessManagerException extends Exception {
    public AccessManagerException() {
    }

    public AccessManagerException(String message) {
        super(message);
    }

    public AccessManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccessManagerException(Throwable cause) {
        super(cause);
    }

    public AccessManagerException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
