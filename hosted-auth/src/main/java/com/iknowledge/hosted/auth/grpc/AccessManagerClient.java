package com.iknowledge.hosted.auth.grpc;

import com.iknowledge.hosted.auth.protobuf.AccessManagerGrpc;
import com.iknowledge.hosted.auth.protobuf.CreateUserRequestProto;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public final class AccessManagerClient {
    private final Logger logger = LoggerFactory.getLogger(AccessManagerClient.class);

    private final ManagedChannel channel;
    private final AccessManagerGrpc.AccessManagerBlockingStub blockingStub;
    private final AccessManagerGrpc.AccessManagerStub asyncStub;

    /**
     * Construct client for accessing AccessManager server at {@code host:port}.
     */
    public AccessManagerClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port).usePlaintext(true));
    }

    /**
     * Construct client for accessing AccessManager server using the existing channel.
     */
    public AccessManagerClient(ManagedChannelBuilder<?> channelBuilder) {
        channel = channelBuilder.build();
        blockingStub = AccessManagerGrpc.newBlockingStub(channel);
        asyncStub = AccessManagerGrpc.newStub(channel);
    }

    /**
     * main
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        AccessManagerClient client = new AccessManagerClient("localhost", 8980);
        client.createUser("name", "myPass", true);
    }

    /**
     * Shutdown
     * @throws InterruptedException
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * Create user
     * @param userName
     * @param password
     * @param isAdmin
     */
    public void createUser(String userName, String password, boolean isAdmin) {
        final CreateUserRequestProto.CreateUserRequest request =
                CreateUserRequestProto.CreateUserRequest
                .newBuilder()
                .setUserName(userName)
                .setUserPass(password)
                .setIsAdmin(isAdmin)
                .build();
        try {
            blockingStub.createUser(request);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
