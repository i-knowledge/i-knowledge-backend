package com.iknowledge.hosted.auth.grpc;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.auth.AccessManager;
import com.iknowledge.hosted.auth.AccessManagerException;
import com.iknowledge.hosted.auth.protobuf.AccessManagerGrpc;
import com.iknowledge.hosted.auth.protobuf.CreateGroupRequestProto;
import com.iknowledge.hosted.auth.protobuf.CreateUserRequestProto;
import com.iknowledge.hosted.auth.protobuf.RemoveUserFromGroupRequestProto;
import com.iknowledge.hosted.auth.protobuf.UserGroupRequestProto;
import com.iknowledge.hosted.auth.protobuf.UserInfoRequestProto;
import com.iknowledge.hosted.auth.protobuf.UserInfoResponseProto;
import com.iknowledge.hosted.hbase.repositories.users.UserInfo;
import com.iknowledge.hosted.protobuf.EmptyProto;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.util.List;

final class AccessManagerService extends AccessManagerGrpc.AccessManagerImplBase {
    private final AccessManager accessManager;

    /**
     *
     * @param accessManager
     */
    public AccessManagerService(AccessManager accessManager) {

        this.accessManager = accessManager;
    }

    @Override
    public void createUser(CreateUserRequestProto.CreateUserRequest request,
                           StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        responseObserver.onNext(createUserInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void getUserInfo(UserInfoRequestProto.UserInfoRequest request,
                            StreamObserver<
                                    UserInfoResponseProto.UserInfoResponse> responseObserver) {
        responseObserver.onNext(getUserInfoInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void getUserGroups(UserInfoRequestProto.UserInfoRequest request,
                              StreamObserver<
                                      UserInfoResponseProto.UserInfoResponse> responseObserver) {
        responseObserver.onNext(getUserGroupsInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void removeUser(UserInfoRequestProto.UserInfoRequest request,
                           StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        responseObserver.onNext(removeUserInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void addUserToGroup(RemoveUserFromGroupRequestProto.RemoveUserFromGroupRequest request,
                               StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        responseObserver.onNext(addUserToGroupInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void createGroup(CreateGroupRequestProto.CreateGroupRequest request,
                            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        responseObserver.onNext(createGroupInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    @Override
    public void removeGroup(UserGroupRequestProto.UserGroupRequest request,
                            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        responseObserver.onNext(removeGroupInternal(request, responseObserver));
        responseObserver.onCompleted();
    }

    private EmptyProto.EmptyResponse createUserInternal(
            CreateUserRequestProto.CreateUserRequest request,
            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        try {
            accessManager.createUser(request.getUserName(),
                    request.getUserPass(), request.getIsAdmin());
        }
        catch (StoreException | AccessManagerException e) {
            Metadata trailers = new Metadata(); // fill up if needed
            responseObserver.onError(Status.INTERNAL.withDescription(e.getMessage())
                    .asRuntimeException(trailers));
        }

        return EmptyProto.EmptyResponse.getDefaultInstance();
    }

    private UserInfoResponseProto.UserInfoResponse getUserInfoInternal(
            UserInfoRequestProto.UserInfoRequest request,
            StreamObserver<UserInfoResponseProto.UserInfoResponse> responseObserver) {
        try {
            final UserInfo userInfo = accessManager.getUserInfo(request.getUserName());
            return userInfo.map();
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return UserInfoResponseProto.UserInfoResponse.getDefaultInstance();
    }

    private UserInfoResponseProto.UserInfoResponse getUserGroupsInternal(
            UserInfoRequestProto.UserInfoRequest request,
            StreamObserver<UserInfoResponseProto.UserInfoResponse> responseObserver) {

        try {
            final List<String> userGroups = accessManager.getUserGroups(request.getUserName());
            final UserInfoResponseProto.UserInfoResponse.Builder builder =
                    UserInfoResponseProto.UserInfoResponse.newBuilder();
            return builder.setUserName(request.getUserName())
                    .addAllUserGroups(userGroups)
                    .build();
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return UserInfoResponseProto.UserInfoResponse.getDefaultInstance();
    }

    private EmptyProto.EmptyResponse removeUserInternal(
            UserInfoRequestProto.UserInfoRequest request,
            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        try {
            accessManager.removeUser(request.getUserName());
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return EmptyProto.EmptyResponse.getDefaultInstance();
    }

    private EmptyProto.EmptyResponse addUserToGroupInternal(
            RemoveUserFromGroupRequestProto.RemoveUserFromGroupRequest request,
            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {
        try {
            accessManager.addUserToGroup(request.getUserName(), request.getUserGroup());
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return EmptyProto.EmptyResponse.getDefaultInstance();
    }

    private EmptyProto.EmptyResponse createGroupInternal(
            CreateGroupRequestProto.CreateGroupRequest request,
            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {

        try {
            accessManager.createGroup(request.getGroupName(), request.getGroupDescription());
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return EmptyProto.EmptyResponse.getDefaultInstance();
    }

    private EmptyProto.EmptyResponse removeGroupInternal(
            UserGroupRequestProto.UserGroupRequest request,
            StreamObserver<EmptyProto.EmptyResponse> responseObserver) {

        try {
            accessManager.removeGroup(request.getGroupName());
        }
        catch (StoreException e) {
            responseObserver.onError(Status.INTERNAL
                    .withDescription(e.getMessage())
                    .asRuntimeException());
        }
        return EmptyProto.EmptyResponse.getDefaultInstance();
    }
}
