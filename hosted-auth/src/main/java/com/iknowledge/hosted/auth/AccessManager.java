package com.iknowledge.hosted.auth;

import com.google.common.base.Strings;
import com.iknowledge.auth.tools.AuthTools;
import com.iknowledge.auth.tools.PasswordInfo;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.users.UserInfo;
import com.iknowledge.hosted.hbase.repositories.users.UserStore;
import com.iknowledge.hosted.hbase.repositories.users.groups.UserGroupInfo;
import com.iknowledge.hosted.hbase.repositories.users.groups.UserGroupStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.text.MessageFormat.format;

/**
 * Responsible for managing users, groups and relations between them
 */
public final class AccessManager {
    private final Logger logger;

    private final AuthTools authTools;
    private final UserStore userStore;
    private final UserGroupStore userGroupStore;

    /**
     *
     * @param authTools
     * @param userStore
     * @param userGroupStore
     */
    public AccessManager(final AuthTools authTools,
                         final UserStore userStore,
                         final UserGroupStore userGroupStore) {
        this.authTools = authTools;
        this.userStore = userStore;
        this.userGroupStore = userGroupStore;
        logger = LoggerFactory.getLogger(AccessManager.class);
    }

    /**
     * Plain method for user creation
     * @param userName
     * @param password
     * @param isAdmin
     * @throws AccessManagerException
     * @throws StoreException
     */
    public void createUser(String userName, String password, boolean isAdmin)
            throws AccessManagerException, StoreException {
        createUserInternal(userName, password, isAdmin);
    }

    /**
     * Get full user info based on user name
     * @param userName
     * @throws StoreException
     */
    public UserInfo getUserInfo(String userName) throws StoreException {
        checkState(!Strings.isNullOrEmpty(userName), "UserName should be defined");
        return userStore.get(userName);
    }

    /**
     * Get groups by user name
     * @param userName
     * @throws StoreException
     */
    public List<String> getUserGroups(String userName) throws StoreException {
        checkState(!Strings.isNullOrEmpty(userName), "UserName should be defined");
        return userStore.getUserGroups(userName);
    }

    /**
     * Completely remove user from system
     * @param userName
     * @throws StoreException
     */
    public void removeUser(String userName) throws StoreException {
        checkState(!Strings.isNullOrEmpty(userName), "UserName should be defined");
        final List<String> userGroups = getUserGroups(userName);
        userStore.delete(userName);
        for (String userGroup : userGroups) {
            userGroupStore.removeUserFromGroup(userGroup, userName);
        }
    }

    /**
     * Add user to the specified group
     * @param userName
     * @param userGroup
     * @throws StoreException
     */
    public void addUserToGroup(String userName, String userGroup) throws StoreException {
        userStore.addUserToGroup(userName, userGroup);
        // NOTE: reversed order of parameter compared to prev line
        userGroupStore.addUserToGroup(userGroup, userName);
    }

    /**
     * Remove user from the specified group
     * @param userName
     * @param userGroup
     * @throws StoreException
     */
    public void removeUserFromGroup(String userName, String userGroup) throws StoreException {
        userStore.removeUserFromGroup(userName, userGroup);
        // NOTE: reversed order of parameter compared to prev line
        userGroupStore.removeUserFromGroup(userGroup, userName);
    }

    /**
     * Add new user group
     * @param groupName
     * @param groupDescription
     * @throws StoreException
     */
    public void createGroup(String groupName, String groupDescription) throws StoreException {
        userGroupStore.put(groupName, groupDescription);
    }

    /**
     * Completely remove user group
     * @param groupName
     */
    public void removeGroup(String groupName) throws StoreException {
        final UserGroupInfo userGroupInfo = userGroupStore.get(groupName);
        userGroupStore.delete(groupName);
        for (String user : userGroupInfo.getUsers()) {
            userStore.removeUserFromGroup(user, groupName);
        }
    }

    private void createUserInternal(String userName, String password, boolean isAdmin)
            throws AccessManagerException, StoreException {
        checkState(!Strings.isNullOrEmpty(userName), "UserName should be defined");
        checkState(!Strings.isNullOrEmpty(password), "Password should be defined");
        final PasswordInfo passwordInfo;

        try {
            passwordInfo = authTools.getHashForPassword(password.trim(), StandardCharsets.UTF_8);
        }
        catch (NoSuchAlgorithmException e) {
            logger.error(format("Error has occurred during password hash creating. "
                    + "userName: [{0}]", userName), e);
            throw new AccessManagerException(e);
        }

        try {
            if (isAdmin) {
                userStore.put(userName, passwordInfo.getHash(), passwordInfo.getSalt(),
                        UserGroupStore.ADMIN_GROUP_NAME);
                userGroupStore.put(UserGroupStore.ADMIN_GROUP_NAME, "", userName);
            } else {
                userStore.put(userName, passwordInfo.getHash(), passwordInfo.getSalt(),
                        UserGroupStore.ALL_GROUP_NAME);
                userGroupStore.put(UserGroupStore.ALL_GROUP_NAME, "", userName);
            }
        }
        catch (StoreException e) {
            userStore.delete(userName);
        }
    }
}
