package com.iknowledge.hosted.auth.grpc;

import com.iknowledge.hosted.auth.AccessManager;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public final class AccessManagerServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessManagerServer.class);

    private final Server server;
    private final AccessManager accessManager;

    /**
     *
     * @param port
     * @param accessManager
     * @throws IOException
     */
    public AccessManagerServer(int port, AccessManager accessManager) throws IOException {
        this(ServerBuilder.forPort(port), accessManager);
    }

    /**
     * Create a AccessManagerServer server using serverBuilder as a base.
     */
    public AccessManagerServer(ServerBuilder<?> serverBuilder, AccessManager accessManager) {
        server = serverBuilder.addService(new AccessManagerService(accessManager))
                .build();
        this.accessManager = accessManager;
    }

    /**
     * main
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
//        AccessManager accessManager = new AccessManager();
//        AccessManagerServer server = new AccessManagerServer(8980, accessManager);
//        server.start();
//        server.blockUntilShutdown();
    }

    /**
     * Start serving requests.
     */
    public void start() throws IOException {
        server.start();
        System.err.println("Server started, listening on " + server.getPort());

        LOGGER.info("Server started, listening on " + server.getPort());
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may has been reset by its JVM shutdown hook.
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            AccessManagerServer.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    /**
     * Stop serving requests and shutdown resources.
     */
    public void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }
}
