<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>com.iknowledge</groupId>
        <artifactId>i-knowledge-backend</artifactId>
        <version>0.1.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>spark-parent-pom</artifactId>
    <packaging>pom</packaging>

    <properties>
        <javac.version>1.8</javac.version>
        <java.release></java.release>
        <spark.scope>provided</spark.scope>
        <distribution.dir>${project.build.directory}/dist</distribution.dir>
        <docker.image.name>registry.gitlab.com/i-knowledge/spark/${project.artifactId}:${project.version}</docker.image.name>
        <guava.version>15.0</guava.version>
        <reflections.version>0.9.9</reflections.version><!--caused by guava-->
        <commons.cli.version>1.2</commons.cli.version>
        <jackson.databind.version>2.6.5</jackson.databind.version>
        <matrix.toolkits.version>1.0.2</matrix.toolkits.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.reflections</groupId>
                <artifactId>reflections</artifactId>
                <version>${reflections.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-cli</groupId>
                <artifactId>commons-cli</artifactId>
                <version>${commons.cli.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>com.googlecode.matrix-toolkits-java</groupId>
            <artifactId>mtj</artifactId>
            <version>${matrix.toolkits.version}</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.databind.version}</version>
        </dependency>

        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>

        <dependency>
            <groupId>com.iknowledge</groupId>
            <artifactId>spark-task-base</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-shaded-client</artifactId>
        </dependency>

        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
        </dependency>

        <dependency>
            <groupId>commons-configuration</groupId>
            <artifactId>commons-configuration</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.hbase</groupId>
            <artifactId>hbase-mapreduce</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>com.fasterxml.jackson.core</groupId>
                    <artifactId>jackson-databind</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.apache.spark</groupId>
            <artifactId>spark-core_2.11</artifactId>
            <scope>${spark.scope}</scope>
            <exclusions>
                <exclusion>
                    <groupId>io.netty</groupId>
                    <artifactId>netty</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.iknowledge.spark</groupId>
            <artifactId>spark-mllib_2.11</artifactId>
            <scope>${spark.scope}</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-api</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>

        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <targetPath>${project.build.directory}/conf</targetPath>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>src/main/scripts</directory>
                <targetPath>${project.build.directory}/scripts</targetPath>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>../docker/spark-task</directory>
                <targetPath>${distribution.dir}</targetPath>
                <filtering>true</filtering>
            </resource>
        </resources>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-toolchains-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>toolchain</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <toolchains>
                            <jdk>
                                <version>${javac.version}</version>
                            </jdk>
                        </toolchains>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <executions>
                        <execution>
                            <id>package-spark-jar</id>
                            <phase>package</phase>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <configuration>
                                <descriptors>
                                    <descriptor>../assembly/spark-app-jar.xml</descriptor>
                                </descriptors>
                                <outputDirectory>${distribution.dir}</outputDirectory>
                                <appendAssemblyId>true</appendAssemblyId>
                                <archive>
                                    <manifest>
                                        <mainClass>${spark.driver.class}</mainClass>
                                    </manifest>
                                </archive>
                            </configuration>
                        </execution>
                        <execution>
                            <id>package-spark-app</id>
                            <phase>package</phase>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <configuration>
                                <descriptors>
                                    <descriptor>../assembly/spark-app-tar.xml</descriptor>
                                </descriptors>
                                <outputDirectory>${distribution.dir}</outputDirectory>
                                <appendAssemblyId>false</appendAssemblyId>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>io.fabric8</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <configuration>
                        <images combine.children="append">
                            <image>
                                <alias>${project.artifactId}</alias>
                                <name>${docker.image.name}</name>
                                <run>
                                    <skip>true</skip>
                                </run>
                                <build>
                                    <dockerFileDir>${distribution.dir}</dockerFileDir>
                                    <tags>
                                        <tag>${project.version}</tag>
                                    </tags>
                                </build>
                            </image>
                        </images>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
</project>
