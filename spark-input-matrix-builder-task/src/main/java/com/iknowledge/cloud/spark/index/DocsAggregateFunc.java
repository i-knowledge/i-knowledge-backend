package com.iknowledge.cloud.spark.index;

import org.apache.spark.api.java.function.Function2;

/**
 * TODO: write doc
 */
final class DocsAggregateFunc
    implements Function2<Bm25DocCollectionStats, Document, Bm25DocCollectionStats> {

    @Override
    public Bm25DocCollectionStats call(Bm25DocCollectionStats collectionStats,
                                       Document document) {

        collectionStats.docsCount++;
        collectionStats.averageDocLength += document.length();
        return collectionStats;
    }
}
