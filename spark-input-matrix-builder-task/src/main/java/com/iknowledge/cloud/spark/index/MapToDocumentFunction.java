package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.iknowledge.hbase.client.HConfig;
import scala.Tuple2;

import java.util.Iterator;
import java.util.List;

public final class MapToDocumentFunction
    extends DocVectorStoreContainer
    implements FlatMapFunction<Iterator<Tuple2<ImmutableBytesWritable, Result>>, Document> {

    public MapToDocumentFunction(final String tableName, final HConfig hadoopConf) {
        super(tableName, hadoopConf);
    }

    @Override
    public Iterator<Document> call(
        final Iterator<Tuple2<ImmutableBytesWritable, Result>> kvList) {
        return new Iterator<Document>() {

            private final DocVectorStore store = docVectorStore();

            @Override
            public boolean hasNext() {
                return kvList.hasNext();
            }

            @Override
            public Document next() {
                final Tuple2<ImmutableBytesWritable, Result> kv = kvList.next();
                Id docId = Id.of(kv._1.get());
                try {
                    final List<TokenProto.Token> tokens = store.reader(docId).readVector();
                    return new Document(docId, tokens);
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}
