package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.cloud.spark.HContainer;
import com.iknowledge.hbase.repositories.StoreConfig;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;

import java.io.IOException;

public class DocVectorStoreContainer extends HContainer {

    private final String tableName;
    private final HConfig hadoopConf;

    public DocVectorStoreContainer(String tableName, final HConfig hadoopConf) {
        super(hadoopConf.asConfiguration());

        this.tableName = tableName;
        this.hadoopConf = hadoopConf;
    }

    protected DocVectorStore docVectorStore() {
        try {
            final Connection connection = ConnectionFactory.createConnection(hconfig());
            return new DocVectorStore(StoreConfig.builder()
                                                 .withConnection(connection)
                                                 .withTableName(tableName)
                                                 .withHconfig(hadoopConf)
                                                 .build());
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
