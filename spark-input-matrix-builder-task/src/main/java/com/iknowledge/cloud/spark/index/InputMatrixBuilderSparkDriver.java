package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.spark.HadoopSparkTaskBase;
import com.iknowledge.common.Id;
import no.uib.cipr.matrix.DenseMatrix;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.FloatDenseMatrix;
import org.apache.spark.mllib.linalg.FloatVector;
import org.apache.spark.mllib.linalg.distributed.FloatRow;
import org.apache.spark.mllib.linalg.distributed.SVD;
import org.apache.spark.mllib.linalg.distributed.SvdFloatMatrix;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.IndexRecord;
import scala.collection.Iterator;

import java.util.Map;
import java.util.UUID;

/**
 * Spark task driver class description here.
 */
public final class InputMatrixBuilderSparkDriver extends HadoopSparkTaskBase {

    private static final long serialVersionUID = UUID.randomUUID().getLeastSignificantBits();
    private static final String DOC_VECTOR_TABLE = "doc.vector.table";
    private static final String ZK_QUORUM = "zookeeper.quorum";
    private static final String HBASE_ZNODE = "hbase.znode";
    private static final String HBASE_RPC_TIMEOUT = "hbase.rpc.timeout";
    private static final String HBASE_RETRY_COUNT = "hbase.retry.count";
    private static final String HBASE_RETRY_BACKOFF = "hbase.retry.backoff";
    private static final String HBASE_SCAN_BATCH_SIZE = "hbase.scan.batch.size";
    private static final String HBASE_SCAN_CACHE_SIZE = "hbase.scan.cache.size";
    private static final String HBASE_CONNECTION_THREADS = "hbase.connection.threads";
    private static final String HBASE_THREAD_POOL_MAX_TASKS = "hbase.thread.pool.max.tasks";
    private static final String SVD_K_PARAM = "svd.k.param";
    private static final String POWER_ITERATIONS = "power.iterations";
    private static final String MATRICES_TABLE_NAME = "hbase.matrices.table";
    private static final String BATCH_SIZE = "batch.size";

    private HConfig hbaseConf;

    /**
     * Create instance of Spark task driver.
     */
    public InputMatrixBuilderSparkDriver() {
        super("Spark task description here", createCmdOptions());
    }

    @Override
    protected void executeProcessing(final JavaSparkContext sparkCtx,
                                     final PropertiesConfiguration driverConf) {
        final int svdK = driverConf.getInt(SVD_K_PARAM);
        final int powerIterations = driverConf.getInt(POWER_ITERATIONS);
        final String matricesTable = driverConf.getString(MATRICES_TABLE_NAME);
        final int batchSize = driverConf.getInt(BATCH_SIZE);


        final DocVectorHBaseInput input
            = new DocVectorHBaseInput(sparkCtx,
                                      hbaseConf,
                                      driverConf.getString(DOC_VECTOR_TABLE));

        final IndexTask task = new IndexTask(1.2, 0.75, StorageLevel.DISK_ONLY_2());
        final JavaRDD<IndexRecord<Long, Id>> indexedDocsRdd = task.execute(input.readDocs());
        indexedDocsRdd.persist(StorageLevel.DISK_ONLY_2());

        final PostMappingTask postMappingTask = new PostMappingTask();
        final JavaPairRDD<Id, Long> postsMapping = postMappingTask.execute(indexedDocsRdd);
        postsMapping.persist(StorageLevel.DISK_ONLY_2());

        final long postMappingsCount = postsMapping.count();

        PostIndexedRecordsTask postIndexedRecordsTask = new PostIndexedRecordsTask();
        final JavaPairRDD<Id, Iterable<IndexRecord<Long, Id>>> recordsGroupedByPostId =
                postIndexedRecordsTask.execute(indexedDocsRdd);

        final FloatRowsTask floatRowsTask = new FloatRowsTask(postMappingsCount);
        final JavaRDD<FloatRow<Long>> indexedRows = floatRowsTask.execute(postsMapping,
                recordsGroupedByPostId);

        indexedRows.persist(StorageLevel.DISK_ONLY_2());
        long rowsCount = indexedRows.count();

        SvdParams svdParams = new SvdParams();
        svdParams.svdK = svdK;
        svdParams.rowCount = rowsCount;
        svdParams.columnCount = postMappingsCount;
        svdParams.powerIterations = powerIterations;
        try {
            createAndSaveSvd(matricesTable, indexedRows.rdd(), svdParams, postsMapping, batchSize);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

        logger.info("Matrix build finished");
    }

    @Override
    protected Configuration loadHadoopConf(final PropertiesConfiguration driverProps) {
        hbaseConf = HConfig.newBuilder()
                           .connectionThreads(driverProps.getInt(HBASE_CONNECTION_THREADS,
                                              Runtime.getRuntime().availableProcessors() * 4))
                           .zkQuorum(driverProps.getString(ZK_QUORUM))
                           .znode(driverProps.getString(HBASE_ZNODE))
                           .readRpcTimeout(driverProps.getInt(HBASE_RPC_TIMEOUT, 15000))
                           .writeRpcTimeout(driverProps.getInt(HBASE_RPC_TIMEOUT, 15000))
                           .retryCount(driverProps.getInt(HBASE_RETRY_COUNT, 5))
                           .retryCount(driverProps.getInt(HBASE_RETRY_BACKOFF, 5000))
                           .scanBatchSize(driverProps.getInt(HBASE_SCAN_BATCH_SIZE, 100))
                           .scanBatchSize(driverProps.getInt(HBASE_SCAN_CACHE_SIZE, 30))
                           .threadPoolMaxTasks(driverProps.getInt(HBASE_THREAD_POOL_MAX_TASKS, 50))
                           .build();
        return hbaseConf.asConfiguration();
    }

    @Override
    protected SparkConf loadSparkConf(final PropertiesConfiguration driverProps) {
        return super.loadSparkConf(driverProps);
    }

    /**
     * Create SVD and save its internal matrices (U, sigma and V) into the appropriate store
     *
     * @param matricesTable
     * @param indexedRows
     */
    private void createAndSaveSvd(String matricesTable,
                                  RDD<FloatRow<Long>> indexedRows,
                                  SvdParams svdParams,
                                  JavaPairRDD<Id, Long> postMapping,
                                  int batchSize) throws Exception {
        // containers initialization
//        MatricesStoreContainer matricesStoreContainer =
//                new MatricesStoreContainer(matricesTable, hcontainer.hconfig());
////        TokenMappingStoreContainer tokenMappingStoreContainer =
////                new TokenMappingStoreContainer(tokenMappingTable, hcontainer.hconfig());
//        PostMappingStoreContainer postMappingStoreContainer =
//                new PostMappingStoreContainer(postMappingTable, hcontainer.hconfig());
//        MatrixStore matrixStore = null;
////        TokenMappingStore tokenMappingStore = null;
//        PostMappingStore postMappingStore = null;
//        try {
//            matrixStore = matricesStoreContainer.store();
////            tokenMappingStore = tokenMappingStoreContainer.store();
//            postMappingStore = postMappingStoreContainer.store();
//        }
//        catch (IOException e) {
//            logger.error("Cannot create tables.", e);
//            throw e;
//        }

        indexedRows.persist(StorageLevel.DISK_ONLY_2());
        // matrix and svd creation
        SvdFloatMatrix<Long> svdFloatMatrix =
                new SvdFloatMatrix<>(indexedRows, svdParams.rowCount,
                        svdParams.columnCount, StorageLevel.DISK_ONLY_2());
        final SVD<Long> svd = svdFloatMatrix.directSvd(svdParams.svdK, svdParams.powerIterations);


        logger.info("Initial matrix size: rowCount: [{}], columnCount: [{}]",
                svdParams.rowCount, svdParams.columnCount);
        logger.info("RowMap size: [{}]", svd.rowMap().size());
        logger.info("U matrix sizes: rowCount: [{}], columnCount: [{}]",
                svd.U().numRows(), svd.U().numCols());
        logger.info("Sigma matrix size: [{}]",
                svd.s().size());
        logger.info("Plain V matrix sizes: rowCount: [{}], columnCount: [{}]",
                svd.V().numRows(), svd.V().numCols());


        // U matrix saving
        final Map<Object, Long> rowMap = svd.rowMap();
        final FloatDenseMatrix u = (FloatDenseMatrix) svd.U();
        final float[] sigmaValues = svd.s().toArray();

        DenseMatrix invertibleSigmaMatrix = new DenseMatrix(sigmaValues.length, sigmaValues.length);
        for (int i = 0; i < sigmaValues.length; i++) {
            float sigmaValue = sigmaValues[i];
            invertibleSigmaMatrix.set(i, i, 1 / sigmaValue);
        }

        final double[] doubles = new double[u.values().length];
        for (int i = 0; i < u.values().length; i++) {
            float value = u.values()[i];
            doubles[i] = value;
        }

        no.uib.cipr.matrix.DenseMatrix denseUMatrix =
                new DenseMatrix(u.numRows(), u.numCols(), doubles, false);
        DenseMatrix multResult = new DenseMatrix(denseUMatrix.numRows(),
                invertibleSigmaMatrix.numColumns());

        denseUMatrix.mult(invertibleSigmaMatrix, multResult);

//        final List<Double> dataList = Doubles.asList(multResult.getData());
//        final List<List<Double>> partitions = Lists.partition(dataList, multResult.numRows());
//        for (int i = 0; i < partitions.size(); i++) {
//            final List<Double> partittion = partitions.get(i);
//            try {
//                matrixStore.putUsRecords(i, Doubles.toArray(partittion));
//            }
//            catch (Exception e) {
//                logger.error("Cannot write plain U and sigma multiplication result record.", e);
//                throw e;
//            }
//        }


        final Iterator<FloatVector> vectorIterator = u.rowIter();
        long currentIndex = 0;

//        while (vectorIterator.hasNext()) {
//            final FloatVector vector = vectorIterator.next();
//            final float[] values = vector.toArray();
//            try {
//                final Long token = rowMap.get(currentIndex);
//                matrixStore.putURecord(token, currentIndex, values);
//            }
//            catch (Exception e) {
//                logger.error("Cannot write U matrix record.", e);
//                throw e;
//            }
//
//            currentIndex++;
//        }

        // sigma saving
//        try {
//            matrixStore.putSigmaRecord(sigmaValues);
//        }
//        catch (Exception e) {
//            logger.error("Cannot write sigma values", e);
//            throw e;
//        }

        // transpose V matrix saving
//        final FloatMatrix transposeVMatrix = svd.V().transpose();
//        logger.info("Transpose V matrix sizes: rowCount: [{}], columnCount: [{}]",
//                transposeVMatrix.numRows(), transposeVMatrix.numCols());
//        currentIndex = 0;
//        final Iterator<FloatVector> iterator = transposeVMatrix.rowIter();
//        while (iterator.hasNext()) {
//            final FloatVector vector = iterator.next();
//            final float[] values = vector.toArray();
//            try {
//                final long postMappingId = currentIndex;
//                final Tuple2<Id, Long> record =
//                        postMapping.filter(s -> s._2() == postMappingId)
//                        .first();
////                matrixStore.putVRecord(record._2(), values);
//            }
//            catch (Exception e) {
//                logger.error("Cannot write V transpose matrix record.", e);
//                throw e;
//            }
//            currentIndex++;
//        }

//        // V matrix saving
//        currentIndex = 0;
//        final Iterator<FloatVector> matrixIterator = svd.V().rowIter();
//        while (matrixIterator.hasNext()) {
//            final FloatVector vector = matrixIterator.next();
//            final float[] values = vector.toArray();
//            try {
//                matrixStore.putPlainVRecord(currentIndex, values);
//            }
//            catch (Exception e) {
//                logger.error("Cannot write plain V matrix record.", e);
//                throw e;
//            }
//            currentIndex++;
//        }
    }

    class SvdParams {
        public int svdK;
        public long rowCount;
        public long columnCount;
        public int powerIterations;
    }
}
