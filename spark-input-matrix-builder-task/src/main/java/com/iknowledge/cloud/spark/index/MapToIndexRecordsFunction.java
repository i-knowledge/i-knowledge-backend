package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.IndexRecord;
import scala.Tuple2;

import java.util.Iterator;

public final class MapToIndexRecordsFunction
    extends DocVectorStoreContainer
    implements FlatMapFunction<Iterator<Tuple2<ImmutableBytesWritable, Result>>,
                               IndexRecord<Long, Id>> {

    public MapToIndexRecordsFunction(final String tableName, final HConfig hadoopConf) {
        super(tableName, hadoopConf);
    }

    @Override
    public Iterator<IndexRecord<Long, Id>> call(
        final Iterator<Tuple2<ImmutableBytesWritable, Result>> kvList) {

        return new Iterator<IndexRecord<Long, Id>>() {

            private Id docId;
            private Iterator<TokenProto.Token> tokens;
            private final DocVectorStore store = docVectorStore();

            @Override
            public boolean hasNext() {
                boolean hasNext = tokens != null && tokens.hasNext();
                if (!hasNext && kvList.hasNext()) {
                    hasNext = true;
                    final Tuple2<ImmutableBytesWritable, Result> kv = kvList.next();
                    docId = Id.of(kv._1.get());
                    try {
                        tokens = store.reader(docId)
                                      .readVector()
                                      .iterator();
                    }
                    catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                return hasNext;
            }

            @Override
            public IndexRecord<Long, Id> next() {
                final TokenProto.Token token = tokens.next();
                return new IndexRecord<>(token.getId(), docId, 1L, 0.0d);
            }
        };
    }
}
