package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.FloatSparseVector;
import org.apache.spark.mllib.linalg.distributed.FloatRow;
import org.iknowledge.nlp.tools.IndexRecord;
import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

/**
 * Task for generating rdd of FloatRows by postMappings
 */
public final class FloatRowsTask implements Serializable {

    private static final long serialVersionUID = 372808121150468360L;
    private final long postMappingsCount;

    public FloatRowsTask(final long postMappingsCount) {
        this.postMappingsCount = postMappingsCount;
    }

    public JavaRDD<FloatRow<Long>> execute(
            final JavaPairRDD<Id, Long> postsMapping,
            final JavaPairRDD<Id, Iterable<IndexRecord<Long, Id>>> recordsGroupedByPostId) {
        // join previously created RDDs and create new rdd where key is a token (r.token() - Long)
        // and value is a newly created postId (s._2()._2() - Long)
        final JavaRDD<IndexRecord<Long, Integer>> newIndexedDocsRdd =
                recordsGroupedByPostId.join(postsMapping)
                .flatMap(record -> {
                    final Iterable<IndexRecord<Long, Id>> indexRecords = record._2()._1();
                    final Long docId = record._2()._2();
                    List<IndexRecord<Long, Integer>> list = new ArrayList<>();
                    for (IndexRecord<Long, Id> indexRecord : indexRecords) {
                        list.add(new IndexRecord(indexRecord.token(),
                                Math.toIntExact(docId),
                                indexRecord.hits(),
                                indexRecord.weight())
                        );
                    }
                    return list.iterator();
                });

        // group records by token
        final JavaPairRDD<Long, List<IndexRecord<Long, Integer>>> recordsGroupedByToken =
                newIndexedDocsRdd.groupBy(record -> record.token())
                        .mapToPair(kv -> {
                            final Long tokenId = kv._1();
                            final Iterable<IndexRecord<Long, Integer>> indexRecords = kv._2();
                            final List<IndexRecord<Long, Integer>> list =
                                    StreamSupport.stream(indexRecords.spliterator(), false)
                                            .sorted(Comparator.comparingLong(IndexRecord::postId))
                                            .collect(toList());
                            return new Tuple2<>(tokenId, list);
                        });

        // create float rows
        return recordsGroupedByToken.map(kv -> {
            final List<IndexRecord<Long, Integer>> indexRecords = kv._2();

            final int[] indices = new int[indexRecords.size()];
            final float[] floats = new float[indexRecords.size()];

            for (int i = 0; i < indexRecords.size(); i++ ) {
                final IndexRecord<Long, Integer> indexRecord = indexRecords.get(i);
                indices[i] = indexRecord.postId();
                floats[i] = (float) indexRecord.weight();
            }
            final FloatSparseVector vector =
                    new FloatSparseVector(Math.toIntExact(postMappingsCount),
                            indices,
                            floats);
            final Long tokenId = kv._1();
            return new FloatRow<>(tokenId, vector);

        });
    }
}
