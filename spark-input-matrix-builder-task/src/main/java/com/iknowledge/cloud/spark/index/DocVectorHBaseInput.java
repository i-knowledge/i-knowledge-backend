package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.spark.HBaseInput;
import com.iknowledge.common.Id;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.IndexRecord;

import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

/**
 * HBase input for document vector table.
 */
public class DocVectorHBaseInput extends HBaseInput {

    public DocVectorHBaseInput(final JavaSparkContext sparkContext,
                               final HConfig hconf,
                               final String tableName) {

        super(sparkContext, hconf, tableName);
    }

    /**
     * Read document vectors as index records(for each document token).
     */
    public JavaRDD<IndexRecord<Long, Id>> readDocsAsIndexRecords() {
        return readRaw().mapPartitions(new MapToIndexRecordsFunction(tableName, hconfig));
    }

    /**
     * Read document vector as document instance.
     */
    public JavaRDD<Document> readDocs() {
        return readRaw().mapPartitions(new MapToDocumentFunction(tableName, hconfig))
                        .filter(doc -> doc.length() > 0);
    }

    @Override
    protected Scan createScanner() {
        return new Scan().addFamily(VECTOR_CF_BYTES)
                         .setFilter(new KeyOnlyFilter(false))
                         .readVersions(1)
                         .setBatch(hconfig.scanBatchSize())
                         .setCacheBlocks(false)
                         .setCaching(hconfig.scanCacheSize());
    }
}
