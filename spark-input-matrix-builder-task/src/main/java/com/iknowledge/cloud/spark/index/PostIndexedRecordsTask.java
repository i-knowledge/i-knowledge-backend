package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.iknowledge.nlp.tools.IndexRecord;
import scala.Tuple2;

import java.io.Serializable;

public final class PostIndexedRecordsTask implements Serializable {

    private static final long serialVersionUID = 1468823448652231029L;

    public JavaPairRDD<Id, Iterable<IndexRecord<Long, Id>>> execute(
            final JavaRDD<IndexRecord<Long, Id>> indexedDocsRdd) {
        // group indexed docs records by post id
        return indexedDocsRdd.groupBy(s -> s.postId())
                        .mapToPair(s -> new Tuple2<>(s._1(), s._2()));
    }
}
