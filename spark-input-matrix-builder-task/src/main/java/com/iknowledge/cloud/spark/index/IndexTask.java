package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.storage.StorageLevel;
import org.iknowledge.nlp.tools.Bm25IndexRecord;
import org.iknowledge.nlp.tools.IndexRecord;
import scala.Tuple2;

import java.io.Serializable;

/**
 * Index builder task class.
 * <p>
 *     Class take as input Spark RDD with documents which have to be indexed.
 *     Result of building index is RDD with {@link IndexRecord} structures.
 * </p>
 */
public final class IndexTask implements Serializable {

    private final double bm25ParamK;
    private final double bm25ParamB;
    private final StorageLevel storageLevel;

    /**
     * Create instance of {@link IndexTask}.
     *
     * @param bm25ParamK
     * @param bm25ParamB
     * @param storageLevel
     */
    public IndexTask(final double bm25ParamK,
                     final double bm25ParamB,
                     final StorageLevel storageLevel) {

        this.bm25ParamK = bm25ParamK;
        this.bm25ParamB = bm25ParamB;
        this.storageLevel = storageLevel;
    }

    /**
     * Method build index using passed document vector collection.
     *
     * @param docsRdd Document collection.
     *
     * @return Spark RDD with index data.
     */
    public JavaRDD<IndexRecord<Long, Id>> execute(JavaRDD<Document> docsRdd) {
        final JavaRDD<Bm25IndexRecord<Long, Id>> indexRecords
            = docsRdd.flatMap(new MapToBm25RecordFunction())
                     .persist(storageLevel);
        // count documents in which each token appears
        final JavaPairRDD<Long, Long> tokenDocCount
            = indexRecords.mapToPair(record -> new Tuple2<>(record.token(), 1L))
                          .aggregateByKey(0L, ( v1, v2 ) -> v1 + v2,
                                          ( v1, v2 ) -> v1 + v2)
                          .persist(storageLevel);

        final JavaPairRDD<Long, Bm25IndexRecord<Long, Id>> tokenToRecordRdd
            = indexRecords.mapToPair(record -> new Tuple2<>(record.token(), record));

        JavaRDD<Bm25IndexRecord<Long, Id>> indexRecordRdd
            = tokenDocCount.join(tokenToRecordRdd)
                           .map(kv -> {
                               final Tuple2<Long, Bm25IndexRecord<Long, Id>> recordInfo = kv._2;
                               final Bm25IndexRecord<Long, Id> indexRecord = recordInfo._2;
                               final Long tokenDocsCount = recordInfo._1;
                               indexRecord.setDocsCountForToken(tokenDocsCount);
                               return indexRecord;
                           });

        final Bm25DocCollectionStats bm25Stats
            = docsRdd.aggregate(new Bm25DocCollectionStats(),
                                new DocsAggregateFunc(),
                                new DocsFinalAggregateFunction());

        if (bm25Stats.docsCount <= 0) {
            throw new IllegalStateException("No documents found, indexing stopped");
        }

        bm25Stats.averageDocLength /= bm25Stats.docsCount;

        return indexRecordRdd.map(new Bm25Function(bm25Stats, bm25ParamK, bm25ParamB));
    }
}
