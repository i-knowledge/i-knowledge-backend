package com.iknowledge.cloud.spark.index;

import java.io.Serializable;

/**
 * Document collection statistics for BM25 computation.
 */
class Bm25DocCollectionStats implements Serializable {

    public int docsCount;
    public long averageDocLength;
}
