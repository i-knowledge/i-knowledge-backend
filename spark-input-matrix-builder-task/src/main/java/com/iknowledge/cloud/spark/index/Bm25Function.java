package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.function.Function;
import org.iknowledge.nlp.tools.Bm25;
import org.iknowledge.nlp.tools.Bm25IndexRecord;
import org.iknowledge.nlp.tools.IndexRecord;

/**
 * Function for calculating bm25 weight for token
 */
final class Bm25Function implements Function<Bm25IndexRecord<Long, Id>, IndexRecord<Long, Id>> {

    private final int docsCount;
    private final long docLengthAverage;
    private final double paramK;
    private final double paramB;

    /**
     * Create instance.
     */
    public Bm25Function(Bm25DocCollectionStats stats,
                        double paramK,
                        double paramB) {

        this.docsCount = stats.docsCount;
        this.docLengthAverage = stats.averageDocLength;
        this.paramK = paramK;
        this.paramB = paramB;
    }

    @Override
    public IndexRecord<Long, Id> call(Bm25IndexRecord<Long, Id> record) {
        Bm25 bm25 = new Bm25(docsCount, docLengthAverage, paramK, paramB);
        return bm25.calculate(record);
    }
}
