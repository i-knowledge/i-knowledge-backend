package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Class represent document with title and text.
 */
public class Document {

    private final Id id;
    private final List<TokenProto.Token> tokens;

    /**
     * Create instance of {@link Document}.
     *
     * @param id Document ID.
     * @param tokens Document tokens.
     */
    public Document(final Id id, List<TokenProto.Token> tokens) {
        requireNonNull(id, "Document ID cannot be null");

        this.id = id;
        this.tokens = tokens != null
                        ? tokens
                        : Collections.EMPTY_LIST;
    }

    /**
     * Get document ID.
     */
    public Id id() {
        return id;
    }

    /**
     * Document tokens in protobuf format.
     */
    public List<TokenProto.Token> tokens() {
        return tokens;
    }

    /**
     * Document length.
     */
    public long length() {
        return tokens.size();
    }
}
