package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.iknowledge.nlp.tools.IndexRecord;

import java.io.Serializable;

public final class PostMappingTask implements Serializable {

    private static final long serialVersionUID = 372808121150468360L;

    public JavaPairRDD<Id, Long> execute(final JavaRDD<IndexRecord<Long, Id>> indexedDocsRdd) {
        // set sequential id for each post id
        return indexedDocsRdd.groupBy(s -> s.postId())
                        .map(s -> s._1())
                        .zipWithIndex();
    }
}
