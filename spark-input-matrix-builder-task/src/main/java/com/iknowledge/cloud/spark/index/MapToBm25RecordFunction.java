package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.iknowledge.nlp.tools.Bm25IndexRecord;

import java.util.Iterator;

import static java.util.stream.Collectors.toList;

public final class MapToBm25RecordFunction
    implements FlatMapFunction<Document, Bm25IndexRecord<Long, Id>> {

    @Override
    public Iterator<Bm25IndexRecord<Long, Id>> call(final Document document) {
        return document.tokens()
                       .stream()
                       .map(token -> new Bm25IndexRecord<>(token.getId(),
                                                           document.id(),
                                                           1L,
                                                           0.0d,
                                                           document.length()))
                       .collect(toList())
                       .iterator();
    }
}
