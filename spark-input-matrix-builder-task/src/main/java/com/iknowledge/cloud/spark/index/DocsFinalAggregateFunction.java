package com.iknowledge.cloud.spark.index;

import org.apache.spark.api.java.function.Function2;

/**
 * TODO: write doc
 */
public class DocsFinalAggregateFunction
    implements Function2<Bm25DocCollectionStats,
                            Bm25DocCollectionStats,
                            Bm25DocCollectionStats> {
    @Override
    public Bm25DocCollectionStats call(Bm25DocCollectionStats collectionStats,
                                       Bm25DocCollectionStats collectionStats2) {

        collectionStats.docsCount += collectionStats2.docsCount;
        collectionStats.averageDocLength += collectionStats2.averageDocLength;
        return collectionStats;
    }
}
