package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.awaitility.Awaitility;
import org.iknowledge.hbase.client.HConfig;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static com.iknowledge.cloud.spark.index.DocVectorGenerator.generateDocs;
import static org.assertj.core.api.Assertions.assertThat;

@Test
public class ContainerIt {

    private TableName vectorDb;
    private DocVectorStore vectorStore;
    private Connection connection;
    private GenericContainer sparkTaskContainer;

    @BeforeClass
    public void setUp() throws IOException, StoreException {
        vectorDb = TableName.valueOf("matrix_build_it_doc_vector_table");

        final HConfig config = HConfig.newBuilder()
                                      .retryCount(5)
                                      .retryBackoff(3000)
                                      .readRpcTimeout(25000)
                                      .writeRpcTimeout(25000)
                                      .operationTimeout(25000)
                                      .build();
        connection = ConnectionFactory.createConnection(config.asConfiguration());

        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(vectorDb.getNameAsString())
                                                   .withHconfig(config)
                                                   .build();
        vectorStore = new DocVectorStore(storeConfig);

        try {
            vectorStore.createTable();
        }
        catch (TableAlreadyExistsStoreException e) {
            //ignore
        }

        sparkTaskContainer
            = new GenericContainer(System.getProperty("task.image.name"))
                    .withEnv("SPARK_MASTER", "spark://localhost:7077")
                    .withEnv("EXECUTOR_MEMORY", "512M")
                    .withEnv("DRIVER_MEMORY", "512M")
                    .withEnv("EXECUTOR_DIRECT_MAX_MEMORY", "512M")
                    .withEnv("DRIVER_DIRECT_MAX_MEMORY", "512M")
                    .withLogConsumer(new Consumer<OutputFrame>() {
                        @Override
                        public void accept(final OutputFrame outputFrame) {
                            System.out.println(outputFrame.getUtf8String());
                        }
                    })
                    .withNetworkMode("host")
                    .withClasspathResourceMapping("spark.conf",
                                                  System.getProperty("app.folder") + "/spark.conf",
                                                  BindMode.READ_ONLY);
    }

    @AfterClass
    public void tearDown() throws Exception {
        sparkTaskContainer.stop();
        vectorStore.close();
        connection.getAdmin().disableTable(vectorDb);
        connection.getAdmin().deleteTable(vectorDb);
        connection.close();
    }

    @Test
    public void runTaskTest() throws Exception {
        generateDocs(vectorStore, 500, 100);

        sparkTaskContainer.start();
        Awaitility.await()
                  .pollInSameThread()
                  .atMost(5, TimeUnit.MINUTES)
                  .pollInterval(5, TimeUnit.SECONDS)
                  .until(() -> !sparkTaskContainer.isRunning());
        assertThat(vectorStore.reader(Id.ZERO_ID).readVector())
            .hasSize(1);
    }
}
