package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.cloud.hbase.repositories.DocVectorWriter;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;

import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public final class DocVectorGenerator {

    private DocVectorGenerator() {}

    /**
     * Generate random document vectors.
     *
     * @param vectorStore
     * @param docCount
     * @param tokenCountMax
     * @throws Exception
     */
    public static void generateDocs(DocVectorStore vectorStore,
                             final int docCount,
                             final int tokenCountMax) throws Exception {

        for (int i = 0 ; i < docCount ; i++) {
            try (final DocVectorWriter writer = vectorStore.writer(Id.from(i))) {
                writer.appendTokens(
                    IntStream.range(0, ThreadLocalRandom.current().nextInt(1, tokenCountMax))
                             .mapToObj(idx ->
                                           TokenProto.Token
                                               .newBuilder()
                                               .setId(ThreadLocalRandom.current()
                                                                       .nextLong(1, tokenCountMax))
                                               .setPosition(idx)
                                               .build())
                             .collect(toList()));
                writer.commit();
            }
        }
    }
}
