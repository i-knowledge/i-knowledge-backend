package com.iknowledge.cloud.spark.index;

import com.iknowledge.cloud.hbase.repositories.DocVectorStore;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.nlp.tools.IndexRecord;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import static com.iknowledge.cloud.spark.index.DocVectorGenerator.generateDocs;
import static org.assertj.core.api.Assertions.assertThat;

@Test
public class IndexTaskIt {

    private TableName vectorDbName;
    private DocVectorStore vectorStore;
    private Connection connection;
    private HConfig hconfig;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        vectorDbName = TableName.valueOf("vector_db_" + ThreadLocalRandom.current().nextInt(10000));

        hconfig = HConfig.newBuilder()
                         .retryCount(5)
                         .retryBackoff(3000)
                         .readRpcTimeout(25000)
                         .writeRpcTimeout(25000)
                         .operationTimeout(25000)
                         .build();
        connection = ConnectionFactory.createConnection(hconfig.asConfiguration());

        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(vectorDbName.getNameAsString())
                                                   .withHconfig(hconfig)
                                                   .build();
        vectorStore = new DocVectorStore(storeConfig);
        vectorStore.createTable();
    }

    @AfterClass
    public void after() throws Exception {
        vectorStore.close();
        connection.getAdmin().disableTable(vectorDbName);
        connection.getAdmin().deleteTable(vectorDbName);
        connection.close();
    }

    @Test
    public void testDocsIndexing() throws Exception {

        final int maxTokenId = 1000;
        final int docCount = 500;
        final JavaRDD<IndexRecord<Long, Id>> indexedDocsRdd = createIndexRecords(maxTokenId,
                docCount, "test-docs-indexing");

        final List<IndexRecord<Long, Id>> indexRecords = indexedDocsRdd.collect();

        assertThat(indexRecords)
            .isNotEmpty();

        Set<Id> docsSet = new HashSet<>();
        for (IndexRecord<Long, Id> record : indexRecords) {
            assertThat(record)
                .isNotNull()
                .extracting(IndexRecord::postId, IndexRecord::token)
                .isNotNull();
            assertThat(record.token())
                .isGreaterThan(0);
            assertThat(record.hits())
                .isGreaterThanOrEqualTo(1);
            assertThat(record.weight())
                .isGreaterThan(0);
            docsSet.add(record.postId());
        }
        assertThat(docsSet)
            .hasSize(docCount);
    }

    protected JavaRDD<IndexRecord<Long, Id>> createIndexRecords(
            final int maxTokenId,
            final int docCount,
            final String sparkAppName)
            throws Exception {

        generateDocs(vectorStore, docCount, maxTokenId);

        final SparkConf sparkConf = new SparkConf().setAppName(sparkAppName)
                .setMaster("local[*]");
        final JavaSparkContext sparkCtx = new JavaSparkContext(sparkConf);
        final DocVectorHBaseInput input
                = new DocVectorHBaseInput(sparkCtx,
                hconfig,
                this.vectorDbName.getNameAsString());

        final IndexTask task = new IndexTask(1.2, 0.75, StorageLevel.MEMORY_ONLY());
        return task.execute(input.readDocs());
    }
}
