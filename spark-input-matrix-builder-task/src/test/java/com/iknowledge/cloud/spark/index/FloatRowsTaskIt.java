package com.iknowledge.cloud.spark.index;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.distributed.FloatRow;
import org.apache.spark.storage.StorageLevel;
import org.iknowledge.nlp.tools.IndexRecord;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public final class FloatRowsTaskIt extends IndexTaskIt {
    @BeforeClass
    @Override
    public void setup() throws StoreException, IOException {
        super.setup();

    }

    @AfterClass
    @Override
    public void after() throws Exception {
        super.after();
    }

    @Test
    public void testFloatRowsTask() throws Exception {
        final int maxTokenId = 1000;
        final int docCount = 500;
        final JavaRDD<IndexRecord<Long, Id>> indexedDocsRdd =
                createIndexRecords(docCount, maxTokenId, "test-float-rows-task");
        indexedDocsRdd.persist(StorageLevel.MEMORY_ONLY());
        final long tokensCount = indexedDocsRdd.groupBy(s -> s.token()).count();

        final PostMappingTask postMappingTask = new PostMappingTask();
        final JavaPairRDD<Id, Long> postsMapping = postMappingTask.execute(indexedDocsRdd);

        postsMapping.persist(StorageLevel.MEMORY_ONLY());
        final long postMappingsCount = postsMapping.count();

        PostIndexedRecordsTask postIndexedRecordsTask = new PostIndexedRecordsTask();
        final JavaPairRDD<Id, Iterable<IndexRecord<Long, Id>>> recordsGroupedByPostId =
                postIndexedRecordsTask.execute(indexedDocsRdd);

        final FloatRowsTask floatRowsTask = new FloatRowsTask(postMappingsCount);
        final JavaRDD<FloatRow<Long>> indexedRowsRdd = floatRowsTask.execute(postsMapping,
                recordsGroupedByPostId);
        final List<FloatRow<Long>> floatRows = indexedRowsRdd.collect();

        assertThat(floatRows)
                .isNotEmpty();

        assertThat((long) floatRows.size())
                .isEqualTo(tokensCount);
    }
}
