package com.iknowledge.hbase.repositories;

import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * Interface for HBase store implementation.
 * Store intended to be used like repository class which contains
 * data manipulation and data access methods on some HBase table.
 * <b>Note</b>: Implementation MUST be thread-safe.
 */
public abstract class Store implements AutoCloseable {

    protected final StoreConfig storeConfig;

    /**
     * @param storeConfig Store connection instance
     */
    public Store(StoreConfig storeConfig) {
        requireNonNull(storeConfig, "Store config missed");

        this.storeConfig = storeConfig;
    }

    protected Table getTable() throws StoreException {
        try {
            return this.storeConfig.connection().getTable(this.storeConfig.tableName());
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    protected Admin getAdmin() throws StoreException {
        try {
            return this.storeConfig.connection().getAdmin();
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Truncate the existent HBase table
     * @throws StoreException
     */
    public void truncateTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                admin.disableTable(this.storeConfig.tableName());
                admin.truncateTable(this.storeConfig.tableName(), true);
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Check if HBase table exists
     * @return true if table exists
     * @throws StoreException
     */
    protected boolean isTableExists() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                return admin.tableExists(this.storeConfig.tableName());
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    @Override
    public void close() throws Exception {}

    /**
     * Overridable method to create table for this store
     *
     * @throws TableAlreadyExistsStoreException
     * @throws StoreException
     */
    public void createTable() throws StoreException {}
}

