package com.iknowledge.hbase.repositories;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.iknowledge.hbase.client.HConfig;

public final class StoreConfig {

    private final Connection connection;
    private final TableName tableName;
    private final HConfig hconfig;

    private StoreConfig(final Builder builder) {
        connection = builder.connection;
        tableName = TableName.valueOf(builder.tableName);
        hconfig = builder.hconfig;
    }

    public static IConnection builder() {
        return new Builder();
    }

    public Connection connection() {
        return connection;
    }

    public TableName tableName() {
        return tableName;
    }

    public HConfig hconfig() {
        return hconfig;
    }

    public interface IBuild {

        StoreConfig build();
    }

    public interface IHconfig {

        IBuild withHconfig(HConfig val);
    }

    public interface ITableName {

        IHconfig withTableName(String val);
    }

    public interface IConnection {

        ITableName withConnection(Connection val);
    }

    public static final class Builder implements IHconfig, ITableName, IConnection, IBuild {

        private HConfig hconfig;
        private String tableName;
        private Connection connection;

        private Builder() {}

        @Override
        public IBuild withHconfig(final HConfig val) {
            hconfig = val;
            return this;
        }

        @Override
        public IHconfig withTableName(final String val) {
            tableName = val;
            return this;
        }

        @Override
        public ITableName withConnection(final Connection val) {
            connection = val;
            return this;
        }

        public StoreConfig build() {
            return new StoreConfig(this);
        }
    }
}

