package com.iknowledge.hbase.repositories;

public class TableAlreadyExistsStoreException extends StoreException {

    public TableAlreadyExistsStoreException() {
    }

    public TableAlreadyExistsStoreException( final String message ) {
        super(message);
    }

    public TableAlreadyExistsStoreException( final String message, final Throwable cause ) {
        super(message, cause);
    }

    public TableAlreadyExistsStoreException( final Throwable cause ) {
        super(cause);
    }

    public TableAlreadyExistsStoreException( final String message, final Throwable cause,
                                             final boolean enableSuppression,
                                             final boolean writableStackTrace ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
