package com.iknowledge.hbase.repositories;

import org.apache.hadoop.hbase.util.Bytes;

/**
 * HBase commonly used default values
 */
public final class HDefaults {
    /**
     * Bytes of the boolean 'true' value
     */
    public static final byte[] BOOL_TRUE_BYTES = Bytes.toBytes(true);

    /**
     * Bytes of the boolean 'false' value
     */
    public static final byte[] BOOL_FALSE_BYTES = Bytes.toBytes(false);

    private HDefaults() {}
}
