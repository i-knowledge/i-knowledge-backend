package com.iknowledge.common;

import com.google.common.primitives.Ints;
import org.apache.commons.codec.binary.Hex;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;

/**
 * ID container
 */
public final class Id implements Serializable {

    /**
     * Zero ID with length 1
     */
    public static final Id ZERO_ID = new Id(new byte[] { 0 });

    private static final SecureRandom numberGenerator = new SecureRandom();

    private final byte[] id;

    private Id(byte[] id) {
        this.id = id;
    }

    /**
     * Create ID from byte array
     *
     * @param id ID byte array
     *
     * @return
     */
    public static Id of(byte[] id) {
        requireNonNull(id);

        return new Id(id);
    }

    /**
     * Create ID from integer value
     *
     * @param id ID integer value
     *
     * @return
     */
    public static Id from(int id) {
        return new Id(Ints.toByteArray(id));
    }

    /**
     * Generate new ID with length of 16 bytes
     */
    public static Id len16() {
        return generate(16);
    }

    /**
     * Generate new ID with length of 8 bytes
     */
    public static Id len8() {
        return generate(8);
    }

    /**
     * Generate new ID with length of 4 bytes
     */
    public static Id len4() {
        return generate(4);
    }

    /**
     * ID bytes
     * @return
     */
    public byte[] bytes() {
        return id;
    }

    /**
     * ID length
     * @return
     */
    public int length() {
        return id.length;
    }

    /**
     * Create new ID instance with passed integer prefix
     *
     * @param prefix Integer prefix which will be written as 4 byte prefix
     *
     * @return New ID instance
     */
    public Id withPrefix(int prefix) {
        final byte[] newId = new byte[id.length + Integer.BYTES];
        System.arraycopy(Ints.toByteArray(prefix), 0, newId, 0, Integer.BYTES);
        System.arraycopy(id, 0, newId, Integer.BYTES, id.length);
        return new Id(newId);
    }

    /**
     * Create new ID instance with passed integer suffix
     *
     * @param suffix Integer suffix which will be written as 4 byte prefix
     *
     * @return New ID instance
     */
    public Id withSuffix(int suffix) {
        final byte[] newId = new byte[id.length + Integer.BYTES];
        System.arraycopy(id, 0, newId, 0, id.length);
        System.arraycopy(Ints.toByteArray(suffix), 0, newId, id.length, Integer.BYTES);
        return new Id(newId);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        Id that = (Id) other;

        return Arrays.equals(this.id, that.id);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(id);
    }

    @Override
    public String toString() {
        return Hex.encodeHexString(id);
    }

    private static Id generate(final int length) {
        final byte[] id = new byte[length];
        numberGenerator.nextBytes(id);
        return new Id(id);
    }
}
