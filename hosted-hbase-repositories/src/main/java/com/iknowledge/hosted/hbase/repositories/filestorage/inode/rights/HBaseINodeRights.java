package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeRightsProto;

/**
 * Base class for the inode rights
 */
public abstract class HBaseINodeRights {
    private final Id entityId;
    private final INodeRights entityRights;

    /**
     *
     * @param entityId
     * @param entityRights
     */
    public HBaseINodeRights(final Id entityId, INodeRights entityRights) {
        this.entityId = entityId;
        this.entityRights = entityRights;
    }

    /**
     * Get specified entity id, that is connected with rights on the inode
     * @return
     */
    public Id getEntityId() {
        return entityId;
    }

    /**
     * Get specified entities' rights
     * @return
     */
    public INodeRights getEntityRights() {
        return entityRights;
    }

    /**
     * Map the current instance to the protobuf one
     * @return
     */
    public INodeRightsProto.INodeRights proto() {
        return INodeRightsProto.INodeRights.getDefaultInstance();
    }

    @Override
    public boolean equals(Object obj) {
        final HBaseINodeRights nodeRights = (HBaseINodeRights) obj;
        if (!entityId.equals(nodeRights.getEntityId())) {
            return false;
        }

        if (!this.entityRights.equals(nodeRights.getEntityRights())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return entityId.hashCode();
    }
}
