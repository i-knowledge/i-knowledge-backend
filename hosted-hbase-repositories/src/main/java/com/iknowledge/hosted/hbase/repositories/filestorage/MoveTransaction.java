package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Arrays;

import static java.text.MessageFormat.format;

/**
 * <p>
 *     Class represent transaction which add existing inode as proposed child
 *     to some new parent(move inode).
 * </p>
 * <p>
 *     Used to atomically change parent of inode as two step process:<br/>
 *     <ol>
 *         <li>add inode as proposed child to new parent</li>
 *         <li>commit inode as valid child of new parent</li>
 *     </ol>
 *     Detach of inode from old parent should be called between begin and commit operations.
 * </p>
 * If some of this steps fail, we can restore consistent state of inode by checking
 * children state of old and new parent.
 *
 * @throws StoreException
 */
class MoveTransaction implements FsTransaction {

    private final Fs.Transaction.MoveCtx ctx;
    private final INodeImageStore imageStore;
    private final Table table;

    public MoveTransaction(Fs.Transaction.MoveCtx ctx, INodeImageStore imageStore, Table table) {
        this.ctx = ctx;
        this.imageStore = imageStore;
        this.table = table;
    }

    @Override
    public void apply() throws StoreException {
        final byte[] srcId = ctx.getSrcId().toByteArray();
        final byte[] srcParentId = ctx.getSrcParentId().toByteArray();
        final byte[] sinkParentId = ctx.getSinkParentId().toByteArray();
        try {
            imageStore.detachFromParent(srcId, srcParentId, table);
            imageStore.attachToParent(srcId, sinkParentId, table);
        }
        catch (IOException e) {
            throw new StoreException(format("Move transaction failed for inode: {0}, "
                                            + "new parent id = {1}",
                                            Arrays.toString(srcId),
                                            Arrays.toString(sinkParentId)),
                                     e);
        }
    }
}
