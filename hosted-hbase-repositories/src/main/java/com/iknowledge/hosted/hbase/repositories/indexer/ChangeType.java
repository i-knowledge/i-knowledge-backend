package com.iknowledge.hosted.hbase.repositories.indexer;

import org.apache.hadoop.hbase.util.Bytes;

public enum ChangeType {

    /**
     * New/existing document indexing event
     */
    INDEX_DOC(0),

    /**
     * Remove existing indexed document
     */
    REMOVE_DOC(1);

    final byte[] byteArray;

    ChangeType(int byteArray) {
        this.byteArray = Bytes.toBytes(byteArray);
    }

    static ChangeType from(byte[] type) {
        final int typeVal = Bytes.toInt(type);
        switch (typeVal) {
            case 0:
                return INDEX_DOC;
            case 1:
                return REMOVE_DOC;
            default:
                throw new IllegalArgumentException("Unknown change type");
        }
    }
}
