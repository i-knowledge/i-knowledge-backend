package com.iknowledge.hosted.hbase.repositories.indexer;

import com.google.common.annotations.VisibleForTesting;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;

import java.io.IOException;

import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

/**
 * Documents vector store.
 */
public final class DocVectorStore extends Store {

    /**
     * @param storeConfig Store connection instance
     */
    public DocVectorStore(final StoreConfig storeConfig) {
        super(storeConfig);
    }

    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor vectorCf
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(VECTOR_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setBlockCacheEnabled(false)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(vectorCf);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch ( IOException e ) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Create new document vector writer
     *
     * @param docId Document ID.
     *
     * @return Document writer instance
     *
     * @throws StoreException
     */
    public DocVectorWriter writer(int srcId, Id docId) throws StoreException {
        return new DocVectorWriter(srcId, docId, getTable());
    }

    @VisibleForTesting
    DocVectorWriter writer(int srcId, Id docId, int vectorVersion) throws StoreException {
        return new DocVectorWriter(srcId, docId, vectorVersion, getTable());
    }

    /**
     * Create new document vector reader
     *
     * @param docId Document ID.
     *
     * @return Document reader instance
     *
     * @throws StoreException
     */
    public DocVectorReader reader(int srcId, Id docId) throws StoreException {
        return new DocVectorReader(srcId, docId, getTable());
    }

    /**
     * Remove document from store.
     *
     * @param docId Document ID
     */
    public void remove(int srcId, Id docId) throws StoreException {
        try (final Table table = getTable()) {
            final byte[] id = docId.withPrefix(srcId).bytes();
            table.delete(new Delete(id));
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }
}
