package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectorChanges.CHANGES_META_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectorChanges.CHANGES_TYPE_QF_BYTES;

public final class DocVectorChangesStore extends Store {

    /**
     * Create instance of {@link DocVectorChangesStore}
     *
     * @param storeConfig Store connection instance
     */
    public DocVectorChangesStore(final StoreConfig storeConfig) {
        super(storeConfig);
    }

    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor metaCf
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(CHANGES_META_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(metaCf);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch ( IOException e ) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }


    /**
     * Add document change event.
     *
     * @param docId Full document ID.
     * @param changeType Change type
     *
     * @throws StoreException
     */
    public void addChange(int srcId, Id docId, ChangeType changeType) throws StoreException {
        final byte[] id = docId.withPrefix(srcId).bytes();
        try (Table table = getTable()) {
            table.put(new Put(id).addColumn(CHANGES_META_CF_BYTES,
                                                    CHANGES_TYPE_QF_BYTES,
                                                    changeType.byteArray));
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Download pending changes
     *
     * @param maxChanges Max changes to download.
     *
     * @return Changes list.
     *
     * @throws StoreException
     */
    public List<Change> loadChanges(int maxChanges) throws StoreException {
        List<Change> changes = new ArrayList<>(maxChanges);

        final Scan scan = new Scan().addFamily(CHANGES_META_CF_BYTES)
                                    .setBatch(storeConfig.hconfig().scanBatchSize())
                                    .setCaching(storeConfig.hconfig().scanCacheSize())
                                    .readVersions(1);

        try (Table table = getTable()) {
            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    for (Cell cell : result.listCells()) {
                        if (changes.size() >= maxChanges) {
                            return changes;
                        }
                        final ChangeType type = ChangeType.from(CellUtil.cloneValue(cell));
                        changes.add(new Change(type, CellUtil.cloneRow(cell)));
                    }
                }
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        return changes;
    }

    /**
     * Remove change from store.
     *
     * @param change Change instance.
     *
     * @throws StoreException
     */
    public void removeChange(Change change) throws StoreException {
        try (Table table = getTable()) {
            table.delete(new Delete(change.docId));
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

}
