package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

/**
 * Interface define inode which can be accessed by user(not internal system inode)
 */
public interface UserLevelINode
    extends MutableINode<UserLevelINode>,
            AccessRestrictedINode {

    INodeSnapshot<? extends UserLevelINode> snapshot();
}
