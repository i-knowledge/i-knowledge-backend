package com.iknowledge.hosted.hbase.repositories.users.groups;

import java.util.List;

/**
 * Class that contains information about user group
 */
public final class UserGroupInfo {
    private final String groupName;
    private final String desc;
    private final byte[] id;
    private final List<String> users;

    /**
     *  @param groupName
     * @param desc
     * @param id
     */
    public UserGroupInfo(String groupName, String desc,
                         byte[] id, List<String> users) {
        this.groupName = groupName;
        this.desc = desc;
        this.id = id;
        this.users = users;
    }

    /**
     * Group name
     * @return
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Complete text description of the group
     * @return
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Users that are included to the current group
     * @return
     */
    public List<String> getUsers() {
        return users;
    }

    /**
     * User group id
     * @return
     */
    public byte[] getId() {
        return id;
    }
}
