package com.iknowledge.hosted.hbase.repositories.filestorage;

import java.util.Objects;
import java.util.function.Consumer;

public final class FsLogConfig {

    private final long maxLogSize;
    private final int compactLogEverySec;
    private final int awaitTerminationSec;
    private final Consumer<Throwable> shutdownListener;

    private FsLogConfig(final Builder builder) {
        maxLogSize = builder.maxLogSize;
        compactLogEverySec = builder.compactLogEverySec;
        shutdownListener = builder.shutdownListener;
        awaitTerminationSec = builder.awaitTerminationSec;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Max log size
     * @return
     */
    public long maxLogSize() {
        return maxLogSize;
    }

    /**
     * Log compaction schedule: every N minutes log compaction will be started.
     * @return
     */
    public int compactLogEveryMinutes() {
        return compactLogEverySec;
    }

    public Consumer<Throwable> shutdownListener() {
        return shutdownListener;
    }

    public int awaitTerminationSec() {
        return awaitTerminationSec;
    }

    public static final class Builder {

        private long maxLogSize = 1000;
        private int compactLogEverySec = 60 * 60;
        private int awaitTerminationSec = 60;
        private Consumer<Throwable> shutdownListener = RuntimeException::new;

        private Builder() {}

        public Builder maxLogSize(final long maxLogSize) {
            this.maxLogSize = maxLogSize;
            return this;
        }

        public Builder compactLogEverySec(final int seconds) {
            this.compactLogEverySec = seconds;
            return this;
        }

        public Builder shutdownListener(final Consumer<Throwable> shutdownListener) {
            this.shutdownListener = shutdownListener;
            return this;
        }

        public Builder awaitTerminationSec(final int awaitTerminationSec) {
            this.awaitTerminationSec = awaitTerminationSec;
            return this;
        }

        public FsLogConfig build() {
            Objects.requireNonNull(shutdownListener, "Shutdown listener must be set");
            return new FsLogConfig(this);
        }
    }
}
