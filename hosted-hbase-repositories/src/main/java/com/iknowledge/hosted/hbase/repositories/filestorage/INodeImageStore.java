package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeHUtil;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserGroupRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeRightsProto;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.iknowledge.hbase.client.Batch;
import org.iknowledge.hbase.client.HConfig;
import org.iknowledge.hbase.client.ScanBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_META_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeChildren.INODE_CHILD_PREFIX_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeHUtil.getChildId;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeHUtil.getChildQf;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeHUtil.getUserGroupRightsQf;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeHUtil.getUserRightsQf;
import static org.iknowledge.hbase.client.util.HUtils.throwIfBatchFailed;

/**
 * Class contains representation of inode in HBase and contains associated operation on it.
 * This is stateless class which can be instantiated only once.
 */
final class INodeImageStore {

    private final int scanBatchSize;
    private final int scanCacheSize;

    public INodeImageStore(HConfig config) {
        this.scanBatchSize = config.scanBatchSize();
        this.scanCacheSize = config.scanCacheSize();
    }

    /**
     * Remove inode from FS image.
     *
     * @param table HBase table with inodes
     */
    public void remove(byte[] inodeId, Table table) throws IOException {
        table.delete(new Delete(inodeId));
    }

    /**
     * Detach inode from parent.
     *
     * @param table
     *
     * @throws StoreException
     */
    public void detachFromParent(byte[] inodeId, byte[] parentId, Table table)
            throws IOException {

        final Delete parentChildDelete = new Delete(parentId)
                                                 .addColumn(INODE_CF_BYTES, getChildQf(inodeId));
        table.delete(parentChildDelete);
    }

    /**
     * Load inode metadata
     *
     * @param inodeId inode ID
     * @param table HBase table with inodes
     *
     * @return Metadata instance or null if not found
     *
     * @throws StoreException
     */
    public INodeProto.INode getMeta( byte[] inodeId, Table table )
            throws IOException {

        final Result result = table.get(new Get(inodeId)
                                                .addColumn(INODE_CF_BYTES, INODE_META_QF_BYTES)
                                                .readVersions(1));
        if (result.isEmpty()) {
            return null;
        }
        final Cell metaCell = result.getColumnLatestCell(INODE_CF_BYTES,
                                                         INODE_META_QF_BYTES);
        return INodeProto.INode.parseFrom(CellUtil.cloneValue(metaCell));
    }

    /**
     * Load inode children list
     *
     * @param inodeId inode ID
     *
     * @param table HBase table with inodes
     * @return List of inode children
     *
     * @throws StoreException
     */
    public List<byte[]> getChildren( final byte[] inodeId, final Table table )
            throws IOException {

        final Scan scan = new ScanBuilder()
                                  .setStartRow(inodeId)
                                  .setStopRow(inodeId)
                                  .setStopRowInclusive(true)
                                  .build()
                                  .setBatch(scanBatchSize)
                                  .setCaching(scanCacheSize)
                                  .setCacheBlocks(false)
                                  .readVersions(1)
                                  .addFamily(INODE_CF_BYTES)
                                  .setFilter(new ColumnPrefixFilter(INODE_CHILD_PREFIX_QF_BYTES));

        List<byte[]> children = new ArrayList<>();
        try (ResultScanner scanner = table.getScanner(scan)) {
            scanner.forEach(result -> {
                while (result.advance()) {
                    children.add(getChildId(result.current()));
                }
            });
            return children;
        }
    }

    /**
     * Get all rights that are connected with the specified inode
     *
     * @param inodeId
     * @param table
     *
     * @return
     *
     * @throws StoreException
     */
    public UserRights getRights(final byte[] inodeId, final Table table)
            throws IOException, StoreException {

        final Result result = table.get(new Get(inodeId)
                .addFamily(INODE_CF_BYTES)
                .readVersions(1));
        if (result.isEmpty()) {
            return null;
        }
        List<HBaseINodeRights> rights = new ArrayList<>();
        final List<Cell> cells = result.listCells();
        for (Cell cell : cells) {
            final HBaseINodeRights iNodeRights = INodeHUtil.getRights(cell);
            if (iNodeRights == null) {
                continue;
            }
            rights.add(iNodeRights);
        }
        final List<HBaseINodeRights> userRights = rights.stream()
                .filter(s -> s.getClass() == INodeUserRights.class)
                .collect(Collectors.toList());

        final List<HBaseINodeRights> userGroupRights = rights.stream()
                .filter(s -> s.getClass() == INodeUserGroupRights.class)
                .collect(Collectors.toList());
        return new UserRights(userRights, userGroupRights);
    }

    /**
     * Remove user rights that are connected with the specified inode
     * @param inodeId
     * @param userId
     * @param table
     * @throws StoreException
     */
    public void removeUserRights(final byte[] inodeId, final byte[] userId, final Table table)
            throws IOException {
        final Delete delete = new Delete(inodeId)
                .addColumn(INODE_CF_BYTES, getUserRightsQf(userId));
        table.delete(delete);
    }

    /**
     * Remove user group rights that are connected with the specified inode
     *
     * @param inodeId
     * @param userGroupId
     * @param table
     *
     * @throws StoreException
     */
    public void removeUserGroupRights(final byte[] inodeId, final byte[] userGroupId,
                                      final Table table)
            throws IOException {
        final Delete delete = new Delete(inodeId)
                .addColumn(INODE_CF_BYTES, getUserGroupRightsQf(userGroupId));
        table.delete(delete);
    }

    /**
     * Attache rights that are connected with the specified inode
     * @param inodeId
     * @param rights
     * @param table
     * @throws StoreException
     */
    public void attachRights(final byte[] inodeId, final List<INodeRightsProto.INodeRights> rights,
                                 final Table table, final int batchSize)
            throws IOException {

        final List<INodeRightsProto.INodeRights> userRightsProto = rights.stream()
                .filter(s -> s.getType() == INodeRightsProto.INodeRights.Type.USER)
                .collect(Collectors.toList());
        final List<INodeRightsProto.INodeRights> userGroupRightsProto = rights.stream()
                .filter(s -> s.getType() == INodeRightsProto.INodeRights.Type.USER_GROUP)
                .collect(Collectors.toList());

        final List<INodeUserRights> userRights = userRightsProto.stream()
                .map(s -> INodeUserRights.map(s.getUserRights()))
                .collect(Collectors.toList());

        final List<INodeUserGroupRights> userGroupRights = userGroupRightsProto.stream()
                .map(s -> INodeUserGroupRights.map(s.getUserGroupRights()))
                .collect(Collectors.toList());
        try {
            Batch.<HBaseINodeRights>newBuilder()
                    .withBatchSize(batchSize)
                    .withObjectCollection(userRights)
                    .withMapper(mapUserRightsToPut(inodeId))
                    .withTable(table)
                    .build()
                    .call();

            Batch.<HBaseINodeRights>newBuilder()
                    .withBatchSize(batchSize)
                    .withObjectCollection(userGroupRights)
                    .withMapper(mapUserGroupRightsToPut(inodeId))
                    .withTable(table)
                    .build()
                    .call();
        }
        catch (Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * Add or update existing inode metadata
     *
     * @param inode
     * @param table
     * @throws IOException
     */
    public void putInodeMeta(final byte[] inodeId,
                             final INodeProto.INode inode,
                             final Table table) throws IOException {

        final Put newNodePut = new Put(inodeId)
                                       .addColumn(INODE_CF_BYTES,
                                                     INODE_META_QF_BYTES,
                                                     inode.toByteArray());
        table.put(newNodePut);
    }

    /**
     * Attach inode to parent.
     *
     * @param inode
     * @param parent
     * @param table
     *
     * @throws StoreException
     */
    public void attachToParent(byte[] inode, byte[] parent, Table table)
            throws IOException {

        if ( parent != null) {
            final Put addChildNodeToParentPut
                    = new Put(parent)
                              .addColumn(INODE_CF_BYTES, getChildQf(inode), null);
            table.put(addChildNodeToParentPut);
        }
    }

    /**
     * Attach user access rights to inode
     *
     * @param inodeId
     * @param userRights
     * @param userGroupRights
     * @param table
     * @param batchSize
     * @throws IOException
     */
    public void addInodeRights(final byte[] inodeId,
                                final List<HBaseINodeRights> userRights,
                                final List<HBaseINodeRights> userGroupRights,
                                final Table table,
                                final int batchSize)
            throws IOException {

        try {
            Batch.<HBaseINodeRights>newBuilder()
                    .withBatchSize(batchSize)
                    .withObjectCollection(userRights)
                    .withMapper(mapUserRightsToPut(inodeId))
                    .withTable(table)
                    .build()
                    .call();

            Batch.<HBaseINodeRights>newBuilder()
                    .withBatchSize(batchSize)
                    .withObjectCollection(userGroupRights)
                    .withMapper(mapUserGroupRightsToPut(inodeId))
                    .withTable(table)
                    .build()
                    .call();
        }
        catch (Exception e) {
            throw new IOException(e);
        }
    }

    private Function<HBaseINodeRights, Row> mapUserGroupRightsToPut(byte[] inodeId) {
        return right -> new Put(inodeId)
                .addColumn(INODE_CF_BYTES,
                        INodeHUtil.getUserGroupRightsQf(right.getEntityId().bytes()),
                        right.getEntityRights().getRights());
    }

    private Function<HBaseINodeRights, Row> mapUserRightsToPut(byte[] inodeId) {
        return right -> new Put(inodeId)
                .addColumn(INODE_CF_BYTES,
                        getUserRightsQf(right.getEntityId().bytes()),
                        right.getEntityRights().getRights());
    }

    private void execBatch( final Table table, final List<Row> batch ) throws IOException {
        final Object[] results = new Object[batch.size()];
        try {
            table.batch(batch, results);
        }
        catch (InterruptedException e) {
            throw new IOException(e);
        }
        throwIfBatchFailed(results);
    }
}
