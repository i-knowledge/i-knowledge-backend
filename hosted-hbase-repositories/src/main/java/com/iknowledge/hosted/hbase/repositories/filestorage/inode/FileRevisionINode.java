package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

public final class FileRevisionINode implements INode {

    private final Id id;
    private final Path sysPath;
    private final FileINode fileInode;
    private final ReentrantReadWriteLock lock;
    private final long revision;

    /**
     * Create new revision inode
     *
     * @param revision
     * @param sysPath
     * @param fileInode
     */
    public FileRevisionINode(Id id, long revision, Path sysPath, FileINode fileInode) {
        requireNonNull(id);
        requireNonNull(sysPath);
        requireNonNull(fileInode);

        this.id = id;
        this.sysPath = sysPath;
        this.fileInode = fileInode;
        this.lock = new ReentrantReadWriteLock(true);
        this.revision = revision;
    }

    @Override
    public Id id() {
        return id;
    }

    @Override
    public String name() {
        return fileInode.name() + "_" + revision;
    }

    @Override
    public FileINode parent() {
        return fileInode;
    }

    /**
     * Full file system path used to read/write inode data into underlying storage
     *
     * @implNote Must be read-only value and not change during time and don't require lock.
     *
     * @return
     */
    public Path sysPath() {
        return sysPath;
    }

    @Override
    public Path path() {
        return Paths.get(fileInode.path() + "_" + revision);
    }

    @Override
    public ReadWriteLock lock() {
        return lock;
    }

    /**
     * File revision number
     */
    public long revision() {
        return revision;
    }

    @Override
    public INodeProto.INode proto() {
        return INodeProto.INode.newBuilder()
                               .setType(INodeProto.INode.Type.FILE_REVISION)
                               .setName(name())
                               .setSysPath(sysPath.toString())
                               .setRevision(revision)
                               .build();
    }

    @Override
    public boolean equals( Object other ) {
        if ( this == other ) {
            return true;
        }
        if ( other == null || getClass() != other.getClass() ) {
            return false;
        }

        FileRevisionINode that = ( FileRevisionINode ) other;

        return Objects.equals(this.revision, that.revision)
               && Objects.equals(this.fileInode, that.fileInode);
    }

    @Override
    public int hashCode() {
        return hash(revision, fileInode);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                   .add("id = " + id)
                   .add("name = " + name())
                   .add("revision = " + revision)
                   .add("path = " + path())
                   .add("sysPath = " + sysPath)
                   .toString();
    }
}
