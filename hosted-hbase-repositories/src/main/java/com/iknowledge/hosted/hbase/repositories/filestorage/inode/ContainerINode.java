package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import java.util.Collection;
import java.util.Optional;

public interface ContainerINode<T extends INode> extends INode {

    /**
     * Add new child to the current inode.
     *
     * @implNote Before call, must acquire write lock
     *
     * @param child
     */
    void addChild(INode child) throws IllegalArgumentException;

    /**
     * Remove child inode
     *
     * @implNote Before call, must acquire write lock
     *
     * @param child
     */
    boolean removeChild(INode child);

    /**
     * Method search inode in current children list by name.
     *
     * @param name Child name
     *
     * @return Found child inode
     */
    Optional<? extends T> findChildByName(String name);

    /**
     * Return children inodes of current inode
     *
     * @implNote Before call, must acquire read lock
     *
     * @return
     */
    Collection<? extends T> children();
}
