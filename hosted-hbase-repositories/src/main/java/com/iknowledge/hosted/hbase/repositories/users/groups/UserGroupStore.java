package com.iknowledge.hosted.hbase.repositories.users.groups;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.iknowledge.hbase.repositories.HDefaults.BOOL_TRUE_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.COMMON_CF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.COMMON_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.DESC_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.DESC_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.ID_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.ID_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.USERS_CF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.UserGroups.USERS_CF_BYTES;
import static java.text.MessageFormat.format;

/**
 * Store to operate with data in UserGroups table
 */
public final class UserGroupStore extends Store {
    /**
     * Default group for all users
     */
    public static final String ALL_GROUP_NAME = "All";

    /**
     * Default group for system admins
     */
    public static final String ADMIN_GROUP_NAME = "Admin";

    private static final byte[] ADMIN_GROUP_ID = Bytes.toBytes(0);
    private static final byte[] ALL_GROUP_ID = Bytes.toBytes(1);
    private final Logger logger;

    /**
     * @param storeConfig HBase store config
     */
    public UserGroupStore(StoreConfig storeConfig) {
        super(storeConfig);
        logger = LoggerFactory.getLogger(UserGroupStore.class);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor cfCommonDesc
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(COMMON_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setVersions(1, 1);
                final ColumnFamilyDescriptor cfUserGroupsDesc
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(USERS_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(cfCommonDesc)
                          .addColumnFamily(cfUserGroupsDesc);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Add new user group
     * @param groupName
     * @param groupDesc
     * @throws StoreException
     */
    public void put(String groupName, String groupDesc) throws StoreException {
        Put put = new Put(Bytes.toBytes(groupName))
                .addColumn(COMMON_CF_BYTES, DESC_QF_BYTES, Bytes.toBytes(groupDesc))
                .addColumn(COMMON_CF_BYTES, ID_QF_BYTES, getGroupId(groupName));
        try (Table table = getTable()) {
            table.put(put);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during user group creating. "
                    + "GroupName: [{0}], GroupDesc: [{1}]", groupName, groupDesc), e);
            throw new StoreException(e);
        }
    }

    /**
     * Add new user group
     * @param groupName
     * @param groupDesc
     * @param userName
     * @throws StoreException
     */
    public void put(String groupName, String groupDesc, String userName) throws StoreException {
        put(groupName, groupDesc);
        addUserToGroup(groupName, userName);
    }

    /**
     * Add user to the group
     *
     * @param groupName
     * @param userName
     *
     * @throws StoreException
     */
    public void addUserToGroup(String groupName, String userName) throws StoreException {
        Put put = new Put(Bytes.toBytes(groupName))
                .addColumn(USERS_CF_BYTES, Bytes.toBytes(userName), BOOL_TRUE_BYTES);

        try (Table table = getTable()) {
            table.put(put);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during adding user into the group. "
                    + "GroupName: [{0}], UserName: [{1}]", groupName, userName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Get full information about group by its name
     * @param groupName
     * @return
     */
    public UserGroupInfo get(String groupName) throws StoreException {
        Get get = new Get(Bytes.toBytes(groupName));
        try (Table table = getTable()) {
            final Result result = table.get(get);
            return map(result);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during getting user group info. "
                    + "GroupName: [{0}]", groupName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Remove user from the specified group
     * @param groupName
     * @param userName
     * @throws StoreException
     */
    public void removeUserFromGroup(String groupName, String userName) throws StoreException {
        Delete delete = new Delete(Bytes.toBytes(groupName))
                .addColumn(USERS_CF_BYTES, Bytes.toBytes(userName));
        try (Table table = getTable()) {
            table.delete(delete);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during removing user from the group. "
                    + "GroupName: [{0}], UserName: [{1}]", groupName, userName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Completely remove user group
     *
     * @param groupName
     *
     * @throws StoreException
     */
    public void delete(String groupName) throws StoreException {
        Delete delete = new Delete(Bytes.toBytes(groupName));
        try (Table table = getTable()) {
            table.delete(delete);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during removing user group. "
                    + "GroupName: [{0}]", groupName), e);
            throw new StoreException(e);
        }
    }

    private UserGroupInfo map(Result result) throws StoreException {
        String groupName = "";
        String groupDesc = "";
        byte[] id = null;
        List<String> users = new ArrayList<>();

        final List<Cell> cells = result.listCells();
        for (Cell cell : cells) {
            groupName = Bytes.toString(CellUtil.cloneRow(cell));
            final String family = Bytes.toString(CellUtil.cloneFamily(cell));
            final String qualifier = Bytes.toString(CellUtil.cloneQualifier(cell));
            final byte[] valueBytes = CellUtil.cloneValue(cell);

            if (family.equals(COMMON_CF)) {
                switch (qualifier) {
                    case DESC_QF:
                        groupDesc = Bytes.toString(valueBytes);
                        break;

                    case ID_QF:
                        id = valueBytes;
                        break;

                    default: throw new StoreException("Unknown qualifier in user groups Common CF");
                }
            }

            if (family.equals(USERS_CF)) {
                final boolean isInGroup = Bytes.toBoolean(valueBytes);
                if (isInGroup) {
                    users.add(qualifier);
                }
            }
        }

        return new UserGroupInfo(groupName, groupDesc, id, users);
    }

    private byte[] getGroupId(String groupName) {
        switch (groupName) {
            case ALL_GROUP_NAME:
                return ALL_GROUP_ID;

            case ADMIN_GROUP_NAME:
                return ADMIN_GROUP_ID;

            default:
                return Id.len16().bytes();
        }
    }
}
