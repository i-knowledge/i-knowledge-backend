package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.nio.file.Paths;

final class INodeMeta {

    private final INodeProto.INode meta;

    public INodeMeta(INodeProto.INode meta) {
        this.meta = meta;
    }

    /**
     * Check is this inode corrupted
     *
     * @return
     */
    public boolean isInvalid() {
        return meta == null
               || !meta.hasType()
               || !meta.hasName();
    }

    public INodeProto.INode.Type type() {
        return meta.getType();
    }

    public String name() {
        return meta.getName();
    }

    public long revision() {
        return meta.hasRevision()
                ? meta.getRevision()
                : -1L;
    }

    public Path sysPath() {
        return Paths.get(meta.getSysPath());
    }
}
