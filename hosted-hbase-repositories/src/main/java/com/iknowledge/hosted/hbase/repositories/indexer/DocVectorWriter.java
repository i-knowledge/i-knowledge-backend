package com.iknowledge.hosted.hbase.repositories.indexer;

import com.google.common.annotations.VisibleForTesting;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.RowMutations;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

/**
 * Writer class which create new version of document vector in HBase.
 */
public final class DocVectorWriter extends DocVectorAccessor {

    private final int vectorVersion;
    private int nextBlockNum;

    public DocVectorWriter(final int srcId,
                           final Id docId,
                           final Table indexTable) throws StoreException {

        super(srcId, docId, indexTable);

        vectorVersion = nextVectorVersion();
    }

    @VisibleForTesting
    DocVectorWriter(final int srcId,
                    final Id docId,
                    final int vectorVersion,
                    final Table indexTable)  {

        super(srcId, docId, indexTable);

        this.vectorVersion = vectorVersion;
    }

    /**
     * Write next block of document tokens.
     *
     * @param tokens Token list.
     */
    public void writeBlock(final Iterable<TokenProto.Token> tokens) throws StoreException {
        TokenProto.Block block = TokenProto.Block.newBuilder()
                                                 .addAllToken(tokens)
                                                 .build();
        final byte[] versionBytes = Bytes.toBytes(vectorVersion);
        final Put blockPut = new Put(docId.bytes())
                                    .addColumn(VECTOR_CF_BYTES,
                                                  blockColumnQf(versionBytes, nextBlockNum),
                                                  block.toByteArray());

        try {
            indexTable.put(blockPut);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        nextBlockNum++;
    }

    /**
     * Commit this version of document vector.
     */
    public void commit() throws StoreException {
        final RowMutations rowMutations = new RowMutations(docId.bytes());
        try {
            final Put newVersionPut = versionPut(vectorVersion);
            rowMutations.add(newVersionPut);
            final byte[] prevVersion = getPrevVersion(vectorVersion);
            final Delete prevVectorDelete = versionDelete(prevVersion,
                                                          blocksInVersion(prevVersion));
            if (prevVectorDelete != null) {
                rowMutations.add(prevVectorDelete);
            }
            indexTable.mutateRow(rowMutations);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private int nextVectorVersion() throws StoreException {
        int nextVersion = loadCurrentVersion().orElse(INITIAL_VERSION_NUM);
        if (nextVersion < UNCOMMITED_VERSIONS_RANGE_END) {
            final int uncommittedVersion = nextVersion * -1;
            removeUncommitedVector(uncommittedVersion);
            nextVersion = uncommittedVersion;
        }
        else {
            nextVersion++;
            if (nextVersion < UNCOMMITED_VERSIONS_RANGE_END) {
                nextVersion = INITIAL_VERSION_NUM;
            }
        }
        // set as uncommited
        saveVersion(nextVersion * -1);
        return nextVersion;
    }

    private void saveVersion(int version) throws StoreException {
        try {
            indexTable.put(versionPut(version));
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private Put versionPut(int version) {
        return new Put(docId.bytes()).addColumn(VECTOR_CF_BYTES,
                                           LAST_VERSION_QF_BYTES,
                                           Bytes.toBytes(version));
    }

    private byte[] getPrevVersion(final int curVersion) {
        final int prevVersion;
        if (vectorVersion - 1 < INITIAL_VERSION_NUM) {
            prevVersion = Integer.MAX_VALUE;
        }
        else {
            prevVersion = curVersion - 1;
        }
        return Bytes.toBytes(prevVersion);
    }
}
