package com.iknowledge.hosted.hbase.repositories.users;

import com.iknowledge.common.Id;

import java.util.List;

/**
 * Information about base information about user
 */
public final class UserInfoLite {
    private final Id userId;
    private final List<Id> groupsId;

    /**
     *
     * @param userId
     * @param groupsId
     */
    public UserInfoLite(Id userId, List<Id> groupsId) {
        this.userId = userId;
        this.groupsId = groupsId;
    }

    /**
     * User id
     * @return
     */
    public Id getUserId() {
        return userId;
    }

    /**
     * List of bytes of groups' ids
     * @return
     */
    public List<Id> getGroupsId() {
        return groupsId;
    }
}
