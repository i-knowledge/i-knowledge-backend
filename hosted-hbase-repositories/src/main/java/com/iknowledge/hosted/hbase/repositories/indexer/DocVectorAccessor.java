package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.OptionalInt;

import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

abstract class DocVectorAccessor implements AutoCloseable {

    protected static final int UNCOMMITED_VERSIONS_RANGE_END = 0;
    protected static final int INITIAL_VERSION_NUM = 1;

    protected final Table indexTable;
    protected final Id docId;

    public DocVectorAccessor(final int srcId, final Id docId, final Table indexTable) {
        this.docId = docId.withPrefix(srcId);
        this.indexTable = indexTable;
    }

    @Override
    public void close() throws Exception {
        indexTable.close();
    }

    protected void removeUncommitedVector(int lastVersion) throws StoreException {

        final byte[] versionBytes = Bytes.toBytes(lastVersion);
        final int blocksInVector;
        try {
            blocksInVector = blocksInVersion(versionBytes);
            final Delete delete = versionDelete(versionBytes, blocksInVector);
            if (delete != null) {
                indexTable.delete(delete);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    protected int blocksInVersion(final byte[] version) throws IOException {
        final FilterList filterList
            = new FilterList(FilterList.Operator.MUST_PASS_ALL,
                             new ColumnPrefixFilter(version),
                             new KeyOnlyFilter());
        final Get get = new Get(docId.bytes())
                            .setFilter(filterList)
                            .readVersions(1);
        final Result result = indexTable.get(get);

        return result.size();
    }

    protected Delete versionDelete(byte[] version, int blocks) {

        if (blocks <= 0) {
            return null;
        }

        final Delete delete = new Delete(docId.bytes());
        for (int blockIdx = 0 ; blockIdx < blocks ; blockIdx++) {
            final byte[] blockColumn = blockColumnQf(version, blockIdx);
            delete.addColumns(VECTOR_CF_BYTES, blockColumn);
        }
        return delete;
    }

    protected byte[] blockColumnQf(final byte[] versionBytes, final int blockNum) {
        return Bytes.add(versionBytes, Bytes.toBytes(blockNum));
    }

    protected OptionalInt loadCurrentVersion() throws StoreException {
        OptionalInt curVersion;
        try {
            final Get get = new Get(docId.bytes())
                                .addColumn(VECTOR_CF_BYTES, LAST_VERSION_QF_BYTES)
                                .readVersions(1);
            final Result result;
            result = indexTable.get(get);
            if (result.isEmpty()) {
                curVersion = OptionalInt.empty();
            }
            else {
                final Cell versionCell = result.getColumnLatestCell(VECTOR_CF_BYTES,
                                                                    LAST_VERSION_QF_BYTES);
                final byte[] versionBytes = CellUtil.cloneValue(versionCell);
                curVersion = OptionalInt.of(Bytes.toInt(versionBytes));
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        return curVersion;
    }
}
