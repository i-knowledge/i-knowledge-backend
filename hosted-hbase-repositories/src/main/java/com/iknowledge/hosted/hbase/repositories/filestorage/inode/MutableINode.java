package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

public interface MutableINode<T extends INode> extends INode {

    /**
     * Current version number of this inode.
     * Version number increases when some modification performed on inode.
     *
     * @return
     */
    int version();

    /**
     * Get inode state to user.
     *
     * @implNote Before call, must acquire read lock
     *
     * @return
     */
    State state();

    /**
     * Detach inode from FS tree.
     *
     * @implNote Before call, must acquire write lock
     *
     */
    void detach();

    /**
     * Rename current inode
     * @return
     */
    void rename(String name);

    /**
     * Change parent of inode
     */
    void changeParent(ContainerINode<T> newParent);
}
