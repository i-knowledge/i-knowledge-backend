package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.google.common.annotations.VisibleForTesting;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_APPLIED_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.TX_LOG_ROW_BYTES;

/**
 * Class represent FS log pointer values: last appended transaction ID, last applied transaction ID.
 * Class contains method to safe access and modifications of pointer values.
 */
final class FsLogPointer {

    static final long NO_TX = 0L;
    private static final byte[] ZERO_BYTES = Bytes.toBytes(NO_TX);

    private volatile long lastTxId;
    private volatile long lastAppliedTxId;

    @VisibleForTesting
    FsLogPointer() throws StoreException {
        this(NO_TX, NO_TX);
    }

    /**
     * Load initial pointer values
     *
     * @throws StoreException
     */
    FsLogPointer(long lastTxId, long lastAppliedTxId) throws StoreException {
        if (lastAppliedTxId > lastTxId && lastTxId <= NO_TX) {
            throw new StoreException("FS log pointer has corrupted structure: "
                                     + "txId=" + lastTxId
                                     + ", appliedTxId=" + lastAppliedTxId);
        }
        this.lastTxId = lastTxId;
        this.lastAppliedTxId = lastAppliedTxId;
    }

    /**
     * Create FS log pointer by reading state from HBase table.
     *
     * @param logTable HBase table
     * @return
     */
    public static FsLogPointer readFrom(Table logTable) throws StoreException {
        // initialize FS log pointer structure if doesn't exists:
        // set all pointers to -1 to indicate that no records was in log until this moment
        // and create HBase cells for pointers with empty value to indicate that no transactions
        // was applied
        try {
            if (!logTable.exists(new Get(TX_LOG_ROW_BYTES)
                                         .addColumn(INODE_CF_BYTES, LAST_TX_ID_BYTES))) {
                logTable.put(new Put(TX_LOG_ROW_BYTES).addColumn(INODE_CF_BYTES,
                                                                    LAST_TX_ID_BYTES,
                                                                    ZERO_BYTES));
            }
            if (!logTable.exists(new Get(TX_LOG_ROW_BYTES)
                                         .addColumn(INODE_CF_BYTES, LAST_APPLIED_TX_ID_BYTES))) {
                logTable.put(new Put(TX_LOG_ROW_BYTES).addColumn(INODE_CF_BYTES,
                                                                    LAST_APPLIED_TX_ID_BYTES,
                                                                    ZERO_BYTES));
            }
        }
        catch (IOException e) {
            throw new StoreException("Fail to initialize FS log pointer structure", e);
        }

        long lastTxId = getLastTxId(logTable);
        long lastAppliedTxId = getLastAppliedTxId(logTable);
        return new FsLogPointer(lastTxId, lastAppliedTxId);
    }

    public long lastTxId() {
        return lastTxId;
    }

    public long lastAppliedTxId() {
        return lastAppliedTxId;
    }

    public void setLastAppliedTxId(final long lastAppliedTxId) {
        // this 2 checks is debug checks to prevent early builds fatal bugs
        if (lastAppliedTxId <= NO_TX) {
            throw new IllegalArgumentException("Last applied tx ID cannot be less than 0");
        }
        if (lastTxId == NO_TX) {
            throw new IllegalArgumentException("ID cannot applied until first "
                                               + "transaction will be appended to log");
        }
        this.lastAppliedTxId = lastAppliedTxId;
    }

    /**
     * Get current size of log.
     *
     * @apiNote Returned size can be greater than real log size due to gaps between record IDs
     * @return
     */
    public long logSize() {
        // reduce volatile read cost
        final long lastAppliedId = this.lastAppliedTxId;
        final long lastId = this.lastTxId;
        return lastAppliedId <= lastId
                    ? lastId - lastAppliedId
                    : Long.MAX_VALUE - lastAppliedId + lastId;
    }

    public long nextTxId() throws StoreException {
        // reduce volatile read/write cost
        final long lastAppliedId = this.lastAppliedTxId;
        long lastId = this.lastTxId;
        if (lastAppliedId <= lastId) {
            try {
                lastId = Math.addExact(lastId, 1L);
            }
            catch (ArithmeticException e) {
                lastId = 1;
            }
        }
        else if (lastId + 1 >= lastAppliedId) {
            throw new StoreException("FS transaction ID overflows last committed "
                                     + "transaction ID. Cyclic buffer of tx ID try to overflow"
                                     + " second time and previous transactions not applied yet."
                                     + "Ensure that log replay periodically run "
                                     + "in background. Until log transaction is full "
                                     + "no changes will be accepted and FS will remain in "
                                     + "read only mode.");
        }
        else {
            lastId++;
        }
        this.lastTxId = lastId;
        return lastId;
    }

    private static long getLastAppliedTxId(final Table table) throws StoreException {
        try {
            final Get get = new Get(TX_LOG_ROW_BYTES)
                                    .addColumn(INODE_CF_BYTES, LAST_APPLIED_TX_ID_BYTES)
                                    .setCacheBlocks(false)
                                    .readVersions(1);
            return getTxId(table, get);
        }
        catch (Exception e) {
            throw new StoreException("Fail to initialize FS log pointer structure: "
                                     + "can't read last applied TX ID", e);
        }
    }

    private static long getLastTxId(final Table table) throws StoreException {
        try {
            final Get get = new Get(TX_LOG_ROW_BYTES)
                                    .addColumn(INODE_CF_BYTES, LAST_TX_ID_BYTES)
                                    .setCacheBlocks(false)
                                    .readVersions(1);
            return getTxId(table, get);
        }
        catch (Exception e) {
            throw new StoreException("Fail to initialize FS log pointer structure: "
                                     + "can't read last TX ID", e);
        }
    }

    private static long getTxId(final Table table, final Get pointerGet)
            throws Exception {

        final Result result = table.get(pointerGet);
        if (result.isEmpty()) {
            throw new StoreException("Pointer not found");
        }
        final Cell txCell = result.listCells().get(0);
        final byte[] value = CellUtil.cloneValue(txCell);
        return Bytes.toLong(value);
    }
}
