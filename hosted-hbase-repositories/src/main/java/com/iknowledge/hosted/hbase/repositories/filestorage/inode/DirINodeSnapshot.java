package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

public class DirINodeSnapshot implements INodeSnapshot<DirectoryINode> {

    private final int version;
    private final String dirSnapStrRepr;
    private final DirectoryINode dirInode;

    public DirINodeSnapshot(DirectoryINode dirInode) {
        this.dirInode = dirInode;
        this.dirSnapStrRepr = dirInode.toString();
        this.version = dirInode.version();
    }

    @Override
    public boolean isModified() {
        return dirInode.version() != version;
    }

    @Override
    public DirectoryINode inode() {
        return dirInode;
    }

    @Override
    public String toString() {
        return "Snapshot: " + dirSnapStrRepr
                + System.lineSeparator()
                + "Current state: " + dirInode;
    }
}
