package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Arrays;

import static java.text.MessageFormat.format;

final class RemoveTransaction implements FsTransaction {

    private final Fs.Transaction.RemoveCtx ctx;
    private final INodeImageStore inodeStore;
    private final Table table;

    public RemoveTransaction(Fs.Transaction.RemoveCtx ctx,
                             INodeImageStore inodeStore,
                             Table table) {
        this.ctx = ctx;
        this.inodeStore = inodeStore;
        this.table = table;
    }

    @Override
    public void apply() throws StoreException {
        final byte[] id = ctx.getId().toByteArray();
        final byte[] parentId = ctx.getParentId().toByteArray();
        try {
            inodeStore.remove(id, table);
            inodeStore.detachFromParent(id, parentId, table);
        }
        catch (IOException e) {
            throw new StoreException(format("Inode removal transaction failed: "
                                            + "inode={0}, parent={1}",
                                            Arrays.toString(id),
                                            Arrays.toString(parentId)),
                                     e);
        }
    }
}
