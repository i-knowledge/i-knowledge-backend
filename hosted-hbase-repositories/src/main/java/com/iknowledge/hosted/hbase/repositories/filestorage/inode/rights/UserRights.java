package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import com.iknowledge.common.Id;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class that contains user rights information
 */
public class UserRights {

    private final Map<Id, HBaseINodeRights> userRights;
    private final Map<Id, HBaseINodeRights> userGroupRights;

    /**
     *
     * @param userRights
     * @param userGroupRights
     */
    public UserRights(Map<Id, HBaseINodeRights> userRights,
                      Map<Id, HBaseINodeRights> userGroupRights) {
        this.userRights = userRights;
        this.userGroupRights = userGroupRights;
    }

    public UserRights(Collection<HBaseINodeRights> userRights,
                      Collection<HBaseINodeRights> userGroupRights) {
        this(userRights == null
                    ? new HashMap<>()
                    : userRights.stream().collect(Collectors.toMap(s -> s.getEntityId(), s -> s)),
            userGroupRights == null
                    ? new HashMap<>()
                    :  userGroupRights.stream().collect(
                        Collectors.toMap(s -> s.getEntityId(), s -> s)));
    }

    /**
     * Get default instance with empty rights
     * @return
     */
    public static UserRights getEmpty() {
        return new UserRights(new HashMap<>(), new HashMap<>());
    }

    /**
     * Attach all user rights (group and personal)
     * @param rights
     */
    public void attachUserRights(List<HBaseINodeRights> rights) {
        final List<HBaseINodeRights> userRights = rights.stream()
                .filter(s -> s instanceof INodeUserRights)
                .collect(Collectors.toList());
        attachUserRightsInternal(userRights);

        final List<HBaseINodeRights> userGroupRights = rights.stream()
                .filter(s -> s instanceof INodeUserGroupRights)
                .collect(Collectors.toList());
        userGroupRightsInternal(userGroupRights);
    }

    /**
     * Is there any rights in the current object, even personal or group
     * @return
     */
    public boolean isEmpty() {
        int totalSize = getTotalSize();
        return totalSize == 0;
    }

    /**
     * Export all rights, even personal or group into the one list
     * @return
     */
    public List<HBaseINodeRights> exportAll() {
        final List<HBaseINodeRights> accessRights = new ArrayList<>(getTotalSize());
        accessRights.addAll(userRights.values());
        accessRights.addAll(userGroupRights.values());
        return accessRights;
    }

    /**
     * Personal user rights on INode
     * @return
     */
    public Map<Id, HBaseINodeRights> userRights() {
        return userRights;
    }

    /**
     * User group rights on INode
     * @return
     */
    public Map<Id, HBaseINodeRights> userGroupRights() {
        return userGroupRights;
    }

    /**
     * Copy current instance of rights
     * @return
     */
    public UserRights copy() {
        Map<Id, HBaseINodeRights> rightsMap = Collections.unmodifiableMap(userRights);
        Map<Id, HBaseINodeRights> groupRightsMap = Collections.unmodifiableMap(userGroupRights);

        return new UserRights(rightsMap, groupRightsMap);
    }

    public boolean containsUser(Id userId) {
        return userRights.containsKey(userId);
    }

    public HBaseINodeRights getUserRightsByUserId(Id userId) {
        return userRights.get(userId);
    }

    public boolean containsUserGroup(Id userGroupId) {
        return userGroupRights.containsKey(userGroupId);
    }

    public HBaseINodeRights getUserGroupRightsByUserGroupId(Id userGroupId) {
        return userGroupRights.get(userGroupId);
    }

    /**
     * Personal user rights on INode
     * @param rights
     */
    private void attachUserRightsInternal(List<HBaseINodeRights> rights) {
        if (userRights == null) {
            return;
        }
        for (HBaseINodeRights right : rights) {
            if (userRights.containsKey(right.getEntityId())) {
                userRights.replace(right.getEntityId(), right);
            } else {
                userRights.put(right.getEntityId(), right);
            }
        }
    }

    private void userGroupRightsInternal(List<HBaseINodeRights> rights) {
        if (userGroupRights == null) {
            return;
        }
        for (HBaseINodeRights right : rights) {
            if (userGroupRights.containsKey(right.getEntityId())) {
                userGroupRights.replace(right.getEntityId(), right);
            } else {
                userGroupRights.put(right.getEntityId(), right);
            }
        }
    }

    private int getTotalSize() {
        return userRights.size() + userGroupRights.size();
    }
}
