package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

public final class ProposedFileRevisionINode implements INode {

    private final Id id;
    private final Path sysPath;
    private final FileINode fileInode;
    private final ReentrantReadWriteLock lock;
    private final String strRepr;
    private final int hash;

    /**
     * Create new proposed revision inode
     *
     * @param sysPath
     * @param fileInode
     */
    public ProposedFileRevisionINode(Id id, Path sysPath, FileINode fileInode) {
        requireNonNull(sysPath);
        requireNonNull(fileInode);

        this.id = id;
        this.sysPath = sysPath;
        this.fileInode = fileInode;
        lock = new ReentrantReadWriteLock(true);

        strRepr = new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                       .add("id = " + id)
                       .add("name = " + fileInode.name())
                       .add("path = " + fileInode.path())
                       .add("sysPath = " + sysPath)
                       .toString();
        hash = hash(sysPath, fileInode);
    }

    /**
     * Map current proposal into valid file revision inode.
     * @param revision
     * @return
     */
    public FileRevisionINode toRevision(long revision) {
        return new FileRevisionINode(id, revision, sysPath, fileInode);
    }

    /**
     * Underlying file system path where revision file located
     * @return
     */
    public Path sysPath() {
        return sysPath;
    }

    @Override
    public Id id() {
        return id;
    }

    @Override
    public String name() {
        return fileInode.name();
    }

    @Override
    public ContainerINode<? extends INode> parent() {
        return fileInode;
    }

    @Override
    public Path path() {
        return fileInode.path();
    }

    @Override
    public ReadWriteLock lock() {
        return lock;
    }

    @Override
    public INodeProto.INode proto() {
        return INodeProto.INode.newBuilder()
                               .setType(INodeProto.INode.Type.FILE_REVISION)
                               .setSysPath(sysPath.toString())
                               .build();
    }

    @Override
    public boolean equals( Object other ) {
        if ( this == other ) {
            return true;
        }
        if ( other == null || getClass() != other.getClass() ) {
            return false;
        }

        ProposedFileRevisionINode that = (ProposedFileRevisionINode) other;

        return Objects.equals(this.sysPath, that.sysPath)
               && Objects.equals(this.fileInode, that.fileInode);
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public String toString() {
        return strRepr;
    }
}
