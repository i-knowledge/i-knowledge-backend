package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_APPLIED_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.TX_LOG_ROW_BYTES;

/**
 * Transaction log reader class.
 */
final class FsLogReader {

    private final Table logTable;
    private final FilterList filterSpecialCols;
    private final long lastTxId;
    private Long currentTxId;
    private long recordsRemains;

    public FsLogReader(Table logTable, FsLogPointer logPointer, long maxRecords) {

        this.logTable = logTable;
        this.lastTxId = logPointer.lastTxId();
        this.currentTxId = logPointer.lastAppliedTxId();
        this.recordsRemains = maxRecords;
        filterSpecialCols = new FilterList(
                new QualifierFilter(CompareOperator.NOT_EQUAL,
                                    new BinaryComparator(LAST_TX_ID_BYTES)),
                new QualifierFilter(CompareOperator.NOT_EQUAL,
                                    new BinaryComparator(LAST_APPLIED_TX_ID_BYTES)));
    }

    /**
     * Read next log record.
     *
     * @return Log record or null if reader has no records anymore.
     *
     * @throws StoreException
     */
    public TxRecord next() throws StoreException {
        while (moveToNextTx()) {
            try {
                final byte[] txIdQf = Bytes.toBytes(currentTxId);
                final Get txRecordGet = new Get(TX_LOG_ROW_BYTES)
                                                .addColumn(INODE_CF_BYTES, txIdQf)
                                                .setFilter(filterSpecialCols)
                                                .readVersions(1)
                                                .setCacheBlocks(true);
                final Result result = logTable.get(txRecordGet);
                if (result.isEmpty()) {
                    // log can have ID gaps(due to errors or something else)
                    // we have to continue reading from next ID(but doesn't account gap as record)
                    recordsRemains++;
                    continue;
                }
                final Cell txCell = result.getColumnLatestCell(INODE_CF_BYTES, txIdQf);
                final byte[] idBytes = CellUtil.cloneQualifier(txCell);
                long txId = Bytes.toLong(idBytes);
                final byte[] txProto = CellUtil.cloneValue(txCell);
                return new TxRecord(txId, idBytes, parseTx(txId, txProto));
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
        return null;
    }

    private boolean moveToNextTx() {
        if (recordsRemains <= 0L) { // reach max number of records to read
            return false;
        }
        recordsRemains--;

        if (currentTxId == lastTxId) { // reach last log record
            return false;
        }
        else if (currentTxId < lastTxId) {
            // simple linear scan of log(no overflow of txId happend before)
            currentTxId++;
        }
        else {
            // sometimes before txId exceed max long value and overflow => and start again from 0.
            // we scan log as "cyclic buffer" => starting from some txId and moving
            // to max long value.
            // after when reach end of buffer => we starts from beginning(from 0) and move
            // forward until read requested count of records or reach end of transaction log
            try {
                currentTxId = Math.addExact(currentTxId, 1L);
            }
            catch (ArithmeticException e) {
                currentTxId = 1L;
            }
        }
        return true;
    }

    private Fs.MultiTransaction parseTx(final long txId, final byte[] txProto)
            throws StoreException {

        final Fs.MultiTransaction tx;
        try {
            tx = Fs.MultiTransaction.parseFrom(txProto);
        }
        catch (Exception e) {
            throw new StoreException("Can't parse FS log transaction message with ID="
                                     + txId, e);
        }
        return tx;
    }
}
