package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;

public interface FsTransaction {

    void apply() throws StoreException;
}
