package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.locks.StampedLock;

import static com.iknowledge.hosted.hbase.repositories.HConsts.Tokens.LAST_TOKEN_ID_ROWKEY_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Tokens.TOKEN_ID_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Tokens.TOKEN_MAP_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Tokens.TOKEN_META_CF_BYTES;

/**
 * HBase token store.
 */
public final class TokenStore extends Store {

    /**
     * Initial token ID value.
     */
    public static final long INITIAL_ID = 0L;
    private static final long NOT_EXISTS = Long.MAX_VALUE + 1; //must be some negative value

    private StampedLock lock = new StampedLock();
    private long nextId;

    /**
     * @param storeConfig Store connection instance
     */
    public TokenStore(final StoreConfig storeConfig) throws StoreException {
        super(storeConfig);

        nextId = readNextId();
    }

    /**
     * @param storeConfig Store connection instance
     */
    public TokenStore(final StoreConfig storeConfig,
                      boolean createTable,
                      boolean ignoreIfExists) throws StoreException {
        super(storeConfig);

        if (createTable) {
            try {
                createTable();
            }
            catch (TableAlreadyExistsStoreException e) {
                if (!ignoreIfExists) {
                    throw e;
                }
            }
        }
        nextId = readNextId();
    }

    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor metaCf
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(TOKEN_META_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setBlockCacheEnabled(false)
                              .setVersions(1, 1);
                final ColumnFamilyDescriptor mapCf
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(TOKEN_MAP_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setBlockCacheEnabled(false)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(metaCf)
                          .addColumnFamily(mapCf);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch ( IOException e ) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Put token into table.
     *
     * @param token Token value
     *
     * @return ID of passed token.
     *
     * @throws StoreException
     */
    public long putToken(String token) throws StoreException {
        final long stamp = lock.writeLock();
        try (Table table = getTable()) {
            if (nextId < INITIAL_ID) {
                nextId = repairNextId(revertId(nextId), table);
            }
            return getOrPut(table, token);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        finally {
            lock.unlockWrite(stamp);
        }
    }

    private long getOrPut(Table table, final String token) throws StoreException, IOException {
        final byte[] row = Bytes.toBytes(token);
        final long existingId = getTokenId(row, table);

        if (existingId != NOT_EXISTS) {
            return existingId;
        }

        long tokenId;
        try {
            tokenId = nextId;
            proposeLastId(tokenId, table);
            final Put newTokenPut = new Put(row).addColumn(TOKEN_MAP_CF_BYTES,
                                                              TOKEN_ID_QF_BYTES,
                                                              Bytes.toBytes(tokenId));
            table.put(newTokenPut);
            commitLastId(tokenId, table);
            nextId++;
            return tokenId;
        }
        catch (IOException e) {
            // id update transaction failed => revert last possible ID value
            // to detect fail later and try to repair
            nextId = revertId(nextId);
            throw new StoreException(e);
        }
    }

    private long readNextId() throws StoreException {
        long nxtId;
        try (Table table = getTable()) {
            final Get lastIdGet = new Get(LAST_TOKEN_ID_ROWKEY_BYTES)
                                        .addColumn(TOKEN_META_CF_BYTES, TOKEN_ID_QF_BYTES)
                                        .readVersions(1)
                                        .setCacheBlocks(false);
            final Result result = table.get(lastIdGet);

            if (result.isEmpty()) {
                nxtId = INITIAL_ID;
            }
            else {
                final Cell idCell = result.getColumnLatestCell(TOKEN_META_CF_BYTES,
                                                               TOKEN_ID_QF_BYTES);
                nxtId = extractId(idCell);
                if (nxtId < INITIAL_ID) { // id update transaction failed
                    nxtId = repairNextId(revertId(nxtId), table);
                }
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        return nxtId;
    }

    private long repairNextId(final long lastProposedId, final Table table) throws IOException {
        SingleColumnValueFilter valueFilter
            = new SingleColumnValueFilter(TOKEN_MAP_CF_BYTES,
                                          TOKEN_ID_QF_BYTES,
                                          CompareOperator.NOT_EQUAL,
                                          Bytes.toBytes(lastProposedId));
        valueFilter.setFilterIfMissing(true);
        final Scan scan = new Scan().addColumn(TOKEN_MAP_CF_BYTES, TOKEN_ID_QF_BYTES)
                                    .setCacheBlocks(false)
                                    .setCaching(storeConfig.hconfig().scanCacheSize())
                                    .setBatch(storeConfig.hconfig().scanBatchSize())
                                    .readVersions(1)
                                    .setFilter(valueFilter);

        boolean tokenFound = false;
        try (ResultScanner scanner = table.getScanner(scan)) {
            for (Result result : scanner) {
                if (result.isEmpty()) {
                    continue;
                }
                tokenFound = true;
                break;
            }
        }

        long nxtId = tokenFound
                     ? lastProposedId + 1
                     : lastProposedId;
        commitLastId(nxtId - 1, table);
        return nxtId;
    }

    private long getTokenId(final byte[] token, final Table table)
        throws IOException, StoreException {

        final Result tokenGetResult = table.get(new Get(token)
                                                     .addColumn(TOKEN_MAP_CF_BYTES,
                                                                TOKEN_ID_QF_BYTES)
                                                     .readVersions(1)
                                                     .setCacheBlocks(true));
        if (tokenGetResult.isEmpty()) {
            return NOT_EXISTS;
        }
        final Cell idCell = tokenGetResult.getColumnLatestCell(TOKEN_MAP_CF_BYTES,
                                                               TOKEN_ID_QF_BYTES);
        return extractId(idCell);
    }

    private void proposeLastId(final long id, final Table table) throws IOException {
        final Put nextIdPut = new Put(LAST_TOKEN_ID_ROWKEY_BYTES)
                                    .addColumn(TOKEN_META_CF_BYTES,
                                                  TOKEN_ID_QF_BYTES,
                                                  Bytes.toBytes(revertId(id)));

        table.put(nextIdPut);
    }

    private void commitLastId(final long id, final Table table) throws IOException {
        commitLastId(Bytes.toBytes(id), table);
    }

    private void commitLastId(final byte[] id, final Table table) throws IOException {
        table.put(new Put(LAST_TOKEN_ID_ROWKEY_BYTES)
                        .addColumn(TOKEN_META_CF_BYTES, TOKEN_ID_QF_BYTES, id));
    }

    private long extractId(final Cell idCell) throws StoreException {
        final byte[] bytes = CellUtil.cloneValue(idCell);
        if (bytes.length < 8) {
            throw new StoreException("Invalid token ID value: " + Arrays.toString(bytes));
        }
        return Bytes.toLong(bytes);
    }

    private long revertId(final long id) {
        return id * -1;
    }
}
