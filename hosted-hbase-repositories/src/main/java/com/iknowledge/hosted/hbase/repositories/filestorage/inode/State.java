package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

public enum State {

    /**
     * inode attached to FS tree and can be accessed by everyone
     */
    ATTACHED,

    /**
     * inode detached(removed) from FS tree. This is terminal state.
     */
    DETACHED
}
