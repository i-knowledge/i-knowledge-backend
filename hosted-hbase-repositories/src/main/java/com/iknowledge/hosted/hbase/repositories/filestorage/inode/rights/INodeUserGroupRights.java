package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import com.google.protobuf.ByteString;
import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeRightsProto;

/**
 * Represent rights that are connected with user group
 */
public final class INodeUserGroupRights extends HBaseINodeRights {
    /**
     *
     * @param userGroupId
     * @param userGroupRights
     */
    public INodeUserGroupRights(Id userGroupId, INodeRights userGroupRights) {
        super(userGroupId, userGroupRights);
    }

    @Override
    public INodeRightsProto.INodeRights proto() {
        final INodeRightsProto.INodeRights.UserGroupRights userGroupRights =
                INodeRightsProto.INodeRights
                .UserGroupRights.newBuilder()
                .setGroupId(ByteString.copyFrom(getEntityId().bytes()))
                .setRights(ByteString.copyFrom(getEntityRights().getRights()))
                .build();
        return INodeRightsProto.INodeRights.newBuilder()
                .setType(INodeRightsProto.INodeRights.Type.USER_GROUP)
                .setUserGroupRights(userGroupRights)
                .build();
    }

    /**
     * Map the specified proto object to the simple one
     * @param rights
     * @return
     */
    public static INodeUserGroupRights map(INodeRightsProto.INodeRights.UserGroupRights rights) {
        return new INodeUserGroupRights(Id.of(rights.getGroupId().toByteArray()),
                INodeRightsUtils.getRights(rights.getRights().toByteArray()));
    }
}
