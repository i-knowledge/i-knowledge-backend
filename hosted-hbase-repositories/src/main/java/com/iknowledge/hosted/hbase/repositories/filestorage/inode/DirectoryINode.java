package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

public class DirectoryINode
    implements UserLevelINode, ContainerINode<UserLevelINode> {

    private final Id nodeId;
    private String name;
    private DirectoryINode parent;
    private final ReadWriteLock lock;
    private final HashMap<String, UserLevelINode> children;
    private final UserRights userRights;
    private State state = State.ATTACHED;
    private int version;

    /**
     * Create {@link DirectoryINode} with ID, name and parent and children list.
     *
     * @param name User name of inode(for instance, file name or dir name which user
     *                    provide for this inode)
     *
     * @param parent Parent inode(usually directory)
     */
    private DirectoryINode(Id nodeId,
                           String name,
                           DirectoryINode parent,
                           Collection<UserLevelINode> children,
                           UserRights userRights) {

        this.nodeId = nodeId;
        this.name = name;
        this.parent = parent;
        lock = new ReentrantReadWriteLock(true);

        this.children = new HashMap<>();
        children.forEach(this::addChild);

        this.userRights = userRights;
    }

    /**
     * Create new {@link DirectoryINode}(assign ID, name and parent) which doesn't exist before.
     *
     * @param name Directory name
     * @param parent Parent directory inode
     */
    public DirectoryINode(String name, DirectoryINode parent) {

        this(Id.len16(),
             name,
             parent,
             UserRights.getEmpty());
    }

    /**
     * Create clone of {@link DirectoryINode} with new parent node and new name but same ID.
     *
     * @param inode inode to clone
     * @param newParent New parent inode
     */
    public DirectoryINode(DirectoryINode inode, String newName, DirectoryINode newParent) {
        this(inode.id(),
             newName,
             newParent,
             inode.children(),
             inode.userRights);
    }

    /**
     * Load existing {@link DirectoryINode} with ID, name but with no parent(root node).
     *
     */
    public DirectoryINode(Id nodeId,
                          String name,
                          UserRights userRights) {

        this(nodeId, name, null, userRights);
    }

    /**
     * Load existing {@link DirectoryINode} with ID, name and parent.
     *
     * @param name User name of inode(for instance, file name or dir name which user
     *                    provide for this inode)
     *
     * @param parent Parent inode(usually directory)
     */
    public DirectoryINode(Id nodeId,
                          String name,
                          DirectoryINode parent,
                          UserRights userRights) {

        this(nodeId, name, parent, Collections.emptyList(), userRights);
    }

    @Override
    public Id id() {
        return nodeId;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void rename(final String name) {
        this.name = name;
        version++;
    }

    @Override
    public ContainerINode<? extends INode> parent() {
        return parent;
    }

    @Override
    public void changeParent(final ContainerINode<UserLevelINode> newParent) {
        checkArgument(newParent instanceof DirectoryINode, "Directory parent may only be other "
                                                        + "directory");
        this.parent = (DirectoryINode)newParent;
        version++;
    }

    @Override
    public Path path() {
        return INode.readFullPath(this);
    }

    @Override
    public ReadWriteLock lock() {
        return lock;
    }

    @Override
    public void addChild( final INode child ) throws IllegalArgumentException {
        requireNonNull(child);
        checkArgument(child instanceof UserLevelINode, "Child must be user level inode");

        final UserLevelINode childInode = children.putIfAbsent(child.name(),
                                                               (UserLevelINode) child);
        if (childInode != null) {
            throw new IllegalArgumentException("Child " + child.toString()
                                          + "already exists in " + toString());
        }
    }

    @Override
    public boolean removeChild( final INode child ) {
        requireNonNull(child);
        checkArgument(child instanceof UserLevelINode, "Child must be user level inode");

        return children.remove(child.name()) != null;
    }

    @Override
    public Optional<UserLevelINode> findChildByName(String childName) {
        return Optional.ofNullable(children.get(childName));
    }

    @Override
    public Collection<UserLevelINode> children() {
        return children.values();
    }

    @Override
    public State state() {
        return state;
    }

    @Override
    public void detach() {
        this.state = State.DETACHED;
        version++;
    }

    @Override
    public INodeProto.INode proto() {
        return INodeProto.INode.newBuilder()
                               .setName(name)
                               .setType(INodeProto.INode.Type.DIR)
                               .build();
    }

    @Override
    public UserRights userRights() {
        return userRights;
    }

    @Override
    public int version() {
        return version;
    }

    @Override
    public INodeSnapshot<DirectoryINode> snapshot() {
        return new DirINodeSnapshot(this);
    }

    @Override
    public boolean equals( Object other ) {
        if ( this == other ) {
            return true;
        }
        if ( other == null || getClass() != other.getClass() ) {
            return false;
        }

        DirectoryINode that = ( DirectoryINode ) other;

        return Objects.equals(this.name, that.name)
                    && Objects.equals(this.parent, that.parent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parent);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                   .add("id = " + nodeId)
                   .add("state = " + state)
                   .add("name = " + name)
                   .add("path = " + path())
                   .toString();
    }
}
