package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeRightsProto;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Transaction class which attach user rights to some
 * {@link com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode}
 */
public class AttachAccessRightsTransaction implements FsTransaction {

    private final Fs.Transaction.RightsCtx ctx;
    private final INodeImageStore inodeStore;
    private final Table table;

    /**
     * Create instance of {@link AttachAccessRightsTransaction}
     *
     * @param ctx
     * @param imageStore
     * @param table
     */
    public AttachAccessRightsTransaction(Fs.Transaction.RightsCtx ctx,
                                         INodeImageStore imageStore,
                                         Table table) {
        this.ctx = ctx;
        this.inodeStore = imageStore;
        this.table = table;
    }

    @Override
    public void apply() throws StoreException {
        final List<INodeRightsProto.INodeRights> rights = ctx.getRightsList();
        if (rights.isEmpty()) {
            return;
        }
        final byte[] inodeId = ctx.getId().toByteArray();
        try {
            inodeStore.attachRights(inodeId, rights, table, rights.size());
        }
        catch (IOException e) {
            throw new StoreException("Attach access rights transaction failed for inode with ID= "
                                     + Arrays.toString(inodeId),
                                     e);
        }
    }
}
