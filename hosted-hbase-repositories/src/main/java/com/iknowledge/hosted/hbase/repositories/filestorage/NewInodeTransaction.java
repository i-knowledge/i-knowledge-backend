package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.Arrays;

public class NewInodeTransaction implements FsTransaction {

    private final Fs.Transaction.CreateCtx ctx;
    private final INodeImageStore inodeStore;
    private final Table table;

    /**
     * Create instance of {@link NewInodeTransaction}
     *
     * @param ctx
     * @param imageStore
     * @param table
     */
    public NewInodeTransaction(Fs.Transaction.CreateCtx ctx,
                               INodeImageStore imageStore,
                               Table table) {
        this.ctx = ctx;
        this.inodeStore = imageStore;
        this.table = table;
    }

    @Override
    public void apply() throws StoreException {
        try {
            final byte[] id = ctx.getId().toByteArray();
            final byte[] parentId = ctx.getParentId().toByteArray();
            inodeStore.attachToParent(id, parentId, table);
            inodeStore.putInodeMeta(id, ctx.getNewInode(), table);
        }
        catch (IOException e) {
            throw new StoreException("Inode add transaction failed: id="
                                     + Arrays.toString(ctx.getId().toByteArray())
                                     + ", parentId="
                                     + Arrays.toString(ctx.getParentId().toByteArray()),
                                     e);
        }
    }
}
