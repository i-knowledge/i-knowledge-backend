package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import java.util.Objects;

/**
 * Represent user or user group rights that can be applied to inode
 */
public final class INodeRights {
    private final boolean canRead;
    private final boolean canWrite;
    private final boolean canUpload;

    /**
     *
     * @param canRead
     * @param canWrite
     * @param canUpload
     */
    public INodeRights(final boolean canRead, final boolean canWrite,
                       final boolean canUpload) {
        this.canRead = canRead;
        this.canWrite = canWrite;
        this.canUpload = canUpload;
    }

    /**
     * Can the user or the user group read inode data
     * @return
     */
    public boolean isCanRead() {
        return canRead;
    }

    /**
     * Can the user or the user group write data to inode
     * @return
     */
    public boolean isCanWrite() {
        return canWrite;
    }

    /**
     * Can the user or the user group upload data to inode
     * @return
     */
    public boolean isCanUpload() {
        return canUpload;
    }

    /**
     * Convert inode rights into the byte array
     * @return
     */
    public byte[] getRights() {
        return INodeRightsUtils.getRights(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final INodeRights iNodeRights = (INodeRights) obj;
        if (this.canRead != iNodeRights.isCanRead()
                || this.canWrite != iNodeRights.isCanWrite()
                || this.isCanUpload() != iNodeRights.isCanUpload()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(canRead, canWrite, canUpload);
    }
}
