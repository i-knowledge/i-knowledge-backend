package com.iknowledge.hosted.hbase.repositories.indexer;

/**
 * Describe indexed document change
 */
public final class Change {

    /**
     * Change event type
     */
    public final ChangeType type;

    /**
     * ID of changed document
     */
    public final byte[] docId;

    public Change(final ChangeType type, final byte[] docId) {
        this.type = type;
        this.docId = docId;
    }
}
