package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Summary;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.iknowledge.hbase.client.DynamicBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_APPLIED_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.TX_LOG_ROW_BYTES;
import static com.iknowledge.hosted.hbase.repositories.filestorage.FsLog.METRICS_SUBSYSTEM;
import static com.iknowledge.hosted.hbase.repositories.filestorage.HBaseFsImageStore.METRICS_NAMESPACE;

final class FsLogCompaction extends Store {

    private static final Logger LOGGER = LoggerFactory.getLogger(FsLogCompaction.class);

    private final Summary compactionTimeMetric;
    private final Counter compactionsMetric;
    private final INodeImageStore imageStore;
    private final FsLogConfig config;
    private final ScheduledExecutorService replayScheduleExecutor;
    private final ExecutorService replayExecutor;
    private final AtomicBoolean replayStarted = new AtomicBoolean();
    private final FsLogPointer logPointer;
    private final Counter forceCompactionsMetric;
    private final Counter forceCompactionsRejectedMetric;
    private ScheduledFuture<?> scheduledFuture;

    /**
     * Create instance of {@link FsLogCompaction}
     *
     * @param logPointer
     * @param imageStore
     * @param config
     * @param metricsRegistry
     * @param storeConfig
     * @throws StoreException
     */
    public FsLogCompaction(final FsLogPointer logPointer,
                           final INodeImageStore imageStore,
                           final FsLogConfig config,
                           final CollectorRegistry metricsRegistry,
                           final StoreConfig storeConfig) {

        super(storeConfig);

        this.logPointer = logPointer;
        this.imageStore = imageStore;
        this.config = config;

        final ThreadFactory threadFactory
                = new ThreadFactoryBuilder()
                        .setDaemon(true)
                        .setNameFormat("fs-log-replay-%d")
                        .setUncaughtExceptionHandler((thread, ex) ->
                                                         config.shutdownListener().accept(ex))
                        .build();
        replayScheduleExecutor = Executors.newSingleThreadScheduledExecutor(threadFactory);
        replayExecutor = Executors.newSingleThreadExecutor(threadFactory);

        compactionTimeMetric = Summary.build()
                                      .name("compactions_time_ms")
                                      .namespace(METRICS_NAMESPACE)
                                      .subsystem(METRICS_SUBSYSTEM)
                                      .help("Compactions time in milliseconds")
                                      .register(metricsRegistry);
        compactionsMetric = Counter.build()
                                   .name("compactions_count_total")
                                   .namespace(METRICS_NAMESPACE)
                                   .subsystem(METRICS_SUBSYSTEM)
                                   .help("Total number of compactions happened")
                                   .register(metricsRegistry);
        forceCompactionsMetric = Counter.build()
                                   .name("force_compactions_count_total")
                                   .namespace(METRICS_NAMESPACE)
                                   .subsystem(METRICS_SUBSYSTEM)
                                   .help("Total number of forced compactions(caused by log size) "
                                         + "happened")
                                   .register(metricsRegistry);
        forceCompactionsRejectedMetric
                = Counter.build()
                         .name("force_compactions_rejected_count_total")
                         .namespace(METRICS_NAMESPACE)
                         .subsystem(METRICS_SUBSYSTEM)
                         .help("Rejections number for forced compactions(caused by log size)")
                         .register(metricsRegistry);
    }

    @Override
    public void close() throws Exception {
        replayExecutor.shutdown();
        replayScheduleExecutor.shutdown();
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
        boolean stopped = replayExecutor.awaitTermination(config.awaitTerminationSec(),
                                                          TimeUnit.SECONDS);
        if (!stopped) {
            LOGGER.warn("FS log replay tasks not finished in specified timeout {} {}",
                        config.awaitTerminationSec(),
                        TimeUnit.SECONDS);
        }
        stopped = replayScheduleExecutor.awaitTermination(config.awaitTerminationSec(),
                                                          TimeUnit.SECONDS);
        if (!stopped) {
            LOGGER.warn("FS log background replay task not finished in specified timeout {} {}",
                        config.awaitTerminationSec(),
                        TimeUnit.SECONDS);
        }
        super.close();
    }

    /**
     * Start background task which periodically apply last transactions,
     * form consistent FS image snapshot and remove applied transactions from log.
     */
    public void schedule() throws StoreException {
        if (scheduledFuture != null) {
            return;
        }

        try {
            scheduledFuture = replayScheduleExecutor.scheduleWithFixedDelay(
                    this::replayTask,
                    0, config.compactLogEveryMinutes(), TimeUnit.SECONDS);
        }
        catch (Exception e) {
            throw new StoreException(e);
        }
    }

    /**
     * Force to start background task which apply last transactions,
     * form consistent FS image snapshot and remove applied transactions from log.
     */
    public void force() throws StoreException {
        if (replayStarted.get()) { //to reduce no-op task submit when have many requests
            return;
        }
        try {
            forceCompactionsMetric.inc();
            replayExecutor.submit(this::replayTask);
        }
        catch (RejectedExecutionException e) {
            // can be safely ignored because executor queue filled with same tasks
            forceCompactionsRejectedMetric.inc();
        }
        catch (Exception e) {
            throw new StoreException(e);
        }
    }

    /**
     * Method execute replay process which apply all pending transactions
     * to form FS image snapshot with all changes and after that clear transaction log.
     * This method usually called on system start before loading FS image snapshot to memory.
     */
    public void replaySync() throws StoreException {
        if (logPointer.logSize() == 0) { // nothing to apply
            LOGGER.info("Log compaction finished with result: NOTHING TO COMPACT");
            return;
        }
        if (!replayStarted.compareAndSet(false, true)) {
            return;
        }

        LOGGER.info("Start log compaction...");
        final Summary.Timer timer = compactionTimeMetric.startTimer();
        try {
            executeReplay(logPointer.logSize());
            compactionsMetric.inc();
            LOGGER.info("Log compaction finished");
        }
        catch (StoreException e) {
            throw e;
        }
        catch (Exception e) {
            throw new StoreException(e);
        }
        finally {
            replayStarted.set(false);
            timer.close();
        }
    }

    private void replayTask() {
        try {
            replaySync();
        }
        catch (Exception e) {
            LOGGER.error("Log compaction failed, start shutdown... {}{}",
                         System.lineSeparator(), e);
            config.shutdownListener().accept(e);
        }
    }

    private void executeReplay(final long recordsToReplay) throws Exception {
        try (Table logTable = getTable()) {
            final DynamicBatch<Delete> batch = DynamicBatch.<Delete>newBuilder()
                                                       .withBatchSize(100) //TODO: adaptive value
                                                       .withTable(logTable)
                                                       .withMapper(o -> o)
                                                       .build();
            final FsLogReader logReader = new FsLogReader(logTable, logPointer, recordsToReplay);
            TxRecord txRecord = logReader.next(); //TODO: read records in batch mode
            long lastAppliedId = 0;
            while (txRecord != null) {
                applyTransaction(txRecord.txMsg);
                lastAppliedId = txRecord.id;
                batch.append(new Delete(TX_LOG_ROW_BYTES)
                                 .addColumn(INODE_CF_BYTES, txRecord.idBytes));
                if (Thread.interrupted()) {
                    break;
                }
                txRecord = logReader.next();
            }
            batch.finish();
            // saving of last applied tx ID after removal of tx record is safe:
            // transaction already applied to FS image snapshot and if we can't save last ID
            // we start from it again after restart but skip non-existent record IDs
            if (lastAppliedId > 0) {
                logTable.put(new Put(TX_LOG_ROW_BYTES).addColumn(INODE_CF_BYTES,
                                                                    LAST_APPLIED_TX_ID_BYTES,
                                                                    Bytes.toBytes(lastAppliedId)));
                logPointer.setLastAppliedTxId(lastAppliedId);
            }
        }
    }

    private void applyTransaction(final Fs.MultiTransaction multiTx) throws StoreException {
        try (Table table = getTable()) {
            for (Fs.Transaction tx : multiTx.getTxListList()) {
                FsTransaction fsTx;
                switch (tx.getType()) {
                    case CREATE:
                        fsTx = new NewInodeTransaction(tx.getCreateCtx(), imageStore, table);
                        break;
                    case REMOVE:
                        fsTx = new RemoveTransaction(tx.getRemoveCtx(), imageStore, table);
                        break;
                    case MOVE:
                        fsTx = new MoveTransaction(tx.getMoveCtx(), imageStore, table);
                        break;
                    case RENAME:
                        fsTx = new RenameTransaction(tx.getRenameCtx(),
                                                     imageStore,
                                                     table);
                        break;
                    case ACCESS_RIGHTS:
                        fsTx = new AttachAccessRightsTransaction(tx.getRightsCtx(),
                                                                 imageStore,
                                                                 table);
                        break;
                    default:
                        throw new StoreException("Unknown exception type " + tx.getType());
                }
                fsTx.apply();
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }
}
