package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;

public final class InvalidTransactionFormatException extends StoreException {

    public InvalidTransactionFormatException(final String message) {
        super(message);
    }
}
