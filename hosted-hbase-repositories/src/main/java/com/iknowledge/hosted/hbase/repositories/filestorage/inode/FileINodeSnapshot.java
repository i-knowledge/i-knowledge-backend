package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

public class FileINodeSnapshot implements INodeSnapshot<FileINode> {

    private final int version;
    private final String fileSnapStrRepr;
    private final FileINode fileInode;

    public FileINodeSnapshot(FileINode fileInode) {
        this.fileInode = fileInode;
        this.fileSnapStrRepr = fileInode.toString();
        this.version = fileInode.version();
    }

    @Override
    public boolean isModified() {
        return fileInode.version() != version;
    }

    @Override
    public FileINode inode() {
        return fileInode;
    }

    @Override
    public String toString() {
        return "Snapshot: " + fileSnapStrRepr
               + System.lineSeparator()
               + "Current state: " + fileInode;
    }
}
