package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;

public class FileINode implements UserLevelINode, ContainerINode<FileRevisionINode> {

    private final Id nodeId;
    private String name;
    private DirectoryINode parent;
    private final ReadWriteLock lock;
    private final HashMap<Long, FileRevisionINode> revisions;
    private final UserRights userRights;
    private State state = State.ATTACHED;
    private int version;

    private FileINode(Id nodeId,
                      String name,
                      DirectoryINode parent,
                      HashMap<Long, FileRevisionINode> revisions,
                      UserRights userRights) {

        requireNonNull(nodeId, "id cannot be null for file inode");
        requireNonNull(name, "display name cannot be null for file inode");
        requireNonNull(parent, "parent cannot be null for file inode");
        requireNonNull(userRights, "userRights cannot be null for file inode");

        this.nodeId = nodeId;
        this.name = name;
        this.parent = parent;
        lock = new ReentrantReadWriteLock(true);

        this.revisions = revisions;
        this.userRights = userRights;
    }

    /**
     * Create deep clone of {@link FileINode} with new parent node and new name but same ID and
     * same children list.
     *
     * @param inode inode to clone
     * @param newParent New parent inode
     */
    private FileINode(FileINode inode, String newName, DirectoryINode newParent) {
        this(inode.id(),
             newName,
             newParent,
             new HashMap<>(inode.children().size()),
             inode.userRights);
    }

    /**
     * Create new {@link FileINode}(assign ID, name and parent) which doesn't exist before.
     *
     * @param name File name
     * @param parent Parent inode(usually directory)
     */
    public FileINode(String name, DirectoryINode parent) {
        this(Id.len16(),
             name,
             parent,
             UserRights.getEmpty());
    }

    /**
     * Load existing {@link FileINode} with ID, name and parent.
     *
     * @param name User name of inode(for instance, file name or dir name which user
     *                    provide for this inode)
     *
     * @param parent Parent inode(usually directory)
     */
    public FileINode(Id nodeId,
                     String name,
                     DirectoryINode parent,
                     UserRights userRights) {
        this(nodeId,
             name,
             parent,
             new HashMap<>(),
             userRights);
    }

    @Override
    public Id id() {
        return nodeId;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void rename(final String name) {
        this.name = name;
        version++;
    }

    @Override
    public ContainerINode<? extends INode> parent() {
        return parent;
    }

    @Override
    public void changeParent(final ContainerINode<UserLevelINode> newParent) {
        checkState(newParent instanceof DirectoryINode, "File parent may only be directory inode");
        this.parent = (DirectoryINode) newParent;
        version++;
    }

    @Override
    public Path path() {
        return INode.readFullPath(this);
    }

    @Override
    public ReadWriteLock lock() {
        return lock;
    }

    @Override
    public void addChild( final INode newRevision ) throws IllegalArgumentException {
        requireNonNull(newRevision);
        checkArgument(newRevision instanceof FileRevisionINode, "Child must be file revision "
                                                                + "instance");

        FileRevisionINode revInode = (FileRevisionINode) newRevision;
        if (revisions.putIfAbsent(revInode.revision(), revInode) != null) {
            throw new IllegalArgumentException("Revision inode " + newRevision + " already exists");
        }
    }

    @Override
    public boolean removeChild( final INode child ) {
        requireNonNull(child);
        checkArgument(child instanceof FileRevisionINode, "Child must be file revision "
                                                                + "instance");

        final long revision = ((FileRevisionINode) child).revision();
        return revisions.remove(revision) != null;
    }

    /**
     * Get revision inode by revision number
     *
     * @param revision revision number
     * @return
     */
    public Optional<FileRevisionINode> findRev(final long revision) {
        return Optional.ofNullable(revisions.get(revision));
    }

    @Override
    public Optional<FileRevisionINode> findChildByName(final String name) {
        throw new UnsupportedOperationException("Can't search child for file inode");
    }

    @Override
    public Collection<FileRevisionINode> children() {
        return revisions.values();
    }

    @Override
    public State state() {
        return state;
    }

    @Override
    public void detach() {
        this.state = State.DETACHED;
        version++;
    }

    public INode copy() {
        return new FileINode(this, name, parent);
    }

    /**
     * Create cloned file inode which only has new name and parent, but other fields remains the
     * same.
     */
    public FileINode copy(String newName, DirectoryINode newParent) {
        return new FileINode(this, newName, newParent);
    }

    @Override
    public INodeProto.INode proto() {
        return INodeProto.INode.newBuilder()
                               .setName(name)
                               .setType(INodeProto.INode.Type.FILE)
                               .build();
    }

    @Override
    public UserRights userRights() {
        return userRights;
    }

    @Override
    public int version() {
        return version;
    }

    @Override
    public INodeSnapshot<FileINode> snapshot() {
        return new FileINodeSnapshot(this);
    }

    @Override
    public boolean equals( Object other ) {
        if ( this == other ) {
            return true;
        }
        if ( other == null || getClass() != other.getClass() ) {
            return false;
        }

        FileINode that = ( FileINode ) other;

        return Objects.equals(this.name, that.name)
                && Objects.equals(this.parent, that.parent);
    }

    @Override
    public int hashCode() {
        return hash(name, parent);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                     .add("id = " + nodeId)
                     .add("state = " + state)
                     .add("name = " + name)
                     .add("path = " + path())
                     .toString();
    }
}
