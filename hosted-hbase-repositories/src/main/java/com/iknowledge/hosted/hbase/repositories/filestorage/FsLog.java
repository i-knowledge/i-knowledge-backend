package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.google.common.annotations.VisibleForTesting;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.HConsts;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static com.google.common.collect.Lists.newArrayList;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.TX_LOG_ROW_BYTES;

/**
 * FS log implementation which store log in the same table which stores FS image(in some
 * predefined row)
 */
final class FsLog extends Store {

    static final String METRICS_SUBSYSTEM = "fs_log";
    private static final Logger LOGGER = LoggerFactory.getLogger(FsLog.class);

    private final FsTransactionValidator txValidator = new FsTransactionValidator();
    private final FsLogPointer logPointer;
    private final FsLogConfig config;
    private final FsLogCompaction logCompaction;

    /**
     * @param storeConfig HBase config
     * @param config Transaction log config
     */
    public FsLog(final StoreConfig storeConfig,
                 final FsLogConfig config,
                 final INodeImageStore imageStore,
                 final CollectorRegistry metricsRegistry)
        throws StoreException {

        super(storeConfig);
        this.config = config;
        try (Table table = getTable()) {
            logPointer = FsLogPointer.readFrom(table);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        logCompaction = new FsLogCompaction(logPointer,
                                            imageStore,
                                            config,
                                            metricsRegistry,
                                            storeConfig);
    }

    @VisibleForTesting
    FsLog(final StoreConfig storeConfig,
                 final FsLogConfig config,
                 final FsLogPointer fsLogPointer,
                 final INodeImageStore imageStore,
                 final CollectorRegistry metricsRegistry) {

        super(storeConfig);
        this.config = config;
        logPointer = fsLogPointer;
        logCompaction = new FsLogCompaction(logPointer,
                                            imageStore,
                                            config,
                                            metricsRegistry,
                                            storeConfig);
    }

    /**
     * Append transaction to log
     *
     * @param tx Transaction proto
     *
     * @throws StoreException
     */
    public synchronized void appendTx(Fs.MultiTransaction tx) throws StoreException {
        txValidator.validate(tx);

        final byte[] lastTxIdBytes = Bytes.toBytes(logPointer.nextTxId());

        final Put txPut = new Put(HConsts.FsImage.TX_LOG_ROW_BYTES)
                              .addColumn(INODE_CF_BYTES,
                                         lastTxIdBytes,
                                         tx.toByteArray());
        final Put idPut = new Put(TX_LOG_ROW_BYTES)
                              .addColumn(INODE_CF_BYTES,
                                         LAST_TX_ID_BYTES,
                                         lastTxIdBytes);

        try (Table table = getTable()) {
            table.put(newArrayList(txPut, idPut));
        }
        catch (IOException e) {
            throw new StoreException("Can't append transaction to log", e);
            //TODO: maybe stop service here?
        }

        runCompactionIfRequired();
    }

    /**
     * Method execute replay process which apply all pending transactions
     * to form FS image snapshot with all changes and after that clear transaction log.
     * This method usually called on system start before loading FS image snapshot to memory.
     */
    public void fullReplay() throws StoreException {
        logCompaction.replaySync();
    }

    /**
     * Start background thread which periodically apply last transactions,
     * form consistent FS image snapshot and remove applied transactions from log.
     */
    public void backgroundReplay() throws StoreException {
        logCompaction.schedule();
    }

    @Override
    public void close() throws Exception {
        logCompaction.close();
        super.close();
    }

    private void runCompactionIfRequired() throws StoreException {
        if (logPointer.logSize() >= config.maxLogSize()) {
            logCompaction.force();
        }
    }
}
