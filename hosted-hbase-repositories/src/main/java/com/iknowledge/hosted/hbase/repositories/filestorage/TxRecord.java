package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;

public final class TxRecord {

    /**
     * Transaction ID
     */
    public final long id;

    /**
     * Transaction ID bytes
     */
    public final byte[] idBytes;

    /**
     * Transaction message with all required information to apply it
     */
    public final Fs.MultiTransaction txMsg;

    public TxRecord(final long id, final byte[] idBytes, final Fs.MultiTransaction txMsg) {
        this.id = id;
        this.idBytes = idBytes;
        this.txMsg = txMsg;
    }
}
