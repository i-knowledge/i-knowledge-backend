package com.iknowledge.hosted.hbase.repositories;

import org.apache.hadoop.hbase.util.Bytes;

/**
 * Column family and qualifier constants for HBase database.
 */
public final class HConsts {

    /**
     * Users HBase table constants
     */
    public static class Users {
        /**
         * Users groups column family
         */
        public static final String USER_GROUPS_CF = "ug";

        /**
         * Users groups column family bytes
         */
        public static final byte[] USER_GROUPS_CF_BYTES = Bytes.toBytes(USER_GROUPS_CF);

        /**
         * Users table column family
         */
        public static final String COMMON_CF = "c";

        /**
         * Users table column family bytes
         */
        public static final byte[] COMMON_CF_BYTES = Bytes.toBytes(COMMON_CF);

        /**
         * Qualifier for password
         */
        public static final String PASS_QF = "pass";

        /**
         * Qualifier bytes for password
         */
        public static final byte[] PASS_QF_BYTES = Bytes.toBytes(PASS_QF);

        /**
         * Qualifier for password salt
         */
        public static final String PASS_SALT_QF = "salt";

        /**
         * Qualifier bytes for password salt
         */
        public static final byte[] PASS_SALT_QF_BYTES = Bytes.toBytes(PASS_SALT_QF);

        /**
         * Qualifier for user id
         */
        public static final String ID_QF = "id";

        /**
         * Qualifier bytes for user id
         */
        public static final byte[] ID_QF_BYTES = Bytes.toBytes(ID_QF);
    }

    /**
     * Users groups HBase table constants
     */
    public static class UserGroups {
        /**
         * Users groups common info table column family
         */
        public static final String COMMON_CF = "c";

        /**
         * Users groups common info table column family bytes
         */
        public static final byte[] COMMON_CF_BYTES = Bytes.toBytes(COMMON_CF);

        /**
         * Column family for users belonging to the current group
         */
        public static final String USERS_CF = "c";

        /**
         * Column family bytes for users belonging to the current group
         */
        public static final byte[] USERS_CF_BYTES = Bytes.toBytes(USERS_CF);

        /**
         * Qualifier for group description
         */
        public static final String DESC_QF = "desc";

        /**
         * Qualifier bytes for group description
         */
        public static final byte[] DESC_QF_BYTES = Bytes.toBytes(DESC_QF);

        /**
         * Qualifier for group id
         */
        public static final String ID_QF = "id";

        /**
         * Qualifier bytes for group id
         */
        public static final byte[] ID_QF_BYTES = Bytes.toBytes(ID_QF);
    }

    public static class FsImage {

        /**
         * inode container column family name
         */
        public static final String INODE_CF = "inode";

        /**
         * inode container column family name bytes
         */
        public static final byte[] INODE_CF_BYTES = Bytes.toBytes(INODE_CF);

        /**
         * inode metadata qualifier name
         */
        public static final String INODE_META_QF = "meta";

        /**
         * inode metadata qualifier name bytes
         */
        public static final byte[] INODE_META_QF_BYTES = Bytes.toBytes(INODE_META_QF);

        /**
         * FS transaction log row ID
         */
        public static final String TX_LOG_ROW = "tx_log";

        /**
         * FS transaction log row ID
         */
        public static final byte[] TX_LOG_ROW_BYTES = Bytes.toBytes(TX_LOG_ROW);

        /**
         * ID of last FS transaction
         */
        public static final String LAST_TX_ID = "last_tx_id";

        /**
         * ID of last FS transaction
         */
        public static final byte[] LAST_TX_ID_BYTES = Bytes.toBytes(LAST_TX_ID);

        /**
         * ID of last applied FS transaction
         */
        public static final String LAST_APPLIED_TX_ID = "last_applied_tx_id";

        /**
         * ID of last applied FS transaction
         */
        public static final byte[] LAST_APPLIED_TX_ID_BYTES = Bytes.toBytes(LAST_APPLIED_TX_ID);

        public static class INodeChildren {

            /**
             * Child inodes qualifier name
             */
            public static final String INODE_CHILD_PREFIX_QF = "child_";

            /**
             * Child inodes qualifier name bytes
             */
            public static final byte[] INODE_CHILD_PREFIX_QF_BYTES
                    = Bytes.toBytes(INODE_CHILD_PREFIX_QF);
        }

        public static class INodeRights {

            /**
             * Inode user rights qualifier name
             */
            public static final String INODE_USER_RIGHTS_PREFIX_QF = "ur_";

            /**
             * Inode user rights qualifier name bytes
             */
            public static final byte[] INODE_USER_RIGHTS_PREFIX_QF_BYTES
                    = Bytes.toBytes(INODE_USER_RIGHTS_PREFIX_QF);

            /**
             * Inode user group rights qualifier name
             */
            public static final String INODE_GROUP_USER_RIGHTS_PREFIX_QF = "ugr_";

            /**
             * Inode user group rights qualifier name bytes
             */
            public static final byte[] INODE_GROUP_USER_RIGHTS_PREFIX_QF_BYTES
                    = Bytes.toBytes(INODE_GROUP_USER_RIGHTS_PREFIX_QF);
        }
    }

    public static class Tokens {

        /**
         * Token meta column family in token mapping table
         */
        public static final String TOKEN_META_CF = "meta";

        /**
         * Token meta column family in token mapping table
         */
        public static final byte[] TOKEN_META_CF_BYTES = Bytes.toBytes(TOKEN_META_CF);

        /**
         * Last token ID row key in token mapping table(stored in meta CF)
         */
        public static final String LAST_TOKEN_ID_ROWKEY = "last_id";

        /**
         * Last token ID row key in token mapping table(stored in meta CF)
         */
        public static final byte[] LAST_TOKEN_ID_ROWKEY_BYTES = Bytes.toBytes(LAST_TOKEN_ID_ROWKEY);

        /**
         * Token mapping column family in token mapping table
         */
        public static final String TOKEN_MAP_CF = "map";

        /**
         * Token mapping column family in token mapping table
         */
        public static final byte[] TOKEN_MAP_CF_BYTES = Bytes.toBytes(TOKEN_MAP_CF);

        /**
         * Token ID column in token mapping table
         */
        public static final String TOKEN_ID_QF = "id";

        /**
         * Token ID column in token mapping table
         */
        public static final byte[] TOKEN_ID_QF_BYTES = Bytes.toBytes(TOKEN_ID_QF);

    }

    public static class DocVectors {

        /**
         * Doc vector column family
         */
        public static final String VECTOR_CF = "vector";

        /**
         * Doc vector column family
         */
        public static final byte[] VECTOR_CF_BYTES = Bytes.toBytes(VECTOR_CF);

        /**
         * Last doc vector version metadata info
         */
        public static final String LAST_VERSION_QF = "version";

        /**
         * Last doc vector version metadata info
         */
        public static final byte[] LAST_VERSION_QF_BYTES = Bytes.toBytes(LAST_VERSION_QF);
    }

    public static class DocVectorChanges {

        /**
         * Changes metadata info
         */
        public static final String CHANGES_META_CF = "meta";

        /**
         * Changes metadata info
         */
        public static final byte[] CHANGES_META_CF_BYTES = Bytes.toBytes(CHANGES_META_CF);

        /**
         * Changes type qualifier
         */
        public static final String CHANGES_TYPE_QF = "type";

        /**
         * Changes type qualifier
         */
        public static final byte[] CHANGES_TYPE_QF_BYTES = Bytes.toBytes(CHANGES_TYPE_QF);
    }
}
