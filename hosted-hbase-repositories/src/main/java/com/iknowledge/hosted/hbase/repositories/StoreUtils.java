package com.iknowledge.hosted.hbase.repositories;

import org.apache.hadoop.hbase.util.Bytes;

public final class StoreUtils {

    private StoreUtils() {}

    /**
     * Remove the specified byte array prefix from the another byte array
     * @param bytes
     * @param prefix
     * @return
     */
    public static byte[] removePrefix(final byte[] bytes, byte[] prefix) {
        final int prefixStartIndex = Bytes.indexOf( bytes, prefix );

        // if the prefix is not found
        if (prefixStartIndex < 0) {
            return new byte[0];
        }
        return Bytes.copy(bytes, prefix.length, bytes.length - prefix.length);
    }
}
