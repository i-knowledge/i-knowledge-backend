package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.google.protobuf.ByteString;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.ContainerINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileRevisionINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserGroupRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

import static com.google.common.base.Preconditions.checkArgument;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.asUsualFile;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;
import static java.util.Objects.requireNonNull;

public final class HBaseFsImageStore extends Store {

    static final String METRICS_NAMESPACE = "hbase_fs_image";
    private static final Logger LOGGER = LoggerFactory.getLogger(HBaseFsImageStore.class);

    private static final Id ROOT_INODE_ID = Id.ZERO_ID;
    private static final String ROOT_INODE_NAME = "/";
    private static final byte[] ADMIN_USER_GROUP_ID = new byte[] { 0 };

    private final INodeImageStore inodeStore;
    private final CollectorRegistry metricsRegistry;
    private final FsLogConfig fsLogConfig;
    private FsLog fsLog;

    /**
     * Initialize FS image store
     *
     * @param storeConfig HBase connection config
     *
     */
    public HBaseFsImageStore(StoreConfig storeConfig,
                             FsLogConfig fsLogConfig,
                             CollectorRegistry metricsRegistry) {

        super(storeConfig);
        this.fsLogConfig = fsLogConfig;
        inodeStore = new INodeImageStore(storeConfig.hconfig());
        this.metricsRegistry = metricsRegistry;
    }

    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor cfDesc
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(INODE_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setBlockCacheEnabled(false)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(cfDesc);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch ( IOException e ) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    @Override
    public void close() throws Exception {
        if (fsLog != null) {
            fsLog.close();
        }
        super.close();
    }

    /**
     * Add new inode to FS image without children data(skip all children and
     * set children count to 0).
     * Method doesn't check if it already exists and overwrite previous value.
     *
     * @param inode inode instance
     *
     * @throws StoreException
     */
    public void addInode(INode inode) throws StoreException {
        requireNonNull(inode);

        final Fs.Transaction.CreateCtx ctx
                = Fs.Transaction.CreateCtx.newBuilder()
                                          .setId(ByteString.copyFrom(inode.id().bytes()))
                                          .setParentId(ByteString.copyFrom(inode.parent()
                                                                                .id()
                                                                                .bytes()))
                                          .setNewInode(inode.proto())
                                          .build();
        final Fs.Transaction tx
                = Fs.Transaction.newBuilder()
                                .setType(Fs.Transaction.Type.CREATE)
                                .setCreateCtx(ctx)
                                .build();
        final Fs.MultiTransaction multiTx = Fs.MultiTransaction.newBuilder()
                                                               .addTxList(tx)
                                                               .build();
        fsLog.appendTx(multiTx);
    }

    /**
     * Method create transaction to move inode with rename.
     *
     * @param srcInode Inode to move
     * @param renamedInode New inode which represent moved and renamed inode
     *
     * @throws StoreException
     */
    public void rename(INode srcInode, INode renamedInode) throws StoreException {

        requireNonNull(srcInode);
        requireNonNull(renamedInode);

        final ByteString srcId = ByteString.copyFrom(srcInode.id().bytes());
        final ByteString srcParentId = ByteString.copyFrom(srcInode.parent().id().bytes());
        final ByteString sinkParentId = ByteString.copyFrom(renamedInode.parent().id().bytes());
        final Fs.Transaction.RenameCtx.Builder moveCtx
                = Fs.Transaction.RenameCtx.newBuilder()
                                        .setSrcId(srcId)
                                        .setSrcParentId(srcParentId)
                                        .setSinkParentId(sinkParentId)
                                        .setNewName(renamedInode.name());
        final Fs.Transaction tx = Fs.Transaction.newBuilder()
                                                         .setType(Fs.Transaction.Type.RENAME)
                                                         .setRenameCtx(moveCtx)
                                                         .build();
        final Fs.MultiTransaction multiTx = Fs.MultiTransaction.newBuilder()
                                                               .addTxList(tx)
                                                               .build();
        fsLog.appendTx(multiTx);
    }

    /**
     * Method create transaction which used to move inode.
     *
     * @param inode inode instance which should be moved
     * @param newParent new parent inode(target directory)
     *
     * @throws StoreException
     */
    public void move(INode inode, INode newParent) throws StoreException {
        requireNonNull(inode);
        requireNonNull(newParent);

        final ByteString srcId = ByteString.copyFrom(inode.id().bytes());
        final ByteString srcParentId = ByteString.copyFrom(inode.parent().id().bytes());
        final ByteString sinkParentId = ByteString.copyFrom(newParent.id().bytes());
        final Fs.Transaction.MoveCtx.Builder moveCtx
                = Fs.Transaction.MoveCtx.newBuilder()
                                          .setSrcId(srcId)
                                          .setSrcParentId(srcParentId)
                                          .setSinkParentId(sinkParentId);
        final Fs.Transaction tx = Fs.Transaction.newBuilder()
                                                         .setType(Fs.Transaction.Type.MOVE)
                                                         .setMoveCtx(moveCtx)
                                                         .build();
        final Fs.MultiTransaction multiTx = Fs.MultiTransaction.newBuilder()
                                                               .addTxList(tx)
                                                               .build();
        fsLog.appendTx(multiTx);
    }

    /**
     * Attach user rights to the inode
     * @param inodeId Rights must be attached to the specified inodeId
     * @param rights User rights to the specified inode
     * @throws StoreException
     */
    public void attachUserRights( final byte[] inodeId,
                                  final List<HBaseINodeRights> rights) throws StoreException {
        requireNonNull(inodeId);
        checkArgument(inodeId.length > 0, "INode id must be present");
        requireNonNull(rights);

        final Fs.Transaction.RightsCtx.Builder ctx
                = Fs.Transaction.RightsCtx.newBuilder()
                                           .setId(ByteString.copyFrom(inodeId));
        for (HBaseINodeRights right : rights) {
            ctx.addRights(right.proto());
        }
        final Fs.Transaction tx
                = Fs.Transaction.newBuilder()
                                .setType(Fs.Transaction.Type.ACCESS_RIGHTS)
                                .setRightsCtx(ctx)
                                .build();
        final Fs.MultiTransaction multiTx = Fs.MultiTransaction.newBuilder()
                                                               .addTxList(tx)
                                                               .build();
        fsLog.appendTx(multiTx);
    }

    /**
     * Method format FS image: remove all existing inodes, create FS root, etc.
     */
    public void format() throws StoreException {
        if (isTableExists()) {
            truncateTable();
        }
        else {
            createTable();
        }

        List<HBaseINodeRights> groupRights = new ArrayList<>(1);
        INodeRights adminRights = new INodeRights(true, true, true);
        INodeUserGroupRights adminGroupRights = new INodeUserGroupRights(
                Id.of(ADMIN_USER_GROUP_ID), adminRights);
        groupRights.add(adminGroupRights);
        final List<HBaseINodeRights> userRights = new ArrayList<>();
        final DirectoryINode root = new DirectoryINode(ROOT_INODE_ID,
                                                       ROOT_INODE_NAME,
                                                       new UserRights(userRights,
                                                       groupRights));
        try (Table table = getTable()) {
            // if failed => it cause creation of InvalidINode
            // inode object in FS in-memory tree
            inodeStore.putInodeMeta(root.id().bytes(), root.proto(), table);
            inodeStore.addInodeRights(root.id().bytes(),
                                      userRights,
                                      groupRights,
                                      table,
                                      50);
        }
        catch (IOException e) {
            throw new StoreException("Inode add operation failed: " + root, e);
        }
    }

    /**
     * Method load entire file system image into in-memory data structure
     *
     * @return Root inode or empty if HBase FS image not initialized
     */
    public Optional<FsMap> load() throws StoreException {
        if (!isTableExists()) {
            return Optional.empty();
        }

        initFsLog();

        final FsMap fsImage;
        try (Table table = getTable()) {
            final INodeProto.INode metaProto = loadMeta(ROOT_INODE_ID, table);
            final INodeMeta rootMeta = new INodeMeta(metaProto);
            if (rootMeta.isInvalid()) {
                return Optional.empty();
            }

            final UserRights rights = loadAccessRights(ROOT_INODE_ID, table);

            DirectoryINode rootInode = new DirectoryINode(ROOT_INODE_ID,
                                                          rootMeta.name(),
                                                          rights);
            Queue<ContainerINode> pendingInodes = new LinkedList<>();
            pendingInodes.add(rootInode);
            fsImage = new FsMap(rootInode);

            while (!pendingInodes.isEmpty()) {
                final ContainerINode inode = pendingInodes.poll();
                processChildren(inode, fsImage, pendingInodes, table);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        fsLog.backgroundReplay();
        return Optional.of(fsImage);
    }

    /**
     * Remove container inode(with all children) from FS image and detach it from parent.
     * Method not fail if inode not exists.
     *
     * @param inode inode instance
     *
     * @throws StoreException
     */
    public <T extends INode> void removeContainerInode(ContainerINode<T> inode)
        throws StoreException {

        requireNonNull(inode);

        final Fs.MultiTransaction.Builder multiTx = Fs.MultiTransaction.newBuilder();
        for (T child : inode.children()) {
            final Fs.Transaction tx = removeInodeTx(child.id(), inode.id().bytes());
            multiTx.addTxList(tx);
        }
        final Fs.Transaction inodeTx = removeInodeTx(inode.id(), inode.parent().id().bytes());
        multiTx.addTxList(inodeTx);
        fsLog.appendTx(multiTx.build());
    }

    /**
     * Remove inode from FS image and detach it from parent. Method not fail if inode not exists.
     *
     * @param inode inode instance
     *
     * @throws StoreException
     */
    public void removeInode(INode inode) throws StoreException {
        requireNonNull(inode);

        final Fs.Transaction inodeTx = removeInodeTx(inode.id(), inode.parent().id().bytes());
        fsLog.appendTx(Fs.MultiTransaction.newBuilder()
                                          .addTxList(inodeTx)
                                          .build());
    }

    private void removeInode(final Id inodeId,
                             final byte[] parentId) throws StoreException {

        final Fs.Transaction tx = removeInodeTx(inodeId, parentId);
        final Fs.MultiTransaction multiTx = Fs.MultiTransaction.newBuilder()
                .addTxList(tx)
                .build();
        fsLog.appendTx(multiTx);
    }

    private void initFsLog() throws StoreException {
        if (fsLog == null) {
            fsLog = new FsLog(storeConfig, fsLogConfig, inodeStore, metricsRegistry);
        }
        fsLog.fullReplay();
    }

    private Fs.Transaction removeInodeTx(final Id inodeId, final byte[] parentId) {

        final Fs.Transaction.RemoveCtx ctx
                = Fs.Transaction.RemoveCtx.newBuilder()
                                          .setId(ByteString.copyFrom(inodeId.bytes()))
                                          .setParentId(ByteString.copyFrom(parentId))
                                          .build();
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.REMOVE)
                             .setRemoveCtx(ctx)
                             .build();

    }

    private void processChildren(final ContainerINode parent,
                                 final FsMap fsMap,
                                 final Queue<ContainerINode> pendingInodes,
                                 final Table table)
            throws StoreException {

        final List<byte[]> children = loadChildren(parent, table);

        for ( byte[] childId : children) {
            final Id inodeId = Id.of(childId);
            final INodeMeta meta = new INodeMeta(loadMeta(inodeId, table));
            final UserRights rights = loadAccessRights(inodeId, table);

            if ( meta.isInvalid() ) {
                removeInode(inodeId, parent.id().bytes());
                continue;
            }

            switch ( meta.type() ) {
                case FILE:
                    if (isUsualDir(parent)) {
                        DirectoryINode dirParent = asUsualDir(parent);
                        FileINode file = new FileINode(inodeId,
                                              meta.name(),
                                              dirParent,
                                              rights);
                        dirParent.addChild(file);
                        fsMap.put(file);
                        pendingInodes.add(file);
                    }
                    break;
                case DIR:
                    if (isUsualDir(parent)) {
                        DirectoryINode dirParent = asUsualDir(parent);
                        DirectoryINode dir = new DirectoryINode(inodeId,
                                                   meta.name(),
                                                   dirParent,
                                                   rights);
                        dirParent.addChild(dir);
                        fsMap.put(dir);
                        pendingInodes.add(dir);
                    }
                    break;
                case FILE_REVISION:
                    if (isUsualFile(parent)) {
                        FileINode fileParent = asUsualFile(parent);
                        FileRevisionINode rev = new FileRevisionINode(inodeId,
                                                      meta.revision(),
                                                      meta.sysPath(),
                                                      fileParent);
                        fileParent.addChild(rev);
                        fsMap.put(rev);
                    }
                    break;
                default:
                    // ignore
                    break;
            }
        }
    }

    private List<byte[]> loadChildren(final INode inode, final Table table) throws StoreException {
        try {
            return inodeStore.getChildren(inode.id().bytes(), table);
        }
        catch (IOException e) {
            throw new StoreException("Can't load children list for inode: "
                                     + inode.toString(),
                                     e);
        }
    }

    private UserRights loadAccessRights(final Id inodeId, final Table table)
            throws StoreException {

        try {
            return inodeStore.getRights(inodeId.bytes(), table);
        }
        catch (IOException e) {
            throw new StoreException("Can't load access rights for root node", e);
        }
    }

    private INodeProto.INode loadMeta(final Id inodeId, final Table table)
            throws StoreException {

        try {
            return inodeStore.getMeta(inodeId.bytes(), table);
        }
        catch (IOException e) {
            throw new StoreException("Can't get meta for inode ID=" + inodeId, e);
        }
    }
}
