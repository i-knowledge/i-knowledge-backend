package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

/**
 * inode snapshot instance with version number taken during snapshot creation.
 */
public interface INodeSnapshot<T extends MutableINode> {

    boolean isModified();

    T inode();
}
