package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

/**
 * HBase Document vector reader class.
 */
public final class DocVectorReader extends DocVectorAccessor {

    private final byte[] currentVersion;
    private final int blocks;
    private int nextBlock;

    public DocVectorReader(final int srcId,
                           final Id docId,
                           final Table indexTable) throws StoreException {

        super(srcId, docId, indexTable);
        try {
            currentVersion = Bytes.toBytes(currentVectorVersion());
            blocks = blocksInVersion(currentVersion);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Read next block of tokens in current document vector.
     *
     * @return Next block or null if all blocks read for this document.
     */
    public List<TokenProto.Token> nextBlock() throws StoreException {
        TokenProto.Block block;
        if (nextBlock < blocks) {
            try {
                block = readBlock(nextBlock);
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
            nextBlock++;
        }
        else {
            block = null;
        }
        return block == null
                ? null
                : block.getTokenList();
    }

    private int currentVectorVersion() throws StoreException {
        try {
            int curVersion;
            final Get get = new Get(docId.bytes())
                                .addColumn(VECTOR_CF_BYTES, LAST_VERSION_QF_BYTES)
                                .readVersions(1);
            final Result result;
            result = indexTable.get(get);
            if (result.isEmpty()) {
                curVersion = UNCOMMITED_VERSIONS_RANGE_END;
            }
            else {
                final Cell versionCell = result.getColumnLatestCell(VECTOR_CF_BYTES,
                                                                    LAST_VERSION_QF_BYTES);
                final byte[] bytes = CellUtil.cloneValue(versionCell);
                final int lastVersion = Bytes.toInt(bytes);
                if (lastVersion < UNCOMMITED_VERSIONS_RANGE_END) { // last version was not committed
                    final int uncommitedVersion = lastVersion * -1;
                    removeUncommitedVector(uncommitedVersion);
                    curVersion = uncommitedVersion - 1;
                }
                else {
                    curVersion = lastVersion;
                }
            }
            return curVersion;
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    protected TokenProto.Block readBlock(final int blockNum) throws IOException {
        final byte[] columnQf = blockColumnQf(currentVersion, blockNum);
        final Get get = new Get(docId.bytes())
                            .addColumn(VECTOR_CF_BYTES, columnQf)
                            .readVersions(1);
        final Result result = indexTable.get(get);
        if (result.isEmpty()) {
            return null;
        }
        final Cell blockCell = result.getColumnLatestCell(VECTOR_CF_BYTES, columnQf);
        final byte[] blockBytes = CellUtil.cloneValue(blockCell);
        return TokenProto.Block.parseFrom(blockBytes);
    }
}
