package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import java.util.BitSet;

/**
 * Utility class to work with inode rights
 */
public final class INodeRightsUtils {

    private static final int CAN_READ_INDEX = 0;
    private static final int CAN_WRITE_INDEX = 1;
    private static final int CAN_UPLOAD_INDEX = 2;

    private INodeRightsUtils() {}

    /**
     * Convert inode rights into the byte array
     * @param rights
     * @return
     */
    public static byte[] getRights(INodeRights rights) {
        BitSet bitSet = new BitSet();

        if (rights.isCanRead()) {
            bitSet.set(CAN_READ_INDEX);
        }

        if (rights.isCanWrite()) {
            bitSet.set(CAN_WRITE_INDEX);
        }

        if (rights.isCanUpload()) {
            bitSet.set(CAN_UPLOAD_INDEX);
        }

        return bitSet.toByteArray();
    }

    /**
     * Convert inode rights from byte array into the INodeRights object
     * @param rights
     * @return
     */
    public static INodeRights getRights(byte[] rights) {
        final BitSet bitSet = fromByteArray(rights);
        return new INodeRights(bitSet.get(CAN_READ_INDEX),
                bitSet.get(CAN_WRITE_INDEX), bitSet.get(CAN_UPLOAD_INDEX));
    }

    private static BitSet fromByteArray(byte[] bytes) {
        BitSet bits = new BitSet();
        for (int i = 0; i < bytes.length * 8; i++) {
            if ((bytes[bytes.length - i / 8 - 1] & (1 << (i % 8))) > 0) {
                bits.set(i);
            }
        }
        return bits;
    }
}
