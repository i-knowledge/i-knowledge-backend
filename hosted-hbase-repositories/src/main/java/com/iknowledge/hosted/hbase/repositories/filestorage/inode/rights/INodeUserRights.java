package com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights;

import com.google.protobuf.ByteString;
import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeRightsProto;

/**
 * Represent rights that are connected with user
 */
public final class INodeUserRights extends HBaseINodeRights {

    /**
     * @param userId
     * @param userRights
     */
    public INodeUserRights(Id userId, INodeRights userRights) {
        super(userId, userRights);
    }

    @Override
    public INodeRightsProto.INodeRights proto() {
        final INodeRightsProto.INodeRights.UserRights userRights = INodeRightsProto.INodeRights
                .UserRights.newBuilder()
                .setUserId(ByteString.copyFrom(getEntityId().bytes()))
                .setRights(ByteString.copyFrom(getEntityRights().getRights()))
                .build();
        return INodeRightsProto.INodeRights.newBuilder()
                .setType(INodeRightsProto.INodeRights.Type.USER)
                .setUserRights(userRights)
                .build();
    }

    /**
     * Map the specified proto object to the simple one
     * @param rights
     * @return
     */
    public static INodeUserRights map(INodeRightsProto.INodeRights.UserRights rights) {
        return new INodeUserRights(Id.of(rights.getUserId().toByteArray()),
                INodeRightsUtils.getRights(rights.getRights().toByteArray()));
    }
}
