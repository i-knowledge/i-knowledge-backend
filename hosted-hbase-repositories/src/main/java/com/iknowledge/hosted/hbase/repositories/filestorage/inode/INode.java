package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.function.Function;

/**
 * Represent FS inode.
 * Inheritors must implement equals, hashCode and optionally toString methods
 */
public interface INode {

    /**
     * Node ID.
     *
     * @implNote This call return raw byte array without copying for performance reasons.
     *           Don't overwrite values in it.
     *           Must be read-only value and not change during time and don't require lock.
     * @return
     */
    Id id();

    /**
     * User-friendly name of inode(for instance, file name or dir name which user provide for this
     * inode)
     */
    String name();

    /**
     * Parent inode or null if this inode is root
     */
    ContainerINode<? extends INode> parent();

    /**
     * Full file system path used to display inode path to user.
     * This path build using display names of current inode and it parents.
     */
    Path path();

    /**
     * inode lock object
     * @return
     */
    ReadWriteLock lock();

    /**
     * Maps current inode metadata into protobuf
     * @return
     */
    INodeProto.INode proto();

    /**
     * Return full file system path(contains display name of inodes) for some inode
     *
     * @param inode
     *
     * @return
     */
    static Path readFullPath(INode inode) {
        return readFullPath(inode, INode::name);
    }

    /**
     * Return full file system path(contains system name of inodes) for some inode
     *
     * @param inode
     *
     * @return
     */
    static Path readFullPath(INode inode, Function<INode, String> inodeNameSupplier) {
        final List<String> pathComponents = new ArrayList<>();

        inode.lock().readLock().lock();
        pathComponents.add(inodeNameSupplier.apply(inode));
        inode.lock().readLock().unlock();
        INode parent = inode.parent();
        while (parent != null) {
            parent.lock().readLock().lock();
            pathComponents.add(inodeNameSupplier.apply(parent));
            INode parentTmp = parent.parent();
            parent.lock().readLock().unlock();
            parent = parentTmp;
        }

        if (pathComponents.size() == 1) { //root path or some inode without parent
            return Paths.get(pathComponents.get(0));
        }
        Collections.reverse(pathComponents);
        return Paths.get(pathComponents.get(0),
                         pathComponents.subList(1, pathComponents.size()).toArray(new String[0]));
    }
}
