package com.iknowledge.hosted.hbase.repositories.users;

import com.google.protobuf.ByteString;
import com.iknowledge.common.Id;
import com.iknowledge.hosted.auth.protobuf.UserInfoResponseProto;

import java.util.List;

/**
 * Class that contains information about user, his groups and other user level information
 */
public final class UserInfo {
    private final String userName;
    private final byte[] passwordHash;
    private final byte[] passwordSalt;
    private final Id id;
    private final List<String> userGroups;

    /**
     *  @param userName
     * @param passwordHash
     * @param passwordSalt
     * @param id
     * @param userGroups
     */
    public UserInfo(String userName, byte[] passwordHash, byte[] passwordSalt,
                    byte[] id, List<String> userGroups) {
        this.userName = userName;
        this.passwordHash = passwordHash;
        this.passwordSalt = passwordSalt;
        this.id = Id.of(id);
        this.userGroups = userGroups;
    }

    /**
     * Password hash
     * @return
     */
    public byte[] getPasswordHash() {
        return passwordHash;
    }

    /**
     * Password salt that is used to generate password hash
     * @return
     */
    public byte[] getPasswordSalt() {
        return passwordSalt;
    }

    /**
     * Groups in which user is belonging to
     * @return
     */
    public List<String> getUserGroups() {
        return userGroups;
    }

    /**
     * User name (login)
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * User id
     * @return
     */
    public Id getId() {
        return id;
    }

    /**
     * Map the current object into the appropriate proto object
     * @return
     */
    public UserInfoResponseProto.UserInfoResponse map() {
        final UserInfoResponseProto.UserInfoResponse.Builder builder =
                UserInfoResponseProto.UserInfoResponse.newBuilder();
        return builder.setUserName(userName)
                .setPasswordHash(ByteString.copyFrom(passwordHash))
                .setPasswordSalt(ByteString.copyFrom(passwordSalt))
                .addAllUserGroups(userGroups)
                .build();
    }

    /**
     * Map the specified UserInfo proto object into the simple one
     * @param proto
     * @return
     */
    public static UserInfo map(UserInfoResponseProto.UserInfoResponse proto) {
        return new UserInfo(proto.getUserName(), proto.getPasswordHash().toByteArray(),
                proto.getPasswordSalt().toByteArray(),
                proto.getId().toByteArray(), proto.getUserGroupsList());
    }
}
