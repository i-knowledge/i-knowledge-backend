package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

public final class INodeTypes {

    private INodeTypes() {}

    /**
     * Is passed inode is user level inode(inode which can be displayed to user)
     *
     * @param inode
     * @return
     */
    public static boolean isUserLevelInode(INode inode) {
        return inode instanceof UserLevelINode;
    }

    /**
     * Check is passed inode is a usual file inode which can be accessed by user
     *
     * @param inode
     *
     * @return
     */
    public static boolean isUsualFile(INode inode) {
        return inode instanceof FileINode;
    }

    /**
     * Check is passed inode is a usual file inode which can be accessed by user
     *
     * @param inode
     *
     * @return
     */
    public static FileINode asUsualFile(INode inode) {
        return (FileINode)inode;
    }

    /**
     * Check is passed inode is a usual directory inode which can be accessed by user
     *
     * @param inode
     *
     * @return
     */
    public static boolean isUsualDir(INode inode) {
        return inode instanceof DirectoryINode;
    }

    /**
     * Check is passed inode is a usual directory inode which can be accessed by user
     *
     * @param inode
     *
     * @return
     */
    public static DirectoryINode asUsualDir(INode inode) {
        return (DirectoryINode) inode;
    }

    /**
     * Check is passed inode is a file revision inode
     *
     * @param inode
     * @return
     */
    public static boolean isRevision(INode inode) {
        return inode instanceof FileRevisionINode;
    }

    /**
     * Check is passed inode is a file revision inode
     *
     * @param inode
     * @return
     */
    public static FileRevisionINode asRevision(INode inode) {
        return (FileRevisionINode)inode;
    }

    /**
     * Check is passed inode is a file snapshot inode
     *
     * @param inode
     * @return
     */
    public static boolean isFileSnapshot(INodeSnapshot inode) {
        return inode instanceof FileINodeSnapshot;
    }

    /**
     * Check is passed inode is a file snapshot inode
     *
     * @param inode
     * @return
     */
    public static FileINodeSnapshot asFileSnapshot(INodeSnapshot inode) {
        return (FileINodeSnapshot)inode;
    }

    /**
     * Check is passed inode is a dir snapshot inode
     *
     * @param inode
     * @return
     */
    public static boolean isDirSnapshot(INodeSnapshot inode) {
        return inode instanceof DirINodeSnapshot;
    }

    /**
     * Check is passed inode is a dir snapshot inode
     *
     * @param inode
     * @return
     */
    public static DirINodeSnapshot asDirSnapshot(INodeSnapshot inode) {
        return (DirINodeSnapshot)inode;
    }
}
