package com.iknowledge.hosted.hbase.repositories.users;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.iknowledge.hbase.repositories.HDefaults.BOOL_TRUE_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.COMMON_CF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.COMMON_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.ID_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.ID_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.PASS_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.PASS_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.PASS_SALT_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.PASS_SALT_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.USER_GROUPS_CF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.Users.USER_GROUPS_CF_BYTES;
import static java.text.MessageFormat.format;

/**
 * Store to operate with data in Users table
 */
public final class UserStore extends Store {

    private final Logger logger;

    /**
     * @param storeConfig Hbase store config
     */
    public UserStore(StoreConfig storeConfig) {

        super(storeConfig);
        this.logger = LoggerFactory.getLogger(UserStore.class);

    }

    @SuppressWarnings("Duplicates")
    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor cfCommonDesc
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(COMMON_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setVersions(1, 1);
                final ColumnFamilyDescriptor cfUserGroupsDesc
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(USER_GROUPS_CF_BYTES)
                              .setBloomFilterType(BloomType.ROW)
                              .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                              .setCompressionType(Compression.Algorithm.SNAPPY)
                              .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                              .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(cfCommonDesc)
                          .addColumnFamily(cfUserGroupsDesc);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Add user to table
     * @param userName
     * @param passwordHash
     * @param passwordSalt
     * @param groupName
     * @throws StoreException
     */
    public void put(String userName, byte[] passwordHash, byte[] passwordSalt,
                    String groupName) throws StoreException {
        Put put = new Put(Bytes.toBytes(userName))
                .addColumn(COMMON_CF_BYTES, PASS_QF_BYTES, passwordHash)
                .addColumn(COMMON_CF_BYTES, PASS_SALT_QF_BYTES, passwordSalt)
                .addColumn(COMMON_CF_BYTES, ID_QF_BYTES, Id.len16().bytes())
                .addColumn(USER_GROUPS_CF_BYTES, Bytes.toBytes(groupName), BOOL_TRUE_BYTES);

        try (Table table = getTable()) {
            table.put(put);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during user creation. "
                    + "UserName: [{0}], GroupName: [{1}]",
                    userName, groupName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Add user to the specified group
     * @param userName
     * @param groupName
     * @throws StoreException
     */
    public void addUserToGroup(String userName, String groupName) throws StoreException {
        Put put = new Put(Bytes.toBytes(userName))
                .addColumn(USER_GROUPS_CF_BYTES, Bytes.toBytes(groupName), BOOL_TRUE_BYTES);
        try (Table table = getTable()) {
            table.put(put);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during adding user to the group. "
                            + "UserName: [{0}], GroupName: [{1}]",
                    userName, groupName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Delete all information about user
     * @param userName
     * @throws StoreException
     */
    public void delete(String userName) throws StoreException {
        Delete delete = new Delete(Bytes.toBytes(userName));
        try (Table table = getTable()) {
            table.delete(delete);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during user deletion. "
                    + "UserName: [{0}]", userName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Delete user from the specified group
     *
     * @param userName
     *
     * @throws StoreException
     */
    public void removeUserFromGroup(String userName, String groupName) throws StoreException {
        Delete delete = new Delete(Bytes.toBytes(userName))
                .addColumn(USER_GROUPS_CF_BYTES, Bytes.toBytes(groupName));
        try (Table table = getTable()) {
            table.delete(delete);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during removing user from the group. "
                    + "UserName: [{0}], userGroup: [{1}]", userName, groupName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Get full user info by user name
     * @param userName
     * @return {@link UserInfo} instance
     * @throws StoreException
     */
    public UserInfo get(String userName) throws StoreException {
        Get get = new Get(Bytes.toBytes(userName));
        try (Table table = getTable()) {
            final Result result = table.get(get);
            return map(result);
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during getting user info. "
                    + "UserName: [{0}]", userName), e);
            throw new StoreException(e);
        }
    }

    /**
     * Get groups by user name
     *
     * @param userName
     *
     * @return
     *
     * @throws StoreException
     */
    public List<String> getUserGroups(String userName) throws StoreException {
        Get get = new Get(Bytes.toBytes(userName))
                .addFamily(USER_GROUPS_CF_BYTES);
        try (Table table = getTable()) {
            final Result result = table.get(get);
            final UserInfo userInfo = map(result);
            return userInfo.getUserGroups();
        }
        catch (IOException e) {
            logger.error(format("Error has occurred during getting user info. "
                    + "UserName: [{0}]", userName), e);
            throw new StoreException(e);
        }
    }

    private UserInfo map(Result result) throws StoreException {
        String userName = "";
        byte[] passwordHash = null;
        byte[] passwordSalt = null;
        byte[] id = null;
        List<String> userGroups = new ArrayList<>();

        final List<Cell> cells = result.listCells();
        for (Cell cell : cells) {
            userName = Bytes.toString(CellUtil.cloneRow(cell));
            final String family = Bytes.toString(CellUtil.cloneFamily(cell));
            final String qualifier = Bytes.toString(CellUtil.cloneQualifier(cell));
            final byte[] valueBytes = CellUtil.cloneValue(cell);
            if (family.equals(COMMON_CF)) {
                switch (qualifier) {
                    case PASS_QF:
                        passwordHash = valueBytes;
                        break;

                    case PASS_SALT_QF:
                        passwordSalt = valueBytes;
                        break;

                    case ID_QF:
                        id = valueBytes;
                        break;

                    default: throw new StoreException("Unknown qualifier in users Common CF");
                }
            }

            if (family.equals(USER_GROUPS_CF)) {
                final boolean isInGroup = Bytes.toBoolean(valueBytes);
                if (isInGroup) {
                    userGroups.add(qualifier);
                }
            }
        }
        return new UserInfo(userName, passwordHash, passwordSalt, id, userGroups);
    }
}
