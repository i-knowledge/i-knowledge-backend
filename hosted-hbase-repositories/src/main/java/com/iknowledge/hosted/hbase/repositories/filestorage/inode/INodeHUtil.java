package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.StoreUtils;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeRightsUtils;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserGroupRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.INodeUserRights;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.util.Bytes;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeChildren.INODE_CHILD_PREFIX_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeRights.INODE_GROUP_USER_RIGHTS_PREFIX_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeRights.INODE_GROUP_USER_RIGHTS_PREFIX_QF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeRights.INODE_USER_RIGHTS_PREFIX_QF;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INodeRights.INODE_USER_RIGHTS_PREFIX_QF_BYTES;
import static java.text.MessageFormat.format;

public final class INodeHUtil {

    private INodeHUtil() {}

    /**
     * Form child qualifier for current inode which used
     * in parent inode to mark this inode as child.
     *
     * @return
     */
    public static byte[] getChildQf( final byte[] inodeId ) {
        return Bytes.add(INODE_CHILD_PREFIX_QF_BYTES, inodeId);
    }

    /**
     * Extract inode ID from HBase cell which represent child inode
     *
     * @param child
     * @return
     */
    public static byte[] getChildId( final Cell child ) {
        final byte[] qf = CellUtil.cloneQualifier(child);
        return StoreUtils.removePrefix(qf, INODE_CHILD_PREFIX_QF_BYTES);
    }

    /**
     * Form user rights qualifier for the current inode
     * @param userId
     * @return
     */
    public static byte[] getUserRightsQf( final byte[] userId) {
        return Bytes.add(INODE_USER_RIGHTS_PREFIX_QF_BYTES, userId);
    }

    /**
     * Form user group rights qualifier for the current inode
     * @param groupId
     * @return
     */
    public static byte[] getUserGroupRightsQf(final byte[] groupId) {
        return Bytes.add(INODE_GROUP_USER_RIGHTS_PREFIX_QF_BYTES, groupId);
    }

    /**
     * Get inode rights from the specified hbase cell
     * @param cell
     * @return
     */
    public static HBaseINodeRights getRights(final Cell cell) throws StoreException {
        final byte[] qf = CellUtil.cloneQualifier(cell);
        final String qualifier = Bytes.toString(qf);
        if (!qualifier.startsWith(INODE_USER_RIGHTS_PREFIX_QF)
                && !qualifier.startsWith(INODE_GROUP_USER_RIGHTS_PREFIX_QF)) {
            return null;
        }
        final byte[] userId = StoreUtils.removePrefix(qf, INODE_USER_RIGHTS_PREFIX_QF_BYTES);
        final byte[] rights = CellUtil.cloneValue(cell);
        final INodeRights iNodeRights = INodeRightsUtils.getRights(rights);
        if (userId.length > 0) {
            return new INodeUserRights(Id.of(userId), iNodeRights);
        } else {
            final byte[] groupId =
                    StoreUtils.removePrefix(qf, INODE_GROUP_USER_RIGHTS_PREFIX_QF_BYTES);
            if (groupId.length < 1) {
                throw new StoreException(
                        format("Unable to remove prefix [{0}] from qualifier [{1}]",
                                INODE_GROUP_USER_RIGHTS_PREFIX_QF, qualifier));
            }
            return new INodeUserGroupRights(Id.of(groupId), iNodeRights);
        }
    }
}
