package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.common.Id;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;

import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isRevision;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualDir;
import static com.iknowledge.hosted.hbase.repositories.filestorage.inode.INodeTypes.isUsualFile;

/**
 * Class represent FS image map: ID to inode instance.
 * It contains full FS image and used as fast access to inode by it ID.
 */
public final class FsMap {

    private final DirectoryINode fsRoot;
    private final ConcurrentMap<Id, INode> inodeMap = new ConcurrentHashMap<>();
    private final LongAdder files = new LongAdder();
    private final LongAdder dirs = new LongAdder();
    private final LongAdder revs = new LongAdder();

    FsMap(final DirectoryINode fsRoot) {
        this.fsRoot = fsRoot;
        put(fsRoot); // count root dir
    }

    /**
     * Add inode if not already exists.
     *
     * @param inode
     *
     * @return True if not already in map
     */
    public boolean put(INode inode) {
        final boolean added = inodeMap.putIfAbsent(inode.id(), inode) == null;
        if (added) {
            if (isUsualFile(inode)) {
                incFiles();
            }
            else if (isUsualDir(inode)) {
                incDirs();
            }
            else if (isRevision(inode)) {
                incRevs();
            }
        }
        return added;
    }

    /**
     * Replace existing inode with new inode with same ID.
     *
     * @param inode Inode which have to be replaced
     * @param replace New inode
     *
     * @return True if replaced
     */
    public <T extends INode> boolean replace(T inode, T replace) {
        if (!inode.id().equals(replace.id())) {
            throw new IllegalArgumentException("Replace can be performed for inodes with same ID");
        }
        return inodeMap.replace(inode.id(), replace) != null;
    }

    /**
     * Remove inode by id
     *
     * @param id
     *
     * @return True if found and removed
     */
    public boolean remove(Id id) {
        final INode inode = inodeMap.remove(id);
        if (inode != null) {
            if (isUsualDir(inode)) {
                decDirs();
            }
            else if (isUsualFile(inode)) {
                decFiles();
            }
            else if (isRevision(inode)) {
                decRevs();
            }
        }
        return inode != null;
    }

    /**
     * Remove inode by id
     *
     * @param inode
     *
     * @return True if found and removed
     */
    public boolean remove(INode inode) {
        return remove(inode.id());
    }

    /**
     * Get inode by id
     *
     * @param id
     *
     * @return
     */
    public Optional<INode> get(Id id) {
        final INode inode = inodeMap.get(id);
        return Optional.ofNullable(inode);
    }

    public DirectoryINode root() {
        return fsRoot;
    }

    public long files() {
        return files.sum();
    }

    public long dirs() {
        return dirs.sum();
    }

    public long revs() {
        return revs.sum();
    }

    private void incFiles() {
        files.increment();
    }

    private void decFiles() {
        files.decrement();
    }

    private void incDirs() {
        dirs.increment();
    }

    private void decDirs() {
        dirs.decrement();
    }

    private void incRevs() {
        revs.increment();
    }

    private void decRevs() {
        revs.decrement();
    }
}
