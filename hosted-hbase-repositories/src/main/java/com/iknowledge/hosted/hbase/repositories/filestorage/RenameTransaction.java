package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.INodeProto;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Arrays;

import static java.text.MessageFormat.format;

/**
 * Class represent transaction which used to rename file(or move file with rename)
 * <p>
 *     Move with rename consist of 3 steps:
 *     <ol>
 *         <li>add new(moved) inode as proposed child to parent(target folder)</li>
 *         <li>create new inode for moved file</li>
 *         <li>commit new inode as valid child of parent</li>
 *     </ol>
 * </p>
 * Before third step caller make some actions and make decision to commit or
 * rollback proposed inode.
 */
class RenameTransaction implements FsTransaction {

    private final Fs.Transaction.RenameCtx ctx;
    private final INodeImageStore imageStore;
    private final Table table;

    public RenameTransaction(Fs.Transaction.RenameCtx ctx,
                             INodeImageStore imageStore,
                             Table table) {

        this.ctx = ctx;
        this.imageStore = imageStore;
        this.table = table;
    }

    @Override
    public void apply() throws StoreException {
        final byte[] srcId = ctx.getSrcId().toByteArray();
        // usually source and sink for rename are same, but in future
        // we can support move with rename simultaneously
        final byte[] srcParentId = ctx.getSrcParentId().toByteArray();
        final byte[] sinkParentId = ctx.getSinkParentId().toByteArray();
        try {
            final INodeProto.INode meta = imageStore.getMeta(srcId, table);
            if (meta == null) {
                // this is not possible case, removed inode cannot be renamed
                // but we protect ourself from bugs
                return;
            }
            // skip if simply rename inode without moving to another parent
            if (!Bytes.equals(srcParentId, sinkParentId)) {
                imageStore.detachFromParent(srcId, srcParentId, table);
                imageStore.attachToParent(srcId, sinkParentId, table);
            }
            final INodeProto.INode updatedProto = meta.toBuilder()
                                                      .setName(ctx.getNewName())
                                                      .build();
            imageStore.putInodeMeta(srcId, updatedProto, table);
        }
        catch (IOException e) {
            throw new StoreException(format("Rename transaction failed for inode: {0}, "
                                            + "new parent id = {1}",
                                            Arrays.toString(srcId),
                                            Arrays.toString(sinkParentId)),
                                     e);
        }
    }
}
