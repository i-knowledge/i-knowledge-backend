package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;

public final class FsTransactionValidator {

    /**
     * Validate protobuf transaction
     * @param multiTx
     * @throws StoreException
     */
    public void validate(final Fs.MultiTransaction multiTx) throws StoreException {
        for (Fs.Transaction transaction : multiTx.getTxListList()) {
            switch (transaction.getType()) {
                case CREATE:
                    if (!transaction.hasCreateCtx()) {
                        throw new InvalidTransactionFormatException(
                                "Create transaction miss context object");
                    }
                    break;
                case REMOVE:
                    if (!transaction.hasRemoveCtx()) {
                        throw new InvalidTransactionFormatException(
                                "Remove transaction miss context object");
                    }
                    break;
                case MOVE:
                    if (!transaction.hasMoveCtx()) {
                        throw new InvalidTransactionFormatException(
                                "Move transaction miss context object");
                    }
                    break;
                case RENAME:
                    if (!transaction.hasRenameCtx()) {
                        throw new InvalidTransactionFormatException(
                                "Rename transaction miss context object");
                    }
                    break;
                case ACCESS_RIGHTS:
                    if (!transaction.hasRightsCtx()) {
                        throw new InvalidTransactionFormatException(
                                "Attach rights transaction miss context object");
                    }
                    break;
                default:
                    throw new StoreException("Unknown transaction type " + transaction.getType());
            }
        }
    }
}
