package com.iknowledge.hosted.hbase.repositories.filestorage.inode;

import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.UserRights;

public interface AccessRestrictedINode extends INode {

    /**
     * Personal user and user group rights on INode
     * @return
     */
    UserRights userRights();
}
