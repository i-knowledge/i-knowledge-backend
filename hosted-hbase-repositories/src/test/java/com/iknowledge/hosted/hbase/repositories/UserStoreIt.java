package com.iknowledge.hosted.hbase.repositories;

import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.users.UserInfo;
import com.iknowledge.hosted.hbase.repositories.users.UserStore;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public final class UserStoreIt {
    private static UserStore store;


    /**
     * Initialization method.
     */
    @BeforeClass
    public static void setUp() throws Exception {
        String tableName = UUID.randomUUID().toString();
        final HConfig hconfig = HConfig.newBuilder()
                                       .retryBackoff(5000)
                                       .retryCount(5)
                                       .build();
        final Connection connection = ConnectionFactory.createConnection(hconfig.asConfiguration());
        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(tableName)
                                                   .withHconfig(hconfig)
                                                   .build();
        store = new UserStore(storeConfig);
        store.createTable();
    }

    /**
     * User adding test
     */
    @Test
    public void testUserAdd() throws StoreException {
        final byte[] passHash = UUID.randomUUID().toString().getBytes();
        final byte[] passSalt = UUID.randomUUID().toString().getBytes();
        String userName = "userName";
        String groupName = "group";

        store.put(userName, passHash, passSalt, groupName);
        final UserInfo userInfo = store.get(userName);

        assertThat(userInfo)
                .isNotNull();
        assertThat(userInfo.getUserName())
                .isEqualTo(userName);
        assertThat(userInfo.getPasswordHash())
                .isEqualTo(passHash);
        assertThat(userInfo.getPasswordSalt())
                .isEqualTo(passSalt);
        assertThat(userInfo.getUserGroups())
                .contains(groupName);
    }
}
