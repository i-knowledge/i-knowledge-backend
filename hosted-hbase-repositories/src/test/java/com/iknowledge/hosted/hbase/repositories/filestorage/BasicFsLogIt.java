package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.client.Table;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Test(groups = "fs_log_reader")
public class BasicFsLogIt extends FsLogTestBase {

    @Test
    public void testAppendTx() throws StoreException {
        assertThat(getLastTxId())
                .isEqualTo(0);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);
        generateCreateTxs(1);
        assertThat(getLastTxId())
                .isEqualTo(1);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);
    }

    @Test
    public void testAppendAndReplayTx() throws Exception {
        assertThat(getLastTxId())
                .isEqualTo(0);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);
        final int txCount = MAX_LOG_SIZE - 1;
        generateCreateTxs(txCount);
        assertThat(getLastTxId())
                .isEqualTo(txCount);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);
        txLog.fullReplay();
        assertThat(getLastTxId())
                .isEqualTo(txCount);
        assertThat(getLastAppliedTxId())
                .isEqualTo(txCount);
        assertThat(getSavedLastTxId())
                .isEqualTo(txCount);
        assertThat(getSavedLastAppliedTxId())
                .isEqualTo(txCount);
        assertThat(isHBaseLogEmpty()).isTrue();
    }

    @Test
    public void testAppendTxAndAutoReplayWhenMaxLogSizeReached()
        throws Exception {

        assertThat(getLastTxId())
                .isEqualTo(0);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);
        generateCreateTxs(MAX_LOG_SIZE);
        Thread.sleep(2 * 1000); //wait when compaction finished
        assertThat(getLastTxId())
                .isEqualTo(MAX_LOG_SIZE);
        assertThat(getLastAppliedTxId())
                .isEqualTo(MAX_LOG_SIZE);
        assertThat(getSavedLastTxId())
                .isEqualTo(MAX_LOG_SIZE);
        assertThat(getSavedLastAppliedTxId())
                .isEqualTo(MAX_LOG_SIZE);
        assertThat(isHBaseLogEmpty()).isTrue();
    }

    @Test
    public void testBackgroundReplay() throws Exception {
        FsLogConfig logConf = FsLogConfig.newBuilder()
                                         .awaitTerminationSec(logConfig.awaitTerminationSec())
                                         .compactLogEverySec(1)
                                         .maxLogSize(MAX_LOG_SIZE)
                                         .shutdownListener(logConfig.shutdownListener())
                                         .build();
        try (final Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            final FsLogPointer logPointer = FsLogPointer.readFrom(logTable);
            this.fsLogPointer = logPointer;
            txLog = new FsLog(storeConfig,
                              logConf,
                              logPointer,
                              inodeImageStore,
                              new CollectorRegistry());
        }
        txLog.backgroundReplay();

        assertThat(getLastTxId())
                .isEqualTo(0);
        assertThat(getLastAppliedTxId())
                .isEqualTo(0);

        final int txCount = MAX_LOG_SIZE > 10
                            ? 10
                            : MAX_LOG_SIZE - 1;
        generateCreateTxs(txCount);

        Thread.sleep(2 * 1000); //wait when compaction finished

        assertThat(getLastTxId())
                .isEqualTo(txCount);
        assertThat(getLastAppliedTxId())
                .isEqualTo(txCount);
        assertThat(getSavedLastTxId())
                .isEqualTo(txCount);
        assertThat(getSavedLastAppliedTxId())
                .isEqualTo(txCount);
        assertThat(isHBaseLogEmpty()).isTrue();
    }

    @Test
    public void testReplayWhenIdOverflowsMaxLongValue() throws Exception {
        saveLastAppliedTxId(Long.MAX_VALUE);
        final long lastTxId = (long) MAX_LOG_SIZE - 2;
        saveLastTxId(lastTxId);

        try (final Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            final FsLogPointer logPointer = FsLogPointer.readFrom(logTable);
            this.fsLogPointer = logPointer;
            txLog = new FsLog(storeConfig,
                              logConfig,
                              logPointer,
                              inodeImageStore,
                              new CollectorRegistry());
        }

        final int generatedTxsCount = 1;
        generateCreateTxs(generatedTxsCount);
        txLog.fullReplay();

        final long expectedTxId = lastTxId + generatedTxsCount;
        assertThat(getLastTxId())
                .isEqualTo(expectedTxId);
        assertThat(getLastAppliedTxId())
                .isEqualTo(expectedTxId);
        assertThat(getSavedLastTxId())
                .isEqualTo(expectedTxId);
        assertThat(getSavedLastAppliedTxId())
                .isEqualTo(expectedTxId);
        assertThat(isHBaseLogEmpty()).isTrue();
    }

    @Test
    public void testAllTxTypeAppendAndReplay() throws Exception {
        final int txCount = 5;
        generateCreateTxs(txCount);
        generateRemoveTxs(txCount);
        generateMoveTxs(txCount);
        generateRenameTxs(txCount);
        generateAccessRightTxs(txCount);

        txLog.fullReplay();
        final long expectedLastAppliedId = txCount * 5;
        assertThat(getLastTxId())
                .isEqualTo(expectedLastAppliedId);
        assertThat(getLastAppliedTxId())
                .isEqualTo(expectedLastAppliedId);
        assertThat(getSavedLastTxId())
                .isEqualTo(expectedLastAppliedId);
        assertThat(getSavedLastAppliedTxId())
                .isEqualTo(expectedLastAppliedId);
        assertThat(isHBaseLogEmpty()).isTrue();
    }
}
