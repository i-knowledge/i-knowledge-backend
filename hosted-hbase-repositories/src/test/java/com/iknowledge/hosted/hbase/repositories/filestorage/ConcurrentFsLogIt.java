package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.client.Table;
import org.awaitility.Awaitility;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@Test(dependsOnGroups = "fs_log_reader")
public class ConcurrentFsLogIt extends FsLogTestBase {

    private ExecutorService threadPool;
    private int poolSize;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        super.setup();
        poolSize = Runtime.getRuntime().availableProcessors() * 4;
        threadPool = Executors.newFixedThreadPool(poolSize);
    }

    @Test
    public void appendTxs() throws Exception {
        assertThat(getLastTxId())
            .isEqualTo(0);
        assertThat(getLastAppliedTxId())
            .isEqualTo(0);

        List<CompletableFuture> futures = new ArrayList<>();
        long totalTxCount = generateTxs(futures);
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                         .get();

        awaitGeneration(futures);
        awaitCompaction(totalTxCount);
        assertThat(isHBaseLogEmpty()).isTrue();
        assertThat(getLastTxId())
            .isEqualTo(totalTxCount);
        assertThat(getSavedLastTxId())
            .isEqualTo(totalTxCount);
        assertThat(getSavedLastAppliedTxId())
            .isEqualTo(totalTxCount);
    }

    @Test
    public void appendTxsWithIdOverflow() throws Exception {
        final int beforeOverflow = 100;
        final long lastTxId = Long.MAX_VALUE - beforeOverflow;
        saveLastAppliedTxId(lastTxId);
        saveLastTxId(lastTxId);

        try (final Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            final FsLogPointer logPointer = FsLogPointer.readFrom(logTable);
            this.fsLogPointer = logPointer;
            txLog = new FsLog(storeConfig,
                              logConfig,
                              logPointer,
                              inodeImageStore,
                              new CollectorRegistry());
        }

        assertThat(getLastTxId())
            .isEqualTo(lastTxId);
        assertThat(getLastAppliedTxId())
            .isEqualTo(lastTxId);

        List<CompletableFuture> futures = new ArrayList<>();
        long totalTxCount = generateTxs(futures);
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                         .get();

        awaitGeneration(futures);
        final long expectedLastAppliedId = totalTxCount - beforeOverflow;
        awaitCompaction(expectedLastAppliedId);
        assertThat(isHBaseLogEmpty()).isTrue();
        assertThat(getLastTxId())
            .isEqualTo(expectedLastAppliedId);
        assertThat(getSavedLastTxId())
            .isEqualTo(expectedLastAppliedId);
        assertThat(getSavedLastAppliedTxId())
            .isEqualTo(expectedLastAppliedId);
    }

    private void awaitGeneration(final List<CompletableFuture> futures) {
        assertThat(futures)
            .allMatch(f -> f.isDone()
                           && !f.isCompletedExceptionally()
                           && !f.isCancelled());
    }

    private void awaitCompaction(final long expectedLastAppliedId) {
        Awaitility.waitAtMost(3, TimeUnit.MINUTES)
                  .until(() -> {
                      try {
                          txLog.fullReplay();
                      }
                      catch (StoreException e) {
                          throw new RuntimeException(e);
                      }
                      return getLastAppliedTxId() == expectedLastAppliedId;
                  });
    }

    private long generateTxs(final List<CompletableFuture> futures) {
        final int createCount = 200;
        final int removeCount = 270;
        final int renameCount = 250;
        final int moveCount = 230;
        final int rightsCount = 400;
        long totalTxCount = 0;
        final int tasks = poolSize / 4;
        for (int i = 0 ; i < tasks ; i++) {
            futures.add(submit(() -> generateCreateTxs(createCount)));
            totalTxCount += createCount;
            futures.add(submit(() -> generateRemoveTxs(removeCount)));
            totalTxCount += removeCount;
            futures.add(submit(() -> generateRenameTxs(renameCount)));
            totalTxCount += renameCount;
            futures.add(submit(() -> generateMoveTxs(moveCount)));
            totalTxCount += moveCount;
            futures.add(submit(() -> generateAccessRightTxs(rightsCount)));
            totalTxCount += rightsCount;
        }
        return totalTxCount;
    }

    private CompletableFuture<Void> submit(CheckedRunnable runnable) {
        return CompletableFuture.runAsync(() -> {
            try {
                runnable.run();
            }
            catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }, threadPool);
    }

    interface CheckedRunnable {
        void run() throws Exception;
    }
}
