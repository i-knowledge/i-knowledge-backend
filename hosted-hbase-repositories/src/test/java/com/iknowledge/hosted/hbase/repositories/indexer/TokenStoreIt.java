package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;

@Test
public final class TokenStoreIt {

    private TableName tokenDb;
    private TokenStore tokenStore;
    private Connection connection;

    @DataProvider(name = "noOverlapTokens")
    public Object[][] noOverlapTokens() {
        return new Object[][] {
            { "This",
              "isn't",
              "really",
              "possible",
              "without",
              "tricks",
              "Here",
              "is",
              "a",
              "way",
              "that",
              "works",
              "by",
              "replacing",
              "the",
              "text",
              "with",
              "an",
              "image",
              "of",
              "type",
              "thing",
              "typically",
              "done",
              "Javascript",
              "how",
              "it",
              "can",
              "be",
              "jQuery",
              "Obligatory",
              "hack",
              "CSS",
              "right",
              "place",
              "to",
              "do",
              "but",
              "if",
              "in",
              "some",
              "situation",
              "you",
              "have",
              "third",
              "party",
              "library",
              "iframe",
              "only",
              "customized" }
        };
    }

    @DataProvider(name = "overlapTokens")
    public Object[][] overlapTokens() {
        return new Object[][] {
            { new String[] { "This",
                             "isn't",
                             "without",
                             "really",
                             "possible",
                             "without",
                             "tricks",
                             "Here",
                             "is",
                             "a",
                             "way",
                             "that",
                             "works",
                             "by",
                             "replacing",
                             "the",
                             "is",
                             "type",
                             "text",
                             "with",
                             "an",
                             "image",
                             "of",
                             "type",
                             "thing",
                             "typically",
                             "done",
                             "Javascript",
                             "how",
                             "it",
                             "can",
                             "be",
                             "jQuery",
                             "Obligatory",
                             "hack",
                             "CSS",
                             "right",
                             "place",
                             "to",
                             "do",
                             "but",
                             "if",
                             "in",
                             "do",
                             "some",
                             "situation",
                             "you",
                             "have",
                             "third",
                             "party",
                             "have",
                             "library",
                             "iframe",
                             "only",
                             "customized" },
              50 }
        };
    }

    @BeforeClass
    public void setup() throws StoreException, IOException {
        tokenDb = TableName.valueOf("token_db_" + ThreadLocalRandom.current().nextInt(10000));

        final HConfig config = HConfig.newBuilder()
                                      .retryCount(5)
                                      .retryBackoff(3000)
                                      .readRpcTimeout(25000)
                                      .writeRpcTimeout(25000)
                                      .operationTimeout(25000)
                                      .build();
        connection = ConnectionFactory.createConnection(config.asConfiguration());

        tokenStore = new TokenStore(StoreConfig.builder()
                                               .withConnection(connection)
                                               .withTableName(tokenDb.getNameAsString())
                                               .withHconfig(config)
                                               .build(),
                                    true,
                                    true);
    }

    @AfterClass
    public void after() throws Exception {
        tokenStore.close();
        connection.getAdmin().disableTable(tokenDb);
        connection.getAdmin().deleteTable(tokenDb);
        connection.close();
    }

    @AfterMethod()
    public void cleanupDb() throws IOException {
        connection.getAdmin().disableTable(tokenDb);
        connection.getAdmin().truncateTable(tokenDb, false);
    }

    @Test(dataProvider = "noOverlapTokens", invocationCount = 5)
    public void testTokenIdAllocation(String[] tokens) throws StoreException {
        long expectedId = tokenStore.putToken(tokens[0]);
        for (String token : tokens) {
            assertThat(tokenStore.putToken(token))
                .isEqualTo(expectedId);
            expectedId++;
        }
    }

    @Test(dataProvider = "overlapTokens", invocationCount = 5)
    public void testTokenIdAllocationWithSameTokens(String[] tokens, int uniqueTokensCount)
        throws StoreException {

        Set<Long> uniqueCount = new HashSet<>();
        for (String token : tokens) {
            uniqueCount.add(tokenStore.putToken(token));
        }
        assertThat(uniqueCount.size())
            .isEqualTo(uniqueTokensCount);
    }

    @Test(dataProvider = "noOverlapTokens", threadPoolSize = 5)
    public void testConcurrentTokenIdAllocation(String[] tokens)
        throws Exception {

        final int tasks = Runtime.getRuntime().availableProcessors();
        final ExecutorService pool = Executors.newFixedThreadPool(tasks);
        Set<Long> uniqueCount = new ConcurrentSkipListSet<>();
        List<CompletableFuture> futures = new ArrayList<>(tasks);
        futures.add(CompletableFuture.runAsync(() -> {
            for (String token : tokens) {
                try {
                    uniqueCount.add(tokenStore.putToken(token));
                }
                catch (StoreException e) {
                    throw new RuntimeException(e);
                }
            }
        }, pool));

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).get();

        assertThat(uniqueCount.size())
            .isEqualTo(tokens.length);

        assertThat(uniqueCount)
            .containsSequence(LongStream.range(0, tokens.length)
                                        .collect(() -> new ArrayList<Long>(),
                                                 ArrayList::add,
                                                 ArrayList::addAll));
    }
}
