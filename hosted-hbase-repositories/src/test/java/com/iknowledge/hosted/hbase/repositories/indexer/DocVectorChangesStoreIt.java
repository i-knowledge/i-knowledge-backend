package com.iknowledge.hosted.hbase.repositories.indexer;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.Assertions.assertThat;

@Test
public final class DocVectorChangesStoreIt {

    private TableName changesDb;
    private DocVectorChangesStore changesStore;
    private Connection connection;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        changesDb = TableName.valueOf("changes_db_" + ThreadLocalRandom.current().nextInt(10000));

        final HConfig config = HConfig.newBuilder()
                                      .retryCount(5)
                                      .retryBackoff(3000)
                                      .readRpcTimeout(25000)
                                      .writeRpcTimeout(25000)
                                      .operationTimeout(25000)
                                      .build();
        connection = ConnectionFactory.createConnection(config.asConfiguration());

        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(changesDb.getNameAsString())
                                                   .withHconfig(config)
                                                   .build();
        changesStore = new DocVectorChangesStore(storeConfig);
        changesStore.createTable();
    }

    @AfterClass
    public void after() throws Exception {
        changesStore.close();
        connection.getAdmin().disableTable(changesDb);
        connection.getAdmin().deleteTable(changesDb);
        connection.close();
    }

    @AfterMethod()
    public void cleanupDb() throws IOException {
        connection.getAdmin().disableTable(changesDb);
        connection.getAdmin().truncateTable(changesDb, false);
    }

    @Test
    public void testChangesAddAndList() throws StoreException {
        final int indexChanges = 300;
        final int removeChanges = 300;
        generateChanges(indexChanges, removeChanges);
        final int maxChanges = 450;
        final List<Change> changes = changesStore.loadChanges(maxChanges);

        assertThat(changes)
            .hasSize(maxChanges);

        int indexingCount = 0;
        int removeCount = 0;
        for (Change change : changes) {
            switch (change.type) {
                case INDEX_DOC:
                    indexingCount++;
                    break;
                case REMOVE_DOC:
                    removeCount++;
                    break;
                default:
                    break;
            }
        }

        assertThat(indexingCount)
            .isEqualTo(indexChanges);
        assertThat(removeCount)
            .isEqualTo(maxChanges - indexChanges);
    }

    @Test
    public void testChangesRemoval() throws StoreException {
        final int indexChanges = 300;
        final int removeChanges = 300;
        generateChanges(indexChanges, removeChanges);
        final int maxChanges = indexChanges + removeChanges;
        final List<Change> changes = changesStore.loadChanges(maxChanges);

        for (Change change : changes) {
            changesStore.removeChange(change);
        }

        assertThat(changesStore.loadChanges(maxChanges))
            .hasSize(0);
    }

    @Test
    public void testDuplicateChanges() throws StoreException {
        final int totalChanges = 100;
        for (int i = 0 ; i < totalChanges ; i++) {
            changesStore.addChange(1, Id.from(i), ChangeType.INDEX_DOC);
        }
        for (int i = totalChanges / 2 ; i < totalChanges ; i++) {
            changesStore.addChange(1, Id.from(i), ChangeType.REMOVE_DOC);
        }

        final List<Change> changes = changesStore.loadChanges(totalChanges * 2);
        assertThat(changes)
            .hasSize(totalChanges);

        int indexingCount = 0;
        int removeCount = 0;
        for (Change change : changes) {
            switch (change.type) {
                case INDEX_DOC:
                    indexingCount++;
                    break;
                case REMOVE_DOC:
                    removeCount++;
                    break;
                default:
                    break;
            }
        }

        assertThat(removeCount)
            .isEqualTo(totalChanges / 2);
        assertThat(indexingCount)
            .isEqualTo(totalChanges / 2);
    }

    private void generateChanges(int indexChanges, int removeChanges) throws StoreException {
        for (int i = 0 ; i < indexChanges ; i++) {
            changesStore.addChange(1, Id.from(i), ChangeType.INDEX_DOC);
        }
        for (int i = indexChanges ; i < indexChanges + removeChanges ; i++) {
            changesStore.addChange(1, Id.from(i), ChangeType.REMOVE_DOC);
        }
    }
}
