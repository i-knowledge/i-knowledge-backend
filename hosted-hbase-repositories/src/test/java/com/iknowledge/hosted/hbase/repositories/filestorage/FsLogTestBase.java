package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.google.protobuf.ByteString;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hosted.hbase.repositories.HConsts;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.AccessRestrictedINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.DirectoryINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.FileINode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.INode;
import com.iknowledge.hosted.hbase.repositories.filestorage.inode.rights.HBaseINodeRights;
import com.iknowledge.hosted.hbase.repositories.filestorage.protobuf.Fs;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.INODE_CF_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_APPLIED_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.LAST_TX_ID_BYTES;
import static com.iknowledge.hosted.hbase.repositories.HConsts.FsImage.TX_LOG_ROW_BYTES;
import static java.util.stream.Collectors.toList;

public abstract class FsLogTestBase {

    /**
     * Max FS log size(after force compaction will be executed)
     */
    public static final int MAX_LOG_SIZE = 150;

    protected FsLog txLog;
    protected FsLogConfig logConfig;
    protected INodeImageStore inodeImageStore;
    protected FsLogPointer fsLogPointer;
    protected StoreConfig storeConfig;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        TableName tableName = TableName.valueOf("fsLogTestTable_"
                                                + UUID.randomUUID().toString());
        HConfig hconfig = HConfig.newBuilder()
                         .retryCount(10)
                         .retryBackoff(5000)
                         .connectionThreads(Runtime.getRuntime().availableProcessors())
                         .metaLookupThreads(Runtime.getRuntime().availableProcessors() / 2)
                         .build();

        final int threads = Runtime.getRuntime().availableProcessors() * 2;
        Connection connection
            = ConnectionFactory.createConnection(hconfig.asConfiguration(),
                                                 Executors.newFixedThreadPool(threads));

        storeConfig = StoreConfig.builder()
                                 .withConnection(connection)
                                 .withTableName(tableName.toString())
                                 .withHconfig(hconfig)
                                 .build();

        logConfig = FsLogConfig.newBuilder()
                               .awaitTerminationSec(5)
                               .compactLogEverySec(Integer.MAX_VALUE)
                               .maxLogSize(MAX_LOG_SIZE)
                               .shutdownListener(th -> {
                                   throw new RuntimeException(th);
                               })
                               .build();

        inodeImageStore = new INodeImageStore(hconfig);
        new HBaseFsImageStore(storeConfig, logConfig, new CollectorRegistry())
                .createTable();
    }

    @AfterClass
    public void tearDown() throws Exception {
        final Admin admin = storeConfig.connection().getAdmin();
        admin.disableTable(storeConfig.tableName());
        admin.deleteTable(storeConfig.tableName());
        storeConfig.connection().close();
    }

    @BeforeMethod
    public void setupTest() throws StoreException, IOException {
        fsLogPointer = new FsLogPointer();
        txLog = new FsLog(storeConfig,
                          logConfig,
                          fsLogPointer,
                          inodeImageStore,
                          new CollectorRegistry());
    }

    @AfterMethod
    public void tearDownTestMethod() throws Exception {
        txLog.close();
        try (final Table table = storeConfig.connection().getTable(storeConfig.tableName())) {
            table.delete(new Delete(HConsts.FsImage.TX_LOG_ROW_BYTES));
        }
    }

    protected long getLastTxId() {
        return fsLogPointer.lastTxId();
    }

    protected long getLastAppliedTxId() {
        return fsLogPointer.lastAppliedTxId();
    }

    protected void generateCreateTxs(final int txCount) throws StoreException {
        for (int i = 0 ; i < txCount; i++) {
            final DirectoryINode rootInode = new DirectoryINode(generateInodeName(), null);
            final FileINode fileInode = new FileINode(generateInodeName(), rootInode);
            txLog.appendTx(Fs.MultiTransaction.newBuilder()
                                              .addTxList(newCreateTx(fileInode))
                                              .build());
        }
    }

    protected void generateRemoveTxs(final int txCount) throws StoreException {
        for (int i = 0 ; i < txCount; i++) {
            final DirectoryINode rootInode = new DirectoryINode(generateInodeName(), null);
            final FileINode fileInode = new FileINode(generateInodeName(), rootInode);
            txLog.appendTx(Fs.MultiTransaction.newBuilder()
                                              .addTxList(newRemoveTx(fileInode))
                                              .build());
        }
    }

    protected void generateMoveTxs(final int txCount) throws StoreException {
        for (int i = 0 ; i < txCount; i++) {
            final DirectoryINode rootInode = new DirectoryINode(generateInodeName(), null);
            final FileINode fileInode = new FileINode(generateInodeName(), rootInode);
            txLog.appendTx(Fs.MultiTransaction.newBuilder()
                                              .addTxList(newMoveTx(fileInode))
                                              .build());
        }
    }

    protected void generateRenameTxs(final int txCount) throws StoreException {
        for (int i = 0 ; i < txCount; i++) {
            final DirectoryINode rootInode = new DirectoryINode(generateInodeName(), null);
            final FileINode fileInode = new FileINode(generateInodeName(), rootInode);
            txLog.appendTx(Fs.MultiTransaction.newBuilder()
                                              .addTxList(newRenameTx(fileInode))
                                              .build());
        }
    }

    protected void generateAccessRightTxs(final int txCount) throws StoreException {
        for (int i = 0 ; i < txCount; i++) {
            final DirectoryINode rootInode = new DirectoryINode(generateInodeName(), null);
            final FileINode fileInode = new FileINode(generateInodeName(), rootInode);
            txLog.appendTx(Fs.MultiTransaction.newBuilder()
                                              .addTxList(newAttachRightsTx(fileInode))
                                              .build());
        }
    }

    protected void saveLastTxId(long id) throws IOException {
        try (Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            logTable.put(new Put(TX_LOG_ROW_BYTES)
                                 .addColumn(INODE_CF_BYTES,
                                               LAST_TX_ID_BYTES,
                                               Bytes.toBytes(id)));
        }
    }

    protected long getSavedLastTxId() throws IOException, StoreException {
        try (Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            return FsLogPointer.readFrom(logTable).lastTxId();
        }
    }

    protected void saveLastAppliedTxId(long id) throws IOException {
        try (Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            logTable.put(new Put(TX_LOG_ROW_BYTES)
                                 .addColumn(INODE_CF_BYTES,
                                               LAST_APPLIED_TX_ID_BYTES,
                                               Bytes.toBytes(id)));
        }
    }

    protected long getSavedLastAppliedTxId() throws IOException, StoreException {
        try (Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            return FsLogPointer.readFrom(logTable).lastAppliedTxId();
        }
    }

    protected Fs.Transaction newCreateTx(INode inode) {
        final ByteString parentId = ByteString.copyFrom(inode.parent().id().bytes());
        final Fs.Transaction.CreateCtx.Builder ctx
                = Fs.Transaction.CreateCtx.newBuilder()
                                          .setId(generateId())
                                          .setNewInode(inode.proto())
                                          .setParentId(parentId);
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.CREATE)
                             .setCreateCtx(ctx)
                             .build();
    }

    protected Fs.Transaction newRemoveTx(INode inode) {
        final ByteString parentId = ByteString.copyFrom(inode.parent().id().bytes());
        final Fs.Transaction.RemoveCtx.Builder ctx
                = Fs.Transaction.RemoveCtx.newBuilder()
                                          .setId(generateId())
                                          .setParentId(parentId);
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.REMOVE)
                             .setRemoveCtx(ctx)
                             .build();
    }

    protected Fs.Transaction newMoveTx(INode inode) {
        final ByteString parentId = ByteString.copyFrom(inode.parent().id().bytes());
        final Fs.Transaction.MoveCtx.Builder ctx
                = Fs.Transaction.MoveCtx.newBuilder()
                                          .setSrcId(generateId())
                                          .setSinkParentId(parentId)
                                          .setSrcParentId(parentId);
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.MOVE)
                             .setMoveCtx(ctx)
                             .build();
    }

    protected Fs.Transaction newRenameTx(INode inode) {
        final ByteString parentId = ByteString.copyFrom(inode.parent().id().bytes());
        final Fs.Transaction.RenameCtx.Builder ctx
                = Fs.Transaction.RenameCtx.newBuilder()
                                        .setNewName(inode.name()
                                                    + UUID.randomUUID().toString())
                                        .setSrcId(generateId())
                                        .setSinkParentId(parentId)
                                        .setSrcParentId(parentId);
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.RENAME)
                             .setRenameCtx(ctx)
                             .build();
    }

    protected Fs.Transaction newAttachRightsTx(AccessRestrictedINode inode) {
        final Fs.Transaction.RightsCtx.Builder ctx
                = Fs.Transaction.RightsCtx.newBuilder()
                                          .setId(generateId())
                                          .addAllRights(inode.userRights()
                                                            .userRights()
                                                             .values()
                                                             .stream()
                                                             .map(HBaseINodeRights::proto)
                                                             .collect(toList()));
        return Fs.Transaction.newBuilder()
                             .setType(Fs.Transaction.Type.ACCESS_RIGHTS)
                             .setRightsCtx(ctx)
                             .build();
    }

    protected String generateInodeName() {
        final byte[] bytes = new byte[8];
        ThreadLocalRandom.current().nextBytes(bytes);
        return Arrays.toString(bytes);
    }

    private ByteString generateId() {
        return ByteString.copyFrom(Id.len16().bytes());
    }

    protected boolean isHBaseLogEmpty() throws Exception {
        try (Table logTable = storeConfig.connection().getTable(storeConfig.tableName())) {
            return new FsLogReader(logTable, fsLogPointer, Long.MAX_VALUE)
                        .next() == null;
        }
    }
}
