package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import io.prometheus.client.CollectorRegistry;
import org.apache.hadoop.hbase.client.Table;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@Test(groups = "fs_log_reader")
public class FsLogReaderIt extends FsLogTestBase {

    private Table logTable;

    @BeforeMethod
    public void setupTest() throws IOException, StoreException {
        super.setupTest();
        logTable = storeConfig.connection().getTable(storeConfig.tableName());
    }

    @AfterMethod
    public void tearDownTestMethod() throws Exception {
        super.tearDownTestMethod();
        logTable.close();
    }

    @Test
    public void testReadEmptyLog() throws StoreException {
        int maxToRead = 10;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);
        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(0);
    }

    @Test
    public void testBasicReadFromBeginning() throws StoreException {
        generateCreateTxs(5);
        generateRemoveTxs(5);

        int maxToRead = 10;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);
        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(maxToRead);
    }

    @Test
    public void testReadFromBeginningWithGaps() throws StoreException {
        generateCreateTxs(5);
        fsLogPointer.nextTxId();
        generateRemoveTxs(5);
        fsLogPointer.nextTxId();
        generateRenameTxs(5);
        fsLogPointer.nextTxId();
        fsLogPointer.nextTxId();
        generateMoveTxs(5);

        int maxToRead = 20;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);
        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(maxToRead);
    }

    @Test
    public void testReadMoreRecordsThanLogContains() throws StoreException {
        final int txCount = 20;
        generateCreateTxs(txCount);

        int maxToRead = txCount * 2;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);
        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(txCount);
    }

    @Test
    public void testReadLessRecordsThanLogContains() throws StoreException {
        final int txCount = 20;
        generateCreateTxs(txCount);

        int maxToRead = txCount / 2;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);
        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(maxToRead);
    }

    @Test
    public void testReadWhenTxIdOverflowLongValue() throws StoreException, IOException {
        final int txCount = MAX_LOG_SIZE / 2;

        saveLastAppliedTxId(Long.MAX_VALUE - txCount / 2);
        saveLastTxId((int) Math.ceil(txCount / 2d));
        final FsLogPointer fsLogPointer = FsLogPointer.readFrom(logTable);
        txLog = new FsLog(storeConfig,
                          logConfig,
                          fsLogPointer,
                          inodeImageStore,
                          new CollectorRegistry());

        generateCreateTxs(txCount);
        int maxToRead = txCount * 2;
        FsLogReader logReader = new FsLogReader(logTable, fsLogPointer, maxToRead);

        int read = readFromLog(logReader);

        assertThat(read)
            .isEqualTo(txCount);
    }

    private int readFromLog(final FsLogReader logReader)
        throws StoreException {

        TxRecord record = logReader.next();
        int read = 0;
        while (record != null) {
            read++;
            record = logReader.next();
        }
        return read;
    }

}
