package com.iknowledge.hosted.hbase.repositories.filestorage;

import com.iknowledge.hbase.repositories.StoreException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class FsLogPointerTest {

    private FsLogPointer logPointer;

    @BeforeMethod
    public void setupTestMethod() throws StoreException {
        logPointer = new FsLogPointer();
    }

    @Test
    public void testEmptyLogSize() {
        assertThat(logPointer.logSize())
                .isEqualTo(0);
    }

    @Test
    public void tesMaxLogSize() throws StoreException {
        logPointer = new FsLogPointer(Long.MAX_VALUE, FsLogPointer.NO_TX);
        assertThat(logPointer.logSize())
                .isEqualTo(Long.MAX_VALUE);
    }

    @Test
    public void testLogSizeWithFewTransactions() throws StoreException {
        final int txCount = 3;
        long lastTxId = 0;
        for (int i = 0 ; i < txCount ; i++) {
            lastTxId = logPointer.nextTxId();
        }
        assertThat(logPointer.logSize())
                .isEqualTo(txCount);

        logPointer.setLastAppliedTxId(1);
        assertThat(logPointer.logSize())
                .isEqualTo(txCount - 1);

        logPointer.setLastAppliedTxId(lastTxId - 1);
        assertThat(logPointer.logSize())
                .isEqualTo(1);

        logPointer.setLastAppliedTxId(lastTxId);
        assertThat(logPointer.logSize())
                .isEqualTo(0);
    }

    @Test
    public void testLogSizeWithIdOverflow() throws StoreException {
        final long maxTxCount = Long.MAX_VALUE;
        final long lastAppliedTxId = maxTxCount - 10;

        logPointer = new FsLogPointer(maxTxCount, maxTxCount);
        assertThat(logPointer.logSize())
                .isEqualTo(0);

        logPointer = new FsLogPointer(maxTxCount, FsLogPointer.NO_TX);
        assertThat(logPointer.logSize())
                .isEqualTo(maxTxCount);
        logPointer.setLastAppliedTxId(lastAppliedTxId);
        assertThat(logPointer.logSize())
                .isEqualTo(maxTxCount - lastAppliedTxId);

        // overflow tx ID
        assertThat(logPointer.nextTxId())
                .isEqualTo(1);
        assertThat(logPointer.lastTxId())
                .isEqualTo(1);
        assertThat(logPointer.lastAppliedTxId())
                .isEqualTo(lastAppliedTxId);
        assertThat(logPointer.logSize())
                .isEqualTo(maxTxCount - lastAppliedTxId + 1);
    }

    @Test(expectedExceptions = StoreException.class,
            description = "Must fail on second overflow of long value because "
                          + "previous transactions not applied yet")
    public void testLogSizeWithOverflowSecondTime() throws StoreException {
        try {
            logPointer.nextTxId();
        }
        catch (StoreException e) {
            throw new RuntimeException("Unexpected ID overflow on first ID allocation");
        }
        logPointer.setLastAppliedTxId(2);
        logPointer.nextTxId();
    }

    @Test(enabled = false)
    public void testCouncurrentIdAllocation() throws InterruptedException {
        final Set<Long> map = new ConcurrentSkipListSet<>();
        final ExecutorService threadPool
                = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);
        final int idCount = 10000000;
        for (int i = 0 ; i < idCount ; i++) {
            threadPool.submit(() -> {
                try {
                    final long txId = logPointer.nextTxId();
                    map.add(txId);
                }
                catch (StoreException e) {
                    throw new RuntimeException(e);
                }
            });
        }
        threadPool.shutdown();
        threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

        assertThat(map)
                .hasSize(idCount);

        final List<Long> txIdList = new ArrayList<>(map);
        Collections.sort(txIdList);
        long prevId = 0;
        for (long id : txIdList) {
            assertThat(id).isEqualTo(prevId + 1);
            prevId = id;
        }
    }
}
