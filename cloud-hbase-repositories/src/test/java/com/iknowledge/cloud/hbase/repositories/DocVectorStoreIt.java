package com.iknowledge.cloud.hbase.repositories;

import com.google.common.collect.Lists;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;
import static org.assertj.core.api.Assertions.assertThat;

@Test
public final class DocVectorStoreIt {

    private TableName vectorDb;
    private DocVectorStore vectorStore;
    private Connection connection;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        vectorDb = TableName.valueOf("vector_db_" + ThreadLocalRandom.current().nextInt(10000));

        final HConfig config = HConfig.newBuilder()
                                      .retryCount(5)
                                      .retryBackoff(3000)
                                      .readRpcTimeout(25000)
                                      .writeRpcTimeout(25000)
                                      .operationTimeout(25000)
                                      .build();
        connection = ConnectionFactory.createConnection(config.asConfiguration());

        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(vectorDb.getNameAsString())
                                                   .withHconfig(config)
                                                   .build();
        vectorStore = new DocVectorStore(storeConfig);
        vectorStore.createTable();
    }

    @AfterClass
    public void after() throws Exception {
        vectorStore.close();
        connection.getAdmin().disableTable(vectorDb);
        connection.getAdmin().deleteTable(vectorDb);
        connection.close();
    }

    @AfterMethod()
    public void cleanupDb() throws IOException {
        connection.getAdmin().disableTable(vectorDb);
        connection.getAdmin().truncateTable(vectorDb, false);
    }

    @Test
    public void testDocVectorValidAccess() throws Exception {
        final Id docId = Id.len8();
        final List<TokenProto.Token> tokenVector
            = Lists.newArrayList(
                        TokenProto.Token.newBuilder().setId(1).setPosition(0).build(),
                        TokenProto.Token.newBuilder().setId(2).setPosition(1).build(),
                        TokenProto.Token.newBuilder().setId(3).setPosition(2).build(),
                        TokenProto.Token.newBuilder().setId(4).setPosition(3).build(),
                        TokenProto.Token.newBuilder().setId(5).setPosition(4).build(),
                        TokenProto.Token.newBuilder().setId(6).setPosition(5).build());
        writeAndCommitBlocks(docId, tokenVector);
        hasAllWrittenBlocks(docId, tokenVector);
    }

    @Test
    public void testDocVectorValidAccessForDocVectorVersionOverflow() throws Exception {
        final Id docId = Id.len8();
        final List<TokenProto.Token> tokenVector
            = Lists.newArrayList(
                        TokenProto.Token.newBuilder().setId(1).setPosition(0).build(),
                        TokenProto.Token.newBuilder().setId(2).setPosition(1).build(),
                        TokenProto.Token.newBuilder().setId(3).setPosition(2).build(),
                        TokenProto.Token.newBuilder().setId(4).setPosition(3).build(),
                        TokenProto.Token.newBuilder().setId(5).setPosition(4).build(),
                        TokenProto.Token.newBuilder().setId(6).setPosition(5).build());
        writeAndCommitBlocks(docId, Integer.MAX_VALUE, tokenVector);
        hasAllWrittenBlocks(docId, tokenVector);

        writeAndCommitBlocks(docId, tokenVector);
        hasAllWrittenBlocks(docId, tokenVector);

        final Result result = getHBaseDocVector(docId);
        assertThat(result.rawCells())
            .withFailMessage("Previous version not removed from HBase. "
                             + "Must be " + tokenVector.size() + " block and 1 version cell.")
            .hasSize(tokenVector.size() + 1);
    }

    @Test
    public void testUncommitedDocVectorAccess() throws Exception {
        final Id docId = Id.len8();
        final DocVectorWriter writer = vectorStore.writer(docId);
        final List<TokenProto.Token> firstBlock
            = Lists.newArrayList(TokenProto.Token.newBuilder().setId(1).setPosition(0).build(),
                                 TokenProto.Token.newBuilder().setId(2).setPosition(1).build(),
                                 TokenProto.Token.newBuilder().setId(3).setPosition(2).build());
        writer.appendTokens(firstBlock);
        final List<TokenProto.Token> secondBlock
            = Lists.newArrayList(TokenProto.Token.newBuilder().setId(4).setPosition(3).build(),
                                 TokenProto.Token.newBuilder().setId(5).setPosition(4).build(),
                                 TokenProto.Token.newBuilder().setId(6).setPosition(5).build());
        writer.appendTokens(secondBlock);
        writer.close();

        hasNoBlocks(docId);

        final Result result = getHBaseDocVector(docId);
        assertThat(result.rawCells())
            .withFailMessage("Uncommited vector not removed from HBase"
                             + "(found not only version cell)")
            .hasSize(1);
    }

    @Test
    public void testNonExistentDocVectorAccess() throws Exception {
        final Id docId = Id.len8();
        hasNoBlocks(docId);
    }

    @Test
    public void testDocVectorOverride() throws Exception {
        final Id docId = Id.len8();
        final List<TokenProto.Token> firstBlock
            = Lists.newArrayList(TokenProto.Token.newBuilder().setId(1).setPosition(0).build(),
                                 TokenProto.Token.newBuilder().setId(2).setPosition(1).build(),
                                 TokenProto.Token.newBuilder().setId(3).setPosition(2).build());
        writeAndCommitBlocks(docId, firstBlock);
        hasAllWrittenBlocks(docId, firstBlock);

        final List<TokenProto.Token> secondBlock
            = Lists.newArrayList(TokenProto.Token.newBuilder().setId(4).setPosition(3).build(),
                                 TokenProto.Token.newBuilder().setId(5).setPosition(4).build(),
                                 TokenProto.Token.newBuilder().setId(6).setPosition(5).build());
        writeAndCommitBlocks(docId, secondBlock);
        hasAllWrittenBlocks(docId, secondBlock);

        final Result result = getHBaseDocVector(docId);
        assertThat(result.rawCells())
            .withFailMessage("Previous version not removed from HBase. "
                             + "Must be " + secondBlock.size() + " block and 1 version cell.")
            .hasSize(secondBlock.size() + 1);
    }

    @Test
    public void testDocVectorRemoval() throws Exception {
        final Id docId = Id.len8();
        final List<TokenProto.Token> tokenVector
            = Lists.newArrayList(TokenProto.Token.newBuilder().setId(1).setPosition(0).build(),
                                 TokenProto.Token.newBuilder().setId(2).setPosition(1).build(),
                                 TokenProto.Token.newBuilder().setId(3).setPosition(2).build(),
                                 TokenProto.Token.newBuilder().setId(4).setPosition(3).build(),
                                 TokenProto.Token.newBuilder().setId(5).setPosition(4).build(),
                                 TokenProto.Token.newBuilder().setId(6).setPosition(5).build());
        writeAndCommitBlocks(docId, tokenVector);

        vectorStore.remove(docId);

        hasNoBlocks(docId);

        final Result result = getHBaseDocVector(docId);
        assertThat(result.rawCells())
            .withFailMessage("Vector not removed from HBase")
            .hasSize(0);
    }

    private void hasNoBlocks(final Id docId) throws Exception {
        final DocVectorReader reader = vectorStore.reader(docId);
        List<TokenProto.Token> vector = reader.readVector();
        assertThat(vector)
            .isNull();
        reader.close();
    }

    private void hasAllWrittenBlocks(final Id docId,
                                     List<TokenProto.Token> writtenVector) throws Exception {

        final DocVectorReader reader = vectorStore.reader(docId);

        List<TokenProto.Token> vector = reader.readVector();
        assertThat(vector)
            .isEqualTo(writtenVector);

        reader.close();
    }

    private void writeAndCommitBlocks(final Id docId,
                                      List<TokenProto.Token> tokens) throws Exception {

        final DocVectorWriter writer = vectorStore.writer(docId);
        writer.appendTokens(tokens);
        writer.commit();
        writer.close();
    }

    private void writeAndCommitBlocks(final Id docId,
                                      int version,
                                      List<TokenProto.Token> tokens) throws Exception {

        final DocVectorWriter writer = vectorStore.writer(docId, version);
        writer.appendTokens(tokens);
        writer.commit();
        writer.close();
    }

    private Result getHBaseDocVector(final Id docId) throws IOException {
        final Get docVectorVersionGet
            = new Get(docId.bytes())
                  .addFamily(VECTOR_CF_BYTES)
                  .readVersions(1);
        return connection.getTable(vectorDb).get(docVectorVersionGet);
    }
}
