package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.iknowledge.hbase.client.HConfig;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.COLUMNS_CF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.CUR_VERSION_ROW_BYTES;
import static org.assertj.core.api.Assertions.assertThat;

@Test
public class LsiStoreIt {

    private TableName lsiTable;
    private LsiStore lsiStore;
    private Connection connection;

    @BeforeClass
    public void setup() throws StoreException, IOException {
        lsiTable = TableName.valueOf("lsa_db_" + ThreadLocalRandom.current().nextInt(10000));

        final HConfig config = HConfig.newBuilder()
                                      .retryCount(5)
                                      .retryBackoff(3000)
                                      .readRpcTimeout(25000)
                                      .writeRpcTimeout(25000)
                                      .operationTimeout(25000)
                                      .build();
        connection = ConnectionFactory.createConnection(config.asConfiguration());

        final StoreConfig storeConfig = StoreConfig.builder()
                                                   .withConnection(connection)
                                                   .withTableName(lsiTable.getNameAsString())
                                                   .withHconfig(config)
                                                   .build();
        lsiStore = new LsiStore(storeConfig);
        lsiStore.createTable();
    }

    @AfterClass
    public void after() throws Exception {
        lsiStore.close();
        connection.getAdmin().disableTable(lsiTable);
        connection.getAdmin().deleteTable(lsiTable);
        connection.close();
    }

    @DataProvider(name = "matrix")
    public Object[][] vectors() {
        int size = 10;
        final Object[][] testMatrix = new Object[size][2];
        for (int i = 0 ; i < size ; i++) {
            testMatrix[i][0] = new TestMatrix();
            testMatrix[i][1] = new TestMatrix();
        }
        return testMatrix;
    }

    @Test(dependsOnMethods = {"putTwoVersionOfMatrixTest"})
    public void checkIsUncommittedInitialVersionUnaccessibleTest()
        throws StoreException, IOException {

        connection.getAdmin().disableTable(lsiTable);
        connection.getAdmin().truncateTable(lsiTable, false);

        TestMatrix testMatrix = new TestMatrix();
        LsiWriter writer = lsiStore.versionWriter();
        writeMatrix(testMatrix, writer);
        for (Map.Entry<Long, float[]> kv : testMatrix.umatrix.entrySet()) {
            assertThat(lsiStore.getURow(kv.getKey()).isPresent())
                .isFalse();
        }
        assertThat(lsiStore.getSigmaMatrix().isPresent())
            .isFalse();

        for (Map.Entry<Id, float[]> kv : testMatrix.vmatrix.entrySet()) {
            assertThat(lsiStore.getVRow(kv.getKey()).isPresent())
                .isFalse();
        }
        writer.commit();
        checkCommittedMatrix(testMatrix);
    }

    @Test(dataProvider = "matrix")
    public void putTwoVersionOfMatrixTest(TestMatrix testMatrix1, TestMatrix testMatrix2)
        throws StoreException, IOException {

        LsiWriter writer = lsiStore.versionWriter();
        writeMatrix(testMatrix1, writer);
//        checkIsUncommittedInitialVersionUnaccessible(testMatrix1);
        writer.commit();
        checkCommittedMatrix(testMatrix1);

        writer = lsiStore.versionWriter();
        writeMatrix(testMatrix2, writer);
        writer.commit();
        checkCommittedMatrix(testMatrix2);
        // recreate writer to remove stale version
        writer = lsiStore.versionWriter();
        isPrevVersionRemoved(testMatrix2);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void putEmptyUVectorTest() throws StoreException {
        lsiStore.versionWriter().putURow(1, new float[0]);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void putEmptyVVectorTest() throws StoreException {
        lsiStore.versionWriter().putVRow(Id.len4(), new float[0]);
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void putEmptysigmaMatrixTest() throws StoreException {
        lsiStore.versionWriter().putSigmaMatrix(new float[0]);
    }

    private void isPrevVersionRemoved(final TestMatrix testMatrix) throws IOException {
        int rows = testMatrix.umatrix.size() + testMatrix.vmatrix.size() + 1;
        int rowsRead = 0;
        try (Table table = connection.getTable(lsiTable)) {
            try (ResultScanner scanner
                     = table.getScanner(new Scan().addFamily(COLUMNS_CF_BYTES)
                                                  .readVersions(1))) {

                for (Result result : scanner) {
                    for (Cell cell : result.rawCells()) {
                        if (Arrays.equals(CellUtil.cloneRow(cell), CUR_VERSION_ROW_BYTES)) {
                            continue;
                        }
                        rowsRead++;
                    }
                }
            }
        }
        assertThat(rowsRead)
            .isEqualTo(rows);
    }

    private void writeMatrix(final TestMatrix testMatrix, final LsiWriter writer)
        throws StoreException {
        for (Map.Entry<Long, float[]> kv : testMatrix.umatrix.entrySet()) {
            writer.putURow(kv.getKey(), kv.getValue());
        }
        writer.putSigmaMatrix(testMatrix.smatrix);

        for (Map.Entry<Id, float[]> kv : testMatrix.vmatrix.entrySet()) {
            writer.putVRow(kv.getKey(), kv.getValue());
        }
    }

    private void checkCommittedMatrix(final TestMatrix testMatrix)
        throws StoreException {

        for (Map.Entry<Long, float[]> kv : testMatrix.umatrix.entrySet()) {
            assertThat(lsiStore.getURow(kv.getKey()).isPresent())
                .isTrue();
        }
        assertThat(lsiStore.getSigmaMatrix().isPresent())
            .isTrue();

        for (Map.Entry<Id, float[]> kv : testMatrix.vmatrix.entrySet()) {
            assertThat(lsiStore.getVRow(kv.getKey()).isPresent())
                .isTrue();
        }
    }
}
