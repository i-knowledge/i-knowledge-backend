package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

class TestMatrix {

    public final Map<Long, float[]> umatrix;
    public final float[] smatrix;
    public final Map<Id, float[]> vmatrix;

    public TestMatrix() {
        final int usize = ThreadLocalRandom.current().nextInt(1, 100);
        final int ssize = ThreadLocalRandom.current().nextInt(1, 20);
        final int vsize = ThreadLocalRandom.current().nextInt(1, 100);

        umatrix = new HashMap<>(usize);
        for (int i = 0 ; i < usize ; i++) {
            float[] vector = new float[usize];
            for (int j = 0 ; j < vector.length ; j++) {
                vector[j] = ThreadLocalRandom.current().nextFloat();
            }
            umatrix.put(ThreadLocalRandom.current().nextLong(50000), vector);
        }
        smatrix = new float[ssize];
        for (int i = 0 ; i < ssize ; i++) {
            smatrix[i] = ThreadLocalRandom.current().nextFloat();
        }
        vmatrix = new HashMap<>(vsize);
        for (int i = 0 ; i < vsize ; i++) {
            float[] vector = new float[vsize];
            for (int j = 0 ; j < vector.length ; j++) {
                vector[j] = ThreadLocalRandom.current().nextFloat();
            }
            vmatrix.put(Id.len4(), vector);
        }
    }
}
