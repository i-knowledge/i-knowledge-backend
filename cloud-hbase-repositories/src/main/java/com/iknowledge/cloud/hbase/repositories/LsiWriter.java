package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.iknowledge.hbase.client.DynamicBatch;

import java.io.IOException;
import java.util.Arrays;

import static com.google.common.base.Preconditions.checkArgument;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.COLUMNS_CF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.CUR_VERSION_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.CUR_VERSION_ROW_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.NO_VERSIONS_OF_VECTOR_FOUND_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.SIGMA_ROW_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.U_ROW_PREFIX_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.V_ROW_PREFIX_BYTES;
import static java.util.Objects.requireNonNull;

/**
 * LSA matrix writer which can be used to store new version of matrix(and cleanup old version data)
 */
public final class LsiWriter {

    private final Table table;
    private final byte[] curVersionQf;
    private final byte[] newVersionQf;
    private boolean committed;

    LsiWriter(Table table, LsiStore.Versions versionPair) throws StoreException {
        this.table = table;
        this.curVersionQf = versionPair.current;
        this.newVersionQf = versionPair.next;
        if (!Arrays.equals(curVersionQf, NO_VERSIONS_OF_VECTOR_FOUND_QF_BYTES)) {
            removeStaleMatrixVersion();
        }
    }

    /**
     * Commit new matrix version.
     */
    public void commit() throws StoreException {
        checkState();
        updateCurrentVersion(newVersionQf);
        try {
            table.close();
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        finally {
            committed = true;
        }
    }

    /**
     * Put U matrix row with token ID as row key and matrix columns as value
     *
     * @param tokenId Token ID
     * @param rowVector Matrix row columns
     *
     * @throws StoreException
     */
    public void putURow(long tokenId, float[] rowVector) throws StoreException {
        checkState();
        checkVector(rowVector);
        final byte[] byteVector = mapToBytes(rowVector);
        final byte[] rowKey = Bytes.add(U_ROW_PREFIX_BYTES, Bytes.toBytes(tokenId));
        putVector(rowKey, byteVector);
    }

    /**
     * Put sigma matrix.
     *
     * @param matrix Sigma matrix as flattened array of floats(can be column or row oriented)
     *
     * @throws StoreException
     */
    public void putSigmaMatrix(float[] matrix) throws StoreException {
        checkState();
        checkVector(matrix);
        final byte[] byteVector = mapToBytes(matrix);
        putVector(SIGMA_ROW_BYTES, byteVector);
    }

    /**
     * Put V matrix row with document ID as row key and matrix columns as value
     *
     * @param docId Document ID
     * @param rowVector Matrix row columns
     *
     * @throws StoreException
     */
    public void putVRow(Id docId, float[] rowVector) throws StoreException {
        checkState();
        checkVector(rowVector);
        final byte[] byteVector = mapToBytes(rowVector);
        final byte[] rowKey = Bytes.add(V_ROW_PREFIX_BYTES, docId.bytes());
        putVector(rowKey, byteVector);
    }

    /**
     * Cleanup stale version of matrix to make free space for new version.
     *
     * @throws StoreException
     */
    private void removeStaleMatrixVersion() throws StoreException {
        byte[] staleVersionQf = newVersionQf;
        final Scan scan = new Scan().addColumn(COLUMNS_CF_BYTES, staleVersionQf)
                                    .setCacheBlocks(false)
                                    .readVersions(1);
        final DynamicBatch<Cell> batch = DynamicBatch.<Cell>newBuilder()
                                                     .withBatchSize(100)
                                                     .withTable(table)
                                                     .withMapper(this::mapAsDelete)
                                                     .build();
        try (ResultScanner scanner = table.getScanner(scan)) {
            for (Result res : scanner) {
                for (Cell matrixDataCell : res.rawCells()) {
                    batch.append(matrixDataCell);
                }
            }
            batch.finish();
        }
        catch (Exception e) {
            throw new StoreException(e);
        }
    }

    private void updateCurrentVersion(byte[] newVersionQf) throws StoreException {
        final Put put = new Put(CUR_VERSION_ROW_BYTES)
                                .addColumn(COLUMNS_CF_BYTES, CUR_VERSION_QF_BYTES, newVersionQf);
        try {
            table.put(put);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private void putVector(byte[] rowKey, byte[] vector) throws StoreException {
        try {
            final Put rowPut = new Put(rowKey).addColumn(COLUMNS_CF_BYTES,
                                                         this.newVersionQf,
                                                         vector);
            table.put(rowPut);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private byte[] mapToBytes(final float[] rowVector) {
        final byte[] byteVector = new byte[rowVector.length * Float.BYTES];
        int idx = 0;
        for (float val : rowVector) {
            idx = Bytes.putFloat(byteVector, idx, val);
        }
        return byteVector;
    }

    private void checkVector(final float[] rowVector) {
        requireNonNull(rowVector);
        checkArgument(rowVector.length > 0, "Empty vector not allowed");
    }

    private Delete mapAsDelete(final Cell cell) {
        return new Delete(CellUtil.cloneRow(cell))
                    .addColumn(CellUtil.cloneFamily(cell), CellUtil.cloneQualifier(cell));
    }

    private void checkState() {
        if (committed) {
            throw new UnsupportedOperationException("Use of writer after commit");
        }
    }
}
