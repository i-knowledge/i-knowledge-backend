package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.OptionalInt;

import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

public abstract class DocVectorAccessor implements AutoCloseable {

    protected static final int UNCOMMITED_VERSIONS_RANGE_END = 0;
    protected static final int INITIAL_VERSION_NUM = 1;

    protected final Table indexTable;
    protected final Id docId;
    protected final StoreConfig storeConfig;

    public DocVectorAccessor(final Id docId,
                             final Table indexTable,
                             final StoreConfig storeConfig) {

        this.docId = docId;
        this.indexTable = indexTable;
        this.storeConfig = storeConfig;
    }

    @Override
    public void close() throws Exception {
        indexTable.close();
    }

    protected void removeUncommitedVector(int lastVersion)
        throws StoreException {

        final byte[] versionBytes = Bytes.toBytes(lastVersion);
        final Delete delete;
        try {
            delete = versionDelete(versionBytes);
            if (delete != null) {
                indexTable.delete(delete);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    protected Delete versionDelete(byte[] version) throws IOException {

        final FilterList filterList
            = new FilterList(FilterList.Operator.MUST_PASS_ALL,
                             new ColumnPrefixFilter(version),
                             new QualifierFilter(CompareOperator.NOT_EQUAL,
                                                 new BinaryComparator(LAST_VERSION_QF_BYTES)));
        final Scan scan = new Scan().withStartRow(docId.bytes())
                                    .withStopRow(docId.bytes())
                                    .addFamily(VECTOR_CF_BYTES)
                                    .setFilter(filterList)
                                    .setBatch(storeConfig.hconfig().scanBatchSize())
                                    .setCacheBlocks(false)
                                    .setCaching(storeConfig.hconfig().scanCacheSize())
                                    .readVersions(1);
        try (ResultScanner scanner = indexTable.getScanner(scan)) {
            Delete delete = null;
            for (Result result : scanner) {
                if (result.isEmpty()) {
                    continue;
                }
                if (delete == null) {
                    delete = new Delete(docId.bytes());
                }
                for (Cell cell : result.rawCells()) {
                    final byte[] qf = CellUtil.cloneQualifier(cell);
                    delete.addColumns(VECTOR_CF_BYTES, qf);
                }
            }
            return delete;
        }
    }

    protected OptionalInt loadCurrentVersion() throws StoreException {
        OptionalInt curVersion;
        try {
            final Get get = new Get(docId.bytes())
                                .addColumn(VECTOR_CF_BYTES, LAST_VERSION_QF_BYTES)
                                .readVersions(1);
            final Result result;
            result = indexTable.get(get);
            if (result.isEmpty()) {
                curVersion = OptionalInt.empty();
            }
            else {
                final Cell versionCell = result.getColumnLatestCell(VECTOR_CF_BYTES,
                                                                    LAST_VERSION_QF_BYTES);
                final byte[] versionBytes = CellUtil.cloneValue(versionCell);
                curVersion = OptionalInt.of(Bytes.toInt(versionBytes));
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        return curVersion;
    }
}
