package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;

/**
 * HBase Document vector reader class.
 */
public final class DocVectorReader extends DocVectorAccessor {

    private final byte[] currentVersion;

    public DocVectorReader(final Id docId,
                           final Table indexTable,
                           final StoreConfig storeConfig)
        throws StoreException {

        super(docId, indexTable, storeConfig);

        currentVersion = Bytes.toBytes(currentVectorVersion());
    }

    /**
     * Read vector of tokens in current document vector.
     */
    public List<TokenProto.Token> readVector() throws StoreException,
                                                      InvalidDocVectorFormatException {

        try {
            final Get get = new Get(docId.bytes())
                                    .addFamily(VECTOR_CF_BYTES)
                                    .setFilter(docVectorFilter(currentVersion))
                                    .readVersions(1);
            final Result result = indexTable.get(get);
            if (result.isEmpty()) {
                return null;
            }
            List<TokenProto.Token> vector = new ArrayList<>(result.rawCells().length);
            for (Cell cell : result.rawCells()) {
                final byte[] protoBytes = CellUtil.cloneValue(cell);
                vector.add(TokenProto.Token.parseFrom(protoBytes));
            }
            return vector;
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
        catch (Exception e) {
            throw new InvalidDocVectorFormatException(docId, e);
        }
    }

    private int currentVectorVersion() throws StoreException {
        int curVersion = loadCurrentVersion().orElse(UNCOMMITED_VERSIONS_RANGE_END);
        if (curVersion < UNCOMMITED_VERSIONS_RANGE_END) { // last version was not committed
            final int uncommitedVersion = curVersion * -1;
            removeUncommitedVector(uncommitedVersion);
            curVersion = uncommitedVersion - 1;
        }
        return curVersion;
    }

    /**
     * Create HBase filter to find document vector tokens of some version.
     *
     * @param vectorVersion Vector version
     *
     * @return
     */
    private static Filter docVectorFilter(byte[] vectorVersion) {
        return new FilterList(FilterList.Operator.MUST_PASS_ALL,
                              new ColumnPrefixFilter(vectorVersion),
                              new QualifierFilter(CompareOperator.NOT_EQUAL,
                                                  new BinaryComparator(LAST_VERSION_QF_BYTES)));
    }
}
