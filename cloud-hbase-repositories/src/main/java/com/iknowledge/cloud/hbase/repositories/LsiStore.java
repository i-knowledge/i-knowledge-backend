package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.Store;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.TableAlreadyExistsStoreException;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.io.compress.Compression;
import org.apache.hadoop.hbase.io.encoding.DataBlockEncoding;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.COLUMNS_CF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.CUR_VERSION_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.CUR_VERSION_ROW_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix
                  .NO_VERSIONS_OF_VECTOR_FOUND_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.SIGMA_ROW_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.U_ROW_PREFIX_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.VECTOR_V1_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.VECTOR_V2_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.LsaMatrix.V_ROW_PREFIX_BYTES;

/**
 * LSI matrix table store.
 */
public final class LsiStore extends Store {

    /**
     * @param storeConfig
     *     Store connection instance
     */
    public LsiStore(final StoreConfig storeConfig) {
        super(storeConfig);
    }

    @Override
    public void createTable() throws StoreException {
        try (Admin admin = getAdmin()) {
            try {
                final ColumnFamilyDescriptor vectorCf
                    = new ColumnFamilyDescriptorBuilder
                              .ModifyableColumnFamilyDescriptor(COLUMNS_CF_BYTES)
                          .setBloomFilterType(BloomType.ROW)
                          .setCompactionCompressionType(Compression.Algorithm.SNAPPY)
                          .setCompressionType(Compression.Algorithm.SNAPPY)
                          .setDataBlockEncoding(DataBlockEncoding.PREFIX)
                          .setBlockCacheEnabled(true)
                          .setVersions(1, 1);
                final TableDescriptor descriptor
                    = new TableDescriptorBuilder.ModifyableTableDescriptor(storeConfig.tableName())
                          .setCompactionEnabled(true)
                          .setDurability(Durability.SYNC_WAL)
                          .addColumnFamily(vectorCf);
                admin.createTable(descriptor);
            }
            catch (TableExistsException e) {
                throw new TableAlreadyExistsStoreException(e);
            }
            catch ( IOException e ) {
                throw new StoreException(e);
            }
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Create LSA matrix writer to store new version matrix.
     */
    public LsiWriter versionWriter() throws StoreException {
        final Table table = getTable();
        return new LsiWriter(table, Versions.get(table));
    }

    /**
     * Get U matrix row.
     *
     * @throws StoreException
     */
    public Optional<float[]> getURow(long tokenId) throws StoreException {
        final byte[] rowKey = Bytes.add(U_ROW_PREFIX_BYTES, Bytes.toBytes(tokenId));
        return getVectorForKey(rowKey);
    }

    /**
     * Get sigma matrix.
     *
     * @throws StoreException
     */
    public Optional<float[]> getSigmaMatrix() throws StoreException {
        return getVectorForKey(SIGMA_ROW_BYTES);
    }

    /**
     * Get V matrix row.
     *
     * @throws StoreException
     */
    public Optional<float[]> getVRow(Id docId) throws StoreException {
        final byte[] rowKey = Bytes.add(V_ROW_PREFIX_BYTES, docId.bytes());
        return getVectorForKey(rowKey);
    }

    private Optional<float[]> getVectorForKey(final byte[] rowKey) throws StoreException {
        try (Table table = getTable()) {
            final Versions versions = Versions.get(table);
            if (Arrays.equals(versions.current, NO_VERSIONS_OF_VECTOR_FOUND_QF_BYTES)) {
                return Optional.empty();
            }

            final Get get = new Get(rowKey)
                                .addColumn(COLUMNS_CF_BYTES, versions.current)
                                .readVersions(1);
            final Cell cell = table.get(get)
                                   .getColumnLatestCell(COLUMNS_CF_BYTES, versions.current);
            if (cell == null) {
                return Optional.empty();
            }
            final byte[] bytes = CellUtil.cloneValue(cell);
            float[] matrix = new float[bytes.length / Float.BYTES];
            for (int i = 0 ; i < matrix.length ; i++) {
                matrix[i] = Bytes.toFloat(bytes, i * Float.BYTES);
            }
            return Optional.of(matrix);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Class extracts current and next matrix version qualifier names.
     */
    static final class Versions {
        public final byte[] current;
        public final byte[] next;

        private Versions(final byte[] current, final byte[] next) {
            this.current = current;
            this.next = next;
        }

        public static Versions get(Table table) throws StoreException {
            final Cell curVersionCell = getCurrentVersion(table);
            byte[] currentQf;
            byte[] nextQf;
            if (curVersionCell != null) {
                currentQf = CellUtil.cloneValue(curVersionCell);
                nextQf = Arrays.equals(currentQf, VECTOR_V1_QF_BYTES)
                               ? VECTOR_V2_QF_BYTES
                               : VECTOR_V1_QF_BYTES;
            }
            else {
                currentQf = NO_VERSIONS_OF_VECTOR_FOUND_QF_BYTES;
                nextQf = VECTOR_V1_QF_BYTES;
            }
            return new Versions(currentQf, nextQf);
        }

        private static Cell getCurrentVersion(Table table) throws StoreException {
            try {
                final Get curVersionGet = new Get(CUR_VERSION_ROW_BYTES)
                                              .addColumn(COLUMNS_CF_BYTES, CUR_VERSION_QF_BYTES)
                                              .readVersions(1);
                final Result result = table.get(curVersionGet);
                return result.getColumnLatestCell(COLUMNS_CF_BYTES, CUR_VERSION_QF_BYTES);
            }
            catch (IOException e) {
                throw new StoreException(e);
            }
        }
    }
}
