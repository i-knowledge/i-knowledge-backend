package com.iknowledge.cloud.hbase.repositories;

import com.google.common.annotations.VisibleForTesting;
import com.iknowledge.common.Id;
import com.iknowledge.hbase.repositories.StoreConfig;
import com.iknowledge.hbase.repositories.StoreException;
import com.iknowledge.hbase.repositories.indexing.protobuf.TokenProto;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.RowMutations;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.List;

import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.LAST_VERSION_QF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.VECTOR_CF_BYTES;
import static com.iknowledge.cloud.hbase.repositories.HConsts.DocVectors.tokenQf;

public final class DocVectorWriter extends DocVectorAccessor {

    private final int vectorVersion;

    public DocVectorWriter(final Id docId,
                           final Table indexTable,
                           final StoreConfig storeConfig) throws StoreException {

        super(docId, indexTable, storeConfig);

        vectorVersion = nextVectorVersion();
    }

    @VisibleForTesting
    DocVectorWriter(final Id docId,
                    final int vectorVersion,
                    final Table indexTable,
                    final StoreConfig storeConfig) {

        super(docId, indexTable, storeConfig);

        this.vectorVersion = vectorVersion;
    }

    /**
     * Write next block of document tokens.
     *
     * @param tokens Token array.
     */
    public void appendTokens(final List<TokenProto.Token> tokens) throws StoreException {
        Put tokensPut = new Put(docId.bytes());
        for (TokenProto.Token token : tokens) {
            tokensPut.addColumn(VECTOR_CF_BYTES,
                                   tokenQf(vectorVersion, token),
                                   token.toByteArray());
        }
        try {
            indexTable.put(tokensPut);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Commit this version of document vector.
     */
    public void commit() throws StoreException {
        final RowMutations rowMutations = new RowMutations(docId.bytes());
        try {
            final Put newVersionPut = versionPut(vectorVersion);
            rowMutations.add(newVersionPut);
            final byte[] prevVersion = getPrevVersion(vectorVersion);
            final Delete prevVectorDelete = versionDelete(prevVersion);
            if (prevVectorDelete != null) {
                rowMutations.add(prevVectorDelete);
            }
            indexTable.mutateRow(rowMutations);
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private int nextVectorVersion() throws StoreException {
        int nextVersion = loadCurrentVersion().orElse(INITIAL_VERSION_NUM);
        if (nextVersion < UNCOMMITED_VERSIONS_RANGE_END) {
            final int uncommittedVersion = nextVersion * -1;
            removeUncommitedVector(uncommittedVersion);
            nextVersion = uncommittedVersion;
        }
        else {
            nextVersion++;
            if (nextVersion < UNCOMMITED_VERSIONS_RANGE_END) {
                nextVersion = INITIAL_VERSION_NUM;
            }
        }
        // set as uncommited
        saveVersion(nextVersion * -1);
        return nextVersion;
    }

    private void saveVersion(int version) throws StoreException {
        try {
            indexTable.put(versionPut(version));
        }
        catch (IOException e) {
            throw new StoreException(e);
        }
    }

    private Put versionPut(int version) {
        return new Put(docId.bytes()).addColumn(VECTOR_CF_BYTES,
                                                   LAST_VERSION_QF_BYTES,
                                                   Bytes.toBytes(version));
    }

    private byte[] getPrevVersion(final int curVersion) {
        final int prevVersion;
        if (vectorVersion - 1 < INITIAL_VERSION_NUM) {
            prevVersion = Integer.MAX_VALUE;
        }
        else {
            prevVersion = curVersion - 1;
        }
        return Bytes.toBytes(prevVersion);
    }
}
