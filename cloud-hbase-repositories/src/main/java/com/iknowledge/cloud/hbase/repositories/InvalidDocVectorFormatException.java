package com.iknowledge.cloud.hbase.repositories;

import com.iknowledge.common.Id;

public final class InvalidDocVectorFormatException extends Exception {

    private final Id docId;

    public InvalidDocVectorFormatException(final Id docId, final Throwable cause) {
        super(cause);
        this.docId = docId;
    }

    public InvalidDocVectorFormatException(final Id docId, final String message) {

        super(message);
        this.docId = docId;
    }

    @Override
    public String toString() {
        return "Invalid document vector in HBase store for document '"
                    + docId
                    + "'"
                    + System.lineSeparator()
                    + super.toString();
    }
}
