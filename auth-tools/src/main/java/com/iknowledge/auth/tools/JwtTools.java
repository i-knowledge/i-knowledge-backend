package com.iknowledge.auth.tools;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.text.MessageFormat.format;

public final class JwtTools {

    private static final String IS_ADMIN_CLAIM = "admin";
    private static final String USER_GROUPS_CLAIM = "groups";
    private static final String USER_ID_CLAIM = "id";

    private final String secret;
    private final Logger logger;

    /**
     *
     * @param secret
     */
    public JwtTools(String secret) {
        this(secret, LoggerFactory.getLogger(JwtTools.class));
    }

    /**
     *
     */
    public JwtTools(String secret, Logger logger) {
        this.secret = secret;
        this.logger = logger;
    }

    /**
     * Create new jwt token based on user info
     * @param userId
     * @param isAdmin
     * @return
     */
    public String createToken(int userId, boolean isAdmin) {
        Algorithm algorithm = null;
        // TODO: paste claims dict into the parameters
//        final Integer[] groupsId = userGroups.stream()
//                .map(s -> s.getId())
//                .toArray(Integer[]::new);
        try {
            algorithm = Algorithm.HMAC256(secret);
            return JWT.create()
                    .withClaim(USER_ID_CLAIM, userId)
                    .withClaim(IS_ADMIN_CLAIM, isAdmin)
//                    .withArrayClaim(USER_GROUPS_CLAIM, groupsId)
                    .sign(algorithm);
        }
        catch (Exception e) {
            final String errorMsg = format("Error has occurred during creating Jwt token. "
                    + "UserId: {0}", userId);
            logger.error(errorMsg, e);
            throw new IllegalStateException(errorMsg, e);
        }
    }
}
