package com.iknowledge.auth.tools;

/**
 * Class that contains information about password hash and its salt
 */
public final class PasswordInfo {
    private final byte[] hash;
    private final byte[] salt;

    /**
     *
     * @param hash Password hash
     * @param salt Password salt that is used to generate password hash
     */
    public PasswordInfo(byte[] hash, byte[] salt) {
        this.hash = hash;
        this.salt = salt;
    }

    /**
     * Password hash
     * @return
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * Password salt that is used to generate password hash
     * @return
     */
    public byte[] getSalt() {
        return salt;
    }
}
