package com.iknowledge.auth.tools;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public final class AuthTools {

    /**
     * Create hash for plainPassword and verify it with already saved password hash
     * @param plainPassword
     * @param passwordHashBytes
     * @param passwordSaltBytes
     * @return true, if hashes are equal
     * @throws NoSuchAlgorithmException
     */
    public boolean verifyHash(final String plainPassword, final byte[] passwordHashBytes,
                              final byte[] passwordSaltBytes, final Charset charset)
            throws NoSuchAlgorithmException {
        final byte[] hashForPassword =
                getHashForPassword(plainPassword, passwordSaltBytes, charset);
        return MessageDigest.isEqual(hashForPassword, passwordHashBytes);
    }

    /**
     * Create hash for specified password
     *
     * @param password
     *
     * @return
     *
     * @throws NoSuchAlgorithmException
     */
    public PasswordInfo getHashForPassword(final String password, final Charset charset)
            throws NoSuchAlgorithmException {
        final byte[] salt = getSalt();
        final byte[] hashForPassword = getHashForPassword(password, salt, charset);
        PasswordInfo passwordInfo = new PasswordInfo(hashForPassword, salt);
        return passwordInfo;
    }

    private byte[] getHashForPassword(final String password, final byte[] salt,
                                      final Charset charset)
            throws NoSuchAlgorithmException {
        // for more information about MessageDigest algorithms read
        // https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt);
        return md.digest(password.getBytes(charset));
    }

    private byte[] getSalt() throws NoSuchAlgorithmException {
        // for more information about SecureRandom algorithms read
        // https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SecureRandom
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
}
