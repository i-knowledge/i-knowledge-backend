package $package;

import com.iknowledge.cloud.spark.HadoopSparkTaskBase;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.SparkConf;
import org.iknowledge.hbase.client.HConfig;

import java.util.UUID;

/**
 * Spark task driver class description here.
 */
public final class $driverClass extends HadoopSparkTaskBase {

    private static final long serialVersionUID = UUID.randomUUID().getLeastSignificantBits();
    private static final String ZK_QUORUM = "zookeeper.quorum";
    private static final String HBASE_ZNODE = "hbase.znode";
    private static final String HBASE_RPC_TIMEOUT = "hbase.rpc.timeout";
    private static final String HBASE_RETRY_COUNT = "hbase.retry.count";
    private static final String HBASE_RETRY_BACKOFF = "hbase.retry.backoff";
    private static final String HBASE_SCAN_BATCH_SIZE = "hbase.scan.batch.size";
    private static final String HBASE_SCAN_CACHE_SIZE = "hbase.scan.cache.size";
    private static final String HBASE_CONNECTION_THREADS = "hbase.connection.threads";
    private HConfig hbaseConf;

    /**
     * Create instance of Spark task driver.
     */
    public $driverClass() {
        super("Spark task description here", createCmdOptions());
    }

    @Override
    protected void executeProcessing() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected Configuration loadHadoopConf(final PropertiesConfiguration driverProps) {
        hbaseConf = HConfig.newBuilder()
                           .connectionThreads(driverProps.getInt(HBASE_CONNECTION_THREADS,
                                                Runtime.getRuntime().availableProcessors() * 4))
                           .metaLookupThreads(Runtime.getRuntime().availableProcessors() * 2)
                           .zkQuorum(driverProps.getString(ZK_QUORUM))
                           .znode(driverProps.getString(HBASE_ZNODE))
                           .rpcTimeout(driverProps.getInt(HBASE_RPC_TIMEOUT, 15000))
                           .retryCount(driverProps.getInt(HBASE_RETRY_COUNT, 5))
                           .retryCount(driverProps.getInt(HBASE_RETRY_BACKOFF, 5000))
                           .scanBatchSize(driverProps.getInt(HBASE_SCAN_BATCH_SIZE, 100))
                           .scanBatchSize(driverProps.getInt(HBASE_SCAN_CACHE_SIZE, 30))
                           .perServerMaxTasks(Integer.MAX_VALUE)
                           .perRegionMaxTasks(Integer.MAX_VALUE)
                           .threadPoolMaxTasks(Integer.MAX_VALUE)
                           .build();
        return hbaseConf.asConfiguration();
    }

    @Override
    protected SparkConf loadSparkConf(final PropertiesConfiguration driverProps) {
        return super.loadSparkConf(driverProps);
    }
}
