#!/bin/bash

#Spark binaries location, spark user defined in Dockerfile
SPARK_MASTER="${SPARK_MASTER:-"spark://localhost:7077"}"
DRIVER_MODE="client"
TASK_CONF="spark.conf"
LOG4J_CONF_FILE="log4j2.xml"

DRIVER_BASE_DIR=$(pwd)
echo "Driver base directory: ${DRIVER_BASE_DIR}"
APP_JAR="${DRIVER_BASE_DIR}/${project.build.finalName}-assembly.jar"

COMPRESS_RDD="${COMPRESS_RDD:-"true"}"
COMPRESS_CODEC="${COMPRESS_CODEC:-"snappy"}"
INITIAL_EXECUTORS="${INITIAL_EXECUTORS:-"1"}"
DRIVER_MAX_RESULT_SIZE="${DRIVER_MAX_RESULT_SIZE:-"3g"}"
KRYOSERIALIZER_BUFFER_SIZE="${KRYOSERIALIZER_BUFFER_SIZE:-'96k'}"
KRYOSERIALIZER_BUFFER_SIZE_MAX="${KRYOSERIALIZER_BUFFER_SIZE_MAX:-'64m'}"
EXECUTOR_MEMORY="${EXECUTOR_MEMORY:-'1536m'}"
DRIVER_MEMORY="${DRIVER_MEMORY:-'512m'}"
EXECUTOR_CORES="${EXECUTOR_CORES:-1}"
DRIVER_CORES="${DRIVER_CORES:-1}"
EXECUTOR_DIRECT_MAX_MEMORY="${EXECUTOR_DIRECT_MAX_MEMORY:-'256M'}"
DRIVER_DIRECT_MAX_MEMORY="${DRIVER_DIRECT_MAX_MEMORY:-'256M'}"
GC_OPTS="${GC_OPTS:-'-XX:+UseG1GC'}"
COMMON_JVM_OPTS="${COMMON_JVM_OPTS:-'-Djava.net.preferIPv4Stack=true'}"
EXECUTOR_JVM_PARAMS="$COMMON_JVM_OPTS \
                        -XX:MaxDirectMemorySize=$EXECUTOR_DIRECT_MAX_MEMORY \
                        -XX:+UseTLAB \
                        $GC_OPTS"
DRIVER_JVM_PARAMS="$COMMON_JVM_OPTS \
                        -XX:MaxDirectMemorySize=$DRIVER_DIRECT_MAX_MEMORY \
                        -XX:+UseTLAB \
                        $GC_OPTS"
SPARK_CONF_PARAMS="--conf spark.dynamicAllocation.enabled=true \
                    --conf spark.shuffle.service.enabled=true \
                    --conf spark.dynamicAllocation.initialExecutors=$INITIAL_EXECUTORS \
                    --conf spark.executor.userClassPathFirst=true \
                    --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
                    --conf spark.kryoserializer.buffer=$KRYOSERIALIZER_BUFFER_SIZE \
                    --conf spark.kryoserializer.buffer.max=$KRYOSERIALIZER_BUFFER_SIZE_MAX \
                    --conf spark.kryo.registrationRequired=false \
                    --conf spark.kryo.referenceTracking=true \
                    --conf spark.kryo.unsafe=true \
                    --conf spark.io.compression.codec=$COMPRESS_CODEC \
                    --conf spark.driver.maxResultSize=$DRIVER_MAX_RESULT_SIZE \
                    --conf spark.rdd.compress=$COMPRESS_RDD \
                    --conf spark.driver.extraJavaOptions=\"$DRIVER_JVM_PARAMS -Dlog4j.configuration=$LOG4J_CONF_FILE\" \
                    --conf spark.executor.extraJavaOptions=\"$EXECUTOR_JVM_PARAMS -Dlog4j.configuration=$LOG4J_CONF_FILE\""

su ${SPARK_USER} -c "${SPARK_BIN_DIR}/spark-submit \
                    --class ${spark.driver.class} \
                    --master $SPARK_MASTER \
                    --deploy-mode $DRIVER_MODE \
                    --executor-cores $EXECUTOR_CORES \
                    --executor-memory $EXECUTOR_MEMORY \
                    --driver-cores $DRIVER_CORES \
                    --driver-memory $DRIVER_MEMORY \
                    $SPARK_CONF_PARAMS \
                    $APP_JAR \
                    --spark-conf $TASK_CONF"
